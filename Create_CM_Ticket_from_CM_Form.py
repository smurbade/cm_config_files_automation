import pypyodbc, os, re
from xlrd import open_workbook

client_prodtype_device_list_filename = "C:\Verifone\client_prodtype_device_list.xlsx"
workbook = open_workbook(client_prodtype_device_list_filename)
wb = workbook.sheet_by_name("Sheet1")
global queryfieldlist
queryfieldlist = ""
global custsurveyfieldlist
custsurveyfieldlist = []
global fieldexclude_lower
global fieldlist


def getid_addclientname(clientname): # returns ID for 'Client name' from 'Add Client Name' list ; Returns -1 if client is not found
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Add Client Name.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Add Client Name.accdb;\"")

    #query = "SELECT * FROM \"Add Client Name\" WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.lower() + "'"
    #query = "SELECT * FROM \"Add Client Name\" WHERE ([Add Client Name].[Client Name]) = '" + clientname + "'"

    #print(query)
    #print(clientname)

    cur = conn.cursor()
    cur.execute("SELECT [Add Client Name].[ID] FROM [Add Client Name] WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.replace("'", "''").lower() + "'" )
    #cur.execute(query);

    while True:
        row = cur.fetchone()
        if row is None:
            break
        else:
            ID = row[0]
            cur.close()
            conn.close()
            #print(row)
            return ID

    return -1
    cur.close()
    conn.close()


def create_cm_ticket(row, create_update):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
    tablename = "CM Ticket"

    cm_ticket_field_list = ["Ticket Number", "Title", "Client Name", "Product Type", "Device Model", "Type of request", "Priority", "Delivery Date", "Environment", "Product version", "Who is Boarding the Devices?", "Select Bid Options", "Bid #", "Package", "VHQ Company ID", "Dual AID Options", "VCL Configuration Package", "LANEs (one for each device)", "Key File", "VHQ Config File", "Cert to use", "Approver on Client Side", "Delivery instructions", "Comments", "SLA_Stop_Date", "Delivered_date", "Did you input all requested parameters?", "Did you embed correct VCL file for either production or Demo or Enterprise?", "What VCL is chosen?", "Did you sign all packages with Verifone Demo, Point or customer signature?", "What is the signature?", "Did you embed proper media files (MX915 or MX925 or Customized media)?", "Which media chosen?", "Did you check to see if customer requested EMV and provided proper package?", "Which EMV package did you chose?", "Did you test and install your package on a device and got successful transaction?", "Install successful?", "Did you choose correct SCA package for the specified VX device?", "Please answer which SCA package was chosen?", "Please confirm that Client approval has been received for this request", "Total Time Spent on CM Request in Hrs", "Do You confirm all above Checklist information is correct?", "CM Assigned for the request", "Request Status", "VHQ Company ID", "Dual AID Options", "CM Assigned for the request"]

    print("CM Form Row : " + str(row))
    global custsurveyfieldlist

    if create_update == "update":
        print("Updating CM Ticket")
        try:
            query = "UPDATE [" + tablename + "] SET "
            custsurvey_insert_fieldlist = "(replace"
            custsurvey_insert_valuelist = "(replace"
            for index in range(2, len(row)): # ignore ID and Title fields which are first two fields
                value = row[index]
                if row[index] != "" and row[index] != "None" and row[index] is not None :
                    query = query + " [" + custsurveyfieldlist[index] + "] = '" + str(value) + "', "
            query = query[:-2] + " WHERE [Client Name] = " + str(row[2]) + " and [Product Type] = '" + str(row[3]) + "' and [Device Model] = '" + str(row[8]) + "' ;"

            print(query)
            #cur = conn.cursor()
            #cur.execute(query)
            #cur.commit()
        except Exception as e:
            print('Error : ' + str(e))
            print("\nError updating records into " + tablename + " list. Exiting...")
            print(query)
            return False
    else:
        print("Creating CM Ticket : -----------------")

        try:
            query = "INSERT INTO [" + tablename + "] "
            custsurvey_insert_fieldlist = "(replace"
            custsurvey_insert_valuelist = "(replace"
            for index in range(0, len(row)):
                #print("Before : - " + custsurveyfieldlist[index])
                value = row[index]
                if row[index] != "None" and row[index] is not None :
                    #print(custsurveyfieldlist[index] + " : " + str(value) )
                    if isinstance(value, str):
                        value = value.replace("'", "''")
                    if custsurveyfieldlist[index] in (fldname.lower() for fldname in cm_ticket_field_list):
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [" + custsurveyfieldlist[index] + "], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                    if custsurveyfieldlist[index].lower() == "Assignee".lower():
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [CM Assigned for the request], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                    if custsurveyfieldlist[index].lower() == "ID".lower():
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [Ticket Number], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                elif custsurveyfieldlist[index].lower() == "Device Model".lower():
                    custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [Device Model], "
                    custsurvey_insert_valuelist = custsurvey_insert_valuelist + " 'MX915-PCI 3', "


            custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [Click to view Customer Survey selected], "
            custsurvey_insert_valuelist = custsurvey_insert_valuelist + "'https://verifone365.sharepoint.com/sites/NAConfigMgmt/Lists/Customer%20Survey/DispForm.aspx?ID=" + str(get_cust_survey_id(row[2], row[6], row[8])) +  ", " + "Revision 1 - Initial Revision', "

            custsurvey_insert_fieldlist = custsurvey_insert_fieldlist[:-2] + " )"
            custsurvey_insert_fieldlist = custsurvey_insert_fieldlist.replace("(replace", "( ")
            custsurvey_insert_valuelist = custsurvey_insert_valuelist[:-2] + " );"
            custsurvey_insert_valuelist = custsurvey_insert_valuelist.replace("(replace", "( ")

            #print("Insert field list : " + custsurvey_insert_fieldlist)
            #print("Insert value list : " + custsurvey_insert_valuelist)
            query = query + " " + custsurvey_insert_fieldlist + " VALUES " + custsurvey_insert_valuelist
            print(query)
            cur = conn.cursor()
            cur.execute(query)
            cur.commit()
        except Exception as e:
            print('Error : ' + str(e))
            print("\nError inserting records into " + tablename + " list. Exiting...")
            print(query)
            return False


def get_fieldlist():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Form.accdb;\"")

    global queryfieldlist
    global custsurveyfieldlist
    global fieldexclude_lower
    global fieldlist

    '''
    crsr = conn.cursor()

    try:
        # Get field names in CM Form
        res = crsr.execute("SELECT * FROM [CM Form] WHERE 1=0")
        fieldlist = [tuple[0] for tuple in res.description]
        crsr.close()
        #print(fieldlist)
    except pypyodbc.Error:
        crsr.close()
        return 5

        #"id", "title", "client name", "type of request", "environment", "priority", "product type", "product version", "who is boarding the devices?", "device model", "select bid options", "bid  # ", "package", "vhq company id", "dual aid options", "vcl configuration package", "lanes (one for each device)", "key file", "vhq config file", "cert to use", "approver on client side", "assignee", "did you input all requested parameters?", "did you embed correct vcl file for either production or demo or ", "what vcl is chosen?", "did you sign all packages with verifone demo, point or customer ", "what is the signature?", "did you embed proper media files (mx915 or mx925 or customized m", "which media chosen?", "did you check to see if customer requested emv and provided prop", "which emv package did you chose?", "did you test and install your package on a device and got succes", "install successful?", "did you choose correct sca package for the specified vx device?", "please answer which sca package was chosen?", "please confirm that client approval has been received for this r", "total time spent on cm request in hrs", "do you confirm all above checklist information is correct?", "request status", "delivery date", "sla_stop_date", "delivered_date", "customer email"

    fieldexclude = ["split tender", "vsp key", "device boarding – gateway",  "compliance asset id", "files to be uploaded for sca", "type of keys", "tppid", "device network configuration (static/dhcp)", "device boarding – gateway" "vsp key", "device type designator", "vericentre cluster id", "vx signing card", "mx signing card", "config revision",  "sca application (incl# dhi if applicable)",  "netconnect password",  "netconnect username",  "tokenization",  "brand", "group id", "domain",   "tids (one for each device)", "mid", "method of mid/tid entry",  "token type",  "chain code",  "preambleselectwaittime",  "verbose receipts", "cashback by aid", "emv nocvm (configurable dynamic kernel)", "capture signature on pin bypass",  "emv signature cvm threshold", "pin bypass", "consumer option 1", "consumer option 2", "consumer option 3", "consumer option 4", "consumer option 5", "consumer option 6", "store card details for post auth", "device secondary port", "full line item display", "line item display", "receipts", "receipt max characters/line", "card read screen first", "swipe ahead", "allow voids to saf", "allow refunds to saf", "allow gift activations to saf", "transaction floor limit", "saf maximum pending", "total floor limit", "offline days limit",  "saf limits by card type", "saf enabled", "mobile wallet vas", "contactless", "emv contactless", "emv", "dynamic currency conversion", "partial auth", "cashback preset option 1", "cashback preset option 2", "cashback preset option 3", "cashback preset option 4", "cashback preset option 5", "cashback limit", "cashback (only if debit = y)", "countertip preset option 1", "countertip preset option 2", "countertip preset option 3", "countertip preset option 4", "counter tip", "email capture", "loyalty capture",  "sig limit-discover", "sig limit-jcb", "sig limit-amex", "sig limit-diners", "sig limit-mc", "“Deliver to” person", "Comments", "Delivery instructions", "Credit/Debit/EBT Processors", "Gift Processors", "Sig Limit-Visa", "Signature Image Type", "Signature Capture (MX Only)", "Debit Key Part Number / KSN", "EBT", "Private Label", "Pay Cashier", "Gift", "Debit", "Credit", "Industry", "Maintenance Days", "Maintenance End", "Maintenance Start", "Heartbeat Frequency", "Contact Fax Number", "Contact Email Address", "Contact Phone", "Contact Name", "Login Name", "SCA Point Enterprise Processor", "Check Processors", "cm workflow new1", "Change_Perm", "Update_Graphs_Requests_Status", "Update_Graph_Product_Type", "Update_Graphs_Priority", "update_graphs", "Inform_CM_Form_Updates", "Creation time", "DeliveryDate", "CM Workflow new", "Login Name", "Contact Name", "Contact Phone", "Contact Email Address", "Contact Fax Number", "Serial number", "CM Workflow", "DeliveryDate", "Creation time", "Inform_CM_Form_Updates", "Update_Graphs", "Update_Internal_Workflow_Status", "Update_Graphs_Requests_Status", "Update_Graphs_Priority", "Update_Graph_Product_Type", "Update_Graphs_Weekly_Requests", "Update_Graphs_Requests_Distribution", "Create_CM_Checklist", "Update_CM_Checklist", "Update_Request_Status_one_time", "Update_Internal_workflow_status_from_Request_status", "CM Workflow new", "Update_CMChecklist_Field", "Update_Graphs_Requests_Distribution_update", "Checklist_Validation_in_CM_Form", "Update_CM_report", "Create_CM_report", "Assignee_Validation", "Content Type", "App Created By", "App Modified By", "Attachments", "Workflow Instance ID", "File Type", "Modified", "Created", "Created By", "Modified By", "URL Path", "Path", "Item Type", "Encoded Absolute URL", "internal workflow status", "createdthismonth"]
    fieldexclude_lower = [x.lower() for x in fieldexclude]
    custsurveyfieldlist = []
    for i in range(0, len(fieldlist)):
        if fieldlist[i] not in fieldexclude_lower:
            queryfieldlist = queryfieldlist + ", [CM Form].[" + fieldlist[i] + "]"
            custsurveyfieldlist.append(fieldlist[i])
    queryfieldlist = queryfieldlist[2:]

    #print(queryfieldlist)
    print(custsurveyfieldlist)
    '''

    #custsurveyfieldlist = ['id', 'title', 'client name', 'type of request', 'environment', 'priority', 'product type', 'product version', 'who is boarding the devices?', 'device model', 'select bid options', 'bid #', 'package', 'vhq company id', 'dual aid options', 'vcl configuration package', 'lanes (one for each device)', 'key file', 'vhq config file', 'cert to use', 'approver on client side', 'assignee', 'did you input all requested parameters?', 'did you embed correct vcl file for either production or demo or ', 'what vcl is chosen?', 'did you sign all packages with verifone demo, point or customer ', 'what is the signature?', 'did you embed proper media files (mx915 or mx925 or customized m', 'which media chosen?', 'did you check to see if customer requested emv and provided prop', 'which emv package did you chose?', 'did you test and install your package on a device and got succes', 'install successful?', 'did you choose correct sca package for the specified vx device?', 'please answer which sca package was chosen?', 'please confirm that client approval has been received for this r', 'total time spent on cm request in hrs', 'do you confirm all above checklist information is correct?', 'request status', 'delivery date', 'sla_stop_date', 'delivered_date', 'customer email']
    #queryfieldlist = "[CM Form].[id], [CM Form].[title], [CM Form].[client name], [CM Form].[type of request], [CM Form].[environment], [CM Form].[priority], [CM Form].[product type], [CM Form].[product version], [CM Form].[who is boarding the devices?], [CM Form].[device model], [CM Form].[select bid options], [CM Form].[bid #], [CM Form].[package], [CM Form].[vhq company id], [CM Form].[dual aid options], [CM Form].[vcl configuration package], [CM Form].[lanes (one for each device)], [CM Form].[key file], [CM Form].[vhq config file], [CM Form].[cert to use], [CM Form].[approver on client side], [CM Form].[assignee], [CM Form].[did you input all requested parameters?], [CM Form].[did you embed correct vcl file for either production or demo or ], [CM Form].[what vcl is chosen?], [CM Form].[did you sign all packages with verifone demo, point or customer ], [CM Form].[what is the signature?], [CM Form].[did you embed proper media files (mx915 or mx925 or customized m], [CM Form].[which media chosen?], [CM Form].[did you check to see if customer requested emv and provided prop], [CM Form].[which emv package did you chose?], [CM Form].[did you test and install your package on a device and got succes], [CM Form].[install successful?], [CM Form].[did you choose correct sca package for the specified vx device?], [CM Form].[please answer which sca package was chosen?], [CM Form].[please confirm that client approval has been received for this r], [CM Form].[total time spent on cm request in hrs], [CM Form].[do you confirm all above checklist information is correct?], [CM Form].[request status], [CM Form].[delivery date], [CM Form].[sla_stop_date], [CM Form].[delivered_date], [CM Form].[customer email]"
    custsurveyfieldlist = ['id', 'title', 'client name', 'type of request', 'environment', 'priority', 'product type', 'product version', 'device model', 'select bid options', 'bid #', 'package', 'vhq company id', 'dual aid options', 'vcl configuration package', 'lanes (one for each device)', 'key file', 'vhq config file', 'cert to use', 'approver on client side', 'assignee', 'request status', 'delivery date', 'sla_stop_date', 'delivered_date', 'customer email']
    queryfieldlist = "[CM Form].[id], [CM Form].[title], [CM Form].[client name], [CM Form].[type of request], [CM Form].[environment], [CM Form].[priority], [CM Form].[product type], [CM Form].[product version], [CM Form].[device model], [CM Form].[select bid options], [CM Form].[bid #], [CM Form].[package], [CM Form].[vhq company id], [CM Form].[dual aid options], [CM Form].[vcl configuration package], [CM Form].[lanes (one for each device)], [CM Form].[key file], [CM Form].[vhq config file], [CM Form].[cert to use], [CM Form].[approver on client side], [CM Form].[assignee], [CM Form].[request status], [CM Form].[delivery date], [CM Form].[sla_stop_date], [CM Form].[delivered_date], [CM Form].[customer email]"

    #WHERE = "[CM Form].[Request Status] <> 'Delivered' and [CM Form].[Request Status] <> 'Closed' and [CM Form].[Request Status] <> 'Cancelled'"
    #query = "SELECT " + queryfieldlist + " FROM [CM Form] WHERE " + WHERE + " ORDER BY [CM Form].[ID]"
    #query = "SELECT " + queryfieldlist + " FROM [CM Form] ORDER BY [CM Form].[ID]"
    #query = "SELECT [CM Form].[id], [CM Form].[title], [CM Form].[client name], [CM Form].[type of request], [CM Form].[environment], [CM Form].[priority], [CM Form].[product type], [CM Form].[product version], [CM Form].[who is boarding the devices?], [CM Form].[device model], [CM Form].[select bid options], [CM Form].[bid #], [CM Form].[package], [CM Form].[vhq company id], [CM Form].[dual aid options], [CM Form].[vcl configuration package], [CM Form].[lanes (one for each device)], [CM Form].[key file], [CM Form].[vhq config file], [CM Form].[cert to use], [CM Form].[approver on client side], [CM Form].[assignee], [CM Form].[did you input all requested parameters?], [CM Form].[did you embed correct vcl file for either production or demo or ], [CM Form].[what vcl is chosen?], [CM Form].[did you sign all packages with verifone demo, point or customer ], [CM Form].[what is the signature?], [CM Form].[did you embed proper media files (mx915 or mx925 or customized m], [CM Form].[which media chosen?], [CM Form].[did you check to see if customer requested emv and provided prop], [CM Form].[which emv package did you chose?], [CM Form].[did you test and install your package on a device and got succes], [CM Form].[install successful?], [CM Form].[did you choose correct sca package for the specified vx device?], [CM Form].[please answer which sca package was chosen?], [CM Form].[please confirm that client approval has been received for this r], [CM Form].[total time spent on cm request in hrs], [CM Form].[do you confirm all above checklist information is correct?], [CM Form].[request status], [CM Form].[delivery date], [CM Form].[sla_stop_date], [CM Form].[delivered_date], [CM Form].[customer email]  FROM [CM Form] ORDER BY [CM Form].[ID]"
    query = "SELECT [CM Form].[id], [CM Form].[title], [CM Form].[client name], [CM Form].[type of request], [CM Form].[environment], [CM Form].[priority], [CM Form].[product type], [CM Form].[product version], [CM Form].[device model], [CM Form].[select bid options], [CM Form].[bid #], [CM Form].[package], [CM Form].[vhq company id], [CM Form].[dual aid options], [CM Form].[vcl configuration package], [CM Form].[lanes (one for each device)], [CM Form].[key file], [CM Form].[vhq config file], [CM Form].[cert to use], [CM Form].[approver on client side], [CM Form].[assignee],  [CM Form].[request status], [CM Form].[delivery date], [CM Form].[sla_stop_date], [CM Form].[delivered_date], [CM Form].[customer email]  FROM [CM Form] ORDER BY [CM Form].[ID]"

    print(query)
    cur = conn.cursor()
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            return False
            #break
        else:
            if row[0] > 983 and row[0] <= 984:
                print(row)
                create_cm_ticket(row, "create")
                #return True

    cur.close()
    conn.close()
    return True


def get_cust_survey_id(client, prdtype, dev):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")

    cur = conn.cursor()
    WHERE = "[Customer Survey].[Client Name] = " + str((client)) + " and  [Customer Survey].[Product Type] = '" + str(prdtype) + "' and [Customer Survey].[Device Model] = '" + str(dev) + "'"
    query = "SELECT [Customer Survey].[ID] FROM [Customer Survey] WHERE " + WHERE
    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            return False
            #break
        else:
            return row[0]
            return True

    cur.close()
    conn.close()
    return True


get_fieldlist()
