from xlrd import open_workbook
import os, pypyodbc, re
from os.path import expanduser
from pathlib import Path

xmlconfig_mapping_dict = {}
missing_xmlconfig_mapping_dict = {}
xmlconfig_file_dict = {}
configusr1_dict = {}
aidlist_dict = {}
cdt_dict = {}
missing_configusr1_dict = {}
cdt_mapping = {}
point_parameters_mapping = {}
configusr1_dict_default = {}
global package_update_arguments


def delete_default_params_configusr1(device):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Point Parameters.accdb;\"")

    cur = conn.cursor()
    cur.execute("SELECT [Point Parameters].[Parameter], [Point Parameters].[Default Value] FROM [Point Parameters] WHERE [Point Parameters].[Device Model Supported] = '" + device + "' or [Point Parameters].[Device Model Supported] = 'Both'"  )

    while True:
        row = cur.fetchone()
        if row is None:
            break
        else:
            if row[0] != None and row[1] != None:
                point_parameters_mapping[row[0].strip()] = row[1].strip()

    #print(point_parameters_mapping)
    cur.close()
    conn.close()

    line_without_default = ""
    prev = ""

    try:
        with open("extract\config.usr1") as configusr1file:
            for line in configusr1file:
                line = line.strip()
                if "[" not in line and len(line) > 0 and "=" in line:
                    param = (line.split("=")[0]).strip()
                    value =  (line.split("=")[1]).strip()
                    if param in point_parameters_mapping:
                        if value == "":
                            configusr1_dict_default[param] = ""
                        elif isinstance(value, str):
                            if point_parameters_mapping[param].lower() == value.lower():
                                configusr1_dict_default[param] = value # parameters from config.usr1 which have default values
                            else:
                                line_without_default = line_without_default + line + "\n"
                                prev = line
                        else:
                            if point_parameters_mapping[param] == value:
                                configusr1_dict_default[param] = value # parameters from config.usr1 which have default values
                            else:
                                line_without_default = line_without_default + line + "\n"
                                prev = line
                    else:
                        if value == "":
                            configusr1_dict_default[param] = ""
                        else:
                            line_without_default = line_without_default + line + "\n"
                            prev = line
                else:
                    if "[" in prev:
                        line_without_default = line_without_default.replace(prev, "") # if previous line was a section and current line is also a section, it means previous line doesn't have any parameter so remove it
                    prev = line
                    line_without_default = line_without_default + "\n" + line + "\n"

        line_without_default = re.sub('\n\n+', '\n\n', line_without_default) # remove multiple blank line with single blank line
        configusr1file.close()
    except FileNotFoundError:
        print("File Not Found - extract\\config.usr1. Please make sure file is present")
        return 1
    except IOError:
        print("Error opening extract\\config.usr1")
        return 2

    #print(configusr1_dict_default)
    #print(line_without_default)

    configfile = open("extract\\config.usr1", 'w')
    configfile.write(line_without_default)
    configfile.close()

    return 0


def read_xmlconfig_mapping():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\xmlcfg_mapping.accdb;\"")

    cur = conn.cursor()
    cur.execute("SELECT [xmlcfg_mapping].[xmlCfgFile_parameter], [xmlcfg_mapping].[ConfigUsr1_parameter], [xmlcfg_mapping].[Section], [xmlcfg_mapping].[aidlist_parameter], [xmlcfg_mapping].[cdt_parameter], [xmlcfg_mapping].[Device] FROM [xmlcfg_mapping]")

    while True:
        row = cur.fetchone()
        if row is None:
            break
        else:
            key = (row[0], row[5])
            if row[1] != None or row[2] != None or row[3] != None or row[4] != None:
                xmlconfig_mapping_dict[key] = [row[1], row[2], row[3], row[4]]
            else:
                missing_xmlconfig_mapping_dict[row[0]] = row[5]

    #print(xmlconfig_mapping_dict)
    cur.close()
    conn.close()
    return True


def read_xmlconfig_file(xmlconfig_path, device):
    try:
        with open(xmlconfig_path) as xmlconfigfile:
            for line in xmlconfigfile:
                line = line.strip()
                if "<Parameter" in line:
                    param = ( (line.split("ParamName=\"", 1)[1]).split("\"", 1)[0] )
                    value =  ( (line.split("Value=\"", 1)[1]).split("\"", 1)[0] )
                    xmlconfig_file_dict[param] = value
                    if (param, device) in xmlconfig_mapping_dict:
                        #print(xmlconfig_mapping_dict[(param, device)])
                        if ( xmlconfig_mapping_dict[(param, device)][0] != None and xmlconfig_mapping_dict[(param, device)][1] != None ): # If config.usr1 parameter and section name in xmlconfig mapping is not None for xmlconfig parameter
                            if xmlconfig_mapping_dict[(param, device)][1].lower() not in configusr1_dict:
                                configusr1_dict[xmlconfig_mapping_dict[(param, device)][1].lower()] = []
                            configusr1_dict[xmlconfig_mapping_dict[(param, device)][1].lower()].append((xmlconfig_mapping_dict[(param, device)][0], value))
                        elif (xmlconfig_mapping_dict[(param, device)][0] != None and xmlconfig_mapping_dict[(param, device)][1] == None) or (xmlconfig_mapping_dict[(param, device)][0] == None and xmlconfig_mapping_dict[(param, device)][1] != None):
                            missing_configusr1_dict[param] = [device]
                        if (xmlconfig_mapping_dict[(param, device)][2] != None and xmlconfig_mapping_dict[(param, device)][2].lower() != 'na' ):
                            aidlist_dict[param] = value
                        if (xmlconfig_mapping_dict[(param, device)][3] != None and xmlconfig_mapping_dict[(param, device)][3].lower() != 'na' ):
                            cdt_dict[param] = value
                    elif (param, "Both") in xmlconfig_mapping_dict:
                        # print(xmlconfig_mapping_dict[(param, device)])
                        if ( xmlconfig_mapping_dict[(param, "Both")][0] != None and xmlconfig_mapping_dict[(param, "Both")][1] != None ):  # If config.usr1 parameter and section name in xmlconfig mapping is not None for xmlconfig parameter
                            if xmlconfig_mapping_dict[(param, "Both")][1].lower() not in configusr1_dict:
                                configusr1_dict[xmlconfig_mapping_dict[(param, "Both")][1].lower()] = []
                            configusr1_dict[xmlconfig_mapping_dict[(param, "Both")][1].lower()].append((xmlconfig_mapping_dict[(param, "Both")][0], value))
                        elif (xmlconfig_mapping_dict[(param, "Both")][0] != None and xmlconfig_mapping_dict[(param, "Both")][1] == None) or (xmlconfig_mapping_dict[(param, "Both")][0] == None and xmlconfig_mapping_dict[(param, "Both")][1] != None):
                            missing_configusr1_dict[param] = ["Both"]
                        elif (xmlconfig_mapping_dict[(param, "Both")][2] != None and xmlconfig_mapping_dict[(param, "Both")][2].lower() == 'na' ):
                            aidlist_dict[param] = "Both"
                        elif (xmlconfig_mapping_dict[(param, "Both")][3] != None and xmlconfig_mapping_dict[(param, "Both")][3].lower() == 'na' ):
                            cdt_dict[param] = "Both"


        xmlconfigfile.close()
    except FileNotFoundError:
        print("File Not Found - " + xmlconfig_path + ". Please make sure file is present")
        return False
    except IOError:
        print("Error opening " + xmlconfig_path)
        return False

    return True

def create_update_ConfigUsr1():
    if configusr1_dict:
        cfgusr1_file = Path("extract\\config.usr1")
        if not cfgusr1_file.is_file():
            try:
                configusr1filename = "extract\\config.usr1"
                xmlconfigfile = open(configusr1filename, 'w')
                line = ""
                for section in configusr1_dict:
                    line = line + "[" + section + "]\n"
                    for paramvalue in configusr1_dict[section]:
                        line = line + paramvalue[0] + "=" + paramvalue[1] + "\n"
                    line = line + "\n"

                xmlconfigfile.write(line)
                xmlconfigfile.close()
                return True
            except:
                return False
        else:
            try:
                line = ""
                for section in configusr1_dict:
                    for paramvalue in configusr1_dict[section]:
                        line = line + paramvalue[0] + ":CONTROL/config.usr1:" + section + ":" + paramvalue[1] + "\n"
                configusr1updatepackagefilename = "extract\\configusr1updatepackage"
                configusr1updatepackagefile = open(configusr1updatepackagefilename, 'w')
                configusr1updatepackagefile.write(line)
            except:
                return False
    else:
        print("Config Usr1 dict is not created")
        return False


def create_update_aidList():
    if aidlist_dict:
        aidList_file = Path("extract\\aidlist.ini")
        if aidList_file.is_file():
            global package_update_arguments
            package_update_arguments = "aidlist"
            for aidparam in aidlist_dict:
                package_update_arguments = package_update_arguments + " " + aidparam +  "=" + aidlist_dict[aidparam]
    else:
        print("aidList Usr1 dict is not created")
        return False


def convert_cdt(cdttxt_path):
    cdt_lines = 0
    cdt_row_params = 0

    try:
        with open(cdttxt_path) as cdtfile:
            for line in cdtfile:
                line = line.strip()
                if (len(line) > 1) and line[0] != ("\\") and line[0] != ("/") and line[0] != ("#"):
                    cdt_lines+=1
                    param = line.split(' ', 1)[0]

                    if ( cdt_row_params > 1 and cdt_row_params != len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')) ):
                        #print(cdt_row_params)
                        #print(len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')))
                        print("cdt.txt is not in standard format. Below line has more values than other lines above it")
                        print(line)
                        return 1
                    cdt_row_params= len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(','))
                    #print(line)
                    #print(cdt_row_params)
        cdtfile.close()
    except FileNotFoundError:
        print("File Not Found - " + cdttxt_path + ". Please make sure file is present")
        return 2
    except IOError:
        print("Error opening " + cdttxt_path)
        return 3

    #print(cdt_lines)
    #print(cdt_row_params)

    cdt_dict = [[0 for x in range(2)] for y in range(cdt_lines)]
    #cdt_dict = [cdt_lines][cdt_row_params]
    row_ind = 0
    col_ind = 0

    with open(cdttxt_path) as cdtfile:
        for line in cdtfile:
            line = line.strip()
            if (len(line) > 1) and line[0] != ("\\") and line[0] != ("/") and line[0] != ("#"):
                cdt_dict[row_ind][0] = line.split(' ', 1)[0]
                cdt_dict[row_ind][1] = (line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')
                row_ind+=1
    cdtfile.close()

    #print(cdt_dict)

    #print(len(cdt_dict[0][1]))
    #print(cdt_dict[0][0])
    #print(cdt_dict[0][1])
    #print(cdt_dict[1][0])
    #print(cdt_dict[1][1])

    line = ""

    line = line + "RecCnt=" + str(len(cdt_dict[0][1])) + "\n"
    for i in range(0, len(cdt_dict[0][1])):
        if i < 10:
            line = line + "\n[Rec_0" + str(i) + "]\n"
        else:
            line = line + "\n[Rec_" + str(i) + "]\n"
        for j in range(0, len(cdt_dict)):
            if cdt_dict[j][0] in cdt_mapping:
                if cdt_mapping[cdt_dict[j][0]] != "":
                    line = line + str(cdt_mapping[cdt_dict[j][0]]) + "=" + cdt_dict[j][1][i] + "\n"
                    #print(cdt_dict[j][0] + " = " + str(cdt_mapping[cdt_dict[j][0]]))

    #print(line)

    try:
        cdtinifilename = "extract\\cdt.ini"
        cdtinifile = open(cdtinifilename, 'w')
        cdtinifile.write(line)
        cdtinifile.close()
        return 0
    except:
        return 10


def read_conf():
    home = expanduser("~")
    base_path = ""
    if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
    elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
    elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
        base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
    elif os.path.isdir(home + "\\OneDrive - Verifone"):
        home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

    try:
        conffile = base_path + "CMAutomation\\conf.xlsx"
        workbook = open_workbook(conffile)
        wb = workbook.sheet_by_name("cdt mapping")

        for row in range(wb.nrows):
            value = wb.cell(row, 1).value
            cdt_mapping[wb.cell(row, 0).value.strip()] = value

        #print(cdt_mapping)
        return 1
    except:
        print("Error reading conf.xlsx")
        return 0


def convert_aidlist(aidlisttxt_path):
    aidlist_list = []
    try:
        with open(aidlisttxt_path) as configfile:
            for line in configfile:
                line = line.strip()
                if (len(line) > 1 and line[0] != ("\\") and line[0] != ("/") and line[0] != ("#")):
                    line = line.replace('\r\n', '')
                    line = line.replace('\n', '')
                    line = line.strip()
                    # print(line)
                    if line[0] != "#" and line[0] != "/":
                        try:
                            paramvalue = line.split("|")
                            row = []
                            row.insert(0, line.split("|")[0])
                            row.insert(1, line.split("|")[1])
                            row.insert(2, line.split("|")[2])
                            aidlist_list.append(row)
                            #aidlist_list.append(line.split("|", 3))
                        except:
                            print(aidlisttxt_path + " file is not in correct format. Please check below line in " + aidlisttxt_path + " file...")
                            print(paramvalue)
                            configfile.close()
                            return 1
    except FileNotFoundError:
        print("\n" + aidlisttxt_path + " Not Found. Exiting...")
        return 2
    except IOError:
        print("\nError opening " + aidlisttxt_path + " file. Exiting...")
        return 3

    configfile.close()

    #print(aidlist_list)

    comment = "##SupportedAIDS should have all the AIDs supported on terminal. Each AID\n"
    comment = comment + "##should have respective config section also. Failing any such case, that\n"
    comment = comment + "##AID will not be supported.\n"
    comment = comment + "##Each AID should be different number starting from 1, and should in continuity\n"
    comment = comment + "##without any miss. SCA will read AID1,AID2,AID3... so on.\n\n"
    comment = comment + "#Below are parameters per AID\n#\n"
    comment = comment + "#PaymentType			Supported Payment Type(-1 : Both Credit and Debit, 0: Only Credit, 1: Only Debit, 2 : Debit but Toggle Payment Type to Credit if PIN is bypassed)\n"
    comment = comment + "#PaymentMedia			Payment Media\n"
    comment = comment + "#CashbackEnabled		Cash back Enabled or Disabled( 0 : Disabled, 1 : Enabled)\n"
    comment = comment + "#CardAbbrv				Card Abbreviation\n"
    comment = comment + "#signaturelimit			Indicates the signature limit below which signature will not be taken for the EMV card, if signature is selected as the CVM.\n"
    comment = comment + "#capsignonpinbypass		Indicates whether to cappture signature if PIN is bypassed. The transaction amount should be more than or equal to signaturelimit.\n\n"


    line = comment + "[supportedaids]\n"

    for i in range(0, len(aidlist_list)):
        line = line + "aid" + str(i + 1) + "=" + aidlist_list[i][0] + "\n"

    for i in range(0, len(aidlist_list)):
        line = line + "\n[" + aidlist_list[i][0] + ".config]\n"
        line = line + "paymenttype=" + aidlist_list[i][1] + "\n"
        line = line + "paymentmedia=" + aidlist_list[i][2] + "\n\n"

    #print(line)

    try:
        aidlistinifilename = "extract\\Aidlist.ini"
        aidlistinifile = open(aidlistinifilename, 'w')
        aidlistinifile.write(line)
        aidlistinifile.close()
    except:
        return 10

    return 0

#if read_conf():
#    convert_cdt('extract\\cdt.txt')

#convert_aidlist("extract\\aidlist.txt")

'''
read_xmlconfig_mapping()
print("\nParameters mapping in Sharepoint : " + str(xmlconfig_mapping_dict))
if read_xmlconfig_file("extract\\xmlCfgFile.xml"):
    print("\nParameters in xmlConfigFile.xml : " + str(xmlconfig_file_dict))
    print("\nCorresponding Config Usr1 parameters for parameters in xmlConfigFile.xml : " + str(configusr1_dict))
    create_update_ConfigUsr1()
    if missing_xmlconfig_mapping_dict:
        print("\nMapping not present for parameters : " + str(missing_xmlconfig_mapping_dict))
    if missing_configusr1_dict:
        print("\nMapping for either Config user1 parameter name or Section name is missing for parameters : " + str(missing_configusr1_dict))
    if aidlist_dict:
        print("\naidList parameters to be updated : " + str(aidlist_dict))
        create_update_aidList()
        print(package_update_arguments)
    if cdt_dict:
        print("\ncdt parameters to be updated : " + str(cdt_dict))


'''

#delete_default_params_configusr1()
