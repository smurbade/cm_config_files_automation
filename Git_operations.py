import os
import subprocess
import logging
logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
from colorama import init, Fore, Back, Style
init()

class git_processing():
    def __init__(self):
        i=0
        f = open('conf\BitbucketArtifactoryDetails.txt')
        for line in iter(f):
            if i == 0:
                # setting variable to 'From_git' from file's 1st line
                self.repoPath = line.strip('\n')
            if i == 1:
                # setting variable to git repository URL from file's 2nd line
                self.repoUrl = line.strip('\n')
            i+=1
        f.close()
        #self.repoPath = 'From_git'
        #self.repoUrl = 'http://stash.verifone.com:7990/scm/anvc/config_files.git'
        #self.repoUrl = 'https://smurbade@bitbucket.org/smurbade/config_files.git'
        # list to set directory and working tree
        self.dirList = ['--git-dir=' + self.repoPath + '/.git', '--work-tree=' + self.repoPath]
        self.git_env = " --git-dir " + self.repoPath + "/.git --work-tree " + self.repoPath

    def pull_from_git(self):
        print("Synching", self.repoPath, "directory with Bitbucket    : ####################")
        logging.info("Synching" + self.repoPath + "directory with Bitbucket : ####################")

        #init git
        #subprocess.call(['git'] + ['init', self.repoPath])
        git_init = "git init " + self.repoPath
        result = subprocess.run(git_init, shell=True, stdout=subprocess.PIPE)
        if result.returncode == 0:
            #add remote
            #subprocess.call(['git'] + self.dirList + ['remote','add','remote_bitbucket',self.repoUrl])
            #result = subprocess.run(['git', [' remote',' add',' remote_bitbucket '], self.repoUrl], shell=True, stderr=subprocess.PIPE)
            git_remoteadd = "git " + self.git_env + " remote add remote_bitbucket " + self.repoUrl
            result = subprocess.run(git_remoteadd, shell=True, stderr=subprocess.PIPE)
            if result.returncode != 0 and not "remote_bitbucket already exists" in str(result.stderr):
                print(result.stderr)
                print(result)
            if result.returncode == 0 or (result.returncode != 0 and "remote_bitbucket already exists" in str(result.stderr)):
                #print("Pulling files from Bitbucket to ", self.repoPath, " directory")
                '''
                #Check status, returns files to be commited etc, so a working repo exists there
                #subprocess.call(['git'] + self.dirList + ['status'])
                git_status = "git " + self.git_env + " status"
                result = subprocess.run(git_status, shell=True, stdout=subprocess.PIPE)
                #print(result.stdout)
                print(result)
                '''

                #pull files
                #subprocess.call(['git'] + self.dirList + ['pull', 'remote_bitbucket', 'master'])
                git_pull = "git " + self.git_env + " pull remote_bitbucket master"
                result = subprocess.run(git_pull, shell=True, stderr=subprocess.PIPE)
                if result.returncode != 0:
                    print(result.stderr)
                    print(result)
                else:
                    print("All file changes on Bitbucket are synched to \'" + os.getcwd() + '\\' + self.repoPath + "\' directory" )
                '''
                #Check status, returns files to be commited etc, so a working repo exists there
                #subprocess.call(['git'] + self.dirList + ['status'])
                git_status = "git " + self.git_env + " status"
                result = subprocess.run(git_status, shell=True, stdout=subprocess.PIPE)
                #print(result.stdout)
                print(result)
                '''
        else:
            print(result.stderr)
            print(result)

    def add_file_to_git(self):
        print("Checking files modified in", self.repoPath, "directory : ####################")
        logging.info("Checking files modified in"+ self.repoPath+ "directory : ####################")
        #init git
        #subprocess.call(['git'] + ['init', self.repoPath])
        git_init = "git init " + self.repoPath
        result = subprocess.run(git_init, shell=True, stdout=subprocess.PIPE)
        if result.returncode == 0:
            #add remote
            #subprocess.call(['git'] + self.dirList + ['remote','add','remote_bitbucket',self.repoUrl])
            #result = subprocess.run(['git', [' remote',' add',' remote_bitbucket '], self.repoUrl], shell=True, stderr=subprocess.PIPE)
            git_remoteadd = "git " + self.git_env + " remote add remote_bitbucket " + self.repoUrl
            result = subprocess.run(git_remoteadd, shell=True, stderr=subprocess.PIPE)
            if result.returncode != 0 and not "remote_bitbucket already exists" in str(result.stderr):
                print(result.stderr)
                print(result)
            if result.returncode == 0 or (result.returncode != 0 and "remote_bitbucket already exists" in str(result.stderr)):
                #Check status, returns files to be commited etc, so a working repo exists there
                #subprocess.call(['git'] + self.dirList + ['status'])

                #add file
                #subprocess.Popen(['git', '--git-dir', 'From_git/.git', '--work-tree', 'From_git', 'add', '.']).wait()
                git_add = "git " + self.git_env + " add ."
                result = subprocess.run(git_add, shell=True, stderr=subprocess.PIPE)
                #print(result)
                if result.returncode != 0:
                    print(result.stderr)
                    print(result)
                else:
                    #Check if there are files to be committed
                    git_check_untracked_files = "git " + self.git_env + " status --untracked-files=no --porcelain"
                    print(Fore.GREEN)
                    result = subprocess.run(git_check_untracked_files, shell=True) # this is to print which files are modified/added/deleted
                    print(Style.RESET_ALL)
                    result = subprocess.run(git_check_untracked_files, shell=True, stdout=subprocess.PIPE) # this is to capture stdout and check if files are changed or not
                    #print(result)
                    #print(result.stdout)
                    git_commit_or_not = str(result.stdout)
                    #print(git_commit_or_not)
                    if "\"" in str(result.stdout): # if files are there to be committed then above git command will show filenames enclosed in ""
                        #print("Files are there to be committed")
                        self.commit_to_git()
                    else:
                        print(Fore.YELLOW + "No files are modified in \'" + os.getcwd() + '\\' + self.repoPath  + "\' directory so nothing will be uploaded to Bitbucket")
                        print(Style.RESET_ALL)
                    if result.returncode != 0:
                        print(result.stderr)
                        print(result)

                        #Check status, returns files to be commited etc, so a working repo exists there
                #subprocess.call(['git'] + self.dirList + ['status'])
        else:
            print(result.stderr)
            print(result)


    def commit_to_git(self):
        print("Committing to Bitbucket                       : ####################")
        #print("Synching", self.repoPath, "directory with Bitbucket    : ####################")
        logging.info("Committing to Bitbucket                       : ####################")
        print(Fore.YELLOW)
        commitmsg = str(input('Enter one liner of what you changed and hit enter : '))
        print(Style.RESET_ALL)
        #print(commitmsg)
        logging.info(commitmsg)
        #init git
        #subprocess.call(['git'] + ['init', self.repoPath])
        git_init = "git init " + self.repoPath
        result = subprocess.run(git_init, shell=True, stdout=subprocess.PIPE)
        if result.returncode == 0:
            #subprocess.Popen(["git", "commit", "-m" + commitmsg], cwd=self.repoPath).wait()
            git_commit = "git " + self.git_env + " commit -m \"" + commitmsg + "\""
            print(git_commit)
            result = subprocess.run(git_commit, shell=True, stderr=subprocess.PIPE)
            if result.returncode != 0:
                print(Fore.YELLOW + str(result.stderr.decode()))
                print(Style.RESET_ALL)
                #print(result)
            else:
                self.push_to_git()
            #subprocess.call(['git'] + self.dirList + ['status'])
        else:
            print(result.stderr)
            print(result)


    def push_to_git(self):
        print("Uploading files to Bitbucket                  : ####################")
        logging.info("Uploading files to Bitbucket                  : ####################")
        #subprocess.call(['git'] + self.dirList + ['push', 'remote_bitbucket', 'master'])
        git_push = "git " + self.git_env + " push remote_bitbucket master"
        result = subprocess.run(git_push, shell=True, stderr=subprocess.PIPE)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
        else:
            print(Fore.GREEN + "Files uploaded successfully to Bitbucket. Please verify.")
            print(Style.RESET_ALL)
        # subprocess.call(['git'] + self.dirList + ['status'])

'''
# Print repository commits and tags

print(repo.path)
print(repo.workdir)
print(repo.is_bare)
print(repo.is_empty)

objects = {
    'tags': [],
    'commits': [],
}

for objhex in repo:
    obj = repo[objhex]
    if obj.type == pygit2.GIT_OBJ_COMMIT:
        objects['commits'].append({
            'hash': obj.hex,
            'message': obj.message,
            'commit_date': datetime.utcfromtimestamp(
                obj.commit_time).strftime('%Y-%m-%dT%H:%M:%SZ'),
            'author_name': obj.author.name,
            'author_email': obj.author.email,
            'parents': [c.hex for c in obj.parents],
        })
    elif obj.type == pygit2.GIT_OBJ_TAG:
        objects['tags'].append({
            'hex': obj.hex,
            'name': obj.name,
            'message': obj.message,
            'target': base64.b16encode(obj.target).lower(),
            'tagger_name': obj.tagger.name,
            'tagger_email': obj.tagger.email,
        })
    else:
        # ignore blobs and trees
        pass

print(json.dumps(objects, indent=2))

'''
