import appJar
import time
from appJar import gui
import glob
import os, subprocess
from tkinter import Tk
from tkinter import filedialog
from os.path import expanduser
import shutil
import read_cmitems
import verify_media

import insert_access_db_gui
import read_access_db
import Add_Media_Files
import emv
import ConfigUsr1
import read_conf
import common_functions
import Upgrade_config_files

#prodtype = ["SCA Point Classic", "SCA Point Classic Passthrough", "SCA Point Enterprise", "FA/XPI", "CXPI - non MX products", "VHQ", "VCL", "Other"]
#device = ["MX830", "MX850", "MX860", "MX870", "MX880", "MX915-PCI 3", "MX915-PCI 4", "MX925-PCI 3", "MX925-PCI 4", "VX520", "VX690", "VX805", "VX820", "e255", "e315", "e335", "e355", "UX100" ]
#environment = ["Lab", "QA", "UAT", "Pilot", "Prod" ]

read_conf.read_conf_file()

prodtype = read_conf.prodtype
device = read_conf.device
environment = read_conf.environment

script = "scripts\\Cybersource.sh"
log = "logs\\CMAutomation.log"

filestobeinserted = ""
clintname = "Client Name        "
iniclintname = "Client Name         "
upclintname = "Client Name           "
packagecreationcmform = "CM Request #    "
inicreationcmform = "CM Request #     "
upcreationcmform = "CM Request #  "
enablepinbypass = "Pin Bypass - Enable"
remove_configusr1_default = "config.usr1 - Remove parameters set with default values"
convertxmlcfg = "xmlCfgFile.xml - Convert to config.usr1/aidlist/cdt"
updateconfigusrtopackage = "config.usr1 - Update parameters from CM Request into package"
updatevhqtopackage       = "VHQ - Update parameters from CM Request into package"
updatecdttopackage       = "cdt - Update parameters from CM Request into package"
updateaidlisttopackage   = "AidList.ini - Update parameters from CM Request into package"
updateemvtablestopackage = "EMVTables - Update parameters from CM Request into package"
updatcpuuserpwdtopackage = "CPUsernameAndPassword.txt - Update parameters from CM Request into package"
cmrequestotb = "CM Request #        "
vermediapackage = "verify Media package"

sigcapt     = "Enable Signature Capture"
siglimit    = "Signature Limit Amount        :"
saf         = "Enable SAF"
tranfllimit = "Transaction Floor Limit         :"
totfllimit  = "Total Floor Limit                    :"
dayslimit   = "Days Limit                            :"
lineitdisp  = "Enable Line Item Display"
vhqinst     = "VHQ Instance Name             :"
addimage    = "Insert image : Select image to insert from popup"

indpackage = "Individual or local package"
devprod = "devprod"
devprodgen = "devprodgen"
inidevprodgen = "inidevprodgen"
indfile = "individual files"
global mxdevtype
global image_file_path
global config_template_selected
global config_template_selectedd
global xml_package_selected
global cdt_selectedd
global aidlist_selectedd
global prev_requests
global cybersource_template_selected

app=gui("CMAutomation")

def alertupload(retcode, file):
    errmsg = ""
    if retcode == 0:
        app.infoBox("info", file + " records inserted successfully to Sharepoint Parameter Management")
    elif retcode == 1:
        errmsg = file + " is not in correct format"
    elif retcode == 2:
        errmsg = errmsg + file + " Not Found. Exiting..."
    elif retcode == 3:
        errmsg = errmsg + "Error opening " + file + " file. Exiting..."
    elif retcode == 4:
        errmsg = errmsg + "Error deleting " + file + " records from Sharepoint Parameter Management. Exiting..."
    elif retcode == 5:
        errmsg = errmsg + "Exiting..."
    elif retcode == 6:
        errmsg = errmsg + "Error inserting " + file + " records into Sharepoint Parameter Management. Exiting..."
    elif retcode == 7:
        errmsg = errmsg + file + " Records already present in Sharepoint Parameter Management for client/product type/Device/environment combination entered. Exiting ..."
    elif retcode == 8:
        errmsg = errmsg + file + " is not in correct format. Inserting " + file + " into Sharepoint Parameter Management is skipped..."
    elif retcode == 10:
        errmsg = errmsg + file + " is not in correct format. There should be a single space after comma. Inserting " + file + " into Sharepoint Parameter Management is skipped..."
    if errmsg != "":
        app.errorBox("error", errmsg)


def uploadinsertfiles(btn):
    clientID = read_access_db.getid_addclientname(app.getEntry("Client Name "))
    if (clientID == -1):
        app.errorBox("error", "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
    else:
        try:
            prodtype = app.getEntry("Product Type")
            dev = app.getEntry("Device      ")
            env = app.getEntry("Environment ")
            devtype = ""
            doit = 0

            if "MX" in dev or "mx" in dev:
                devtype = "MX"
            elif "VX" in dev or "vx" in dev:
                devtype = "VX"


            #app.addButtons(["Upload Individual Files from Local", "Extract Local package and Upload selected files"], uploadinsertfiles)
            if prodtype and dev and env:
                aidini = app.getCheckBox("AIDList.ini ")
                aidtxt = app.getCheckBox("AIDList.txt ")
                capk = app.getCheckBox("CAPKData.ini ")
                cdtt = app.getCheckBox("cdt.txt ")
                cdtini = app.getCheckBox("cdt.ini ")
                conf = app.getCheckBox("config.usr1 ")
                cpaini = app.getCheckBox("creditPreferredAIDs.ini ")
                ctlsconfig = app.getCheckBox("CTLSConfig.ini ")
                dpaini = app.getCheckBox("DebitPreferredAIDs.ini ")
                dispprompt = app.getCheckBox("DisplayPrompts.ini ")
                emvtables = app.getCheckBox("EMVTables.ini ")
                emvtagshost = app.getCheckBox("EMVTagsReqbyHost.txt ")
                msgxpi = app.getCheckBox("MSGXPI.txt ")
                optflag = app.getCheckBox("OptFlag.ini ")
                pcibitdat = app.getCheckBox("PCI_BIT.DAT ")
                propertiesdata = app.getCheckBox("propertiesdata.xml ")
                saferrcodesdat = app.getCheckBox("SAF_ERR_CODES.DAT ")
                tpscodessaftxt = app.getCheckBox("tpscodesforSAF.txt ")
                vhq = app.getCheckBox("VHQconfig.ini ")
                xmlreceiptfile = app.getCheckBox("xmlReceiptFile.xml ")

                if (aidini == False and aidtxt== False and capk== False and cdtt== False and cdtini== False and conf == False and
                            cpaini== False and ctlsconfig== False and
                            dpaini== False and dispprompt== False and emvtables== False and emvtagshost== False and msgxpi== False and
                            optflag== False and pcibitdat== False and propertiesdata== False and saferrcodesdat== False and
                            tpscodessaftxt== False and vhq == False and xmlreceiptfile== False):
                    app.errorBox("error", "Configuration files Upload to Sharepoint skipped since none of the option selected")
                else:
                    if btn == "Extract Local package and Upload selected files":
                        ###### Start Naming convention
                        path = "extract"
                        filetype = ""

                        if devtype == "MX":
                            filetype = "*.tgz"
                        elif devtype == "VX":
                            filetype = "*.zip"

                        if (len(glob.glob(path + '\\' + filetype))) > 1:
                            app.errorBox("error", "Multiple config packages present in " + path + " folder. Please make sure only 1 is present")
                            doit = 1
                        elif (len(glob.glob(path + '\\' + filetype))) == 0:
                            app.errorBox("error", "Please keep the package in " + path + " folder")
                            doit = 1
                        elif (len(glob.glob(path + '\\' + filetype))) == 1:
                            tgzname = str(glob.glob(path + '\\' + filetype))
                            if dev[:5].lower() not in tgzname.lower() or env.lower() not in tgzname.lower():
                                app.errorBox("error", "Please follow naming convention for package name in " + path + " folder")
                                doit = 1
                        ###### End Naming convention
                            else:
                                filelist = glob.glob(path + "\\*.tgz")
                                if not filelist:
                                    app.errorBox("error", "Package is not present in extract folder")
                                else:
                                    #command = "C:\\msys\\1.0\\bin\\bash.exe untgz.sh"
                                    command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\untgz.sh " + devtype
                                    # print("Extracting Configuration files from package in extract folder. Please wait ...")
                                    FNULL = open(os.devnull, 'w')
                                    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                                    if result.returncode != 0:
                                        print(result.stderr)
                                        print(result)
                                        app.errorBox("error", "Execution of untgz.sh failed")
                                        doit=1
                    if btn == "Upload Individual Files from Local" or doit == 0:
                        uploadedfiles = ""
                        if aidini:
                            aidinicode = insert_access_db_gui.insert_aidlistini(clientID, prodtype, dev, env)
                            if aidinicode == 0:
                                uploadedfiles = uploadedfiles + ", AIDList.ini"
                            else:
                                alertupload(aidinicode, "AIDList.ini")
                        if aidtxt:
                            aidtxtcode = insert_access_db_gui.insert_AIDListtxt(clientID, prodtype, dev, env)
                            if aidtxtcode == 0:
                                uploadedfiles = uploadedfiles + ", AIDList.txt"
                            else:
                                alertupload(aidtxtcode, "AIDList.txt")
                        if (capk):
                            capkcode = insert_access_db_gui.insert_CAPKData(clientID, prodtype, dev, env)
                            if capkcode == 0:
                                uploadedfiles = uploadedfiles + ", CAPKData.ini"
                            else:
                                alertupload(capkcode, "CAPKData.ini")
                        if (cdtt):
                            cdtcode = insert_access_db_gui.insert_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)
                            if cdtcode == 0:
                                uploadedfiles = uploadedfiles + ", cdt.txt"
                            else:
                                alertupload(cdtcode, "cdt.txt")
                        if (cdtini):
                            cdtinicode = insert_access_db_gui.insert_cdtini(clientID, prodtype, dev, env, read_access_db.cdtvxorder)
                            if cdtinicode == 0:
                                uploadedfiles = uploadedfiles + ", cdt.ini"
                            else:
                                alertupload(cdtinicode, "cdt.ini")
                        if (conf):
                            configusr1code = insert_access_db_gui.insert_configusr1(clientID, prodtype, dev, env)
                            if configusr1code == 0:
                                uploadedfiles = uploadedfiles + ", config.usr1"
                            else:
                                alertupload(configusr1code, "config.usr1")
                        if cpaini:
                            cpainicode = insert_access_db_gui.insert_PreferredAIDs(clientID, prodtype, dev, env, "credit")
                            if cpainicode == 0:
                                uploadedfiles = uploadedfiles + ", creditPreferredAIDs.INI"
                            else:
                                alertupload(cpainicode, "creditPreferredAIDs.INI")
                        if (ctlsconfig):
                            ctlscode = insert_access_db_gui.insert_CTLSConfigini(clientID, prodtype, dev, env)
                            if ctlscode == 0:
                                uploadedfiles = uploadedfiles + ", CTLSConfig.ini"
                            else:
                                alertupload(ctlscode, "CTLSConfig.ini")
                        if dpaini:
                            dpainicode = insert_access_db_gui.insert_PreferredAIDs(clientID, prodtype, dev, env, "debit")
                            if dpainicode == 0:
                                uploadedfiles = uploadedfiles + ", DebitPreferredAIDs.INI"
                            else:
                                alertupload(dpainicode, "DebitPreferredAIDs.INI")
                        if dispprompt:
                            disppromptcode = insert_access_db_gui.insert_displayPrompts(clientID, prodtype, dev, env)
                            if disppromptcode == 0:
                                uploadedfiles = uploadedfiles + ", displayPrompts.ini"
                            else:
                                alertupload(disppromptcode, "displayPrompts.ini")
                        if (emvtables):
                            emvtablescode = insert_access_db_gui.insert_EMVTablesINI(clientID, prodtype, dev, env)
                            if emvtablescode == 0:
                                uploadedfiles = uploadedfiles + ", EMVTables.ini"
                            else:
                                alertupload(emvtablescode, "EMVTables.ini")
                        if emvtagshost:
                            emvtagshostcode = insert_access_db_gui.insert_EmvTagsReqbyHost(clientID, prodtype, dev, env)
                            if emvtagshostcode == 0:
                                uploadedfiles = uploadedfiles + ", EmvTagsReqbyHost.txt"
                            else:
                                alertupload(emvtagshostcode, "EmvTagsReqbyHost.txt")
                        if (msgxpi):
                            msgxpicode = insert_access_db_gui.insert_msgxpi(clientID, prodtype, dev, env)
                            if msgxpicode == 0:
                                uploadedfiles = uploadedfiles + ", MSGXPI.txt"
                            else:
                                alertupload(msgxpicode, "MSGXPI.txt")
                        if optflag:
                            optflagcode = insert_access_db_gui.insert_optflag(clientID, prodtype, dev, env)
                            if optflagcode == 0:
                                uploadedfiles = uploadedfiles + ", OptFlag.INI"
                            else:
                                alertupload(optflagcode, "OptFlag.INI")
                        if pcibitdat:
                            pcibitdatcode = insert_access_db_gui.insert_PCI_BIT(clientID, prodtype, dev, env)
                            if pcibitdatcode == 0:
                                uploadedfiles = uploadedfiles + ", PCI_BIT.DAT"
                            else:
                                alertupload(pcibitdatcode, "PCI_BIT.DAT")
                        if propertiesdata:
                            propertiesdatacode = insert_access_db_gui.insert_propertiesdata(clientID, prodtype, dev, env)
                            if propertiesdatacode == 0:
                                uploadedfiles = uploadedfiles + ", propertiesdata.xml"
                            else:
                                alertupload(propertiesdatacode, "propertiesdata.xml")
                        if saferrcodesdat:
                            saferrcodesdatcode = insert_access_db_gui.insert_SAF_Err_Codes(clientID, prodtype, dev, env)
                            if saferrcodesdatcode == 0:
                                uploadedfiles = uploadedfiles + ", SAF_ERR_CODES.DAT"
                            else:
                                alertupload(saferrcodesdatcode, "SAF_ERR_CODES.DAT")
                        if tpscodessaftxt:
                            tpscodessaftxtcode = insert_access_db_gui.insert_tpscodesforSAF(clientID, prodtype, dev, env)
                            if tpscodessaftxtcode == 0:
                                uploadedfiles = uploadedfiles + ", tpscodesforSAF.txt"
                            else:
                                alertupload(tpscodessaftxtcode, "tpscodesforSAF.txt")
                        if (vhq):
                            vhqcode = insert_access_db_gui.insert_vhqconfig(clientID, prodtype, dev, env)
                            if vhqcode == 0:
                                uploadedfiles = uploadedfiles + ", vhqconfig.ini"
                            else:
                                alertupload(vhqcode, "vhqconfig.ini")
                        if xmlreceiptfile:
                            xmlreceiptfiletcode = insert_access_db_gui.insert_xmlReceiptFile(clientID, prodtype, dev, env)
                            if xmlreceiptfiletcode == 0:
                                uploadedfiles = uploadedfiles + ", xmlReceiptFile.xml"
                            else:
                                alertupload(xmlreceiptfiletcode, "xmlReceiptFile.xml")

                        if uploadedfiles != "":
                            uploadedfiles = uploadedfiles[2:]
                            app.infoBox("info", uploadedfiles + " Records inserted successfully to Sharepoint Parameter Management")

            else:
                app.errorBox("error", "Enter values for Product Type / Device / Environment")
        except FileNotFoundError:
            pass


def insertmedia(btn):
    succ = Add_Media_Files.insert_sample_media_gui(app.getEntry("CM Request #  "))
    if succ == 0:
        app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
    elif succ == 1:
        app.errorBox("error", "Execution of media.sh failed")
    elif succ == 2:
        app.errorBox("error", "Applicable for MX915/MX925 only")
    elif succ == 3:
        app.errorBox("error", "Form doesn\'t exist in Sharepoint")
    elif succ == 20:
        app.errorBox("error","Multiple config packages present in extract folder. Please make sure only 1 is present")
    elif succ == 21:
        app.errorBox("error", "Please keep the package in extract folder")
    elif succ == 22:
        app.errorBox("error", "Please follow naming convention for package name in extract folder")


def insertemv(btn):
    succ = emv.insert_emv("gui", app.getEntry("CM Request #"))
    if succ == 0:
        app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
    elif succ == 1:
        app.errorBox("error", "Execution of emv.sh failed")
    elif succ == 2:
        app.errorBox("error", "EMV template package Path doesn\'t exist. Please create this path and place template EMV tgz")
    elif succ == 3:
        app.errorBox("error", "Applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize")
    elif succ == 4:
        app.errorBox("error", "Applicable for valid processor only")
    elif succ == 5:
        app.errorBox("error", "Applicable for MX or VX devices only")
    elif succ == 6:
        app.errorBox("error", "Applicable either for Auto Credit or Auto Debit or Customer Choice options")
    elif succ == 7:
        app.errorBox("error", "Applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize AND EMV as Yes")
    elif succ == 8:
        app.errorBox("error", "Form doesn\'t exist in Sharepoint")
    elif succ == 20:
        app.errorBox("error","Multiple config packages present in extract folder. Please make sure only 1 is present")
    elif succ == 21:
        app.errorBox("error", "Please keep the package in extract folder")
    elif succ == 22:
        app.errorBox("error", "Please follow naming convention for package name in extract folder")

def alertuser(succ, filename):
    if succ == 1:
        app.errorBox("error", "Client Name doesn\'t exist in \'Add Client Name\' list in Sharepoint")
    elif succ == 2:
        app.errorBox("error", "Error creating " + filename)
    elif succ == 3:
        app.errorBox("error", "No Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + filename)


def downloadinsertfiles(btn):
    clientID = read_access_db.getid_addclientname(app.getEntry("Client Name  "))
    if (clientID == -1):
        app.errorBox("error", "Client Name doesn't exist in \'Add Client Name\' list in Sharepoint")
        return False
    else:
        prodtype = app.getEntry("Product Type ")
        dev      = app.getEntry("Device       ")
        env      = app.getEntry("Environment  ")
        doit = 0

        devtype = ""
        if "MX" in dev or "mx" in dev:
            devtype = "MX"
        elif "VX" in dev or "vx" in dev:
            devtype = "VX"

        ###### Start Naming convention
        if btn == "Download Files and Insert into Local Package":
            path = "extract"
            filetype = ""

            if devtype == "MX":
                filetype = "*.tgz"
            elif devtype == "VX":
                filetype = "*.zip"

            if (len(glob.glob(path + '\\' + filetype))) > 1:
                app.errorBox("error","Multiple config packages present in " + path + " folder. Please make sure only 1 is present")
                doit = 1
            elif (len(glob.glob(path + '\\' + filetype))) == 0:
                app.errorBox("error", "Please keep the package in " + path + " folder")
                doit = 1
            elif (len(glob.glob(path + '\\' + filetype))) == 1:
                tgzname = str(glob.glob(path + '\\' + filetype))
                if dev[:5].lower() not in tgzname.lower() or env.lower() not in tgzname.lower():
                    app.errorBox("error", "Please follow naming convention for package name in " + path + " folder")
                    doit = 1
        ###### End Naming convention

        if doit == 0:
            aidini         = app.getCheckBox("AIDList.ini")
            aidtxt         = app.getCheckBox("AIDList.txt")
            capk           = app.getCheckBox("CAPKData.ini")
            cdtt           = app.getCheckBox("cdt.txt")
            cdtini         = app.getCheckBox("cdt.ini")
            conf           = app.getCheckBox("config.usr1")
            cpaini         = app.getCheckBox("creditPreferredAIDs.ini")
            ctlsconfig     = app.getCheckBox("CTLSConfig.ini")
            dpaini         = app.getCheckBox("DebitPreferredAIDs.ini")
            dispprompt     = app.getCheckBox("DisplayPrompts.ini")
            emvtables      = app.getCheckBox("EMVTables.ini")
            emvtagshost    = app.getCheckBox("EMVTagsReqbyHost.txt")
            msgxpi         = app.getCheckBox("MSGXPI.txt")
            optflag        = app.getCheckBox("OptFlag.ini")
            pcibitdat      = app.getCheckBox("PCI_BIT.DAT")
            propertiesdata = app.getCheckBox("propertiesdata.xml")
            saferrcodesdat = app.getCheckBox("SAF_ERR_CODES.DAT")
            tpscodessaftxt = app.getCheckBox("tpscodesforSAF.txt")
            vhq            = app.getCheckBox("VHQconfig.ini")
            xmlreceiptfile = app.getCheckBox("xmlReceiptFile.xml")

            if ( aidini == False and aidtxt == False and capk == False and cdtt == False and cdtini == False and conf == False and cpaini == False and ctlsconfig == False and dpaini == False and dispprompt == False and emvtables == False and
                 emvtagshost == False and msgxpi == False and optflag == False and pcibitdat == False and propertiesdata == False and saferrcodesdat == False and tpscodessaftxt == False and vhq == False and xmlreceiptfile == False):
                app.errorBox("error", "Configuration files download skipped since none of the option selected")
            else:
                succaidini = 999
                succaidtxt = 999
                succcapk = 999
                succcdtt = 999
                succcdtini = 999
                succconf = 999
                succcpaini = 999
                succctls = 999
                succdpaini = 999
                succdispprompt = 999
                succemvtables = 999
                succemvtagshost = 999
                succmsgxpi = 999
                succoptflag = 999
                succpcibitdat = 999
                succpropertiesdata = 999
                succsaferrcodesdat = 999
                succtpscodessaftxt = 999
                succvhq = 999
                succxmlreceiptfile = 999

                filestobeinserted = ""

                if (aidini):
                    succaidini = read_access_db.create_aidlistini(clientID, prodtype, dev, env)
                    if succaidini == 0:
                        filestobeinserted = filestobeinserted + " " + "aidlist.ini"
                    alertuser(succaidini, "aidlist.ini")
                if (aidtxt):
                    succaidtxt = read_access_db.create_AIDListtxt(clientID, prodtype, dev, env)
                    if succaidtxt == 0:
                        filestobeinserted = filestobeinserted + " " + "aidlist.txt"
                    alertuser(succaidtxt, "aidlist.txt")
                if (capk):
                    succcapk = read_access_db.create_CAPKData(clientID, prodtype, dev, env)
                    if succcapk == 0:
                        filestobeinserted = filestobeinserted + " " + "capkdata.ini"
                    alertuser(succcapk, "capkdata.ini")
                if (cdtt):
                    succcdtt = read_access_db.create_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)
                    if succcdtt == 0:
                        filestobeinserted = filestobeinserted + " " + "cdt.txt"
                    alertuser(succcdtt, "cdt.txt")
                if (cdtini):
                    succcdtini = read_access_db.create_cdtini(clientID, prodtype, dev, env)
                    if succcdtini == 0:
                        filestobeinserted = filestobeinserted + " " + "cdt.ini"
                    alertuser(succcdtini, "cdt.ini")
                    if succcdtini == 4:
                        app.errorBox("error", "Error creating CDT.INI")
                if (conf):
                    succconf = read_access_db.create_configusr1(clientID, prodtype, dev, env)
                    if succconf == 0:
                        filestobeinserted = filestobeinserted + " " + "config.usr1"
                    alertuser(succconf, "config.usr1")
                if (cpaini):
                    succcpaini = read_access_db.create_PreferredAIDs(clientID, prodtype, dev, env, "credit")
                    if succcpaini == 0:
                        filestobeinserted = filestobeinserted + " " + "creditPreferredaids.ini"
                    alertuser(succcpaini, "creditPreferredaids.ini")
                if (ctlsconfig):
                    succctls = read_access_db.create_CTLSConfig(clientID, prodtype, dev, env)
                    if succctls == 0:
                        filestobeinserted = filestobeinserted + " " + "ctlsconfig.ini"
                    alertuser(succctls, "ctlsconfig.ini")
                if (dpaini):
                    succdpaini = read_access_db.create_PreferredAIDs(clientID, prodtype, dev, env, "debit")
                    if succdpaini == 0:
                        filestobeinserted = filestobeinserted + " " + "debitPreferredaids.ini"
                    alertuser(succdpaini, "debitPreferredaids.ini")
                if (dispprompt):
                    succdispprompt = read_access_db.create_displayPrompts(clientID, prodtype, dev, env)
                    if succdispprompt == 0:
                        filestobeinserted = filestobeinserted + " " + "displayPrompts.ini"
                    alertuser(succdispprompt, "displayPrompts.ini")
                if (emvtables):
                    succemvtables = read_access_db.create_emvtables(clientID, prodtype, dev, env)
                    if succemvtables == 0:
                        filestobeinserted = filestobeinserted + " " + "emvtables.ini"
                    alertuser(succemvtables, "emvtables.ini")
                if (emvtagshost):
                    succemvtagshost = read_access_db.create_EmvTagsReqbyHost(clientID, prodtype, dev, env)
                    if succemvtagshost == 0:
                        filestobeinserted = filestobeinserted + " " + "EmvTagsReqbyHost.txt"
                    alertuser(succemvtagshost, "EmvTagsReqbyHost.txt")
                if (msgxpi):
                    succmsgxpi = read_access_db.create_MSGXPI(clientID, prodtype, dev, env)
                    if succmsgxpi == 0:
                        filestobeinserted = filestobeinserted + " " + "msgxpi.txt"
                    alertuser(succmsgxpi, "msgxpi.txt")
                if (optflag):
                    succoptflag = read_access_db.create_optflag(clientID, prodtype, dev, env)
                    if succoptflag == 0:
                        filestobeinserted = filestobeinserted + " " + "optflag.ini"
                    alertuser(succoptflag, "optflag.ini")
                if (pcibitdat):
                    succpcibitdat = read_access_db.create_PCI_BIT(clientID, prodtype, dev, env)
                    if succpcibitdat == 0:
                        filestobeinserted = filestobeinserted + " " + "pci_bit.dat"
                    alertuser(succpcibitdat, "pci_bit.dat")
                if (propertiesdata):
                    succpropertiesdata = read_access_db.create_propertiesdata(clientID, prodtype, dev, env)
                    if succpropertiesdata == 0:
                        filestobeinserted = filestobeinserted + " " + "propertiesdata.xml"
                    alertuser(succpropertiesdata, "propertiesdata.xml")
                if (saferrcodesdat):
                    succsaferrcodesdat = read_access_db.create_SAF_Err_Codes(clientID, prodtype, dev, env)
                    if succsaferrcodesdat == 0:
                        filestobeinserted = filestobeinserted + " " + "saf_err_codes.dat"
                    alertuser(succsaferrcodesdat, "saf_err_codes.dat")
                if (tpscodessaftxt):
                    succtpscodessaftxt = read_access_db.create_tpscodesforSAF(clientID, prodtype, dev, env)
                    if succtpscodessaftxt == 0:
                        filestobeinserted = filestobeinserted + " " + "TPSCodesForSAF.txt"
                    alertuser(succtpscodessaftxt, "TPSCodesForSAF.txt")
                if (vhq):
                    succvhq = read_access_db.create_VHQconfig(clientID, prodtype, dev, env)
                    if succvhq == 0:
                        filestobeinserted = filestobeinserted + " " + "vhqconfig.ini"
                    alertuser(succvhq, "vhqconfig.ini")
                if (xmlreceiptfile):
                    succxmlreceiptfile = read_access_db.create_xmlReceiptFile(clientID, prodtype, dev, env)
                    if succxmlreceiptfile == 0:
                        filestobeinserted = filestobeinserted + " " + "xmlReceiptFile.xml"
                    alertuser(succxmlreceiptfile, "xmlReceiptFile.xml")

                if btn == "Download Files and Insert into Local Package":
                    if ( filestobeinserted != "" ):
                        if read_access_db.insert_files_into_package("scripts\\insert_files_into_package.sh", devtype , filestobeinserted):
                            app.infoBox("info", filestobeinserted.strip().replace(" ", ", ") + " file/files were present in Parameter Management in Sharepoint and is/are downloaded and inserted into package @ extract folder.")
                    else:
                        app.errorBox("error", "Config Files couldn't be created from Sharepoint")
                elif btn == "Download Individual Files":
                    if ( succaidini == 0 or succaidtxt == 0 or succcapk == 0 or succcdtt == 0 or succcdtini == 0 or succconf == 0 or succcpaini == 0 or succctls == 0 or succdpaini == 0 or succdispprompt == 0 or succemvtables == 0 or succemvtagshost == 0 or succmsgxpi == 0 or succoptflag == 0 or succpcibitdat == 0 or succpropertiesdata == 0 or succsaferrcodesdat == 0 or succtpscodessaftxt == 0 or succvhq == 0 or succxmlreceiptfile == 0 ):
                        app.infoBox("info", "Selected files are downloaded @ extract folder")
                    else:
                        app.errorBox("error", "Selected config Files couldn't be created or not present in Sharepoint Parameters Management")


def createconfigusr1(btn):
    if (app.getEntry("CM Request # ")) < 1:
        app.errorBox("error", "Please enter correct CM Request Number")
    else:
        configuser1 = ConfigUsr1.config_usr1(app.getEntry("CM Request # "))
        succ = configuser1.kickstart_config_usr1_processing()
        if succ == 0:
            configusrpath = "config.usr1 and vhqconfig.ini files are created at :  " + os.getcwd() + "\\" + str(int(app.getEntry("CM Request # ")))
            app.infoBox("info", configusrpath)
        elif succ == 1:
            errormsg = "Form " + str(int(app.getEntry("CM Request # "))) + " doesn't exist in Sharepoint. Please enter correct form number"
            app.errorBox("info", errormsg)
        elif succ == 2:
            errormsg = "Program supports SCA Point Classic / Passthrough and Enterprise. CM Request # entered is for other than these product type"
            app.errorBox("info", errormsg)
        elif succ == 3:
            errormsg = "Program supports for New clients which means CM request with \'Type of request\' parameter as \'Initial configuration\'. Exiting ..."
            app.errorBox("info", errormsg)
        elif succ == 4:
            errormsg = "Error creating config.usr1 file"
            app.errorBox("info", errormsg)
        elif succ == 5:
            errormsg = "Record info couldn't be pulled from CM Portal due to Invalid characters in Form " + str(int(app.getEntry("CM Request # ")))
            app.errorBox("info", errormsg)
        elif succ == 9:
            errormsg = "config.usr1 is created @ " + os.getcwd() + "\\" + str(int(app.getEntry("CM Request # ")))  + " but vhqconfig.ini couldn't be created"
            app.errorBox("info", errormsg)
        elif succ == 10:
            errormsg = "vhqconfig.ini is created @ " + os.getcwd() + "\\" + str(int(app.getEntry("CM Request # ")))  + " but config.usr1 couldn't be created"
            app.errorBox("info", errormsg)


def cybersource_processing(device, sigenable, signaturelimit, safenable, transfloorlimit, totalfllimit, dayslimits, lineitemdisp, vhqinstance, addimagetopackage):
    sigenableparam = ""
    signaturelimitparam = ""
    safenableparam = ""
    transfloorlimitparam = ""
    totalfllimitparam = ""
    dayslimitsparam = ""
    lineitemdispparam = ""
    vhqinstanceparam = ""
    addimageYN = ""

    if not sigenable and not safenable and not lineitemdisp and not addimagetopackage:
        app.errorBox("error", "None of the parameter options selected... Exiting")
    else:
        if sigenable:
            sigenableparam = "Y"
        else:
            sigenableparam = "N"

        if signaturelimit > 0:
            if app.getRadioButton(indpackage) == "VX":
                signaturelimitparam = str(round(signaturelimit, 2))
                signaturelimitparam = "{:0.2f}".format(signaturelimit) # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
                #print("{:0.2f}".format(signaturelimit))
            elif app.getRadioButton(indpackage) == "MX":
                signaturelimitparam = str(int(signaturelimit))
                print(signaturelimitparam)
        else:
            signaturelimitparam = "\"\""

        if safenable:
            safenableparam = "Y"
        else:
            safenableparam = "N"

        if transfloorlimit > 0:
            transfloorlimitparam = str(int(transfloorlimit))
        else:
            transfloorlimitparam = "\"\""

        if totalfllimit > 0:
            totalfllimitparam = str(int(totalfllimit))
        else:
            totalfllimitparam = "\"\""

        if dayslimits > 0:
            dayslimitsparam = str(int(dayslimits))
        else:
            dayslimitsparam = "\"\""

        if lineitemdisp:
            lineitemdispparam = "Y"
        else:
            lineitemdispparam = "N"

        if vhqinstance != "":
            vhqinstanceparam = str(vhqinstance)
        else:
            vhqinstanceparam = "\"\""

        if addimagetopackage:
            addimageYN = "Y"
            #app.directoryBox("Hi")
            #app.questionBox("Bye", "MX915 or MX925")
        else:
            addimageYN = "N"

        usetemplate = ""
        if app.getRadioButton(devprod) == "Use Lab Template":
            usetemplate = "Lab"
        elif app.getRadioButton(devprod) == "Use Prod Template":
            usetemplate = "Prod"

        #base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        home = expanduser("~")
        base_path = ""
        if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
        elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
            base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
        elif os.path.isdir(home + "\\OneDrive - Verifone"):
            home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

        basefilespath = base_path + "Cybersource BaseFiles\\" + app.getRadioButton(indpackage) + "\\" + usetemplate
        print(basefilespath)
        if not os.path.isdir(basefilespath):
            app.errorBox("error", "Config template path " + basefilespath + " doesn't exist")
        else:
            filetype = "*.tgz"
            if (len(glob.glob(basefilespath + '\\' + filetype))) > 1:
                app.errorBox("error", "Multiple config templates present in " + basefilespath + " folder. Please make sure only 1 is present")
            elif (len(glob.glob(basefilespath + '\\' + filetype))) == 0:
                app.errorBox("error", "Please make sure config template is present in " + basefilespath + " folder")
            elif (len(glob.glob(basefilespath + '\\' + filetype))) == 1:
                fileList = os.listdir("extract")
                try:
                    for fileName in fileList:
                        if os.path.isdir("extract\\" + fileName):
                            shutil.rmtree("extract\\" + fileName)
                        else:
                            os.remove("extract\\" + fileName)
                except:
                    app.errorBox("error", "Error deleting extract\\" + fileName + ". Close this file if it's open")

                configtemplatename = os.listdir(basefilespath)
                print(configtemplatename)
                shutil.copyfile(basefilespath + "\\" + configtemplatename[0], "extract\\" + configtemplatename[0])

                command = ""
                if device == "MX" and addimagetopackage:
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " MX " + str(sigenableparam) + " " + str(signaturelimitparam) + " " + str(safenableparam) + " " + str((transfloorlimitparam)) + " " + str((totalfllimitparam)) + " " + str((dayslimitsparam)) + " " + str(lineitemdispparam) + " " + str(vhqinstanceparam) + " " + str(mxdevtype)  + " \"" + image_file_path + "\""
                if device == "MX" and addimagetopackage == False:
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " MX " + str(sigenableparam) + " " + str(signaturelimitparam) + " " + str(safenableparam) + " " + str((transfloorlimitparam)) + " " + str((totalfllimitparam)) + " " + str((dayslimitsparam)) + " " + str(lineitemdispparam) + " " + str(vhqinstanceparam)
                elif device == "VX":
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " VX " + str(safenableparam) + " " + str((transfloorlimitparam)) + " " + str((totalfllimitparam)) + " " + str((dayslimitsparam)) + " " + str(vhqinstanceparam)

                if command != "":
                    print(command)
                    FNULL = open(os.devnull, 'w')
                    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                    if result.returncode != 0:
                        print(result.stderr)
                        print(result)
                        print("\nExecution of " + script + " failed")
                        app.errorBox("error", "Error creating update package")
                        return False
                    else:
                        print("Updated package is available @ extract folder")
                        app.infoBox("info", "Updated package is available @ extract folder")
                        return True


def createcyberpackage(btn):
    cybersource_processing(app.getRadioButton(indpackage), app.getCheckBox(sigcapt), app.getEntry(siglimit), app.getCheckBox(saf),
                              app.getEntry(tranfllimit), app.getEntry(totfllimit), app.getEntry(dayslimit),
                              app.getCheckBox(lineitdisp), app.getEntry(vhqinst), app.getCheckBox(addimage))


def insertimageoptions(btn):
    app.openTab("CMAutomationTabbedFrame", "Cybersource")
    if app.getCheckBox(addimage):
        global mxdevtype
        global image_file_path
        if app.questionBox("MX915 or MX925", "Yes - MX915\nNo -  MX925") == "yes":
            mxdevtype = "915"
        else:
            mxdevtype = "925"
        root = Tk()
        root.withdraw()
        image_file_path = filedialog.askopenfilename(defaultextension='.png', filetypes=[('Bin file','*.png'), ('All files','*.*')])
        #print(image_file_path)
        msg = "Image path selected : " + image_file_path
        mxdevtypemsg = "Device Type selected : MX" + mxdevtype

        try:
            app.addLabel("devtype", mxdevtypemsg)
            app.addLabel("Imagepathselected", msg)
        except:
            app.setLabel("devtype", mxdevtypemsg)
            app.showWidget(kind=1, name="devtype")
            app.setLabel("Imagepathselected", msg)
            app.showWidget(kind=1, name="Imagepathselected")
    else:
        try:
            app.hideWidget(kind=1 , name="devtype")
            app.hideWidget(kind=1 , name="Imagepathselected")
        except:
            pass

def insertimageoptionspackagecreation(btn):
    app.openTab("CMAutomationTabbedFrame", "Cybersource")
    if app.getCheckBox(addimage):
        global mxdevtype
        global image_file_path
        if app.questionBox("MX915 or MX925", "Yes - MX915\nNo -  MX925") == "yes":
            mxdevtype = "915"
        else:
            mxdevtype = "925"
        root = Tk()
        root.withdraw()
        image_file_path = filedialog.askopenfilename(defaultextension='.png', filetypes=[('Bin file','*.png'), ('All files','*.*')])
        #print(image_file_path)
        msg = "Image path selected : " + image_file_path
        mxdevtypemsg = "Device Type selected : MX" + mxdevtype

        try:
            app.addLabel("devtype", mxdevtypemsg)
            app.addLabel("Imagepathselected", msg)
        except:
            app.setLabel("devtype", mxdevtypemsg)
            app.showWidget(kind=1, name="devtype")
            app.setLabel("Imagepathselected", msg)
            app.showWidget(kind=1, name="Imagepathselected")
    else:
        try:
            app.hideWidget(kind=1 , name="devtype")
            app.hideWidget(kind=1 , name="Imagepathselected")
        except:
            pass


def next(btn):
    app.openTab("CMAutomationTabbedFrame", "Cybersource")
    if app.getRadioButton(indpackage) == "MX":
        try:
            #app.addLabel("instr1", "Select right options option below and hit 'Create Package' button")
            app.addEmptyLabel("empty23")
            app.addCheckBox(sigcapt)
            app.addLabelNumericEntry(siglimit)
            app.addEmptyLabel("empty24")
            app.addCheckBox(saf)
            app.addLabelNumericEntry(tranfllimit)
            app.addLabelNumericEntry(totfllimit)
            app.addLabelNumericEntry(dayslimit)
            app.addEmptyLabel("empty25")
            app.addCheckBox(lineitdisp)
            app.addEmptyLabel("empty26")
            app.addLabelEntry(vhqinst)
            app.addEmptyLabel("empty27")
            app.addCheckBox(addimage)
            app.addButton("Create Package", createcyberpackage)
            app.addEmptyLabel("empty28")
        except:
            #app.showWidget(kind=1, name="instr1")
            app.showWidget(kind=4, name=sigcapt)  # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
            app.showWidget(kind=2, name=siglimit)
            app.showWidget(kind=4, name=saf)
            app.showWidget(kind=2, name=tranfllimit)
            app.showWidget(kind=2, name=totfllimit)
            app.showWidget(kind=2, name=dayslimit)
            app.showWidget(kind=1, name="empty25")
            app.showWidget(kind=4, name=lineitdisp)
            app.showWidget(kind=2, name=vhqinst)
            app.showWidget(kind=4, name=addimage)
            app.showWidget(kind=3, name="Create Package")
    elif app.getRadioButton(indpackage) == "VX":
        try:
            #app.addLabel("instr1", "Select right options option below and hit 'Create Package' button")
            app.addCheckBox(sigcapt)
            app.addLabelNumericEntry(siglimit)
            app.addEmptyLabel("empty24")
            app.addCheckBox(saf)
            app.addLabelNumericEntry(tranfllimit)
            app.addLabelNumericEntry(totfllimit)
            app.addLabelNumericEntry(dayslimit)
            app.addEmptyLabel("empty25")
            app.addCheckBox(lineitdisp)
            app.addEmptyLabel("empty26")
            app.addLabelEntry(vhqinst)
            app.addEmptyLabel("empty27")
            app.addButton("Create Package", createcyberpackage)
            app.addEmptyLabel("empty28")
            app.hideWidget(kind=4, name=sigcapt)  # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
            app.hideWidget(kind=2, name=siglimit)
            app.hideWidget(kind=1, name="empty25")
            app.hideWidget(kind=4, name=lineitdisp)
            app.hideWidget(kind=4, name=addimage)
        except:
            app.hideWidget(kind=4, name=sigcapt)  # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
            app.hideWidget(kind=2, name=siglimit)
            app.hideWidget(kind=1, name="empty25")
            app.hideWidget(kind=4, name=lineitdisp)
            app.hideWidget(kind=4, name=addimage)
            # app.hideWidget(kind=3, name="Update Package")

def process(btn):
    if app.getEntry("Client Name      ") == "":
        app.errorBox("error", "Please enter Client Name")
        return 1
    formnumber = str(int(app.getEntry("CM Request #   ")))
    if int(formnumber) < 1:
        app.errorBox("error", "Please enter correct CM Request Number")
        return 1
    else:
        clintid = read_access_db.getid_addclientname(app.getEntry("Client Name      "))
        if (clintid == -1):
            app.errorBox("error", "Client Name doesn't exist in Sharepoint")
            return 1
        message = ""
        forminfo = read_cmitems.read_cmitems()
        if not forminfo.kickstart_cmitems_processing_gui(formnumber):
            app.errorBox("error", "Form " + formnumber + " doesn't exist in Sharepoint")
            return 1
        forminfo.create_dict()
        clientidfromform = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Client Name')])
        if clientidfromform != clintid:
            app.errorBox("error", "Cliant Name entered (" + app.getEntry(clintname)  + ") does not match with Client Name in CM request")
            return 1

        if app.getCheckBox("Create config.usr1, vhqconfig.ini"):
            configuser1 = ConfigUsr1.config_usr1(formnumber)
            succ = configuser1.kickstart_config_usr1_processing()
            if succ == 0:
                configusrpath = "config.usr1 and vhqconfig.ini files are created at :  " + os.getcwd() + "\\" + formnumber
                app.infoBox("info", configusrpath)
            elif succ == 1:
                errormsg = "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number"
                app.errorBox("info", errormsg)
            elif succ == 2:
                errormsg = "Program supports SCA Point Classic / Passthrough and Enterprise. CM Request # entered is for other than these product type"
                app.errorBox("info", errormsg)
            elif succ == 3:
                errormsg = "Program supports for New clients which means CM request with \'Type of request\' parameter as \'Initial configuration\'. Exiting ..."
                app.errorBox("info", errormsg)
            elif succ == 4:
                errormsg = "Error creating config.usr1 file"
                app.errorBox("info", errormsg)
            elif succ == 5:
                errormsg = "Record info couldn't be pulled from CM Portal due to Invalid characters in Form " + formnumber
                app.errorBox("info", errormsg)
            elif succ == 9:
                errormsg = "config.usr1 is created @ " + os.getcwd() + "\\" + formnumber + " but vhqconfig.ini couldn't be created"
                app.errorBox("info", errormsg)
            elif succ == 10:
                errormsg = "vhqconfig.ini is created @ " + os.getcwd() + "\\" + formnumber + " but config.usr1 couldn't be created"
                app.errorBox("info", errormsg)
        if app.getCheckBox("Insert EMV Templates"):
            succ = emv.insert_emv("gui", formnumber)
            if succ == 0:
                app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
            elif succ == 1:
                app.errorBox("error", "Execution of emv.sh failed")
            elif succ == 2:
                app.errorBox("error",
                             "EMV template package Path doesn\'t exist. Please create this path and place template EMV tgz")
            elif succ == 3:
                app.errorBox("error",
                             "Applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize")
            elif succ == 4:
                app.errorBox("error", "Applicable for valid processor only")
            elif succ == 5:
                app.errorBox("error", "Applicable for MX or VX devices only")
            elif succ == 6:
                app.errorBox("error",
                             "Applicable either for Auto Credit or Auto Debit or Customer Choice options")
            elif succ == 7:
                app.errorBox("error",
                             "Applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize AND EMV as Yes")
            elif succ == 8:
                app.errorBox("info",
                             "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
            elif succ == 20:
                app.errorBox("error",
                             "Multiple config packages present in extract folder. Please make sure only 1 is present")
            elif succ == 21:
                app.errorBox("error", "Please keep the package in extract folder")
            elif succ == 22:
                app.errorBox("error", "Please follow naming convention for package name in extract folder")
        if app.getCheckBox("Insert Media Templates"):
            succ = Add_Media_Files.insert_sample_media_gui(formnumber)
            if succ == 0:
                app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
            elif succ == 1:
                app.errorBox("error", "Execution of media.sh failed")
            elif succ == 2:
                app.errorBox("error", "Applicable for MX915/MX925 only")
            elif succ == 3:
                app.errorBox("info",
                             "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
            elif succ == 20:
                app.errorBox("error","Multiple config packages present in extract folder. Please make sure only 1 is present")
            elif succ == 21:
                app.errorBox("error", "Please keep the package in extract folder")
            elif succ == 22:
                app.errorBox("error", "Please follow naming convention for package name in extract folder")

def processvermedia(btn):
    global config_template_selectedd

    verify_media.read_media_guide()
    arguments = ""
    if app.getRadioButton("select device") == "MX915":
        arguments = "915"
    else:
        arguments = "925"

    scaver = app.getEntry("Enter SCA Version : ")

    if scaver == "":
        app.errorBox("error", "Please enter SCA Version and then press Process button")
        return 1
    else:
        arguments = arguments + " " + scaver

    if config_template_selectedd == "":
        app.errorBox("error", "Please select config package to be verified")
        return 1

    fileList = os.listdir("extract")
    try:
        for fileName in fileList:
            if os.path.isdir("extract\\" + fileName):
                shutil.rmtree("extract\\" + fileName)
            else:
                os.remove("extract\\" + fileName)
    except:
        app.errorBox("error", "Error deleting extract\\" + fileName + ". Close this file if it's open")
        return 1

    try:
        shutil.copyfile(config_template_selectedd, "extract\\" + config_template_selectedd.rsplit('/', 1)[-1])
    except:
        app.errorBox("error", "Error Copying " + config_template_selectedd + " to extract folder")
        return 1


    msg = verify_media.validate_media(arguments)
    if msg == "error":
        app.errorBox("error", "Error processing package")
    elif msg == "":
        app.infoBox("info", "Media package is correct")
    else:
        app.infoBox("info", msg)


def processotb(btn):
    if app.getEntry("Client Name       ") == "":
        app.errorBox("error", "Please enter Client Name")
        return 1
    formnumber = str(int(app.getEntry(cmrequestotb)))
    if int(formnumber) < 1:
        app.errorBox("error", "Please enter correct CM Request Number")
        return 1
    else:
        clintid = read_access_db.getid_addclientname(app.getEntry("Client Name       "))
        if (clintid == -1):
            app.errorBox("error", "Client Name doesn't exist in Sharepoint")
            return 1
        message = ""
        forminfo = read_cmitems.read_cmitems()
        if not forminfo.kickstart_cmitems_processing_gui(formnumber):
            app.errorBox("error", "Form " + formnumber + " doesn't exist in Sharepoint")
            return 1
        forminfo.create_dict()
        clientidfromform = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Client Name')])
        if clientidfromform != clintid:
            app.errorBox("error", "Cliant Name entered (" + app.getEntry(clintname)  + ") does not match with Client Name in CM request")
            return 1

        prodttt = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product Type')])
        if prodttt != "OTB - Small Merchants ( 200 or less Devices )":
            app.errorBox("error", "CM Request " + str(formnumber) + " is not for OTB - Small Merchants ( 200 or less Devices )")
            return 1

        package = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Package')])
        instvhq = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('VHQ Company ID')])
        devtype = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Device Model')])
        if "MX" in devtype:
            devtype = "MX"
        elif "VX" in devtype:
            devtype = "VX"
        else:
            app.errorBox("error", "CM Request " + str(formnumber) + " is not for MX or VX devices. Exiting ...")
            return 1

        #print(package)
        if package == "":
            app.errorBox("error","Package must be selected in CM request")
            return 1
        if instvhq == "":
            app.errorBox("error","VHQ Instance is not selected in CM request")
            return 1

        #base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        home = expanduser("~")
        base_path = ""
        if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
        elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
            base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
        elif os.path.isdir(home + "\\OneDrive - Verifone"):
            home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

        OTB_templates_path = base_path + "OTB Standard Builds\\" + devtype
        filetype = "*" + package[:2] + "*"
        #print(filetype)
        if (len(glob.glob(OTB_templates_path + '\\' + filetype))) > 1:
            app.errorBox("error", "Multiple config templates present in " + OTB_templates_path + " folder for package selection " + package + ". Please make sure only 1 is present")
        elif (len(glob.glob(OTB_templates_path + '\\' + filetype))) == 0:
            app.errorBox("error", "Please make sure config template is present in " + OTB_templates_path + " folder for " + package)
        elif (len(glob.glob(OTB_templates_path + '\\' + filetype))) == 1:
            fileList = os.listdir("extract")
            try:
                for fileName in fileList:
                    if os.path.isdir("extract\\" + fileName):
                        shutil.rmtree("extract\\" + fileName)
                    else:
                        os.remove("extract\\" + fileName)
            except:
                app.errorBox("error", "Error deleting extract\\" + fileName + ". Close this file if it's open")
                return 1

            otbconfigtemplatelist = os.listdir(OTB_templates_path)
            #print(otbconfigtemplatelist)
            for otbconfigtemplate in otbconfigtemplatelist:
                if package[:2] in otbconfigtemplate:
                    break
            #print(otbconfigtemplate)
            try:
                shutil.copyfile(OTB_templates_path + "\\" + otbconfigtemplate, "extract\\" + otbconfigtemplate)
                #print("file copied")
            except:
                app.errorBox("error", "Error copying " + OTB_templates_path + "\\" + otbconfigtemplate + " to extract\\" + otbconfigtemplate)
                return 1

            command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\Create_OTB_Build.sh" + " MX " + str(instvhq)

            if command != "":
                #print(command)
                FNULL = open(os.devnull, 'w')
                result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                if result.returncode != 0:
                    print(result.stderr)
                    print(result)
                    print("\nExecution of Create_OTB_Build.sh failed")
                    app.errorBox("error", "Error update package with VHQ instance")
                    return False
                else:
                    #print("Updated package is available @ extract folder")1
                    cmrepository_path = base_path + "CM Repository\\CM" + formnumber
                    # print(basefilespath.rsplit('/', 1)[-1])
                    try:
                        if os.path.isdir(cmrepository_path):
                            shutil.copyfile("extract\\" + otbconfigtemplate,
                                            cmrepository_path + "\\" + otbconfigtemplate)
                        else:
                            os.makedirs(cmrepository_path)
                            shutil.copyfile("extract\\" + otbconfigtemplate,
                                            cmrepository_path + "\\" + otbconfigtemplate)
                    except:
                        app.errorBox("error", "Error copying package extract\\" + otbconfigtemplate + " to " + cmrepository_path)
                        return 1

                    app.infoBox("info", "Updated package is available @ " + cmrepository_path)


def processpackagecreation(btn):
    if app.getEntry(clintname) == "":
        app.errorBox("error", "Please enter Client Name")
        return 1
    formnumber = str(int(app.getEntry(packagecreationcmform)))
    if int(formnumber) < 1:
        app.errorBox("error", "Please enter correct CM Request Number")
    else:
        clintid = read_access_db.getid_addclientname(app.getEntry(clintname))
        if (clintid == -1):
            app.errorBox("error", "Client Name doesn't exist in Sharepoint")
            return 1
        message = ""
        forminfo = read_cmitems.read_cmitems()
        if not forminfo.kickstart_cmitems_processing_gui(formnumber):
            app.errorBox("error", "Form " + formnumber + " doesn't exist in Sharepoint")
            return 1
        forminfo.create_dict()
        clientidfromform = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Client Name')])
        #print(str(clientidfromform) + ":" + str(clintid))
        if clientidfromform != clintid:
            app.errorBox("error", "Client Name entered (" + app.getEntry(clintname)  + ") does not match with Client Name in CM request")
            return 1
        #print(forminfo.dict)
        device = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Device Model')])
        devicemodel = device
        prodtypp = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product Type')])
        envv = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Environment')])
        scaversion = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product version')])

        #print(scaversion)

        if prodtypp != "Cybersource Basic":
            app.errorBox("error", "Product type for CM request is NOT 'Cybersource Basic'... Exiting")
            return 1

        devmod = ""
        if device.find("MX915") != -1:
            device = "MX"
            devmod = "MX915"
        elif device.find("MX925") != -1:
            device = "MX"
            devmod = "MX925"
        elif device.find("MX") != -1 and device.find("MX915") == -1 and device.find("MX925") == -1:
            device = "MX"
            devmod = devicemodel
        elif device.find("VX") != -1:
            device = "VX"
            devmod = devicemodel
        else:
            device = "Mobile"
            devmod = devicemodel

        usetemplate = ""
        if app.getRadioButton(devprodgen) == "Use Lab Template ":
            usetemplate = "Lab"
        elif app.getRadioButton(devprodgen) == "Use Prod Template ":
            usetemplate = "Production"


        patherrormsg = ""
        #base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        home = expanduser("~")
        base_path = ""
        if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
        elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
            base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
        elif os.path.isdir(home + "\\OneDrive - Verifone"):
            home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        basefilespath = ""
        specific_template = False
        if usetemplate == "Lab" or usetemplate == "Production":
            #basefilespath = base_path + "Cybersource BaseFiles\\" + device + "\\" + usetemplate + "\\" + devmod
            if device == "MX":
                basefilespath = base_path + "Cybersource BaseFiles\\" + device + "\\Config\\" + devmod + "\\" + usetemplate
            else:
                basefilespath = base_path + "Cybersource BaseFiles\\" + device + "\\" + devmod + "\\" + usetemplate
            patherrormsg = "Config template path " + basefilespath + " doesn't exist"
        else:
            if app.getRadioButton(devprodgen) == "Use Golden Package from previous CM Requests":
                past_cm_request = int(app.getEntry("Please enter past CM Request number from above list to grab config package from"))
                #print("Past request entered : " + str(past_cm_request))
                if past_cm_request == 0:
                    app.errorBox("error", "Please enter past CM Request Number")
                    return 1
                global prev_requests
                if past_cm_request not in prev_requests:
                    li = ""
                    for i in prev_requests:
                        if str(i) != str(formnumber):
                            li += str(i) + ", "
                    li = li[:-2]
                    app.errorBox("error", "Please enter Past CM Request number from this list : ( " + li + " )")
                    return 1

                basefilespath = base_path + "CM Repository\\CM" + str(past_cm_request)
                if glob.glob(basefilespath):
                    patherrormsg = basefilespath + " found"
                else:
                    patherrormsg = basefilespath + " not found"
            elif app.getRadioButton(devprodgen) == "Select specific Template":
                global cybersource_template_selected
                basefilespath = cybersource_template_selected
                specific_template = True

        if not glob.glob(basefilespath):
            app.errorBox("error", patherrormsg)
        else:
            filetype = ""
            if usetemplate == "Dev" or usetemplate == "Prod":
                filetype = "*.tgz"
            else:
                if app.getRadioButton(devprodgen) == "Use Golden Package from previous CM Requests":
                    filetype = "*.tgz"

            fileList = os.listdir("extract")
            try:
                for fileName in fileList:
                    if os.path.isdir("extract\\" + fileName):
                        shutil.rmtree("extract\\" + fileName)
                    else:
                        os.remove("extract\\" + fileName)
            except:
                app.errorBox("error", "Error deleting extract\\" + fileName + ". Close this file if it's open")
                return 1

            if specific_template == False:
                if (len(glob.glob(basefilespath + '\\' + filetype))) > 1:
                    app.errorBox("error", "Multiple config templates present in " + basefilespath + " folder. Please make sure only 1 is present")
                    return 1
                elif (len(glob.glob(basefilespath + '\\' + filetype))) == 0:
                    app.errorBox("error", "Please make sure config template is present in " + basefilespath + " folder")
                    return 1
                elif (len(glob.glob(basefilespath + '\\' + filetype))) == 1:
                    configtemplatename = os.listdir(basefilespath)
                    #print(configtemplatename)
                    try:
                        shutil.copyfile(basefilespath + "\\" + configtemplatename[0], "extract\\" + configtemplatename[0])
                    except:
                        app.errorBox("error", "Error copying " + basefilespath + "\\" + configtemplatename[0] + " to extract\\" + configtemplatename[0])
                        return 1
            else:
                configtemplatename = os.path.basename(basefilespath)
                # print(configtemplatename)
                try:
                    shutil.copyfile(basefilespath, "extract\\" + configtemplatename)
                except:
                    app.errorBox("error", "Error copying " + basefilespath + " to extract\\" + configtemplatename)
                    return 1


            '''
            if app.getCheckBox("Insert EMV Templates "):
                succ = emv.insert_emv("gui", formnumber)
                if succ == 0:
                    #app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
                    message = message + "EMV templates inserted into package\n"
                elif succ == 1:
                    app.errorBox("error", "Execution of emv.sh failed")
                elif succ == 2:
                    app.errorBox("error", "EMV template package Path doesn\'t exist. Please create this path and place template EMV tgz")
                elif succ == 3:
                    app.errorBox("error", "Applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize")
                elif succ == 4:
                    app.errorBox("error", "Applicable for valid processor only")
                elif succ == 5:
                    app.errorBox("error", "Applicable for MX or VX devices only")
                elif succ == 6:
                    app.errorBox("error", "Applicable either for Auto Credit or Auto Debit or Customer Choice options")
                elif succ == 7:
                    app.errorBox("error", "Applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize AND EMV as Yes")
                elif succ == 8:
                    app.errorBox("info", "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
                elif succ == 20:
                    app.errorBox("error", "Multiple config packages present in extract folder. Please make sure only 1 is present")
                elif succ == 21:
                    app.errorBox("error", "Please keep the package in extract folder")
                elif succ == 22:
                    app.errorBox("error", "Please follow naming convention for package name in extract folder")

            if app.getCheckBox("Insert Media Templates "):
                succ = Add_Media_Files.insert_sample_media_gui(formnumber)
                if succ == 0:
                    #app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
                    message = message + "Media templates inserted into package\n"
                elif succ == 1:
                    app.errorBox("error", "Execution of media.sh failed")
                elif succ == 2:
                    app.errorBox("error", "Applicable for MX915/MX925 only")
                elif succ == 3:
                    app.errorBox("info", "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
                elif succ == 20:
                    app.errorBox("error", "Multiple config packages present in extract folder. Please make sure only 1 is present")
                elif succ == 21:
                    app.errorBox("error", "Please keep the package in extract folder")
                elif succ == 22:
                    app.errorBox("error", "Please follow naming convention for package name in extract folder")

            if app.getCheckBox(updateconfigusrtopackage) or app.getCheckBox(updatevhqtopackage):
                configuser1 = ConfigUsr1.config_usr1(formnumber)
                succ = configuser1.kickstart_config_usr1_processing()
                if succ == 0:
                    scriptt = "scripts\\updatepackage_with_configusr1_from_form.sh"
                    updatewhichfile = ""
                    if app.getCheckBox(updateconfigusrtopackage) and os.path.isfile("extract\\configusr1updatepackage"):
                        updatewhichfile = updatewhichfile + " config"

                    if app.getCheckBox(updatevhqtopackage) and os.path.isfile("extract\\vhqconfigupdatepackage"):
                        updatewhichfile = updatewhichfile + " vhq"

                    if updatewhichfile != "":
                        command = "\"" + read_conf.git_bash_path[0] + "\" " + scriptt + " " + device + updatewhichfile

                        if command != "":
                            #print(command)
                            FNULL = open(os.devnull, 'w')
                            result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                            if result.returncode != 0:
                                print(result.stderr)
                                print(result)
                                print("\nExecution of " + scriptt + " failed")
                                app.errorBox("error", "Error update package with config.usr1 parameters from CM Request")
                                return 1
                            else:
                                # app.infoBox("info", "Updated package is available @ extract folder")
                                filenames = ""
                                if "config" in updatewhichfile:
                                    filenames = "config.usr1 "
                                if "vhq" in updatewhichfile:
                                    filenames = filenames + "VHQ "
                                message = message + filenames + " parameters from CM Request are updated in package\n"
                        if os.path.isfile("extract\\configusr1updatepackage"):
                            os.remove("extract\\configusr1updatepackage")
                        if os.path.isfile("extract\\vhqconfigupdatepackage"):
                            os.remove("extract\\vhqconfigupdatepackage")
                        shutil.rmtree(formnumber)
                    else:
                        if app.getCheckBox(updateconfigusrtopackage) and not os.path.isfile("extract\\configusr1updatepackage"):
                            app.errorBox("error", "extract\\configusr1updatepackage file not found")
                            return 1
                        if app.getCheckBox(updatevhqtopackage) and not os.path.isfile("extract\\vhqconfigupdatepackage"):
                            app.errorBox("error", "extract\\vhqconfigupdatepackage file not found")
                            return 1
                    #configusrpath = "config.usr1 and vhqconfig.ini files are created at :  " + os.getcwd() + "\\" + formnumber
                    #app.infoBox("info", configusrpath)
                elif succ == 1:
                    errormsg = "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number"
                    app.errorBox("info", errormsg)
                elif succ == 2:
                    errormsg = "Program supports SCA Point Classic / Passthrough and Enterprise. CM Request # entered is for other than these product type"
                    app.errorBox("info", errormsg)
                elif succ == 3:
                    errormsg = "Program supports for New clients which means CM request with \'Type of request\' parameter as \'Initial configuration\'. Exiting ..."
                    app.errorBox("info", errormsg)
                elif succ == 4:
                    errormsg = "Error creating config.usr1 file"
                    app.errorBox("info", errormsg)
                elif succ == 5:
                    errormsg = "Record info couldn't be pulled from CM Portal due to Invalid characters in Form " + formnumber
                    app.errorBox("info", errormsg)
                elif succ == 9:
                    errormsg = "config.usr1 is created @ " + os.getcwd() + "\\" + formnumber + " but vhqconfig.ini couldn't be created"
                    app.errorBox("info", errormsg)
                elif succ == 10:
                    errormsg = "vhqconfig.ini is created @ " + os.getcwd() + "\\" + formnumber + " but config.usr1 couldn't be created"
                    app.errorBox("info", errormsg)
            '''

            sigenableparam = ""
            signaturelimitparam = ""
            safenableparam = ""
            transfloorlimitparam = ""
            totalfllimitparam = ""
            dayslimitsparam = ""
            lineitemdispparam = ""
            vhqinstanceparam = ""
            addimageYN = ""
            signaturelimit = 0
            transfloorlimit = 0
            totalfllimit = 0
            dayslimits = 0

            sigenable = False
            safenable = False
            lineitemdisp = False

            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Signature Capture (MX Only)')]) == "Yes":
                sigenable = True

            #signaturelimit = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Sig Limit-Visa')])
            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Sig Limit-Visa')]) != "":
                signaturelimit = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Sig Limit-Visa')])

            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('SAF Enabled')]) == "Yes":
                safenable = True

            #transfloorlimit = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Transaction Floor Limit')])
            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Transaction Floor Limit')]) != "":
                transfloorlimit = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Transaction Floor Limit')])

            #totalfllimit = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Total Floor Limit')])
            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Total Floor Limit')]) != "":
                totalfllimit = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Total Floor Limit')])

            #dayslimits = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Offline Days Limit')])
            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Offline Days Limit')]) != "":
                dayslimits = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Offline Days Limit')])

            if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Line Item Display')]) == "Yes":
                lineitemdisp = True

            vhqinstance = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('VHQ Company ID')])

            addimagetopackage = app.getCheckBox(addimage)
            cmrepository_path = ""

            if not sigenable and not safenable and not lineitemdisp and not addimagetopackage:
                pass
            else:
                if sigenable:
                    sigenableparam = "Y"
                else:
                    sigenableparam = "N"

                if signaturelimit > 0:
                    if device == "MX":
                        #signaturelimitparam = str(int(signaturelimit))
                        signaturelimitparam = str(round(signaturelimit, 2))
                        signaturelimitparam = "{:0.2f}".format(signaturelimit)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
                        #print(signaturelimitparam)
                else:
                    signaturelimitparam = "\"\""

                if safenable:
                    safenableparam = "Y"
                else:
                    safenableparam = "N"

                if transfloorlimit > 0:
                    transfloorlimitparam = str(int(transfloorlimit))
                else:
                    transfloorlimitparam = "\"\""

                if totalfllimit > 0:
                    totalfllimitparam = str(int(totalfllimit))
                else:
                    totalfllimitparam = "\"\""

                if dayslimits > 0:
                    dayslimitsparam = str(int(dayslimits))
                else:
                    dayslimitsparam = "\"\""

                if lineitemdisp:
                    lineitemdispparam = "Y"
                else:
                    lineitemdispparam = "N"

                if vhqinstance != "":
                    vhqinstanceparam = str(vhqinstance)
                else:
                    vhqinstanceparam = "\"\""

                if addimagetopackage and str(devicemodel).find("MX9") != -1:
                    addimageYN = "Y"
                elif addimagetopackage and str(devicemodel).find("MX9") == -1:
                    addimageYN = "N"
                    message = message + "Uploaded image is not added to package since CM Request Device model is not MX915 or MX925\n"
                else:
                    addimageYN = "N"

                command = ""
                if device == "MX" and addimageYN == "Y":
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " MX " + str(
                        sigenableparam) + " " + str(signaturelimitparam) + " " + str(safenableparam) + " " + str(
                        (transfloorlimitparam)) + " " + str((totalfllimitparam)) + " " + str(
                        (dayslimitsparam)) + " " + str(lineitemdispparam) + " " + str(vhqinstanceparam) + " " + str(
                        mxdevtype) + " \"" + image_file_path + "\""
                if device == "MX" and addimageYN == "N":
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " MX " + str(
                        sigenableparam) + " " + str(signaturelimitparam) + " " + str(safenableparam) + " " + str(
                        (transfloorlimitparam)) + " " + str((totalfllimitparam)) + " " + str(
                        (dayslimitsparam)) + " " + str(lineitemdispparam) + " " + str(vhqinstanceparam)
                elif device == "VX":
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " VX " + str(
                        safenableparam) + " " + str((transfloorlimitparam)) + " " + str(
                        (totalfllimitparam)) + " " + str((dayslimitsparam)) + " " + str(vhqinstanceparam)

                if command != "":
                    #print(command)
                    FNULL = open(os.devnull, 'w')
                    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                    if result.returncode != 0:
                        print(result.stderr)
                        print(result)
                        print("\nExecution of " + script + " failed")
                        app.errorBox("error", "Error creating update package")
                        return False
                    else:
                        message = message + "Cybersource specific options are updated in respective config file in package\n"
                        #print("Updated package is available @ extract folder")
                        #app.infoBox("info", "Updated package is available @ extract folder")
                        cmrepository_path = base_path + "CM Repository\\CM" + formnumber
                        # print(basefilespath.rsplit('/', 1)[-1])
                        if specific_template:
                            try:
                                destpath = ""
                                if "UNSIGNED" not in configtemplatename:
                                    destpath = cmrepository_path + "\\UNSIGNED_" + configtemplatename
                                else:
                                    destpath = cmrepository_path + "\\" + configtemplatename
                                if os.path.isdir(cmrepository_path):
                                    shutil.copyfile("extract\\" + configtemplatename, destpath)
                                else:
                                    os.makedirs(cmrepository_path)
                                    shutil.copyfile("extract\\" + configtemplatename, destpath)
                            except:
                                app.errorBox("error", "Error copying package extract\\" + configtemplatename + " to " + cmrepository_path)
                                return 1
                        else:
                            try:
                                destpath = ""
                                if "UNSIGNED" not in configtemplatename[0]:
                                    destpath = cmrepository_path + "\\UNSIGNED_" + configtemplatename[0]
                                else:
                                    destpath = cmrepository_path + "\\" + configtemplatename[0]
                                if os.path.isdir(cmrepository_path):
                                    shutil.copyfile("extract\\" + configtemplatename[0], destpath)
                                else:
                                    os.makedirs(cmrepository_path)
                                    shutil.copyfile("extract\\" + configtemplatename[0], destpath)
                            except:
                                app.errorBox("error", "Error copying package extract\\" + configtemplatename[0] + " to " + cmrepository_path)
                                return 1

            message = message + "\nPlease find updated package @ " + cmrepository_path
            app.infoBox("info", message)
            return True


def processinitialconfiguration(btn):
    logfile = open(log, 'w')
    logfile.write(time.strftime("%m/%d/%Y - %H:%M:%S"))
    if app.getEntry(iniclintname) == "":
        app.errorBox("error", "Please enter Client Name")
        return 1
    formnumber = str(int(app.getEntry(inicreationcmform)))
    global config_template_selected

    if int(formnumber) < 1:
        app.errorBox("error", "Please enter correct CM Request Number")
    else:
        clintid = read_access_db.getid_addclientname(app.getEntry(iniclintname))
        if (clintid == -1):
            app.errorBox("error", "Client Name doesn't exist in Sharepoint")
            return 1
        message = ""
        forminfo = read_cmitems.read_cmitems()
        if not forminfo.kickstart_cmitems_processing_gui(formnumber):
            app.errorBox("error", "Form " + formnumber + " doesn't exist in Sharepoint")
            return 1
        forminfo.create_dict()
        clientidfromform = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Client Name')])
        #print(str(forminfo.dict))
        if clientidfromform != clintid:
            app.errorBox("error", "Client Name entered (" + app.getEntry(iniclintname) + ") does not match with Client Name in CM request")
            return 1
        # print(forminfo.dict)

        producttype = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product Type')])
        if "SCA" not in producttype:
            app.errorBox("error", "CM Request " + str(formnumber) + " is for Non SCA Product type. Exiting ...")
            logfile = open(log, 'a')
            logfile.write("\n\nCM Request " + str(formnumber) + " is for Non SCA Product type. Exiting ...")
            logfile.close()
            return 1

        cust_sur_revision = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Click to view Customer Survey selected')])
        cust_sur_revision = cust_sur_revision.split("ID=",1)[1]
        if cust_sur_revision == "False#":
            app.errorBox("error", "Customer Survey specified in form " + formnumber + " doesn't exist. Exiting...")
            return 1
        cust_sur_revision = cust_sur_revision.replace('#', '')
        #print(cust_sur_revision)

        if forminfo.create_cust_dict(cust_sur_revision) == False:
            app.errorBox("error", "Customer Survey specified in form " + formnumber + " doesn't exist. Exiting...")
            return 1

        logfile.write("\n\nCM Ticket Number - " + str(formnumber))
        logfile.close()

        # if not app.getCheckBox("Insert EMV Templates ") and not app.getCheckBox(updateconfigusrtopackage) and not app.getCheckBox(updatevhqtopackage) and not app.getCheckBox(updatecdttopackage) and not app.getCheckBox(updateaidlisttopackage) and not app.getCheckBox(updateemvtablestopackage) and not app.getCheckBox(updatcpuuserpwdtopackage) and not app.getCheckBox(enablepinbypass):
        if not app.getCheckBox("Insert EMV Templates ") and not app.getCheckBox(
                updateconfigusrtopackage) and not app.getCheckBox(
            updatevhqtopackage) and not app.getCheckBox(updatecdttopackage) and not app.getCheckBox(
            updateaidlisttopackage) and not app.getCheckBox(
            updatcpuuserpwdtopackage) and not app.getCheckBox(enablepinbypass) and not app.getCheckBox(remove_configusr1_default) and not app.getCheckBox(convertxmlcfg):
            app.infoBox("info", "Package is not touched since none of the options selected")
            return True

        #print(forminfo.cust_dict)
        processor = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Credit/Debit/EBT Processors')])
        if "Chase" in processor:
            processor = "Chase"
        elif "First Data" in processor:
            processor = "FD"
        elif "Global" in processor:
            processor = "Global"
        elif "Vantiv" in processor:
            processor = "Vantiv"

        prodtypp = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product Type')])
        prdtp = ""
        if "Classic" in prodtypp:
            prdtp = "Classic"
        elif "Enterprise" in prodtypp:
            prdtp = "Enterprise"
        device = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Device Model')])
        devicemodel = device
        dualaid = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('EMV Payment Preferred Type')])
        if dualaid == "Auto Credit":
            dualaid = "Autocredit"
        elif dualaid == "Auto Debit":
            dualaid = "Autodebit"
        elif dualaid == "Customer Choice":
            dualaid = "Customerchoice"

        envv = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Environment')])
        scaversion = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product version')])
        sigcapture = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Signature Capture (MX Only)')])
        siglimit = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Sig Limit-Visa')])
        siglimitamex = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Sig Limit-Amex')])
        siglimitmc = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Sig Limit-MC')])
        siglimitdiscover = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Sig Limit-Discover')])
        siglimitjcb = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Sig Limit-JCB')])
        siglimitdiners = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Sig Limit-Diners')])
        pinbypass = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('PIN Bypass')])
        capsignonpinbypass = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Capture Signature on PIN Bypass')])
        cashbackenabled = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Cashback by AID')])
        saflimitbycardtype = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('SAF Limits by Card Type')])
        emv_check = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('EMV')])
        processor_check = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Credit/Debit/EBT Processors')])


        if siglimit != "":
            siglimitparm = str(round(siglimit, 2))
            siglimitparm = "{:0.2f}".format(siglimit)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
        else:
            siglimitparm = "-" # If this parameter is blank in CM request then pass hyphen to script so it can ignroe it

        if siglimitamex != "":
            siglimitamexparm = str(round(siglimitamex, 2))
            siglimitamexparm = "{:0.2f}".format(siglimitamex)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
        else:
            siglimitamexparm = "-"

        if siglimitmc != "":
            siglimitmcparm = str(round(siglimitmc, 2))
            siglimitmcparm = "{:0.2f}".format(siglimitmc)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
        else:
            siglimitmcparm = "-"

        if siglimitdiscover != "":
            siglimitdiscoverparm = str(round(siglimitdiscover, 2))
            siglimitdiscoverparm = "{:0.2f}".format(siglimitdiscover)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
        else:
            siglimitdiscoverparm = "-"

        if siglimitjcb != "":
            siglimitjcbparm = str(round(siglimitjcb, 2))
            siglimitjcbparm = "{:0.2f}".format(siglimitjcb)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
        else:
            siglimitjcbparm = "-"

        if siglimitdiners != "":
            siglimitdinersparm = str(round(siglimitdiners, 2))
            siglimitdinersparm = "{:0.2f}".format(siglimitdiners)  # Convert Signature limit amount to 2 digits after decimal point as required by aidlist.ini file requirement
        else:
            siglimitdinersparm = "-"

        if saflimitbycardtype == "":
            saflimitbycardtype = "-" # If this parameter is blank in CM request then pass hyphen to script so it can ignroe it

        #print(scaversion)

        if prodtypp.find("SCA") == -1:
            app.errorBox("error", "CM request " + str(formnumber) + " is for " + prodtypp +".\nInitial configuration utility is applicable for SCA requests only")
            return 1

        devmod = ""
        if device.find("MX915") != -1:
            device = "MX"
            devmod = "MX915"
        elif device.find("MX925") != -1:
            device = "MX"
            devmod = "MX925"
        elif device.find("MX") != -1 and device.find("MX915") == -1 and device.find("MX925") == -1:
            device = "MX"
            devmod = devicemodel
        elif device.find("VX") != -1:
            device = "VX"
            devmod = devicemodel
        else:
            device = "Mobile"
            devmod = devicemodel

        usetemplate = ""
        if app.getRadioButton(inidevprodgen) == "Use Standard Config template from Sharepoint":
            usetemplate = "standard"
        elif app.getRadioButton(inidevprodgen) == "Select specific Template":
            usetemplate = "specific"
            #print(config_template_selected.rsplit('/', 1)[-1].lower())
            if ".zip" in config_template_selected.rsplit('/', 1)[-1].lower() and device == "MX":
                app.errorBox("error", "CM request " + str(formnumber) + " is for MX so please select tgz instead of " + config_template_selected.rsplit('/', 1)[-1])
                return 1

        patherrormsg = ""
        #base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        home = expanduser("~")
        base_path = ""
        if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
        elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
            base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
        elif os.path.isdir(home + "\\OneDrive - Verifone"):
            home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
            base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
        basefilespath = ""
        specific_template = False

        if usetemplate == "standard":
            # basefilespath = base_path + "Cybersource BaseFiles\\" + device + "\\" + usetemplate + "\\" + devmod
            basefilespath = base_path + "Cybersource BaseFiles\\" + device + "\\" + devmod + "\\" + usetemplate   # CHANGE PATH HERE ACCORDINGLY
            patherrormsg = "Config template path " + basefilespath + " doesn't exist"
        elif usetemplate == "specific":
            basefilespath = config_template_selected
            specific_template = True
            patherrormsg = "Specific Config template selected doesn't exist @ " + basefilespath
        elif app.getRadioButton(inidevprodgen) == "Use Golden Package from previous CM Requests":
            past_cm_request = int(app.getEntry("Please enter past CM Request number from above list to grab config package from "))
            #print("Past request entered : " + str(past_cm_request))
            if past_cm_request == 0:
                app.errorBox("error", "Please enter past CM Request Number")
                return 1
            global prev_requests
            if past_cm_request not in prev_requests:
                li = ""
                for i in prev_requests:
                    if str(i) != str(formnumber):
                        li += str(i) + ", "
                li = li[:-2]
                app.errorBox("error", "Please enter Past CM Request number from this list : ( " + li + " )")
                return 1

            basefilespath = base_path + "CM Repository\\CM" + str(past_cm_request)
            if glob.glob(basefilespath):
                patherrormsg = basefilespath + " found"
            else:
                patherrormsg = basefilespath + " not found"

        if not glob.glob(basefilespath):
            app.errorBox("error", patherrormsg)
        else:
            filetype = ""
            standardproceed = 0
            if usetemplate == "standard":
                filetype = ""
            elif app.getRadioButton(inidevprodgen) == "Use Golden Package from previous CM Requests":
                filetype = "*.tgz"

            fileList = os.listdir("extract")
            fileName = ""
            try:
                for fileName in fileList:
                    if os.path.isdir("extract\\" + fileName):
                        shutil.rmtree("extract\\" + fileName)
                    else:
                        os.remove("extract\\" + fileName)
            except:
                app.errorBox("error", "Error deleting extract\\" + fileName + ". Close this file if it's open")
                return 1

            configtemplatename = ""
            if specific_template == False:
                if (len(glob.glob(basefilespath + '\\' + filetype))) > 1:
                    app.errorBox("error", "Multiple config templates present in " + basefilespath + " folder. Please make sure only 1 is present")
                    return 1
                elif (len(glob.glob(basefilespath + '\\' + filetype))) == 0:
                    app.errorBox("error", "Please make sure config template is present in " + basefilespath + " folder")
                    return 1
                elif (len(glob.glob(basefilespath + '\\' + filetype))) == 1:
                    standardproceed = 1
                    configtemplatename = ""
                    configtemplatename = os.listdir(basefilespath)
                    # print(configtemplatename)
                    try:
                        shutil.copyfile(basefilespath + "\\" + configtemplatename[0], "extract\\" + configtemplatename[0])
                    except:
                        app.errorBox("error", "Error copying " + basefilespath + "\\" + configtemplatename[0] + " to extract\\" + configtemplatename[0])
                        return 1

            else:
                configtemplatename = os.path.basename(basefilespath)
                # print(configtemplatename)
                try:
                    shutil.copyfile(basefilespath, "extract\\" + configtemplatename)
                except:
                    app.errorBox("error", "Error copying " + basefilespath + " to extract\\" + configtemplatename)
                    return 1

            emvdone = False

            if standardproceed == 1 or usetemplate == "specific":
                cmrepository_path = base_path + "CM Repository\\CM" + formnumber
                if app.getCheckBox("Insert EMV Templates "):
                    logfile = open(log, 'a')
                    logfile.write("\n\n################### Insert EMV Templates ########################################################################\n")
                    logfile.close()
                    succ = emv.insert_emv("gui", formnumber, cust_sur_revision)
                    if succ == 0:
                        emvdone = True
                        # app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
                        if specific_template == False:
                            try:
                                destpath = ""
                                if "UNSIGNED" not in configtemplatename[0]:
                                    destpath = cmrepository_path + "\\UNSIGNED_" + configtemplatename[0]
                                else:
                                    destpath = cmrepository_path + "\\" + configtemplatename[0]
                                if os.path.isdir(cmrepository_path):
                                    shutil.copyfile("extract\\" + configtemplatename[0],
                                                    destpath)
                                else:
                                    os.makedirs(cmrepository_path)
                                    shutil.copyfile("extract\\" + configtemplatename[0],
                                                    destpath)
                            except:
                                app.errorBox("error", "Error copying package extract\\" + configtemplatename[
                                    0] + " to " + cmrepository_path)
                                return 1
                        else:
                            try:
                                destpath = ""
                                if "UNSIGNED" not in os.path.basename(basefilespath):
                                    destpath = cmrepository_path + "\\UNSIGNED_" + os.path.basename(basefilespath)
                                else:
                                    destpath = cmrepository_path + "\\" + os.path.basename(basefilespath)
                                if os.path.isdir(cmrepository_path):
                                    shutil.copyfile("extract\\" + os.path.basename(basefilespath),
                                                    destpath)
                                else:
                                    os.makedirs(cmrepository_path)
                                    shutil.copyfile("extract\\" + os.path.basename(basefilespath),
                                                    destpath)
                            except:
                                app.errorBox("error", "Error copying package extract\\" + os.path.basename(
                                    basefilespath) + " to " + cmrepository_path)
                                return 1

                        message = message + "EMV template from " + home + "\\North America Config Manageme - Doc\\EMV\\" + processor + "\\" + prdtp + "\\" + device + "\\" + dualaid + " is inserted into package"
                        logfile = open(log, 'a')
                        logfile.write("\n    EMV template from " + home + "\\North America Config Manageme - Doc\\EMV\\" + processor + "\\" + prdtp + "\\" + device + "\\" + dualaid + " is inserted into package")
                        logfile.write("\n\n    Please note that if there was any existing EMV package then its replaced by above template\n")
                        logfile.close()
                    elif succ == 1:
                        message = message + "Execution of emv.sh failed"
                        logfile = open(log, 'a')
                        logfile.write("Execution of emv.sh failed")
                        logfile.close()
                    elif succ == 2:
                        message = message + "EMV template package Path doesn\'t exist. Please create this path and place template EMV tgz"
                        logfile = open(log, 'a')
                        logfile.write("EMV template package Path doesn\'t exist. Please create this path and place template EMV tgz")
                        logfile.close()
                    elif succ == 3:
                        message = message + "EMV insertion applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize"
                        logfile = open(log, 'a')
                        logfile.write("EMV insertion applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize")
                        logfile.close()
                    elif succ == 4:
                        message = message + "EMV insertion applicable for valid processor only"
                        logfile = open(log, 'a')
                        logfile.write("EMV insertion applicable for valid processor only")
                        logfile.close()
                    elif succ == 5:
                        message = message + "EMV insertion applicable for MX or VX devices only"
                        logfile = open(log, 'a')
                        logfile.write("EMV insertion applicable for MX or VX devices only")
                        logfile.close()
                    elif succ == 6:
                        message = message + "EMV insertion applicable either for Auto Credit or Auto Debit or Customer Choice options"
                        logfile = open(log, 'a')
                        logfile.write("EMV insertion applicable either for Auto Credit or Auto Debit or Customer Choice options")
                        logfile.close()
                    elif succ == 7:
                        message = message + "EMV insertion applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize AND EMV as Yes. CM Request " + str(formnumber) + " has EMV set to No or blank"
                        logfile = open(log, 'a')
                        logfile.write("EMV insertion applicable for SCA Point Classic or SCA Point Classic Passthrough or SCA Point Enterprize AND EMV as Yes. CM Request " + str(formnumber) + " has EMV set to No or blank")
                        logfile.close()
                    elif succ == 8:
                        app.errorBox("info", "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
                        logfile = open(log, 'a')
                        logfile.write("Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
                        logfile.close()
                    elif succ == 20:
                        app.errorBox("error", "Multiple config packages present in extract folder. Please make sure only 1 is present")
                        logfile = open(log, 'a')
                        logfile.write("Multiple config packages present in extract folder. Please make sure only 1 is present")
                        logfile.close()
                    elif succ == 21:
                        app.errorBox("error", "Please keep the package in extract folder")
                        logfile = open(log, 'a')
                        logfile.write("Please keep the package in extract folder")
                        logfile.close()
                    elif succ == 22:
                        app.errorBox("error","Please follow naming convention for package name in extract folder")
                        logfile = open(log, 'a')
                        logfile.write("Please follow naming convention for package name in extract folder")
                        logfile.close()
                    elif succ == 25:
                        message = message + "EMV Template not present in Sharepoint for Processor/Product Type/Device/dual aid option chosen in CM request " + str(formnumber)
                        logfile = open(log, 'a')
                        logfile.write("EMV Template not present in Sharepoint for Processor/Product Type/Device/dual aid option chosen in CM request " + str(formnumber))
                        logfile.close()
                    elif succ == 26:
                        app.errorBox("error", "Customer Survey specified in form " + formnumber + " doesn't exist. Exiting...")
                        logfile = open(log, 'a')
                        logfile.write("Customer Survey specified in form " + formnumber + " doesn't exist. Exiting...")
                        logfile.close()

                '''
                if app.getCheckBox("Insert Media Templates "):
                    succ = Add_Media_Files.insert_sample_media_gui(formnumber)
                    if succ == 0:
                        #app.infoBox("info", "Process completed. Please check updated tgz file @ extract folder")
                        message = message + "Media templates inserted into package\n"
                    elif succ == 1:
                        app.errorBox("error", "Execution of media.sh failed")
                    elif succ == 2:
                        app.errorBox("error", "Applicable for MX915/MX925 only")
                    elif succ == 3:
                        app.errorBox("info", "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number")
                    elif succ == 20:
                        app.errorBox("error", "Multiple config packages present in extract folder. Please make sure only 1 is present")
                    elif succ == 21:
                        app.errorBox("error", "Please keep the package in extract folder")
                    elif succ == 22:
                        app.errorBox("error", "Please follow naming convention for package name in extract folder")
                '''

                #if app.getCheckBox(updateconfigusrtopackage) or app.getCheckBox(enablepinbypass) or app.getCheckBox(updatevhqtopackage) or app.getCheckBox(updatecdttopackage) or app.getCheckBox(updateaidlisttopackage) or app.getCheckBox(updateemvtablestopackage) or app.getCheckBox(updatcpuuserpwdtopackage):
                if app.getCheckBox(updateconfigusrtopackage) or app.getCheckBox(enablepinbypass) or app.getCheckBox(
                            updatevhqtopackage) or app.getCheckBox(updatecdttopackage) or app.getCheckBox(
                            updateaidlisttopackage) or app.getCheckBox(updatcpuuserpwdtopackage) or app.getCheckBox(remove_configusr1_default) or app.getCheckBox(convertxmlcfg):
                    scriptt = "scripts\\updatepackage_with_configusr1_from_form.sh"
                    updatewhichfile = ""

                    cfgusr1present = 0
                    if app.getCheckBox(convertxmlcfg):
                        logfile = open(log, 'a')
                        logfile.write("\n\n################### Convert XmlCfgFile.xml to config.usr1 / AidList / cdt #######################################\n")
                        logfile.close()
                        command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\extract_xmlcfg.sh " + device
                        FNULL = open(os.devnull, 'w')
                        result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                        if result.returncode != 0:
                            print(result.stderr)
                            print(result)
                            app.errorBox("error", "Execution of extract_xmlcfg.sh failed")

                        Upgrade_config_files.read_xmlconfig_mapping()
                        #print("\nParameters mapping in Sharepoint : " + str(Upgrade_config_files.xmlconfig_mapping_dict))
                        parameters = " aidlist"
                        if os.path.exists("extract\\config.usr1"):
                            cfgusr1present = 1
                        cdtpresent = 0
                        if os.path.exists("extract\\cdt.txt") or os.path.exists("extract\\cdt.ini"):
                            cdtpresent = 1
                        aidpresent = 0
                        if os.path.exists("extract\\aidlist.txt") or os.path.exists("extract\\aidlist.ini"):
                            aidpresent = 1
                        if Upgrade_config_files.read_xmlconfig_file("extract\\xmlCfgFile.xml", device):
                            #print("\nParameters in xmlConfigFile.xml : " + str(Upgrade_config_files.xmlconfig_file_dict))
                            #print("\nCorresponding Config Usr1 parameters for parameters in xmlConfigFile.xml : " + str(Upgrade_config_files.configusr1_dict))
                            Upgrade_config_files.create_update_ConfigUsr1()
                            logfile = open(log, 'a')
                            xmlconvertconfigusr1 = ""
                            for i in Upgrade_config_files.configusr1_dict:
                                xmlconvertconfigusr1 = xmlconvertconfigusr1 + "        " + i + ":" + str(Upgrade_config_files.configusr1_dict[i]) + "\n"
                            logfile.write("\n    Below parameters are converted from xmlCfgFile.xml to config.usr1\n" + xmlconvertconfigusr1)
                            logfile.close()

                            #if Upgrade_config_files.missing_xmlconfig_mapping_dict:
                                #message = message + "\n\nxml Mapping is not present for these parameters in CM Portal page : " + str(Upgrade_config_files.missing_xmlconfig_mapping_dict)
                                #print("\nMapping not present for parameters : " + str(Upgrade_config_files.missing_xmlconfig_mapping_dict))
                            #if Upgrade_config_files.missing_configusr1_dict:
                            #    print("\nMapping for either Config user1 parameter name or Section name is missing for parameters : " + str(Upgrade_config_files.missing_configusr1_dict))
                            if Upgrade_config_files.aidlist_dict:
                                #print("\naidList parameters to be updated : " + str(Upgrade_config_files.aidlist_dict))
                                Upgrade_config_files.create_update_aidList()
                                for aidparam in Upgrade_config_files.aidlist_dict:
                                    parameters = parameters + " " + aidparam + " " + Upgrade_config_files.aidlist_dict[aidparam]
                                # print(Upgrade_config_files.package_update_arguments)
                            if Upgrade_config_files.cdt_dict:
                                parameters = parameters + " cdt"
                                #print("\ncdt parameters to be updated : " + str(Upgrade_config_files.cdt_dict))

                        command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\convert_xmlcfg.sh " + device + parameters #If config.usr1/aidlist.ini/cdt.ini/cdt.txt are present in package then those get updated else not
                        FNULL = open(os.devnull, 'w')
                        result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                        if result.returncode != 0:
                            print(result.stderr)
                            print(result)
                            app.errorBox("error", "Execution of extract_xmlcfg.sh failed")
                            return 1

                        if "SLIMIT" in parameters and aidpresent == 1:
                            logfile = open(log, 'a')
                            logfile.write("\n    Signature Limits are updated from xmlCfgFile.xml to aidList\n")
                            logfile.close()
                        if "cdt" in parameters and cdtpresent == 1:
                            logfile = open(log, 'a')
                            logfile.write("\n    Signature Limits are updated from xmlCfgFile.xml to cdt\n")
                            logfile.close()

                        #Remove files which were extracted from package except for config.usr1 which is created from XmlCfgFile.xml
                        if os.path.exists("extract\\xmlCfgFile.xml"):
                            os.remove("extract\\xmlCfgFile.xml")
                        if os.path.exists("extract\\aidlist.txt"):
                            os.remove("extract\\aidlist.txt")
                        if os.path.exists("extract\\aidlist.ini"):
                            os.remove("extract\\aidlist.ini")
                        if os.path.exists("extract\\cdt.ini"):
                            os.remove("extract\\cdt.ini")
                        if os.path.exists("extract\\cdt.txt"):
                            os.remove("extract\\cdt.txt")
                        if os.path.exists("extract\\configusr1updatepackage"):
                            os.remove("extract\\configusr1updatepackage")
                        if os.path.exists("extract\\config.usr1") and cfgusr1present == 1:
                            os.remove("extract\\config.usr1")

                        message = message + "\n\nxmlCfgFile.xml conversion :"
                        #app.getCheckBox(remove_configusr1_default) and not app.getCheckBox(convertxmlcfg)
                        if app.getCheckBox(convertxmlcfg):
                            logfile = open(log, 'a')
                            if Upgrade_config_files.missing_xmlconfig_mapping_dict:
                                mapping_missing = ""
                                for i in (Upgrade_config_files.missing_xmlconfig_mapping_dict):
                                    if Upgrade_config_files.missing_xmlconfig_mapping_dict[i] == None:
                                        mapping_missing = mapping_missing + i + ", "
                                    else:
                                        mapping_missing = mapping_missing + i + ":" + Upgrade_config_files.missing_xmlconfig_mapping_dict[i] + ", "
                                mapping_missing = mapping_missing[:-2]
                                if cfgusr1present == 0:
                                    message = message + "\n   Since config.usr1 is not present in selected package, it's created by converting xmlCfgFile.xml and present in extract folder\n-- xmlCfgFile.xml parameters are updated in aidlist/cdt\n-- Following xmlCfgFile.xml parameters couldn not be converted for which xml Mapping is not present in CM Portal page : " + str(Upgrade_config_files.missing_xmlconfig_mapping_dict)
                                    logfile.write("\n    Since config.usr1 is not present in selected package, it's created by converting xmlCfgFile.xml and present in extract folder\n\n    Following xmlCfgFile.xml parameters couldn not be converted for which xml Mapping is not present in CM Portal page - " + mapping_missing)
                                else:
                                    message = message + "\n   xmlCfgFile.xml parameters are updated in config.usr1/aidlist/cdt except for these parameters for which xml Mapping is not present in CM Portal page : " + str(Upgrade_config_files.missing_xmlconfig_mapping_dict)
                                    logfile.write("\n    xmlCfgFile.xml parameters are updated in config.usr1/aidlist/cdt except for these parameters for which xml Mapping is not present in CM Portal page : " + mapping_missing)
                            else:
                                if cfgusr1present == 0:
                                    message = message + "\n   Since config.usr1 is not present in selected package, it's created by converting xmlCfgFile.xml and present in extract folder\nxmlCfgFile.xml parameters are updated in aidlist/cdt"
                                    logfile.write("\n    Since config.usr1 is not present in selected package, it's created by converting xmlCfgFile.xml and present in extract folder\n")
                                else:
                                    message = message + "\n   xmlCfgFile.xml parameters are updated in config.usr1/aidlist/cdt"
                                    logfile.write("\n    xmlCfgFile.xml parameters are updated in config.usr1/aidlist/cdt")
                            message = message + "\n-- Don't forget to remove xmlCfgFile.xml from package"
                            logfile.write("\n\n    Don't forget to remove xmlCfgFile.xml from package")
                            logfile.close()

                    if app.getCheckBox(updateconfigusrtopackage) or app.getCheckBox(updatevhqtopackage):
                        configuser1 = ConfigUsr1.config_usr1(formnumber)
                        succ = configuser1.kickstart_config_usr1_processing(cust_sur_revision)

                        formnumber_dir = os.getcwd() + '\\' + str(formnumber)

                        try:
                            if os.path.isdir(formnumber_dir):
                                shutil.rmtree(formnumber_dir)
                        except:
                            pass

                        if succ == 0:
                            if app.getCheckBox(updateconfigusrtopackage) and os.path.isfile("extract\\configusr1updatepackage"):
                                updatewhichfile = updatewhichfile + " config"

                            if app.getCheckBox(updateconfigusrtopackage):
                                if emv_check == "No":
                                    updatewhichfile = updatewhichfile + " EMVN"
                                    message = message + "\n\nSince CM Request is for Non FD and EMV is Yes so parameters ( Emvsetupreqd, EMVEnabled, contactlessEMVEnabled, clearexpiryin5f24tag ) are updated in config.usr1"
                                elif emv_check == "Yes":
                                    if "First Data" in processor_check:
                                        updatewhichfile = updatewhichfile + " EMV-FD"
                                        message = message + "\n\nSince CM Request is for Non FD and EMV is Yes so parameters ( Emvsetupreqd, EMVEnabled, contactlessEMVEnabled, clearexpiryin5f24tag ) are updated in config.usr1"
                                    else:
                                        updatewhichfile = updatewhichfile + " EMVFDNON"
                                        message = message + "\n\nSince CM Request is for Non FD and EMV is Yes so parameters ( Emvsetupreqd, EMVEnabled, contactlessEMVEnabled, clearexpiryin5f24tag ) are updated in config.usr1"

                            if app.getCheckBox(updatevhqtopackage) and os.path.isfile(
                                    "extract\\vhqconfigupdatepackage"):
                                updatewhichfile = updatewhichfile + " vhq"
                        elif succ == 1:
                            errormsg = "Form " + formnumber + " doesn't exist in Sharepoint. Please enter correct form number"
                            app.errorBox("info", errormsg)
                        elif succ == 2:
                            errormsg = "Program supports SCA Point Classic / Passthrough and Enterprise. CM Request # entered is for other than these product type"
                            app.errorBox("info", errormsg)
                        elif succ == 3:
                            errormsg = "Program supports for New clients which means CM request with \'Type of request\' parameter as \'Initial configuration\'. Exiting ..."
                            app.errorBox("info", errormsg)
                        elif succ == 4:
                            errormsg = "Error creating config.usr1 file"
                            app.errorBox("info", errormsg)
                        elif succ == 5:
                            errormsg = "Record info couldn't be pulled from CM Portal due to Invalid characters in Form " + formnumber
                            app.errorBox("info", errormsg)
                            return 1
                        elif succ == 9:
                            errormsg = "config.usr1 is created @ " + os.getcwd() + "\\" + formnumber + " but vhqconfig.ini couldn't be created"
                            app.errorBox("info", errormsg)
                        elif succ == 10:
                            errormsg = "vhqconfig.ini is created @ " + os.getcwd() + "\\" + formnumber + " but config.usr1 couldn't be created"
                            app.errorBox("info", errormsg)
                        elif succ == 11:
                            errormsg = "Customer Survey specified in form " + formnumber + " doesn't exist. Exiting..."
                            app.errorBox("info", errormsg)

                    if app.getCheckBox(updateaidlisttopackage):
                        #if sigcapture != "Yes":
                        #    message = message + "\n\nSignature capture in CM Request is not Yes so AidList.ini won't be updated"
                        if sigcapture == "Yes":
                            updatewhichfile = updatewhichfile + " aidlist " + str(siglimitparm) + " " + str(siglimitamexparm) + " " + str(siglimitmcparm) + " " + str(siglimitdiscoverparm) + " " + str(siglimitjcbparm) + " " + str(siglimitdinersparm)
                            #message = message + "\nSignature Limit updated in AidList as per CM Request"
                        if capsignonpinbypass == "Yes":
                            updatewhichfile = updatewhichfile + " 1"
                        elif capsignonpinbypass == "No":
                            updatewhichfile = updatewhichfile + " 0"
                        else:
                            updatewhichfile = updatewhichfile + " -"

                        if cashbackenabled == "Yes":
                            updatewhichfile = updatewhichfile + " 1"
                        elif cashbackenabled == "No":
                            updatewhichfile = updatewhichfile + " 0"
                        else:
                            updatewhichfile = updatewhichfile + " -"

                    if app.getCheckBox(updatecdttopackage):
                        if sigcapture != "Yes":
                            #message = message + "\nSignature capture in CM Request is not Yes so cdt.txt won't be updated"
                            updatewhichfile = updatewhichfile + " cdt -"
                        else:
                            if siglimit != "":
                                updatewhichfile = updatewhichfile + " cdt " + str(int(siglimit))
                            else:
                                updatewhichfile = updatewhichfile + " cdt -" # pass hyphen to script so it can ignroe it when sign limit is blank in CM request
                            #message = message + "\nSignature Limit updated in cdt as per CM Request"
                        if saflimitbycardtype != "-":
                            updatewhichfile = updatewhichfile + " " + str(int(saflimitbycardtype))
                        else:
                            updatewhichfile = updatewhichfile + " -"


                    '''
                    if app.getCheckBox(updateemvtablestopackage):
                        if pinbypass == "Yes":
                            updatewhichfile = updatewhichfile + " emvtables 1"
                        elif pinbypass == "No":
                            updatewhichfile = updatewhichfile + " emvtables 0"
                        else:
                            updatewhichfile = updatewhichfile + " emvtables -"
                        #message = message + "\nPinBypass updated in EMVTables as per CM Request"
                    '''

                    if app.getCheckBox(updatcpuuserpwdtopackage):
                        username = ""
                        password = ""
                        if (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Environment')]) != "Prod":
                            username = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Netconnect Username - Lab')])
                            password = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Netconnect Password - Lab')])
                        else:
                            username = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Netconnect Username - Prod')])
                            password = (list(forminfo.cust_dict.values())[list(forminfo.cust_dict.keys()).index('Netconnect Password - Prod')])
                        if username == "" or password == "":
                            message = message + "\n\nNetconnect Username or Netconnect Password is blank in CM Request " + str(formnumber) + ". So CPUsernameAndPassword.txt won't be updated"
                        else:
                            updatewhichfile = updatewhichfile + " netuserpwd " + username + " " + password

                    if app.getCheckBox(enablepinbypass):
                        if pinbypass == "Yes":
                            updatewhichfile = updatewhichfile + " pinbypass"
                            print("pinbypass checked")
                        else:
                            message = message + "\nSince Pin Bypass parameter in CM Request is NOT Yes, so related config files won't be updated to enable Pin Bypass"

                    type_of_request = ""
                    if app.getRadioButton("Type of request") == "Initial Configuration":
                        type_of_request = "initial"
                    elif app.getRadioButton("Type of request") == "Update Configuration":
                        type_of_request = "update"
                    #print(type_of_request)

                    if updatewhichfile != "":
                        #print(updatewhichfile)
                        command = "\"" + read_conf.git_bash_path[0] + "\" " + scriptt + " " + device + updatewhichfile + " " + type_of_request

                        if command != "":
                            #print(command)
                            FNULL = open(os.devnull, 'w')
                            result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                            if result.returncode != 0:
                                print(result.stderr)
                                print(result)
                                print("\nExecution of " + scriptt + " failed")
                                app.errorBox("error", "Error update package with config.usr1 parameters from CM Request")
                                return 1
                            else:
                                # app.infoBox("info", "Updated package is available @ extract folder")
                                filenames = ""
                                if "config" in updatewhichfile:
                                    filenames = "config.usr1, "
                                if "vhq" in updatewhichfile:
                                    filenames = filenames + "VHQ, "
                                if "pinbypass" in updatewhichfile:
                                    filenames = filenames + "EMVTables, OptFlag, aidList, "
                                if "netuserpwd" in updatewhichfile:
                                    filenames = filenames + "CPUsernameAndPassword.txt, "
                                if "aidlist" in updatewhichfile:
                                    filenames = filenames + "aidList, "
                                if "cdt" in updatewhichfile and "cdt -" not in updatewhichfile and "cdt  -" not in updatewhichfile:
                                    filenames = filenames + "cdt, "

                                filenames = ' '.join(unique_list(filenames.split())) # remove duplicate filenames from 'filenames' variable
                                filenames = filenames[:-1]

                                message = message + "\n\n" + filenames + " files are updated in package with parameters from CM Request"

                                '''
                                        if not app.getCheckBox("Insert EMV Templates ") and not app.getCheckBox(
                                                updateconfigusrtopackage) and not app.getCheckBox(
                                            updatevhqtopackage) and not app.getCheckBox(updatecdttopackage) and not app.getCheckBox(
                                            updateaidlisttopackage) and not app.getCheckBox(
                                            updatcpuuserpwdtopackage) and not app.getCheckBox(enablepinbypass) and not app.getCheckBox(remove_configusr1_default) and not app.getCheckBox(convertxmlcfg):

                                '''

                                if app.getCheckBox(updateconfigusrtopackage):
                                    logfile = open(log, 'a')
                                    if "config" in updatewhichfile:
                                        logfile.write("\n\n\n################### config.usr1 - Update parameters from CM request into package ################################")
                                        logfile.write("\n\nBelow parameters are updated / added in config.usr1 as per CM Request\n")
                                        configusrdictline = ""
                                        for x in configuser1.Config_User1_Dict:
                                            for y in configuser1.Config_User1_Dict[x]:
                                                valuee = configuser1.Config_User1_Dict[x][y]
                                                if isinstance(valuee, float):
                                                    valuee = int(valuee)
                                                if isinstance(valuee, str):
                                                    valuee = str(valuee)
                                                if valuee != "":
                                                    configusrdictline = configusrdictline + "    " + str(x) + " : " + str(y) + "=" + str(valuee) + "\n"
                                                else:
                                                    if y == "devtypesuffix":
                                                        configusrdictline = configusrdictline + "    " + str(x) + " : " + str(y) + "=" + str(valuee) + "\n"
                                        logfile.write(configusrdictline)
                                    logfile.close()
                                if app.getCheckBox(enablepinbypass):
                                    logfile = open(log, 'a')
                                    if "pinbypass" in updatewhichfile:
                                        logfile.write("\n\n################### Enable Pin Bypass ###########################################################################\n")
                                        logfile.write("\nBelow parameters are updated / added provided respective files are present in selected package")
                                        logfile.write("\n    config.usr1   - emv_pinbypass is set to 1")
                                        logfile.write("\n    EMVTables.ini - Pinbypass is set to 1 in all config sections")
                                        logfile.write("\n    OptFlag.ini   - USDebitFlag is set to 1")
                                        logfile.write("\n    aidList.txt / aidList.ini   - Payment Type parameter for VISA (AID - 980840) and MASTERCARD (AID - 42203) is set to 2\n")
                                    logfile.close()
                                if app.getCheckBox(updatevhqtopackage):
                                    logfile = open(log, 'a')
                                    if "vhq" in updatewhichfile and "initial" in type_of_request:
                                        logfile.write("\n\n################### VHQ - Update parameters from CM request into package ########################################\n")
                                        logfile.write("\nSince 'Initial Configuration' is selected in automation gui, vhqconfig.ini content is deleted and below parameters are added provided vhqconfig.ini is present in selected package")
                                        logfile.write("\n    Parameter value updated as per CM Ticket unless it's blank - CustomerId, HeartbeatFreq, MaintenanceStart, MaintenanceEnd, MaintenanceDays")
                                        logfile.write("\n    Parameter values added                               - KeysExchanged=False, url root=https://vhq.verifone.com/MessagingServer/MessageHandler.asmx, ssl validate hostname=TRUE")
                                    elif "vhq" in updatewhichfile and "update" in type_of_request:
                                        logfile.write("\n\n################### VHQ - Update parameters from CM request into package ########################################\n")
                                        logfile.write("\nSince 'Update Configuration' is selected in automation gui, below parameters are added/updated provided vhqconfig.ini is present in selected package")
                                        logfile.write("\n    Parameter value updated as per CM Ticket unless it's blank - CustomerId, HeartbeatFreq, MaintenanceStart, MaintenanceEnd, MaintenanceDays")
                                        logfile.write("\n    Parameter values added/updated                       - KeysExchanged=False, url root=https://vhq.verifone.com/MessagingServer/MessageHandler.asmx, ssl validate hostname=TRUE")
                                    logfile.close()
                                if app.getCheckBox(updatecdttopackage):
                                    logfile = open(log, 'a')
                                    if "cdt" in updatewhichfile:
                                        logfile.write("\n\n\n################### cdt - Update parameters from CM request into package ########################################\n")
                                        if ( sigcapture == "Yes" and siglimit != "" ) or saflimitbycardtype != "-":
                                            logfile.write("\nBelow parameters are updated/added provided cdt.txt / cdt.ini is present in selected package")
                                            if sigcapture == "Yes" and siglimit != "":
                                                logfile.write("\n    If cdt.txt is present in package")
                                                logfile.write("\n        Since Signature Capture is 'Yes' and Signature limit Visa is not blank in CM Ticket so 'SignLimitAmount' parameter values in cdt.txt are updated to " + str(int(siglimit)))
                                                logfile.write("\n    If cdt.ini is present in package")
                                                logfile.write("\n        Since Signature Capture is 'Yes' and Signature limit Visa is not blank in CM Ticket so 'SignatureLimitAmount' parameter values in cdt.ini are updated to " + str(int(siglimit)))
                                            if saflimitbycardtype != "-":
                                                logfile.write("\n        Since 'SAF Limits by card type' is not blank in CM Ticket so 'SAFLimit' parameter values in cdt.ini are updated to " + str(int(saflimitbycardtype)))
                                            else:
                                                logfile.write("\n        Since 'SAF Limits by card type' is blank in CM Ticket so 'SAFLimit' parameter values in cdt.ini are not changed")
                                        else:
                                            logfile.write("\ncdt.txt / cdt.ini not updated since 'Signature limit' and 'SAF Limits by Card type' fields are blank in CM Ticket")
                                    logfile.close()
                                if app.getCheckBox(updateaidlisttopackage):
                                    logfile = open(log, 'a')
                                    if "aidlist" in updatewhichfile:
                                        logfile.write("\n\n\n################### AidList.ini - Update parameters from CM request into package ################################\n")
                                        if sigcapture == "Yes":
                                            logfile.write("\n    Since Signature capture is Yes in CM Request so Signature limits as specified in CM Ticket are updated in aidlist.ini for respective processors. \n    For a particular processor, if signature limit is blank in CM ticket then it will be updated to Visa signature limit in aidlist.ini")
                                        if capsignonpinbypass == "Yes":
                                            logfile.write("\n    Since 'Capture Signature on Pin Bypass' field is Yes in CM Ticket so capsignonpinbypass parameter in aidlist.ini is set to 1")
                                        elif capsignonpinbypass == "No":
                                            logfile.write("\n    Since 'Capture Signature on Pin Bypass' field is No in CM Ticket so capsignonpinbypass parameter in aidlist.ini is set to 0")
                                        else:
                                            logfile.write("\n    Since 'Capture Signature on Pin Bypass' field is blank in CM Ticket so capsignonpinbypass parameter in aidlist.ini is not changed")
                                        if cashbackenabled == "Yes":
                                            logfile.write("\n    Since 'Cahsback by AID' field is Yes in CM Ticket so cashbackenabled parameter in aidlist.ini is set to 1")
                                        elif cashbackenabled == "No":
                                            logfile.write("\n    Since 'Cahsback by AID' field is No in CM Ticket so cashbackenabled parameter in aidlist.ini is set to 0")
                                        else:
                                            logfile.write("\n    Since 'Cahsback by AID' field is blank in CM Ticket so cashbackenabled parameter in aidlist.ini is not changed")
                                    logfile.close()
                                if app.getCheckBox(updatcpuuserpwdtopackage):
                                    logfile = open(log, 'a')
                                    if "netuserpwd" in updatewhichfile:
                                        logfile.write("\n\n\n################### CPUsernameAndPassword.txt - update parameters from CM Ticket into package ###################\n")
                                        logfile.write("\n    username and password parameters in CPUsernameAndPassword.txt are updated to Netconnect Username and Netconnect Password from CM Ticket")
                                    else:
                                        logfile.write("\n\n\n################### CPUsernameAndPassword.txt - update parameters from CM Ticket into package ###################\n")
                                        logfile.write("\n    Netconnect Username or Netconnect Password is blank in CM Request. So CPUsernameAndPassword.txt won't be changed")
                                    logfile.close()

                                #message = message + "\n\n" + filenames + " parameters from CM Request are updated in package\n\nAidlist, cdt, EMVtables files updated as per the selections made in GUI"
                        if os.path.isfile("extract\\configusr1updatepackage"):
                            os.remove("extract\\configusr1updatepackage")
                        if os.path.isfile("extract\\vhqconfigupdatepackage"):
                            os.remove("extract\\vhqconfigupdatepackage")
                        #shutil.rmtree(formnumber)
                    else:
                        if app.getCheckBox(updateconfigusrtopackage) and not os.path.isfile("extract\\configusr1updatepackage"):
                            app.errorBox("error", "extract\\configusr1updatepackage file not found")
                            return 1
                        if app.getCheckBox(updatevhqtopackage) and not os.path.isfile("extract\\vhqconfigupdatepackage"):
                            app.errorBox("error", "extract\\vhqconfigupdatepackage file not found")
                            return 1
                            # configusrpath = "config.usr1 and vhqconfig.ini files are created at :  " + os.getcwd() + "\\" + formnumber
                            # app.infoBox("info", configusrpath)

                    if app.getCheckBox(remove_configusr1_default):
                        command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\insert_extract.sh " + device + " extract config.usr1"
                        FNULL = open(os.devnull, 'w')
                        result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                        if result.returncode != 0:
                            print(result.stderr)
                            print(result)
                            app.errorBox("error", "Execution of insert_extract.sh failed")
                            return 1

                        del_default_succ = Upgrade_config_files.delete_default_params_configusr1(device)

                        if del_default_succ == 0:
                            command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\insert_extract.sh " + device + " insert config.usr1"
                            FNULL = open(os.devnull, 'w')
                            result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                            if result.returncode != 0:
                                print(result.stderr)
                                print(result)
                                app.errorBox("error", "Execution of insert_extract.sh failed")
                                return 1
                            logfile = open(log, 'a')
                            logfile.write("\n\n\n################### config.usr1 - Remove parameters set with default values #####################################\n")
                            logfile.write("\nBelow parameters which were set with default values or blank in config.usr1 in selected package are removed")
                            defvalues = ""
                            for i in Upgrade_config_files.configusr1_dict_default:
                                defvalues = defvalues + "\n    " + i + "=" +Upgrade_config_files.configusr1_dict_default[i]
                            logfile.write(defvalues)
                            logfile.close()
                            message = message + "\n\nParameters set with default values are removed from config.usr1 in package"
                        elif del_default_succ == 1:
                            message = message + "\n\nconfig.usr1 not found in package so parameters set to default could not be removed"
                        elif del_default_succ == 2:
                            message = message + "\n\nError opening config.usr1 so parameters set to default could not be removed"


                    cmrepository_path = base_path + "CM Repository\\CM" + formnumber
                    #print(basefilespath.rsplit('/', 1)[-1])

                    if specific_template == False:
                        try:
                            destpath = ""
                            if "UNSIGNED" not in configtemplatename[0]:
                                destpath = cmrepository_path + "\\UNSIGNED_" + configtemplatename[0]
                            else:
                                destpath = cmrepository_path + "\\" + configtemplatename[0]
                            if os.path.isdir(cmrepository_path):
                                shutil.copyfile("extract\\" + configtemplatename[0], destpath)
                            else:
                                os.makedirs(cmrepository_path)
                                shutil.copyfile("extract\\" + configtemplatename[0], destpath)
                        except:
                            app.errorBox("error", "Error copying package extract\\" + configtemplatename[0] + " to " + cmrepository_path)
                            return 1
                    else:
                        try:
                            destpath = ""
                            if "UNSIGNED" not in os.path.basename(basefilespath):
                                destpath = cmrepository_path + "\\UNSIGNED_" + os.path.basename(basefilespath)
                            else:
                                destpath = cmrepository_path + "\\" + os.path.basename(basefilespath)
                            if os.path.isdir(cmrepository_path):
                                shutil.copyfile("extract\\" + os.path.basename(basefilespath), destpath)
                            else:
                                os.makedirs(cmrepository_path)
                                shutil.copyfile("extract\\" + os.path.basename(basefilespath), destpath)
                        except:
                            app.errorBox("error", "Error copying package extract\\" + os.path.basename(basefilespath) + " to " + cmrepository_path)
                            return 1



                #if not emvdone and app.getCheckBox("Insert EMV Templates ") and not app.getCheckBox(updateconfigusrtopackage) and not app.getCheckBox(updatevhqtopackage) and not app.getCheckBox(updatecdttopackage) and not app.getCheckBox(updateaidlisttopackage) and not app.getCheckBox(updateemvtablestopackage) and not app.getCheckBox(updatcpuuserpwdtopackage) and not app.getCheckBox(enablepinbypass):
                if not emvdone and app.getCheckBox("Insert EMV Templates ") and not app.getCheckBox(
                        updateconfigusrtopackage) and not app.getCheckBox(
                        updatevhqtopackage) and not app.getCheckBox(updatecdttopackage) and not app.getCheckBox(
                        updateaidlisttopackage) and not app.getCheckBox(
                        updatcpuuserpwdtopackage) and not app.getCheckBox(enablepinbypass):
                    app.infoBox("info", message)
                    return True

                message_gui = ""
                if message != "":
                    if specific_template == False:
                        message = message + "\n\n-------------------------------------------------------------------------\nPlease find updated package @ " + cmrepository_path + "\\" + configtemplatename[0]
                        message_gui = "Please find updated package @ " + cmrepository_path + "\\" + configtemplatename[0]
                    else:
                        message = message + "\n\n-------------------------------------------------------------------------\nPlease find updated package @ " + cmrepository_path + "\\" + os.path.basename(basefilespath)
                        message_gui = "Please find updated package @ " + cmrepository_path + "\\" + os.path.basename(basefilespath)
                    message_gui = "\nPlease check detailed logs @ logs/CMAutomation.log\n-------------------------------------------------------------------------\n\n" + message_gui
                    message_gui = message_gui + "\n-------------------------------------------------------------------------\n\nMake sure to use 'Package manager tool' to change file permissions, Test and Sign before sending package to client\n\n"
                    app.infoBox("info", message_gui)
                    logfile = open(log, 'a')
                    logfile.write("\n\n\n#################################################################################################################\n\nMake sure to use 'Package manager tool' to change file permissions, Test and Sign before sending package to client\n\n")
                    logfile.close()
                    return True


def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist

def choosetemplate(btn):
    app.openTab("CMAutomationTabbedFrame", "Initial / Update Configuration")
    global config_template_selected
    if app.getRadioButton(inidevprodgen) == "Select specific Template":
        try:
            app.hideWidget(kind=1, name="ini_prev_requests")
            app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from ")
        except:
            pass
        root = Tk()
        root.withdraw()
        # image_file_path = filedialog.askopenfilename(defaultextension='.png', filetypes=[('Bin file', '*.png'), ('All files', '*.*')])
        config_template_selected = filedialog.askopenfilename(defaultextension='.tgz', filetypes=[
            ('Config Template', '*.tgz; *.zip'), ('All files', '*.*')])
        msg = "Config Template selected : " + config_template_selected

        try:
            app.addLabel("conftempselected", msg)
        except:
            app.setLabel("conftempselected", msg)
            app.showWidget(kind=1, name="conftempselected")
    elif app.getRadioButton(inidevprodgen) == "Use Golden Package from previous CM Requests":
        try:
            app.hideWidget(kind=1, name="conftempselected")
        except:
            pass
        if app.getEntry(iniclintname) == "":
            app.errorBox("error", "Please enter Client Name")
            return 1
        formnumber = str(int(app.getEntry(inicreationcmform)))
        if int(formnumber) < 1:
            app.errorBox("error", "Please enter correct CM Request Number")
        else:
            clintid = read_access_db.getid_addclientname(app.getEntry(iniclintname))
            if (clintid == -1):
                app.errorBox("error", "Client Name doesn't exist in Sharepoint")
                return 1
            message = ""
            forminfo = read_cmitems.read_cmitems()
            if not forminfo.kickstart_cmitems_processing_gui(formnumber):
                app.errorBox("error", "Form " + formnumber + " doesn't exist in Sharepoint")
                return 1
            forminfo.create_dict()
            clientidfromform = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Client Name')])
            #print(str(clientidfromform) + ":" + str(clintid))
            if clientidfromform != clintid:
                app.errorBox("error", "Client Name entered (" + app.getEntry(iniclintname)  + ") does not match with Client Name in CM request")
                return 1
            #print(forminfo.dict)
            device = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Device Model')])
            prodtypp = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product Type')])
            envv = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Environment')])
            #print(device + ":" + prodtypp + ":" + envv)
            #print(read_access_db.get_previous_CM_REquests(clintid, prodtypp, device, envv))
            global prev_requests
            prev_requests = read_access_db.get_previous_CM_REquests(clintid, prodtypp, device, envv)
            prev_requests.sort(reverse=True)
            prev_requests_message = ""
            #print(prev_requests)

            enable_prev_request = False

            if len(prev_requests) > 1:
                home = expanduser("~")
                base_path = ""
                if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
                    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
                elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
                    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
                elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
                    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
                elif os.path.isdir(home + "\\OneDrive - Verifone"):
                    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
                    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
                '''
                if not os.path.isdir(home + "\\SharePoint"):
                    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
                base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\CM Repository\\CM"
                '''

                prev_requests_message = prev_requests_message + "\nBelow is the list of past CM requests for \"" + app.getEntry(iniclintname) + " / " + prodtypp + " / " + device + " / " + envv + "\" combination.\n\n"
                for i in prev_requests:
                    if str(i) != str(formnumber):
                        prev_requests_message += "CM Request " + str(i) + " - "
                        basefilespath = base_path + str(i)
                        filetype = "*.tgz"
                        if (len(glob.glob(basefilespath + '\\' + filetype))) > 1:
                            prev_requests_message += "Multiple config packages exist in Sharepoint @ " + basefilespath + ". Please make sure only 1 is present.\n"
                        elif (len(glob.glob(basefilespath + '\\' + filetype))) == 0:
                            prev_requests_message += "Config package doesn't exist in Sharepoint @ " + basefilespath + "\n"
                        elif (len(glob.glob(basefilespath + '\\' + filetype))) == 1:
                            prev_requests_message += "Config package exists in Sharepoint @ " + basefilespath + "\n"
                            enable_prev_request = True

            else:
                prev_requests_message = prev_requests_message + "\nThere are no previous CM requests for " + app.getEntry(iniclintname) + " / " + prodtypp + " / " + device + " / " + envv + " combination.\n"
                prev_requests_message = prev_requests_message + "Please 'Select specific Template' option above to choose template from your Laptop or dropbox"

            app.showWidget(kind=1, name="ini_prev_requests")
            app.setLabel("ini_prev_requests", prev_requests_message)
            if enable_prev_request:
                app.showWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from ")
            else:
                prev_list = ""
                for i in prev_requests:
                    if str(i) != str(formnumber):
                        prev_list += str(i) + ", "
                prev_list = prev_list[:-2]
                app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from ")
                if len(prev_requests) > 1:
                    app.errorBox("error", "Golden package for past requests ( " + prev_list +  " ) doesn't exist in Sharepoint so you must choose 'Select specific Template' option\n\nNote: Past requests are searched based on Client/Product Type/Device/Environment combination. In this case, it is " + app.getEntry(iniclintname) + " / " + prodtypp + " / " + device + " / " + envv)
                else:
                    app.errorBox("error", "Golden package for past requests doesn't exist in Sharepoint so you must choose 'Select specific Template' option\n\nNote: Past requests are searched based on Client/Product Type/Device/Environment combination. In this case, it is " + app.getEntry(iniclintname) + " / " + prodtypp + " / " + device + " / " + envv)

    else:
        try:
            app.hideWidget(kind=1, name="conftempselected")
            app.hideWidget(kind=1, name="ini_prev_requests")
            app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from ")
        except:
            pass

def choosemediapackage(btn):
    app.openTab("CMAutomationTabbedFrame", "Verify Media")
    global config_template_selectedd
    if app.getCheckBox("Select Config tgz package to be verified"):
        root = Tk()
        root.withdraw()
        # image_file_path = filedialog.askopenfilename(defaultextension='.png', filetypes=[('Bin file', '*.png'), ('All files', '*.*')])
        config_template_selectedd = filedialog.askopenfilename(defaultextension='.tgz', filetypes=[('Config Template', '*.tgz;'), ('All files', '*.*')])
        msg = "Config Template selected : " + config_template_selectedd

        try:
            app.addLabel("conftempselectedd", msg)
        except:
            app.setLabel("conftempselectedd", msg)
            app.showWidget(kind=1, name="conftempselectedd")
    else:
        config_template_selectedd = ""
        try:
            app.hideWidget(kind=1, name="conftempselectedd")
        except:
            pass

def choosecdt(btn):
    app.openTab("CMAutomationTabbedFrame", "Upgrade")
    global cdt_selectedd
    if app.getCheckBox("Upgrade cdt.txt"):
        root = Tk()
        root.withdraw()
        cdt_selectedd = filedialog.askopenfilename(defaultextension='.tgz', filetypes=[('cdt.txt', 'cdt.txt;'), ('All files', '*.*')])
        msg = "cdt.txt file selected : " + cdt_selectedd

        try:
            app.addLabel("cdtselectedd", msg)
        except:
            app.setLabel("cdtselectedd", msg)
            app.showWidget(kind=1, name="cdtselectedd")
    else:
        try:
            app.hideWidget(kind=1, name="cdtselectedd")
        except:
            pass

def choosexmlcfg(btn):
    global xml_package_selected
    root = Tk()
    root.withdraw()
    xml_package_selected = filedialog.askopenfilename(defaultextension='.tgz', filetypes=[('Config Template', '*.tgz; *.zip'), ('All files', '*.*')])
    msg = "Config Package selected : " + xml_package_selected
    try:
        app.addLabel("xmltempselected", msg)
    except:
        app.setLabel("xmltempselected", msg)
        app.showWidget(kind=1, name="xmltempselected")

    if os.path.isdir("extract"):
        try:
            shutil.rmtree("extract")
        except:
            app.errorBox("error", "Error deleting files from extract folder")
            return 1

    if not os.path.isdir("extract"):
        try:
            os.mkdir("extract")
        except:
            app.errorBox("error", "Error creating files from extract folder")
            return 1

    try:
        shutil.copyfile(xml_package_selected, "extract\\" + xml_package_selected.rsplit('/', 1)[-1])
    except:
        app.errorBox("error", "Error Copying " + xml_package_selected + " to extract folder")
        return 1


def chooseaidlist(btn):
    app.openTab("CMAutomationTabbedFrame", "Upgrade")
    global aidlist_selectedd
    if app.getCheckBox("Upgrade Aidlist.txt"):
        root = Tk()
        root.withdraw()
        aidlist_selectedd = filedialog.askopenfilename(defaultextension='.tgz', filetypes=[('aidlist.txt', 'aidlist.txt;'), ('All files', '*.*')])
        msg = "Aidlist.txt file selected : " + aidlist_selectedd

        try:
            app.addLabel("aidlistselectedd", msg)
        except:
            app.setLabel("aidlistselectedd", msg)
            app.showWidget(kind=1, name="aidlistselectedd")
    else:
        try:
            app.hideWidget(kind=1, name="aidlistselectedd")
        except:
            pass

def convert(btn):
    devtype = "MX"
    command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\extract_xmlcfg.sh " + devtype
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
    if result.returncode != 0:
        print(result.stderr)
        print(result)
        app.errorBox("error", "Execution of extract_xmlcfg.sh failed")

    Upgrade_config_files.read_xmlconfig_mapping()
    print("\nParameters mapping in Sharepoint : " + str(Upgrade_config_files.xmlconfig_mapping_dict))
    if Upgrade_config_files.read_xmlconfig_file("extract\\xmlCfgFile.xml"):
        print("\nParameters in xmlConfigFile.xml : " + str(Upgrade_config_files.xmlconfig_file_dict))
        print("\nCorresponding Config Usr1 parameters for parameters in xmlConfigFile.xml : " + str(Upgrade_config_files.configusr1_dict))
        Upgrade_config_files.create_update_ConfigUsr1()
        if Upgrade_config_files.missing_xmlconfig_mapping_dict:
            print("\nMapping not present for parameters : " + str(Upgrade_config_files.missing_xmlconfig_mapping_dict))
        if Upgrade_config_files.missing_configusr1_dict:
            print("\nMapping for either Config user1 parameter name or Section name is missing for parameters : " + str(Upgrade_config_files.missing_configusr1_dict))
        if Upgrade_config_files.aidlist_dict:
            print("\naidList parameters to be updated : " + str(Upgrade_config_files.aidlist_dict))
            Upgrade_config_files.create_update_aidList()
            #print(Upgrade_config_files.package_update_arguments)
        if Upgrade_config_files.cdt_dict:
            print("\ncdt parameters to be updated : " + str(Upgrade_config_files.cdt_dict))

    command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\convert_xmlcfg.sh " + devtype
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
    if result.returncode != 0:
        print(result.stderr)
        print(result)
        app.errorBox("error", "Execution of extract_xmlcfg.sh failed")


def upgrade(btn):
    global cdt_selectedd
    global aidlist_selectedd
    msg = ""

    if app.getCheckBox("Upgrade cdt.txt") and cdt_selectedd == "":
        app.errorBox("error", "Please select cdt.txt to be converted")
    elif app.getCheckBox("Upgrade cdt.txt") and cdt_selectedd != "":
        if Upgrade_config_files.read_conf():
            succ = Upgrade_config_files.convert_cdt(cdt_selectedd)
            if succ == 0:
                msg = "cdt.txt is converted into cdt.ini. Please find @ extract\\cdt.ini\n"
            elif succ == 10:
                msg = msg + "Error creating extract\\cdt.ini\n"
            elif succ == 2:
                msg = msg + cdt_selectedd + " Not found\n"
            elif succ == 3:
                msg = msg + "Error reading " + cdt_selectedd + "\n"

        else:
            msg = msg + "Error reading CMAutomation\\conf.xlsx from Sharepoint\n"

    if app.getCheckBox("Upgrade Aidlist.txt") and aidlist_selectedd == "":
        msg = msg + "Please select aidlist.txt to be converted\n"
    elif app.getCheckBox("Upgrade Aidlist.txt") and aidlist_selectedd != "":
        succ = Upgrade_config_files.convert_aidlist(aidlist_selectedd)
        if succ == 0:
            msg = msg + "Aidlist.txt is converted into Aidlist.ini. Please find @ extract\\Aidlist.ini\n"
        elif succ == 10:
            msg = msg + "Error creating extract\\Aidlist.ini\n"
        elif succ == 2:
            msg = msg + aidlist_selectedd + " Not found\n"
        elif succ == 3:
            msg = msg + "Error reading " + aidlist_selectedd + "\n"
        elif succ == 1:
            msg = msg + aidlist_selectedd + " file format is not correct\n"

    if msg != "":
        app.infoBox("info", msg)


def Cybersourceprevrequests(btn):
    app.openTab("CMAutomationTabbedFrame", "Cybersource")
    if app.getRadioButton(devprodgen) == "Use Golden Package from previous CM Requests":
        try:
            app.hideWidget(kind=1, name="cybertempselected")
        except:
            pass
        if app.getEntry(clintname) == "":
            app.errorBox("error", "Please enter Client Name")
            return 1
        formnumber = str(int(app.getEntry(packagecreationcmform)))
        if int(formnumber) < 1:
            app.errorBox("error", "Please enter correct CM Request Number")
        else:
            clintid = read_access_db.getid_addclientname(app.getEntry(clintname))
            if (clintid == -1):
                app.errorBox("error", "Client Name doesn't exist in Sharepoint")
                return 1
            message = ""
            forminfo = read_cmitems.read_cmitems()
            if not forminfo.kickstart_cmitems_processing_gui(formnumber):
                app.errorBox("error", "Form " + formnumber + " doesn't exist in Sharepoint")
                return 1
            forminfo.create_dict()
            clientidfromform = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Client Name')])
            #print(str(clientidfromform) + ":" + str(clintid))
            if clientidfromform != clintid:
                app.errorBox("error", "Client Name entered (" + app.getEntry(clintname)  + ") does not match with Client Name in CM request")
                return 1
            #print(forminfo.dict)
            device = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Device Model')])
            prodtypp = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Product Type')])
            envv = (list(forminfo.dict.values())[list(forminfo.dict.keys()).index('Environment')])
            #print(device + ":" + prodtypp + ":" + envv)
            #print(read_access_db.get_previous_CM_REquests(clintid, prodtypp, device, envv))
            global prev_requests
            prev_requests = read_access_db.get_previous_CM_REquests(clintid, prodtypp, device, envv)
            prev_requests.sort(reverse=True)
            prev_requests_message = ""
            #print(prev_requests)

            enable_prev_request = False

            if len(prev_requests) > 1:
                '''
                home = expanduser("~")
                if not os.path.isdir(home + "\\SharePoint"):
                    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
                base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\CM Repository\\CM"
                '''
                home = expanduser("~")
                base_path = ""
                if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
                    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
                elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
                    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
                elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
                    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
                elif os.path.isdir(home + "\\OneDrive - Verifone"):
                    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
                    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

                prev_requests_message = prev_requests_message + "\nBelow is the list of past CM requests for \"" + app.getEntry(clintname) + " / " + prodtypp + " / " + device + " / " + envv + "\" combination.\n\n"
                for i in prev_requests:
                    if str(i) != str(formnumber):
                        prev_requests_message += "CM Request " + str(i) + " - "
                        basefilespath = base_path + str(i)
                        filetype = "*.tgz"
                        if (len(glob.glob(basefilespath + '\\' + filetype))) > 1:
                            prev_requests_message += "Multiple config packages exist in Sharepoint @ " + basefilespath + ". Please make sure only 1 is present.\n"
                        elif (len(glob.glob(basefilespath + '\\' + filetype))) == 0:
                            prev_requests_message += "Config package doesn't exist in Sharepoint @ " + basefilespath + "\n"
                        elif (len(glob.glob(basefilespath + '\\' + filetype))) == 1:
                            prev_requests_message += "Config package exists in Sharepoint @ " + basefilespath + "\n"
                            enable_prev_request = True

            else:
                prev_requests_message = prev_requests_message + "\nThere are no previous CM requests for " + app.getEntry(clintname) + " / " + prodtypp + " / " + device + " / " + envv + " combination.\n"
                prev_requests_message = prev_requests_message + "Please select either Lab or Prod Template above."

            app.showWidget(kind=1, name="cyber_prev_requests")
            app.setLabel("cyber_prev_requests", prev_requests_message)
            if enable_prev_request:
                app.showWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from")
            else:
                prev_list = ""
                for i in prev_requests:
                    if str(i) != str(formnumber):
                        prev_list += str(i) + ", "
                prev_list = prev_list[:-2]
                app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from")
                if len(prev_requests) > 1:
                    app.errorBox("error", "Golden package for past requests ( " + prev_list +  " ) doesn't exist in Sharepoint so you must select either 'Use Lab Template' or 'Use Prod Template' option\n\nNote: Past requests are searched based on Client/Product Type/Device/Environment combination. In this case, it is " + app.getEntry(clintname) + " / " + prodtypp + " / " + device + " / " + envv)
                else:
                    app.errorBox("error", "Golden package for past requests doesn't exist in Sharepoint so you must select either 'Use Lab Template' or 'Use Prod Template' option\n\nNote: Past requests are searched based on Client/Product Type/Device/Environment combination. In this case, it is " + app.getEntry(clintname) + " / " + prodtypp + " / " + device + " / " + envv)

    else:
        app.hideWidget(kind=1, name="cyber_prev_requests")
        app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from")
        #app.addLabelNumericEntry("Please enter past CM Request number from above list to grab config package from")
        try:
            app.hideWidget(kind=1, name="cybertempselected")
        except:
            pass
        global cybersource_template_selected
        if app.getRadioButton(devprodgen) == "Select specific Template":
            root = Tk()
            root.withdraw()
            cybersource_template_selected = filedialog.askopenfilename(defaultextension='.tgz', filetypes=[('Config Template', '*.tgz; *.zip'), ('All files', '*.*')])
            msg = "Config Template selected : " + cybersource_template_selected

            try:
                app.addLabel("cybertempselected", msg)
            except:
                app.setLabel("cybertempselected", msg)
                app.showWidget(kind=1, name="cybertempselected")
        else:
            try:
                app.hideWidget(kind=1, name="cybertempselected")
            except:
                pass



##################################################################################################################

app.startTabbedFrame("CMAutomationTabbedFrame")

##################################################################################################################

app.startTab("Initial / Update Configuration")
app.setTabBg("CMAutomationTabbedFrame", "Initial / Update Configuration", "lightblue")
app.setFont(9, font="Verdana")
app.addEmptyLabel("empty201")
app.addLabelNumericEntry(inicreationcmform)
app.setLabelTooltip(inicreationcmform, "\n1. Enter Sharepoint CM Request Number\n\n2. Mandatory field\n")
app.setFocus(inicreationcmform)
app.addLabelEntry(iniclintname)
app.setLabelTooltip(iniclintname, "\n1. Enter Client name which must match Client name in Sharepoint for above entered CM Request number. \n\n2. This is to validate CM is not entering wrong CM Request Number above by mistake\n\n3. Case insensitive field\n\n4. Mandatory\n")
app.addEmptyLabel("empty203")
app.addRadioButton("Type of request", "Initial Configuration")
app.addRadioButton("Type of request", "Update Configuration")
app.setRadioButton("Type of request", "Initial Configuration")
app.addEmptyLabel("empty286")
app.addLabel("choosetemplatee", "----------- Choose Config Template ---------------")
app.setLabelAlign("choosetemplatee", "left")
app.getLabelWidget("choosetemplatee").config(font=("Sans Serif", "12", "bold"))
app.addRadioButton(inidevprodgen, "Use Golden Package from previous CM Requests")
app.addRadioButton(inidevprodgen, "Select specific Template")
#app.addRadioButton(inidevprodgen, "Use Standard Config template from Sharepoint")
#app.setRadioButton(inidevprodgen, "Use Standard Config template from Sharepoint")
app.setRadioButtonChangeFunction(inidevprodgen, choosetemplate)
#app.setRadioButtonTooltip(inidevprodgen, "\n1. Use Standard Config template from Sharepoint - Select this option to automatically choose Standard Config package stored in Sharepoint based on SCA version mentioned in CM Request\n\n2. Select specific Template - Select this option to pick up any config template you want from File explorer\n")
app.setRadioButtonTooltip(inidevprodgen, "\n1. Use Golden Package from previous CM Requests - Select Golden package from past CM requests with same Client, Product type, Device, Environment combination. \n\n2. Select specific Template - Select this option to pick up any config template you want from File explorer\n\n")
app.addEmptyLabel("empty282")
app.addLabel("ini_prev_requests","")
app.setLabelFg("ini_prev_requests", "red")
app.addLabelNumericEntry("Please enter past CM Request number from above list to grab config package from ")
app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from ")
app.addEmptyLabel("empty285")
app.addLabel("chooseoptionsCM", "----------- Select options to create Initial configuration from Template selected above --------------")
app.setLabelAlign("chooseoptionsCM", "left")
app.getLabelWidget("chooseoptionsCM").config(font=("Sans Serif", "12", "bold"))
app.addCheckBox("Insert EMV Templates ")
app.setCheckBoxTooltip("Insert EMV Templates ", "\nBased on Processor, Product Type, Device, Environment fields in CM Request, respective EMV template is picked up from Sharepoint 'EMV' folder and inserted into package\nNote: If there is existing EMV package in the config package selected above then it will be replaced by the EMV template inserted")
#app.addCheckBox("Insert Media Templates ")
app.addCheckBox(enablepinbypass)
app.setCheckBoxTooltip(enablepinbypass, "\nIf 'Pin Bypass' parameter in CM Request is set to Yes then EMVTables.ini, Aidlist.txt/Aidlist.ini, config.usr1, OptFlag.ini files will be updated accordingly to enable Pin Bypass\n")
app.addCheckBox(updatevhqtopackage)
app.setCheckBoxTooltip(updatevhqtopackage, "\nSelect if 'vhqconfig.ini' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(updatecdttopackage)
app.setCheckBoxTooltip(updatecdttopackage, "\nSelect if 'cdt.txt / cdt.ini' in package needs to be updated with Signature amount, SAF Limits by card type field values from CM Request\n")
app.addCheckBox(updateaidlisttopackage)
app.setCheckBoxTooltip(updateaidlisttopackage, "\nSelect if 'Aidlist.ini' in package needs to be updated with signature limits, Capture signature on Pin Bypass, Cahsback by AID fields from CM Request\n")
#app.addCheckBox(updateemvtablestopackage)
#app.setCheckBoxTooltip(updateemvtablestopackage, "\nSelect if 'EMVTables.ini' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(updatcpuuserpwdtopackage)
app.setCheckBoxTooltip(updatcpuuserpwdtopackage, "\nSelect if 'CPUsernameAndPassword.txt' in package needs to be updated with Netconnect Username and Netconnect Password parameters from CM Request\n")
app.addCheckBox(convertxmlcfg)
app.setCheckBoxTooltip(convertxmlcfg, "\nselect if parametrs in xmlcfgFile.xml to be converted to config.usr1/aidlist/cdt parameters\n")
app.addCheckBox(updateconfigusrtopackage)
app.setCheckBoxTooltip(updateconfigusrtopackage, "\nSelect if 'config.usr1' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(remove_configusr1_default)
app.setCheckBoxTooltip(remove_configusr1_default, "\nSelect if parameters set to default values need to be removed from config.usr1\n")
#app.enableEnter(insertmedia)
app.addEmptyLabel("empty202")

#app.addLabel("instr1", "Select right options below and hit 'Create Package' button")
app.addEmptyLabel("empty210")
app.addButton("Process  ", processinitialconfiguration)
app.setButtonBg("Process  ", "brown")
app.setButtonFg("Process  ", "white")
#app.addLabel("msgg", "After hitting 'Process' button, updated package will be available in 'extract' folder ")
#app.getLabelWidget("msgg").config(font=("Sans Serif", "10", "bold"))
app.addEmptyLabel("empty284")
app.stopTab()

##################################################################################################################

app.startTab("Upgrade")
app.setTabBg("CMAutomationTabbedFrame", "Upgrade", "lightblue")
app.addEmptyLabel("empty400")
app.addCheckBox("Upgrade cdt.txt")
app.addEmptyLabel("empty401")
app.addCheckBox("Upgrade Aidlist.txt")
app.addEmptyLabel("empty402")
app.setCheckBoxChangeFunction("Upgrade cdt.txt", choosecdt)
app.setCheckBoxChangeFunction("Upgrade Aidlist.txt", chooseaidlist)

app.addEmptyLabel("empty403")
app.addButton("Upgrade", upgrade)
app.setButtonBg("Upgrade", "brown")
app.setButtonFg("Upgrade", "white")
app.addEmptyLabel("empty404")
app.stopTab()


##################################################################################################################
'''
app.startTab("Convert")
app.setTabBg("CMAutomationTabbedFrame", "Convert", "lightblue")
app.addEmptyLabel("empty500")
app.addCheckBox("Select config package containing xmlCfgFile.xml")
app.addEmptyLabel("empty502")
app.setCheckBoxChangeFunction("Select config package containing xmlCfgFile.xml", choosexmlcfg)

app.addEmptyLabel("empty503")
app.addButton("Convert", convert)
app.setButtonBg("Convert", "brown")
app.setButtonFg("Convert", "white")
app.addEmptyLabel("empty504")
app.stopTab()
'''

##################################################################################################################
'''
app.startTab("Cybersource")
app.setTabBg("CMAutomationTabbedFrame", "Cybersource", "lightblue")
app.setFont(9, font="Verdana")
app.addEmptyLabel("empty101")
app.addLabelNumericEntry(packagecreationcmform)
app.setLabelTooltip(packagecreationcmform, "\n1. Enter Sharepoint CM Request Number\n\n2. Mandatory field\n")
app.addLabelEntry(clintname)
app.setLabelTooltip(clintname, "\n1. Enter Client name which must match Client name in Sharepoint for above entered CM Request number. \n\n2. This is to validate CM is not entering wrong CM Request Number above by mistake\n\n3. Case insensitive field\n\n4. Mandatory\n")
app.addEmptyLabel("empty103")
app.addLabel("choosetemplate", "----------- Choose Config Template --------------")
app.setLabelAlign("choosetemplate", "left")
app.getLabelWidget("choosetemplate").config(font=("Sans Serif", "12", "bold"))
app.addRadioButton(devprodgen, "Use Golden Package from previous CM Requests")
app.addRadioButton(devprodgen, "Use Lab Template ")
app.addRadioButton(devprodgen, "Use Prod Template ")
app.addRadioButton(devprodgen, "Select specific Template")
app.setRadioButton(devprodgen, "Use Lab Template ")
app.addLabel("cyber_prev_requests","")
app.setLabelFg("cyber_prev_requests", "red")
app.addLabelNumericEntry("Please enter past CM Request number from above list to grab config package from")
app.hideWidget(kind=1, name="Please enter past CM Request number from above list to grab config package from")
app.setRadioButtonChangeFunction(devprodgen, Cybersourceprevrequests)
app.setRadioButtonTooltip(devprodgen, "\n1. Use Golden Package from previous CM Requests - Select this option when you have golden package available in Sharepoint for past CM requests for same Client/Product type/Device/Environment combination as this request and you would like to use it for this request as base\n\n2. Use Lab Template - Select this option to create package from scratch using Dev template stored in Sharepoint\n\n3. Use Prod Template - Select this option to create package from scratch using Prod template stored in Sharepoint\n")
app.addEmptyLabel("empty82")
#app.addLabel("chooseoptionsCM", "----------- Select options ---------------------")
#app.setLabelAlign("chooseoptionsCM", "left")
#app.getLabelWidget("chooseoptionsCM").config(font=("Sans Serif", "12", "bold"))
#app.addCheckBox("Insert EMV Templates ")
#app.setCheckBoxTooltip("Insert EMV Templates ", "\nBased on Processor, Product Type, Device, Environment fields in CM Request, respective EMV template is picked up from Sharepoint 'EMV' folder and inserted into package\n")
#app.addCheckBox("Insert Media Templates ")
#app.addCheckBox(updateconfigusrtopackage)
#app.setCheckBoxTooltip(updateconfigusrtopackage, "\nSelect if 'config.usr1' in package needs to be updated with respective fields from CM Request\n")
#app.addCheckBox(updatevhqtopackage)
#app.setCheckBoxTooltip(updatevhqtopackage, "\nSelect if 'vhqconfig.ini' in package needs to be updated with respective fields from CM Request\n")
app.setFocus(packagecreationcmform)
app.addEmptyLabel("empty102")


#app.addLabel("instr1", "Select right options below and hit 'Create Package' button")
app.addLabel("chooseoptionsCyber", "----------- Choose Cybersource specific options --------------")
app.setLabelAlign("chooseoptionsCyber", "left")
app.getLabelWidget("chooseoptionsCyber").config(font=("Sans Serif", "12", "bold"))
app.addCheckBox(sigcapt)
app.setCheckBoxTooltip(sigcapt, "\n1. MX Only\n\n2. Signature Capture will be Enabled / Disabled in aidlist.ini and config.usr1 files in package\n")
app.addLabelNumericEntry(siglimit)
app.setLabelTooltip(siglimit, "\n1. MX Only\n\n2. Signature Limit Amount entered here will be updated in aidlist.ini and config.usr1 files in package\n")
app.addEmptyLabel("empty106")
app.addCheckBox(saf)
app.setCheckBoxTooltip(saf, "\nSAF will be Enabled / Disabled in ( MX - config.usr1 / VX - PropertiesData ) file in package\n")
app.addLabelNumericEntry(tranfllimit)
app.setLabelTooltip(tranfllimit, "\nTransaction Floor Limit entered here will be updated in ( MX - config.usr1 / VX - PropertiesData ) file in package\n")
app.addLabelNumericEntry(totfllimit)
app.setLabelTooltip(totfllimit, "\nTotal Floor Limit entered here will be updated in ( MX - config.usr1 / VX - PropertiesData ) file in package\n")
app.addLabelNumericEntry(dayslimit)
app.setLabelTooltip(dayslimit, "\nDays Limit entered here will be updated in ( MX - config.usr1 / VX - PropertiesData ) file in package\n")
app.addEmptyLabel("empty107")
app.addCheckBox(lineitdisp)
app.setCheckBoxTooltip(lineitdisp, "\n1. MX Only\n\n2. Line Item Display will be Enabled / Disabled in config.usr1 file in package\n")
app.addEmptyLabel("empty108")
app.addLabelEntry(vhqinst)
app.setLabelTooltip(vhqinst, "\nVHQ Instance name entered here will be updated in vhqconfig.ini file in package\n")


app.addCheckBox(addimage)
app.setCheckBoxTooltip(addimage, "\n1. Applicable for MX915 and MX925\n\n2. Select this option if image from your local needs to be inserted into package\n\n3. If CM Request Device model is not MX915/MX925 then this option will be ignored\n")
app.setCheckBoxChangeFunction(addimage, insertimageoptionspackagecreation)
#app.addButton("Create Package", createcyberpackage)
app.addEmptyLabel("empty110")
app.addButton("Process ", processpackagecreation)
app.setButtonBg("Process ", "brown")
app.setButtonFg("Process ", "white")
#app.addLabel("msg", "After hitting 'Process' button, updated package will be available in 'extract' folder ")
#app.getLabelWidget("msg").config(font=("Sans Serif", "10", "bold"))
app.addEmptyLabel("empty84")
app.stopTab()

'''

##################################################################################################################

app.startTab("OTB Request")
app.setTabBg("CMAutomationTabbedFrame", "OTB Request", "lightblue")
app.addEmptyLabel("empty91")
app.addLabelNumericEntry(cmrequestotb)
app.addLabelEntry("Client Name       ")
app.addEmptyLabel("empty92")
#app.addCheckBox("Create OTB build")
app.setFocus(cmrequestotb)
app.addEmptyLabel("empty93")
app.addButton("Process   ", processotb)
app.setButtonBg("Process   ", "brown")
app.setButtonFg("Process   ", "white")
app.addEmptyLabel("empty94")
app.stopTab()

##################################################################################################################

app.startTab("Verify Media")
app.setTabBg("CMAutomationTabbedFrame", "Verify Media", "lightblue")
app.addEmptyLabel("empty300")
app.addCheckBox("Select Config tgz package to be verified")
app.addEmptyLabel("empty301")
app.addLabelEntry("Enter SCA Version : ")
app.setCheckBoxChangeFunction("Select Config tgz package to be verified", choosemediapackage)
app.addEmptyLabel("empty302")
app.addRadioButton("select device", "MX915")
app.addRadioButton("select device", "MX925")

app.addEmptyLabel("empty303")
app.addButton("Process    ", processvermedia)
app.setButtonBg("Process    ", "brown")
app.setButtonFg("Process    ", "white")
app.addEmptyLabel("empty304")
app.stopTab()

##################################################################################################################
'''
app.startTab("Update Configuration")
app.setTabBg("CMAutomationTabbedFrame", "Update Configuration", "lightblue")
app.setFont(9, font="Verdana")
app.addEmptyLabel("empty301")
app.addLabelNumericEntry(upcreationcmform)
app.setLabelTooltip(upcreationcmform, "\n1. Enter Sharepoint CM Request Number\n\n2. Mandatory field\n")
app.setFocus(upcreationcmform)
app.addLabelEntry(upclintname)
app.setLabelTooltip(upclintname, "\n1. Enter Client name which must match Client name in Sharepoint for above entered CM Request number. \n\n2. This is to validate CM is not entering wrong CM Request Number above by mistake\n\n3. Case insensitive field\n\n4. Mandatory\n")
app.addEmptyLabel("empty303")
app.addLabel("choosetemplateupdate", "----------- Choose Config Template ---------------")
app.setLabelAlign("choosetemplateupdate", "left")
app.getLabelWidget("choosetemplateupdate").config(font=("Sans Serif", "12", "bold"))
app.addRadioButton(devprodgen, "Select specific Template")
app.setRadioButton(devprodgen, "Select specific Template")
app.setRadioButtonChangeFunction(devprodgen, choosetemplate)
app.setRadioButtonTooltip(devprodgen, "\n1. Use Standard Config template from Sharepoint - Select this option to automatically choose Standard Config package stored in Sharepoint based on SCA version mentioned in CM Request\n\n2. Select specific Template - Select this option to pick up any config template you want from File explorer\n")
app.addEmptyLabel("empty382")
app.addLabel("chooseoptionsCM", "----------- Select options to create Initial configuration from Template selected above --------------")
app.setLabelAlign("chooseoptionsCM", "left")
app.getLabelWidget("chooseoptionsCM").config(font=("Sans Serif", "12", "bold"))
app.addCheckBox("Insert EMV Templates ")
app.setCheckBoxTooltip("Insert EMV Templates ", "\nBased on Processor, Product Type, Device, Environment fields in CM Request, respective EMV template is picked up from Sharepoint 'EMV' folder and inserted into package\n")
#app.addCheckBox("Insert Media Templates ")
app.addCheckBox(updateconfigusrtopackage)
app.setCheckBoxTooltip(updateconfigusrtopackage, "\nSelect if 'config.usr1' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(updatevhqtopackage)
app.setCheckBoxTooltip(updatevhqtopackage, "\nSelect if 'vhqconfig.ini' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(updatecdttopackage)
app.setCheckBoxTooltip(updatecdttopackage, "\nSelect if 'cdt.txt' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(updateaidlisttopackage)
app.setCheckBoxTooltip(updateaidlisttopackage, "\nSelect if 'AidList.txt' in package needs to be updated with respective fields from CM Request\n")
app.addCheckBox(updateemvtablestopackage)
app.setCheckBoxTooltip(updateemvtablestopackage, "\nSelect if 'EMVTables.ini' in package needs to be updated with respective fields from CM Request\n")
#app.enableEnter(insertmedia)
app.addEmptyLabel("empty302")

#app.addLabel("instr1", "Select right options below and hit 'Create Package' button")
app.addEmptyLabel("empty310")
app.addButton("Process  ", processinitialconfiguration)
app.setButtonBg("Process  ", "brown")
app.setButtonFg("Process  ", "white")
app.addLabel("msgg", "After hitting 'Process' button, updated package will be available in 'extract' folder ")
app.getLabelWidget("msgg").config(font=("Sans Serif", "10", "bold"))
app.addEmptyLabel("empty384")
app.stopTab()

##################################################################################################################


app.startTab("Cybersource")
app.setTabBg("CMAutomationTabbedFrame", "Cybersource", "lightblue")
app.addEmptyLabel("empty21")
app.addRadioButton(indpackage, "MX")
app.addRadioButton(indpackage, "VX")
app.setRadioButton(indpackage, "MX")
app.addEmptyLabel("empty22")
app.addRadioButton(devprod, "Use Lab Template")
app.addRadioButton(devprod, "Use Prod Template")
app.setRadioButton(devprod, "Use Lab Template")

app.setRadioButtonChangeFunction(indpackage, next)
#app.addButton("Next", next)
try:
    #app.addLabel("instr1", "Select right options below and hit 'Create Package' button")
    app.addEmptyLabel("empty23")
    app.addCheckBox(sigcapt)
    app.addLabelNumericEntry(siglimit)
    app.addEmptyLabel("empty24")
    app.addCheckBox(saf)
    app.addLabelNumericEntry(tranfllimit)
    app.addLabelNumericEntry(totfllimit)
    app.addLabelNumericEntry(dayslimit)
    app.addEmptyLabel("empty25")
    app.addCheckBox(lineitdisp)
    app.addEmptyLabel("empty26")
    app.addLabelEntry(vhqinst)
    app.addEmptyLabel("empty27")
    app.addCheckBox(addimage)
    app.setCheckBoxChangeFunction(addimage, insertimageoptions)
    app.addButton("Create Package", createcyberpackage)
    app.addEmptyLabel("empty28")
except:
    #app.showWidget(kind=1, name="instr1")
    app.showWidget(kind=4, name=sigcapt)  # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
    app.showWidget(kind=2, name=siglimit)
    app.showWidget(kind=4, name=saf)
    app.showWidget(kind=2, name=tranfllimit)
    app.showWidget(kind=2, name=totfllimit)
    app.showWidget(kind=2, name=dayslimit)
    app.showWidget(kind=1, name="empty25")
    app.showWidget(kind=4, name=lineitdisp)
    app.showWidget(kind=2, name=vhqinst)
    app.showWidget(kind=4, name=addimage)
    app.showWidget(kind=3, name="Create Package")
app.stopTab()

##################################################################################################################

app.startTab("CM Request Processing")
app.setTabBg("CMAutomationTabbedFrame", "CM Request Processing", "lightblue")
app.addEmptyLabel("empty71")
app.addLabelNumericEntry("CM Request #   ")
app.addLabelEntry("Client Name      ")
app.addEmptyLabel("empty72")
app.addCheckBox("Create config.usr1, vhqconfig.ini")
app.addCheckBox("Insert EMV Templates")
app.addCheckBox("Insert Media Templates")
#app.addCheckBox("Create OTB build")
#app.enableEnter(insertmedia)
app.setFocus("CM Request #   ")
app.addEmptyLabel("empty73")
app.addButton("Process", process)
app.addEmptyLabel("empty74")
app.stopTab()


'''
##################################################################################################################

app.startTab("Upload to Sharepoint")
app.setTabBg("CMAutomationTabbedFrame", "Upload to Sharepoint", "lightblue")

app.addEmptyLabel("empty9")
app.addLabelEntry("Client Name ")
app.setFocus("Client Name ")
app.addLabelAutoEntry("Product Type", prodtype)
app.addLabelAutoEntry("Device      ", device)
app.addLabelAutoEntry("Environment ", environment)
app.addEmptyLabel("empty10")

#------------------------------------

try:
    app.showWidget(kind=1, name="uploadinstr")
    app.showWidget(kind=4, name="AIDList.ini ")
    app.showWidget(kind=4, name="AIDList.txt ")
    app.showWidget(kind=4, name="CAPKData.ini ")
    app.showWidget(kind=4, name="cdt.txt ")
    app.showWidget(kind=4, name="cdt.ini ")
    app.showWidget(kind=4, name="config.usr1 ")
    app.showWidget(kind=4, name="creditPreferredAIDs.ini ")
    app.showWidget(kind=4, name="CTLSConfig.ini ")
    app.showWidget(kind=4, name="DebitPreferredAIDs.ini ")
    app.showWidget(kind=4, name="DisplayPrompts.ini ")
    app.showWidget(kind=4, name="EMVTables.ini ")
    app.showWidget(kind=4, name="EMVTagsReqbyHost.txt ")
    app.showWidget(kind=4, name="MSGXPI.txt ")
    app.showWidget(kind=4, name="OptFlag.ini ")
    app.showWidget(kind=4, name="PCI_BIT.DAT ")
    app.showWidget(kind=4, name="propertiesdata.xml ")
    app.showWidget(kind=4, name="SAF_ERR_CODES.DAT ")
    app.showWidget(kind=4, name="tpscodesforSAF.txt ")
    app.showWidget(kind=4, name="VHQconfig.ini ")
    app.showWidget(kind=4, name="xmlReceiptFile.xml ")

    app.showWidget(kind=3, name="Upload Individual Files from Local")
    app.showWidget(kind=3, name="Extract Local package and Upload selected files")
    #app.showWidget(kind=3, name="Download Files")
except:
    app.addLabel("uploadinstr", "Select files to Upload to Sharepoint")
    app.addCheckBox("AIDList.ini ")
    app.addCheckBox("AIDList.txt ")
    app.addCheckBox("CAPKData.ini ")
    app.addCheckBox("cdt.txt ")
    app.addCheckBox("cdt.ini ")
    app.addCheckBox("config.usr1 ")
    app.addCheckBox("creditPreferredAIDs.ini ")
    app.addCheckBox("CTLSConfig.ini ")
    app.addCheckBox("DebitPreferredAIDs.ini ")
    app.addCheckBox("DisplayPrompts.ini ")
    app.addCheckBox("EMVTables.ini ")
    app.addCheckBox("EMVTagsReqbyHost.txt ")
    app.addCheckBox("MSGXPI.txt ")
    app.addCheckBox("OptFlag.ini ")
    app.addCheckBox("PCI_BIT.DAT ")
    app.addCheckBox("propertiesdata.xml ")
    app.addCheckBox("SAF_ERR_CODES.DAT ")
    app.addCheckBox("tpscodesforSAF.txt ")
    app.addCheckBox("VHQconfig.ini ")
    app.addCheckBox("xmlReceiptFile.xml ")
    app.addEmptyLabel("empty48")
    app.addButtons(["Upload Individual Files from Local", "Extract Local package and Upload selected files"], uploadinsertfiles)
    app.addEmptyLabel("empty49")

#------------------------------------

app.stopTab()

##################################################################################################################

app.startTab("Download from Sharepoint")
app.setTabBg("CMAutomationTabbedFrame", "Download from Sharepoint", "lightblue")
app.addEmptyLabel("empty20")
app.addLabelEntry("Client Name  ")
app.setFocus("Client Name  ")
app.addLabelAutoEntry("Product Type ", prodtype)
app.addLabelAutoEntry("Device       ", device)
app.addLabelAutoEntry("Environment  ", environment)

try:
    app.showWidget(kind=1, name="downloadinstr")
    app.showWidget(kind=4, name="AIDList.ini")
    app.showWidget(kind=4, name="AIDList.txt")
    app.showWidget(kind=4, name="CAPKData.ini")
    app.showWidget(kind=4, name="cdt.txt")
    app.showWidget(kind=4, name="cdt.ini")
    app.showWidget(kind=4, name="config.usr1")
    app.showWidget(kind=4, name="creditPreferredAIDs.ini")
    app.showWidget(kind=4, name="CTLSConfig.ini")
    app.showWidget(kind=4, name="DebitPreferredAIDs.ini")
    app.showWidget(kind=4, name="DisplayPrompts.ini")
    app.showWidget(kind=4, name="EMVTables.ini")
    app.showWidget(kind=4, name="EMVTagsReqbyHost.txt")
    app.showWidget(kind=4, name="MSGXPI.txt")
    app.showWidget(kind=4, name="OptFlag.ini")
    app.showWidget(kind=4, name="PCI_BIT.DAT")
    app.showWidget(kind=4, name="propertiesdata.xml")
    app.showWidget(kind=4, name="SAF_ERR_CODES.DAT")
    app.showWidget(kind=4, name="tpscodesforSAF.txt")
    app.showWidget(kind=4, name="VHQconfig.ini")
    app.showWidget(kind=4, name="xmlReceiptFile.xml")

    app.showWidget(kind=3, name="Download Individual Files")
    app.showWidget(kind=3, name="Download Files and Insert into Local Package")
    #app.showWidget(kind=3, name="Download Files")
except:
    app.addEmptyLabel("empty17")
    app.addLabel("downloadinstr", "Select files to Download from Sharepoint")
    app.addCheckBox("AIDList.ini")
    app.addCheckBox("AIDList.txt")
    app.addCheckBox("CAPKData.ini")
    app.addCheckBox("cdt.txt")
    app.addCheckBox("cdt.ini")
    app.addCheckBox("config.usr1")
    app.addCheckBox("creditPreferredAIDs.ini")
    app.addCheckBox("CTLSConfig.ini")
    app.addCheckBox("DebitPreferredAIDs.ini")
    app.addCheckBox("DisplayPrompts.ini")
    app.addCheckBox("EMVTables.ini")
    app.addCheckBox("EMVTagsReqbyHost.txt")
    app.addCheckBox("MSGXPI.txt")
    app.addCheckBox("OptFlag.ini")
    app.addCheckBox("PCI_BIT.DAT")
    app.addCheckBox("propertiesdata.xml")
    app.addCheckBox("SAF_ERR_CODES.DAT")
    app.addCheckBox("tpscodesforSAF.txt")
    app.addCheckBox("VHQconfig.ini")
    app.addCheckBox("xmlReceiptFile.xml")
    app.addEmptyLabel("empty18")
    app.addButtons(["Download Individual Files", "Download Files and Insert into Local Package"], downloadinsertfiles)
    app.addEmptyLabel("empty19")
app.stopTab()


##################################################################################################################

app.stopTabbedFrame()

##################################################################################################################

app.go()

