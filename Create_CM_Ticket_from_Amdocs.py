import glob, os
import logging, ntpath
import pypyodbc
import datetime
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

Amdocs_file_path = base_path + "Amdocs\\"
logging.basicConfig(filename='logs\\Create_CM_Ticket_from_Amdocs.log',level=logging.DEBUG)
from xlrd import open_workbook

class read_amdocs_case():
    def __init__(self, amdocscasefile):
        self.ticket_details = {}
        self.holidays = []
        self.amdocscase_filename = amdocscasefile
        self.workbook = open_workbook(self.amdocscase_filename)
        self.wb = self.workbook.sheet_by_name("Sheet1")

    def process_amdocs_Case_file(self):
        for row in range(1, self.wb.nrows): # ignore first row in amdocs case file which is header row
            print("########################################################################")
            self.ticket_details.clear()
            self.ticket_details["Title"] =self.wb.cell(row ,0).value
            if self.wb.cell(row ,1).value == "":
                logging.warning("Amdocs Ticket Number is blank. This record won't be processed")
                return 1
            self.ticket_details["Amdocs Ticket Number"] =self.wb.cell(row ,1).value
            if self.wb.cell(row ,2).value == "":
                logging.warning("Ticket Created by field is blank. This record won't be processed")
                return 1
            self.ticket_details["Ticket Created By"] =self.wb.cell(row ,2).value
            self.ticket_details["Group of person who created ticket"] =self.wb.cell(row ,3).value
            self.ticket_details["Client Name"] =self.wb.cell(row ,4).value     # Add logic for various values
            self.ticket_details["Product Type"] =self.wb.cell(row ,5).value    # Add logic for various values
            self.ticket_details["Device"] =self.wb.cell(row ,6).value          # Add logic for various values
            if self.wb.cell(row ,7).value == "P2-Moderate":                    # Add logic for various values
                self.ticket_details["Priority"] = "Medium"
            else:
                self.ticket_details["Priority"] =self.wb.cell(row ,7).value
            self.ticket_details["Environment"] =self.wb.cell(row ,8).value     # Add logic for various values
            if isinstance(self.wb.cell(row ,9).value, float):
                self.ticket_details["Bid"] = int(self.wb.cell(row ,9).value)
            else:
                self.ticket_details["Bid"] = ""
            self.ticket_details["VHQ Instance Name"] =self.wb.cell(row ,10).value
            self.ticket_details["Product Version"] =self.wb.cell(row ,11).value
            self.ticket_details["Case Notes"] =self.wb.cell(row ,12).value
            print(self.ticket_details)
            client_id = self.check_client_exists()
            if client_id == 1:
                print("Client doesn't exists in Sharepoint")
                logging.warning("Client doesn't exists in Sharepoint")
                return 1
            else:
                self.ticket_details["Client ID"] = client_id
            if self.calculate_delivery_date() == 1:
                return 1
            self.get_new_ticket_number()
            self.create_cm_ticket()
            #self.read_attachment_data()

    def read_attachment_data(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")

        cur = conn.cursor()
        query = "SELECT [Customer Survey].Attachments.FileData, [Customer Survey].Attachments.FileName, [Customer Survey].Attachments.FileType, [Customer Survey].Attachments.FileURL FROM [Customer Survey] WHERE [Customer Survey].[ID] = 1361"
        cur.execute(query)

        while True:
            row = cur.fetchone()
            if row is None:
                break
            else:
                print(row)

        print("$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        #query = "INSERT INTO [Customer Survey] ([Revision Number], [Revision], [Client Name], [Product Type], [Device Model], [EMV Payment Preferred Type], [Customer Survey].Attachments.FileData, [Customer Survey].Attachments.FileName"
        #query = query + ") VALUES ( '" + str(self.ticket_details["Title"].replace("'", "''")) + "', '1', '" + str(self.ticket_details["Client ID"]) + "', '" + self.ticket_details["Product Type"] + "', '" + self.ticket_details["Device"] + "', 'Auto Credit', '" + str(bytearray((open("r:\\Amdocs\\emv.log", "rb").read()))).replace("'", "''") + "', 'emv.log')"
        query = "UPDATE [Customer Survey] SET [Customer Survey].[Attachments].[FileData] = '" + str(bytearray((open("r:\\Amdocs\\emv.log", "rb").read()))).replace("'", "''") + "', [Customer Survey].[Attachments].[FileName]= 'emv.log', [Customer Survey].Attachments.FileType = '.log' WHERE [Customer Survey].[ID] = '1349' AND [Customer Survey].[Attachments].[FileName] = ''"
        print(query)
        cur.execute(query)
        cur.commit()
        cur.close()
        conn.close()
        print(open("r:\\Amdocs\\emv.log", "rb").read())


    def create_cm_ticket(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")

        cur = conn.cursor()

        query = "INSERT INTO [CM Ticket] ([Title], [Client Name], [Product Type], [Device Model], [Type of request], [Priority], [Delivery Date], [Environment], [Product version], [Select Bid Options], [Bid #], [VHQ Company ID], [“Deliver to” person], [Comments], [Ticket Number], [tktnumbertxtcopy], [Ticket Created By]"
        #query = query + ") VALUES ( '" + str(self.ticket_details["Title"].replace("'", "''")) + "', '" + str(self.ticket_details["Client ID"]) + "', '" + self.ticket_details["Product Type"] + "', '" + self.ticket_details["Device"] + "', 'Update configuration', '" + self.ticket_details["Priority"] + "', '" + self.ticket_details["Delivery Date"] + "', '" + self.ticket_details["Environment"] + "', '" + str(self.ticket_details["Product Version"].replace("'", "''"))
        query = query + ") VALUES ( '" + str(self.ticket_details["Title"].replace("'", "''")) + "', '" + str(self.ticket_details["Client ID"]) + "', 'Amdocs Case', '" + self.ticket_details["Device"] + "', 'Update configuration', '" + self.ticket_details["Priority"] + "', '" + self.ticket_details["Delivery Date"] + "', '" + self.ticket_details["Environment"] + "', '" + str(self.ticket_details["Product Version"].replace("'", "''"))
        if self.ticket_details["Bid"] != "":
            query = query + "', 'Update Existing Bid', '" + str(self.ticket_details["Bid"])
        query = query + "', '" + self.ticket_details["VHQ Instance Name"].replace("'", "''") + "', '" + str(self.ticket_details["Ticket Created By"]) + "', '" + self.ticket_details["Case Notes"].replace("'", "''") + "', '" + str(self.ticket_details["Ticker Number"]) + "', '" + str(self.ticket_details["Ticker Number"]) + "', '" + str(self.ticket_details["Ticket Created By"]) + "'"
        print(query)
        #cur.execute(query)
        #cur.commit()
        cur.close()


    def check_client_exists(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Add Client Name.accdb;\"")

        cur = conn.cursor()
        query = "SELECT [ID], [Client Name] FROM [Add Client Name] WHERE [Add Client Name].[Client Name] = '" + self.ticket_details["Client Name"] + "'"
        cur.execute(query)

        while True:
            row = cur.fetchone()
            if row is None:
                break
            else:
                cur.close()
                conn.close()
                return int(row[0])

        cur.close()
        conn.close()
        return 1


    def calculate_delivery_date(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Holidays.accdb;\"")

        cur = conn.cursor()
        query = "SELECT [Holiday] FROM [Holidays]"
        cur.execute(query)

        while True:
            row = cur.fetchone()
            if row is None:
                break
            else:
                self.holidays.append(row[0].strftime('%m/%d/%Y'))
        cur.close()
        conn.close()
        #self.holidays.append(datetime.datetime(2017, 12, 4, 0, 0).strftime('%m/%d/%Y'))
        #self.holidays.append(datetime.datetime(2017, 12, 7, 0, 0).strftime('%m/%d/%Y'))
        #self.holidays.append(datetime.datetime(2017, 12, 8, 0, 0).strftime('%m/%d/%Y'))
        print(self.holidays)
        print(self.ticket_details["Priority"])

        if self.ticket_details["Priority"] != "High" and self.ticket_details["Priority"] != "Medium" and self.ticket_details["Priority"] != "Low":
            print("Priority for Ticket is not correct : " + self.ticket_details["Priority"])
            logging.warning("Priority for Ticket is not correct : " + self.ticket_details["Priority"])
        else:
            add_days = 0
            if self.ticket_details["Priority"] == "High":
                add_days = 2
            elif self.ticket_details["Priority"] == "Medium":
                add_days = 4
            elif self.ticket_details["Priority"] == "Low":
            #elif self.ticket_details["Priority"] == "P2-Moderate":
                add_days = 7

            business_days_to_add = add_days
            current_date = datetime.date.today()
            while business_days_to_add > 0:
                current_date += datetime.timedelta(days=1)
                weekday = current_date.weekday()
                #print(current_date)
                if weekday >= 5: # Saturday = 5, sunday = 6
                    #print("Weekend")
                    continue
                elif current_date.strftime('%m/%d/%Y') in self.holidays:
                    #print("Holiday")
                    continue
                business_days_to_add -= 1
            print("Delivery Date : " + str(current_date))
            self.ticket_details["Delivery Date"] = current_date.strftime('%m/%d/%Y')
            print(self.ticket_details)
            return 0
        return 1


    def get_new_ticket_number(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")

        cur = conn.cursor()
        query = "SELECT [Ticket Number] FROM [CM Ticket] ORDER BY [Ticket Number] DESC"
        cur.execute(query)

        while True:
            row = cur.fetchone()
            if row is None:
                break
            else:
                cur.close()
                conn.close()
                self.ticket_details["Ticker Number"] = int(row[0]) + 1
                print("Ticket Number : " + str(self.ticket_details["Ticker Number"]))
                return
        cur.close()
        conn.close()


for file in glob.glob(Amdocs_file_path + "*.xlsx"):
    succ = 0
    if "CMTicketCreated" not in file:
        read_amdocs = read_amdocs_case(file)
        if read_amdocs.process_amdocs_Case_file() == 1:
            succ = 1

    if succ != 1:
        print(os.path.dirname(file) + "\\CMTicketCreated_" + ntpath.basename(file))
        #os.rename(file, os.path.dirname(file) + "\\CMTicketCreated_" + ntpath.basename(file))

