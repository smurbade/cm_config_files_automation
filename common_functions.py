import glob

def validate_tgz(path, client, prodtype, dev, env):
    ###### Start Naming convention
    path = "extract"
    filetype = ""

    if "mx" in dev.lower():
        filetype = "*.tgz"
    elif "vx" in dev.lower():
        filetype = "*.zip"

    if (len(glob.glob(path + '\\' + filetype))) > 1:
        return 20
    elif (len(glob.glob(path + '\\' + filetype))) == 0:
        return 21
    elif (len(glob.glob(path + '\\' + filetype))) == 1:
        tgzname = str(glob.glob(path + '\\' + filetype))
        if dev[:5].lower() not in tgzname.lower() or env.lower() not in tgzname.lower():
            return 22
    return 0
    ###### End Naming convention


