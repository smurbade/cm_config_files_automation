# 1. Delete existing *Paasadvertisements* (old naming convention for media files) and zadv.tgz (new naming convention for media files) files from package
# 2. If device is MX915 then copy 915 media template and rename to zadv.tgz
# 3. If device is MX925 then copy 925 media template and rename to zadv.tgz

#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
if [ "$1" -ne "915" ] && [ "$1" -ne "925" ]
then
	echo "Usage : media.sh 915/925"
	exit
fi
cd extract
rm -rf tmp
mkdir tmp
tgzfile="`ls *.tgz`"

tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
if [[ -n `find . -maxdepth 1 -iname "PaaSAdvertisements*.tgz"` ]]; 
then
	rm PaaSAdvertisements*.tgz
fi

if [[ -n `find . -maxdepth 1 -iname "zadv.tgz"` ]]; 
then
	rm zadv.tgz
fi

if [ "$1" -eq "915" ]
then
	cp ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/MediaFiles/*915*.tgz zadv.tgz
elif [ "$1" -eq "925"  ]
then
	cp ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/MediaFiles/*925*.tgz zadv.tgz
fi

tar -zcvf "$tgzfile" * >> /dev/null
mv "$tgzfile" ../
cd ../
sleep 3s
rm -rf tmp/

