#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
cd extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`
tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
mkdir tmp
vhqtgz=`ls *VHQconfig*.tgz`
tar -zxvf *VHQconfig*.tgz -C tmp/ >> /dev/null
cd tmp/
mkdir tmp
vhqtar=`ls VHQconfig*.tar`
tar -xvf VHQconfig*.tar -C tmp/ >> /dev/null
cd tmp/
cp ../../../$1 .
tar -cvf $vhqtar * >> /dev/null
mv $vhqtar ../
cd ../
rm -rf tmp/
tar -zcvf $vhqtgz * >> /dev/null
mv $vhqtgz ../
cd ../
rm -rf tmp
tar -zcvf $tgzfile * >> /dev/null
mv $tgzfile ../
cd ../
rm -rf tmp/
rm $1 
