#!/bin/bash
logfile="`pwd`/logs/cybersource.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
shopt -s nocasematch

#add_missing_parameter "signaturelimit" "CONTROL/config.usr1" "stb" "$3"
filechanged=0
log_details=""

add_missing_parameter()
{
	if [ "$1" == "dayslimit" ]
	then
		dayslimitfound=`grep -i "dayslimit" $2 | grep -iv ".dayslimit" | wc -l`
		#echo $dayslimitfound
		if [ $dayslimitfound -eq "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			sed -i '/safdayslimit/!s/dayslimit/dayslimit=1=/' $2
			sed -i "/dayslimit=1=/c\dayslimit=$4" $2
			msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
			log_details="${log_details}"$'\n'$msg
		fi
	elif [ "$1" == "cash" ]
	then
		cashfound=`grep -i "cash" $2 | grep -iv "cash." | wc -l`
		if [ $cashfound -eq "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			sed -i '/cashback/!s/cash/cash=1=/' $2
			sed -i "/cash=1=/c\cash=$4" $2
			msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
			log_details="${log_details}"$'\n'$msg
		fi
	elif [ "$1" == "gift" -o "$1" == "GIFT" ]
	then
		cashfound=`grep -i "gift" $2 | grep -iv ".gift." | wc -l`
		if [ $cashfound -eq "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			sed -i '/allowgiftactivatetosaf/!s/gift/gift=1=/' $2
			sed -i "/gift=1=/c\gift=$4" $2
			msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
			log_details="${log_details}"$'\n'$msg
		fi
	else
		grep -i -q "$1" $2
		if [ $? -ne "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				#echo "added $1"
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			#echo "$1 Found"
			grep -i -q "\[$1\]" $2 # make sure that if there is a section of same name as parameter so it can be excluded
			if [ $? -ne "0" ]
			then
				#echo "$1 section of same name as parameter not found"
				#sed -i "/"$1"/Ic $1=$4" $2
				sed -i "/^[[:space:]]*$1[[:space:]]*\=.*/Ic $1 = $4" $2
				#echo "updated $1"
				msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		fi
	fi
	
}


update_config_files()
{

	#if [ -f CONTROL/config.usr1 ]
	if [[ -n `find . -maxdepth 2 -iname config.usr1` ]];
	then
		log_details="${log_details}"$'\n\n'"#######################################################################"
		log_details="${log_details}"$'\n'"config.usr1 :"
		sed -i "/sigcapture/c\sigcapture=$2" CONTROL/config.usr1
		log_details="${log_details}"$'\n'"sigcapture value is set to ${2} in CONTROL/config.usr1"
		if [ $2 == "Y" ]
		then
			if [ "$3" != "" ]; then
				sed -i "/signaturelimit/c\signaturelimit=$3" CONTROL/config.usr1
				add_missing_parameter "signaturelimit" "CONTROL/config.usr1" "stb" "$3"
			fi
		fi
		sed -i "/safenabled/c\safenabled=$4" CONTROL/config.usr1
		log_details="${log_details}"$'\n'"safenabled value is set to ${4} in CONTROL/config.usr1"
		if [ $4 == "Y" ]
		then
			if [ "$5" != "" ]; then
				sed -i "/transactionfloorlimit/c\transactionfloorlimit=$5" CONTROL/config.usr1
				add_missing_parameter "transactionfloorlimit" "CONTROL/config.usr1" "saf" "$5"
			fi
			if [ "$6" != "" ]; then
				sed -i "/totalfloorlimit/c\totalfloorlimit=$6" CONTROL/config.usr1
				add_missing_parameter "totalfloorlimit" "CONTROL/config.usr1" "saf" "$6"
			fi
			if [ "$7" != "" ]; then
				sed -i '/safdayslimit/!s/dayslimit/dayslimit=1=/' CONTROL/config.usr1
				sed -i "/dayslimit=1=/c\dayslimit=$7" CONTROL/config.usr1
				add_missing_parameter "dayslimit" "CONTROL/config.usr1" "saf" "$7"
			fi
		fi
		sed -i "/lineitem_display/c\lineitem_display=$8" CONTROL/config.usr1
		add_missing_parameter "lineitem_display" "CONTROL/config.usr1" "dct" "$8"
		filechanged=1
		grep sigcapture CONTROL/config.usr1 >> /dev/null
		grep signaturelimit CONTROL/config.usr1 >> /dev/null
		grep safenabled CONTROL/config.usr1 >> /dev/null
		grep transactionfloorlimit CONTROL/config.usr1 >> /dev/null
		grep totalfloorlimit CONTROL/config.usr1 >> /dev/null
		grep dayslimit CONTROL/config.usr1 >> /dev/null
		grep lineitem_display CONTROL/config.usr1 >> /dev/null
		#cp CONTROL/config.usr1 ../../../
		#echo "Found config.usr1 -----------"
	fi
	
	if [[ -n `find . -maxdepth 1 -iname vhqconfig.ini` ]]; 
	then
		if [ "$9" != "" ]; then
			log_details="${log_details}"$'\n\n'"#######################################################################"
			log_details="${log_details}"$'\n'"vhqconfig.ini :"
			sed -i "/CustomerId/c\CustomerId=$9" vhqconfig.ini
			add_missing_parameter "CustomerId" "vhqconfig.ini" "vhq" "$9"
			filechanged=1
			#echo "Found VHQconfig.ini ---------";
			grep CustomerId VHQconfig.ini >> /dev/null
			#cp vhqconfig.ini ../../../
		fi
	fi
			
	if [[ -n `find . -maxdepth 1 -iname aidlist.ini` ]]; 
	then 
		log_details="${log_details}"$'\n\n'"#######################################################################"
		log_details="${log_details}"$'\n'"aidlist.ini :"
		if [ $2 == "Y" ]
		then
			sed -i '/#/!s/capsignonpinbypass/capsignonpinbypass=1=/' AIDList.ini
			sed -i "/capsignonpinbypass=1=/c\capsignonpinbypass=1" AIDList.ini
			log_details="${log_details}"$'\n'"capsignonpinbypass value set to 1 in aidlist.ini"
			if [ "$3" != "" ]; then
				sed -i '/#/!s/signaturelimit/signaturelimit=1=/' AIDList.ini
				sed -i "/signaturelimit=1=/c\signaturelimit=$3" AIDList.ini
				log_details="${log_details}"$'\n'"signaturelimit value set to ${3} in aidlist.ini"
			fi
		fi
		if [ $2 == "N" ]
		then
			sed -i '/#/!s/capsignonpinbypass/capsignonpinbypass=0=/' AIDList.ini
			sed -i "/capsignonpinbypass=0=/c\capsignonpinbypass=0" AIDList.ini
			log_details="${log_details}"$'\n'"capsignonpinbypass value set to 0 in aidlist.ini"
		fi
		filechanged=1
		#echo "Found AIDList.ini ---------";
		#grep capsignonpinbypass aidlist.ini
		#grep signaturelimit aidlist.ini
		#cp aidlist.ini ../../../
	fi

	if [[ -n `find . -maxdepth 1 -iname propertiesdata.xml` ]]; 
	then
		if [ $2 == "Y" ]
		then
			#echo "Updating propertiesdata.xml in $foldername3 **********"
			log_details="${log_details}"$'\n\n'"#######################################################################"
			log_details="${log_details}"$'\n'"propertiesdata.xml :"
			sed -i -e "/<name>safenabled<\/name>/I,/<\/value>/ s/<value>.*$/<value>1<\/value>/g;"  propertiesdata.xml
			log_details="${log_details}"$'\n'"safenabled is set to 1 in propertiesdata.xml :"
			if [ "$3" != "" ]; then
				sed -i -e "/<name>TransactionFloorLimit<\/name>/I,/<\/value>/ s/<value>.*$/<value>$3<\/value>/g;"  propertiesdata.xml
				log_details="${log_details}"$'\n'"TransactionFloorLimit is set to ${3} in propertiesdata.xml :"
			fi
			if [ "$4" != "" ]; then
				sed -i -e "/<name>TotalFloorLimit<\/name>/I,/<\/value>/ s/<value>.*$/<value>$4<\/value>/g;"  propertiesdata.xml
				log_details="${log_details}"$'\n'"TotalFloorLimit is set to ${4} in propertiesdata.xml :"
			fi
			if [ "$5" != "" ]; then
				sed -i -e "/<name>DaysLimit<\/name>/I,/<\/value>/ s/<value>.*$/<value>$5<\/value>/g;"  propertiesdata.xml
				log_details="${log_details}"$'\n'"DaysLimit is set to ${5} in propertiesdata.xml :"
			fi
			filechanged=1
		elif [ $2 == "N" ]
		then
			#echo "Updating propertiesdata.xml in $foldername3 **********"
			sed -i -e "/<name>safenabled<\/name>/I,/<\/value>/ s/<value>.*$/<value>0<\/value>/g;"  propertiesdata.xml
			log_details="${log_details}"$'\n'"safenabled is set to 0 in propertiesdata.xml :"
			filechanged=1
			#echo "updated safenabled to 0"
		fi
		#cp propertiesdata.xml ../../../../
	fi
			
}


if [ "$1" = "MX" ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	for filename in *.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		filechanged=0
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"
			#if [ $# -eq "10" -a "${filename2#*$image}" != "$filename2" ]
			if [ $# -eq "11" -a \( "${filename2#*"image"}" != "$filename2" -o "${filename2#*"Image"}" != "$filename2" -o "${filename2#*"IMAGE"}" != "$filename2"  \) ]
			then
				#echo "10th argument provided in $filename2"
				pngfilename=""${10}"_scaidleimage.png"
				if [ -f "${11}" ]
				then
					rm "915_scaidleimage.png" "925_scaidleimage.png" 2>> /dev/null
					ls "${11}"
					cp "${11}" $pngfilename
					if [ $? -eq "0" ]
					then
						filechanged=1
					fi
				fi
			fi

			update_config_files "$@"

			if [ $filechanged -eq 1 ];
			then
				maintgztobechanged=1
				echo "tarring $filename2 ***************************"
				tar -cvf "$filename2" * >> /dev/null
				mv "$filename2" ../
			fi

			cd ../
			rm -rf tmp
			
		done

		if [ $filechanged -eq 1 ];
		then
			echo "tgzing $filename ========================="
			tar -zcvf "$filename" * >> /dev/null
			mv "$filename" ../
		fi

		cd ../
		rm -rf tmp
		
		echo `pwd`
		echo "#########################"
		
	done

	if [ $maintgztobechanged -eq 1 ];
	then
		echo "tgzing $tgzfile ========================="
		tar -zcvf "$tgzfile" * >> /dev/null
		mv "$tgzfile" ../
	fi

	cd ../
	sleep 3s
	rm -rf tmp

#fi
elif [ "$1" = "VX"  ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=""
	zipfile=""
	if ls *.tgz 2>> /dev/null
	then
		tgzfile=`ls *.tgz`
		echo "tgzfile = $tgzfile"
		tar -zxvf *.tgz -C tmp >> /dev/null
	elif ls *.zip >> /dev/null
	then
		zipfile=`ls *.zip`
		echo "zipfile = $zipfile"
		unzip *.zip -d tmp >> /dev/null
	fi
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	innerziptobechanged=0
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			filechanged=0
			unzip "$zipfile2" -d tmp
			cd tmp
				for zipfile3 in *.zip; do
					filechanged=0
					rm -rf tmp
					mkdir tmp
					unzip "$zipfile3" -d tmp
					cd tmp
					for foldername3 in */ ; do
						if [ "$foldername3" != "*/" ]
						then
							echo $foldername3
							cd "$foldername3"
							
							update_config_files "$@"

							if [ $filechanged -eq 1 ]
							then
								innerziptobechanged=1
								maintgztobechanged=1
							fi
							cd ../
						fi
					done
					echo "This is for zipfile 3 : $zipfile3"
					if [ $filechanged -eq 1 ]
					then
						if [ -f "../$zipfile3" ]
						then
							7z a "$zipfile3" *
							mv "$zipfile3" ../
						fi
					fi
					cd ../
					rm -rf tmp
				
				done
				for foldername2 in */ ; do
					if [ "$foldername2" != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"
						
						#process_vhqconfigini "vhq" "CustomerId" $6
						
						update_config_files "$@"
						
						if [ $filechanged -eq 1 ]
						then
							innerziptobechanged=1
							maintgztobechanged=1
						fi
						cd ../
					fi
				done
			echo "This is for zipfile 2 : $zipfile2"

			if [ $innerziptobechanged -eq 1 ]
			then
				if [ -f "../$zipfile2" ]
				then
					7z a "$zipfile2" *
					mv "$zipfile2" ../
				fi
			fi
			cd ../
			rm -rf tmp
		done
		cd ../
	done
	if [ $maintgztobechanged -eq 1 ]
	then
		echo "This is for zipfile : $zipfile"
	
		if [ "$zipfile" != "" ]
		then
			if [ -f "../$zipfile" ]
			then
				7z a "$zipfile" *
				mv "$zipfile" ../
			fi
		elif [ "$tgzfile" != "" ]
		then
			if [ -f ../$tgzfile ]
			then
				echo "tgzing $tgzfile ========================="
				tar -zcvf "$tgzfile" * >> /dev/null
				mv "$tgzfile" ../
			fi
		fi
	fi
	cd ../
	sleep 3s
	rm -rf tmp
fi

echo -e "${log_details}" >> "$logfile"
