#!/bin/bash
logfile="`pwd`/logs/initial_update_configuration.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
shopt -s nocasematch
log_details=""

#add_missing_parameter $parameter $filename $section $value
add_missing_parameter()
{
	if [ "$1" == "dayslimit" ]
	then
		dayslimitfound=`grep -i "dayslimit" $2 | grep -iv ".dayslimit" | wc -l`
		#echo $dayslimitfound
		if [ $dayslimitfound -eq "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			sed -i '/safdayslimit/!s/dayslimit/dayslimit=1=/' $2
			sed -i "/dayslimit=1=/c\dayslimit=$4" $2
			msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
			log_details="${log_details}"$'\n'$msg
		fi
	elif [ "$1" == "cash" ]
	then
		cashfound=`grep -i "cash" $2 | grep -iv "cash." | wc -l`
		if [ $cashfound -eq "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			sed -i '/cashback/!s/cash/cash=1=/' $2
			sed -i "/cash=1=/c\cash=$4" $2
			msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
			log_details="${log_details}"$'\n'$msg
		fi
	elif [ "$1" == "gift" -o "$1" == "GIFT" ]
	then
		cashfound=`grep -i "gift" $2 | grep -iv ".gift." | wc -l`
		if [ $cashfound -eq "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			sed -i '/allowgiftactivatetosaf/!s/gift/gift=1=/' $2
			sed -i "/gift=1=/c\gift=$4" $2
			msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
			log_details="${log_details}"$'\n'$msg
		fi
	else
		grep -i -q "$1" $2
		if [ $? -ne "0" ]
		then
			#echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				#echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
				log_details="${log_details}"$'\n'"Section ${3} added in ${2}"
				msg=$(echo "Parameter ${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				#echo "added $1"
				msg=$(echo "${1} is added under ${3} and set to ${4} in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		else
			#echo "$1 Found"
			grep -i -q "\[$1\]" $2 # make sure that if there is a section of same name as parameter so it can be excluded
			if [ $? -ne "0" ]
			then
				#echo "$1 section of same name as parameter not found"
				sed -i "/"$1"/Ic $1=$4" $2
				#echo "updated $1"
				msg=$(echo "${1} value set to $4 in ${2}" | tr -d '\n' | tr -d '\r')
				log_details="${log_details}"$'\n'$msg
			fi
		fi
	fi
	
}

update_config_files()
{
			if [[ -n `find . -maxdepth 2 -iname config.usr1` ]];
			then
				if [[ "$@" =~ "config" || "$@" =~ "pinbypass" ]]
				then
					log_details="${log_details}"$'\n\n'"#######################################################################"
					log_details="${log_details}"$'\n'"config.usr1 :"
				fi
				if [[ "$@" =~ "config" ]]
				then
					if [[ "$@" =~ "initial" ]]
					then
						truncate -s 0 CONTROL/config.usr1
						log_details="${log_details}"$'\n'"config.usr1 contents are deleted since Initial Configuration selected in GUI. Parameters will be inserted as per the CM Form"
					fi
					#echo "Found config.usr1"
					if [ -f ../../../configusr1updatepackage ]
					then
						#echo "Found configusr1updatepackage"
						while IFS=: read param filee sect val;do
							#echo $param $filee $sect $val
							add_missing_parameter $param $filee $sect $val
							filechanged=1
						done < ../../../configusr1updatepackage
					fi
					if [[ "$@" =~ "EMVFDNON" ]]
					then
						log_details="${log_details}"$'\n'"Since CM Request is for Non FD and EMV is Yes, below four parameters are being changed in CONTROL/config.usr1"
						add_missing_parameter "Emvsetupreqd" "CONTROL/config.usr1" "dct" "1"
						add_missing_parameter "EMVEnabled" "CONTROL/config.usr1" "dct" "Y"
						add_missing_parameter "contactlessEMVEnabled" "CONTROL/config.usr1" "dct" "N"
						sed -i '/clearexpiryin5f24tag/Id' CONTROL/config.usr1
						log_details="${log_details}"$'\n'"Deleting parameter clearexpiryin5f24tag from CONTROL/config.usr1"
					elif [[ "$@" =~ "EMV-FD" ]]
					then
						log_details="${log_details}"$'\n'"Since CM Request is for FD and EMV is Yes, below four parameters are being changed in CONTROL/config.usr1"
						add_missing_parameter "Emvsetupreqd" "CONTROL/config.usr1" "dct" "1"
						add_missing_parameter "EMVEnabled" "CONTROL/config.usr1" "dct" "Y"
						add_missing_parameter "contactlessEMVEnabled" "CONTROL/config.usr1" "dct" "N"
						add_missing_parameter "clearexpiryin5f24tag" "CONTROL/config.usr1" "dct" "Y"
					elif [[ "$@" =~ "EMVN" ]]
					then
						log_details="${log_details}"$'\n'"Since EMV is No in CM Request, below four parameters are being changed in CONTROL/config.usr1"
						sed -i '/Emvsetupreqd/Id' CONTROL/config.usr1
						add_missing_parameter "EMVEnabled" "CONTROL/config.usr1" "dct" "N"
						sed -i '/contactlessEMVEnabled/Id' CONTROL/config.usr1
						sed -i '/clearexpiryin5f24tag/Id' CONTROL/config.usr1
						log_details="${log_details}"$'\n'"Deleting parameters Emvsetupreqd, contactlessEMVEnabled, clearexpiryin5f24tag from CONTROL/config.usr1"
					fi
				fi
				if [[ "$@" =~ "pinbypass" ]]
				then
					add_missing_parameter "emv_pinbypass" "CONTROL/config.usr1" "iab" "1"
					filechanged=1
				fi
			fi
			if [[ -n `find . -maxdepth 2 -iname config.usr1` ]];
			then
				if [[ "$@" =~ "config" ]]
				then
					#echo "Found config.usr1"
					if [ -f ../../../configusr1updatepackage ]
					then
						grep -i safenabled CONTROL/config.usr1 | tr -d '\040\011\012\015' | grep -i safenabled=n >> /dev/null
						if [ $? -eq "0" ]
						then
							log_details="${log_details}"$'\n'"Since SAF enabled is No so Total Floor Limit, Transaction Floor Limit, Offline days limit, SAF MAximum pending is set to blank"
							#echo "Since SAF enabled is No so Total Floor Limit, Transaction Floor Limit, Offline days limit, SAF MAximum pending is being made blank"
							sed -i "/totalfloorlimit/c\totalfloorlimit=" CONTROL/config.usr1
							sed -i "/transactionfloorlimit/c\transactionfloorlimit=" CONTROL/config.usr1
							sed -i "/dayslimit/c\dayslimit=" CONTROL/config.usr1
							sed -i "/SAFMaxPending/c\SAFMaxPending=" CONTROL/config.usr1
						fi
					fi
				fi
			fi
			
			if [[ -n `find . -maxdepth 1 -iname cdt.txt` ]];
			then
				if [[ "$@" =~ "cdt" ]]
				then
					#echo "Found cdt.txt"
					siglimit=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/cdt/)print $(i+1)}'` # If 'cdt is found in arguments to program then get the next argument which is Signature Limit parameter value'
					numparameter=`grep -i "cardlabel" cdt.txt | grep -o "," | wc -l` # count number of parameters by counting number of commas in cdt.txt
					numparameter=$(expr "$numparameter" + "1")
					signlimitline=""
					i=1
					if [ $siglimit != "-" ]; then
						log_details="${log_details}"$'\n\n'"#######################################################################"
						log_details="${log_details}"$'\n'"cdt.txt :"
						while [ "$i" -le "$numparameter" ]; do
							#echo "in cdt loop $i"
							signlimitline="$signlimitline, $siglimit"
							i=$(($i + 1))
						done
						signlimitline=`echo $signlimitline | cut -c3-` # remove first 2 characters
						log_details="${log_details}"$'\n'"SignLimitAmount is updated to $siglimit"
						grep -i SignLimitAmount cdt.txt >> /dev/null
						if [ $? -eq "0" ]
						then
							sed -i "/SignLimitAmount/c\SignLimitAmount $signlimitline" cdt.txt
						else
							sed -i "/ReceiptLimitAmount/Ia SignLimitAmount $signlimitline" cdt.txt
						fi
						#add_missing_parameter "CustomerId" "EMVTables.ini" "vhq" "$9"
						filechanged=1
						grep SignLimitAmount cdt.txt >> /dev/null
					fi
				fi
			fi
			if [[ -n `find . -maxdepth 1 -iname cdt.ini` ]];
			then
				if [[ "$@" =~ "cdt" ]]
				then
					echo "Found cdt.ini"
					siglimit=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/cdt/)print $(i+1)}'` # If 'cdt is found in arguments to program then get the next argument which is Signature Limit parameter value'
					saflimitbycardtype=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/cdt/)print $(i+2)}'` # If 'cdt is found in arguments to program then get the next argument which is SAF Limit by Card Type parameter value'

					log_details="${log_details}"$'\n\n'"#######################################################################"
					log_details="${log_details}"$'\n'"cdt.ini :"
					if [ $siglimit != "-" ]; then
						grep -i SignatureLimitAmount cdt.ini
						if [ $? -eq "0" ]
						then
							sed -i "/SignatureLimitAmount/c\SignatureLimitAmount=$siglimit" cdt.ini
							log_details="${log_details}"$'\n'"SignatureLimitAmount is updated to $siglimit"
						else
							sed -i "/ReceiptLimitAmount/Ia SignatureLimitAmount=$siglimit" cdt.ini
							log_details="${log_details}"$'\n'"SignatureLimitAmount is added and set to $siglimit"
						fi
						#add_missing_parameter "CustomerId" "EMVTables.ini" "vhq" "$9"
						filechanged=1
					fi
					if [ $saflimitbycardtype != "-" ]; then
						grep -i SAFLimit cdt.ini
						if [ $? -eq "0" ]
						then
							sed -i "/SAFLimit/c\SAFLimit=$saflimitbycardtype" cdt.ini
							log_details="${log_details}"$'\n'"SAFLimit is updated to $saflimitbycardtype"
						else
							sed -i "/LUHN/Ia SAFLimit=$saflimitbycardtype" cdt.ini
							log_details="${log_details}"$'\n'"SAFLimit is added and set to $saflimitbycardtype"
						fi
						filechanged=1
					fi
					grep SignatureLimitAmount cdt.ini >> /dev/null
					grep SAFLimit cdt.ini >> /dev/null
				fi
			fi
			if [[ -n `find . -maxdepth 1 -iname aidlist.txt` ]];
			then
				if [[ "$@" =~ "pinbypass" ]]
				then
					#echo "Found aidlist.txt"
					log_details="${log_details}"$'\n\n'"#######################################################################"
					log_details="${log_details}"$'\n'"AidList.txt :"
					sed -i 's/980840.*VISA/980840\|2\|VISA/g' aidlist.txt
					sed -i 's/42203.*MASTERCARD/42203\|2\|MASTERCARD/g' aidlist.txt
					log_details="${log_details}"$'\n'"Payment Type parameter for VISA (AID - 980840) and MASTERCARD (AID - 42203) is set to 2"
					filechanged=1
					#grep "980840" aidlist.txt
					#grep "42203" aidlist.txt
				fi
			fi
			if [[ -n `find . -maxdepth 1 -iname aidlist.ini` ]];
			then
				if [[ "$@" =~ "pinbypass" ]];
				then
					log_details="${log_details}"$'\n\n'"#######################################################################"
					log_details="${log_details}"$'\n'"AidList.ini :"
					#echo "Found aidlist.ini"
					equalto_position=`grep "^[^#]" AIDList.ini  | grep -i cardabbrv |  grep -aob '=' | grep -oE '[0-9]+' | head -1`
					#echo "$paymenttypespacecount"
					paymenttypespacecount=$(expr $equalto_position - 11)
					paymenttypespaces=""
					for count in $(seq 1 $paymenttypespacecount)
					do
						paymenttypespaces=$paymenttypespaces" "
					done
					paymentline="paymenttype"$paymenttypespaces"= 2"
					#echo "$paymentline"
					#sed -i '/980840.config\]$/,/^\[/{/\<paymenttype\>/d} ; /980840.config\]$/Ia paymenttype= 2' aidlist.ini # remove paymenttype in .Config section and then add "paymenttype=2" right after .config section
					#sed -i '/42203.config\]$/,/^\[/{/\<paymenttype\>/d} ; /42203.config\]$/Ia paymenttype = 2' aidlist.ini # remove paymenttype in .Config section and then add "paymenttype=2" right after .config section
					sed -i "/980840.config\]$/,/^\[/{/\<paymenttype\>/d} ; /980840.config\]$/Ia $paymentline" aidlist.ini # remove paymenttype in .Config section and then add "paymenttype=2" right after .config section
					sed -i "/42203.config\]$/,/^\[/{/\<paymenttype\>/d} ; /42203.config\]$/Ia $paymentline" aidlist.ini # remove paymenttype in .Config section and then add "paymenttype=2" right after .config section
					log_details="${log_details}"$'\n'"Payment Type parameter for VISA (AID - 980840) and MASTERCARD (AID - 42203) is set to 2"
					filechanged=1
					grep -A 1 "980840.config" aidlist.ini >> /dev/null
					grep -A 1 "42203.config" aidlist.ini >> /dev/null
				fi
				if [[ "$@" =~ "aidlist" ]];
				then
					#echo "Found aidlist.ini"
					log_details="${log_details}"$'\n\n'"#######################################################################"
					log_details="${log_details}"$'\n'"AidList.ini :"
					siglimitvisa=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+1)}'` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'
					siglimitamex=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+2)}'` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Amex parameter value'
					siglimitmc=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+3)}'` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Mastercard parameter value'
					siglimitdiscover=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+4)}'` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Discover parameter value'
					siglimitjcb=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+5)}'` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit JCB parameter value'
					siglimitdiners=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+6)}'` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Diners parameter value'
					aidlist_capsignonpinbypass=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+7)}'` # If 'aidlist is found in arguments to program then get the next argument which is Pin Bypass value'
					aidlist_cashbackenabled=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/aidlist/)print $(i+8)}'` # If 'aidlist is found in arguments to program then get the next argument which is Cashback by AID parameter value'
					equalto_position=`grep "^[^#]" AIDList.ini  | grep -i cardabbrv |  grep -aob '=' | grep -oE '[0-9]+' | head -1`
					siglimitspacecount=$(expr $equalto_position - 14)
					siglimitspaces=""
					for count in $(seq 1 $siglimitspacecount)
					do
						siglimitspaces=$siglimitspaces" "
					done

					capsignonspacecount=$(expr $equalto_position - 18)
					capsignonspaces=""
					for count in $(seq 1 $capsignonspacecount)
					do
						capsignonspaces=$capsignonspaces" "
					done
					cashbackenabledspacecount=$(expr $equalto_position - 15)
					cashbackenabledspaces=""
					for count in $(seq 1 $cashbackenabledspacecount)
					do
						cashbackenabledspaces=$cashbackenabledspaces" "
					done
					#echo "All Signature limits :  $siglimitvisa ; $siglimitamex : $siglimitmc : $siglimitdiscover : $siglimitjcb : $siglimitdiners"
					if [ $siglimitvisa != "-" ]; then
						sed -i -e '/VISA/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for VISA is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitamex != "-" ]; then
						sed -i -e '/AMEX/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitamex" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for AMEX is updated to $siglimitamex"
						filechanged=1
					elif [ $siglimitamex == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/AMEX/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for AMEX in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitmc != "-" ]; then
						sed -i -e '/MASTERCARD/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitmc" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for MASTERCARD is updated to $siglimitmc"
						filechanged=1
					elif [ $siglimitmc == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/MASTERCARD/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for MASTERCARD in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitdiscover != "-" ]; then
						sed -i -e '/DISCOVER/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitdiscover" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for DISCOVER is updated to $siglimitdiscover"
						filechanged=1
					elif [ $siglimitdiscover == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/DISCOVER/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for DISCOVER in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitjcb != "-" ]; then
						sed -i -e '/JCB/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitjcb" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for JCB is updated to $siglimitjcb"
						filechanged=1
					elif [ $siglimitjcb == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/JCB/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for JCB in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitdiners != "-" ]; then
						sed -i -e '/DINERS/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitdiners" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for DINERS is updated to $siglimitdiners"
						filechanged=1
					elif [ $siglimitdiners == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/DINERS/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for DINERS in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitvisa != "-" ]; then
						for processor in `grep -i paymentmedia AIDList.ini | tr -d ' ' | grep "^[^#]" | grep -v -i "VISA" | grep -v -i "AMEX" | grep -v -i "DISCOVER" | grep -v -i "MASTERCARD" | grep -v -i "JCB" | grep -v -i "DINERS" | cut -d "=" -f 2`
						do
							sed -i -e "/$processor/!b" -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
							filechanged=1
						done
					fi
					if [ $aidlist_capsignonpinbypass != "-" ]; then
						#sed -i '/^#/!s/capsignonpinbypass.*/c\capsignonpinbypass=$aidlist_capsignonpinbypass/' AIDList.ini
						grep capsignonpinbypass AIDList.ini >> /dev/null
						if [ $? -eq "0" ]
						then
							sed -i "/^capsignonpinbypass/c\capsignonpinbypass$capsignonspaces= $aidlist_capsignonpinbypass" AIDList.ini
							log_details="${log_details}"$'\n'"capsignonpinbypass is updated to $aidlist_capsignonpinbypass"
						else
							sed -i "/signaturelimit/Ia capsignonpinbypass$capsignonspaces= $aidlist_capsignonpinbypass" AIDList.ini
							log_details="${log_details}"$'\n'"capsignonpinbypass is added and set to $aidlist_capsignonpinbypass"
						fi
						#sed -i "/capsignonpinbypass/c\capsignonpinbypass=$aidlist_capsignonpinbypass" AIDList.ini
						filechanged=1
					fi
					if [ $aidlist_cashbackenabled != "-" ]; then
						#sed -i '/^#/!s/cashbackenabled.*/c\cashbackenabled=$aidlist_cashbackenabled/' AIDList.ini
						grep cashbackenabled AIDList.ini >> /dev/null
						if [ $? -eq "0" ]
						then
							sed -i "/^cashbackenabled/c\cashbackenabled$cashbackenabledspaces= $aidlist_cashbackenabled" AIDList.ini
							log_details="${log_details}"$'\n'"cashbackenabled is updated to $aidlist_cashbackenabled"
						else
							sed -i "/paymentmedia/Ia cashbackenabled$cashbackenabledspaces= $aidlist_cashbackenabled" AIDList.ini
							log_details="${log_details}"$'\n'"cashbackenabled is added and set to $aidlist_cashbackenabled"
						fi
						#sed -i "/cashbackenabled/c\cashbackenabled=$aidlist_cashbackenabled" AIDList.ini
						filechanged=1
					fi
					#add_missing_parameter "CustomerId" "EMVTables.ini" "vhq" "$9"
					#grep signaturelimit aidlist.ini 
					#grep capsignonpinbypass aidlist.ini
					#grep cashbackenabled aidlist.ini
					#cp EMVTables.ini ../../../
				fi
			fi

			#echo "$filechanged"
			
}


if [ "$1" = "MX" ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	for filename in *.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		filechanged=0
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"

			#update_config_files $@
			if [[ -n `find . -maxdepth 2 -iname config.usr1` ]];
			then
				if [ -f ../../../configusr1updatepackage ]
				then
					while IFS=: read param filee sect val;do
						#echo $param $filee $sect $val
						add_missing_parameter $param $filee $sect $val
						filechanged=1
					done < ../../../configusr1updatepackage
				fi
			fi

			if [[ -n `find . -maxdepth 1 -iname aidlist.ini` ]];
			then
				if [[ "$@" =~ "aidlist" ]];
				then
					#echo "Found aidlist.ini"
					log_details="${log_details}"$'\n\n'"#######################################################################"
					log_details="${log_details}"$'\n'"AidList.ini :"
					
					siglimitvisa=`echo $@  | grep -o 'SLIMIT01[^ ]*' | cut -d "=" -f 2` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'
					siglimitamex=`echo $@  | grep -o 'SLIMIT01[^ ]*' | cut -d "=" -f 2` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Amex parameter value'
					siglimitmc=`echo $@  | grep -o 'SLIMIT01[^ ]*' | cut -d "=" -f 2` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Mastercard parameter value'
					siglimitdiscover=`echo $@  | grep -o 'SLIMIT01[^ ]*' | cut -d "=" -f 2` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Discover parameter value'
					siglimitjcb=`echo $@  | grep -o 'SLIMIT01[^ ]*' | cut -d "=" -f 2` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit JCB parameter value'
					siglimitdiners=`echo $@  | grep -o 'SLIMIT01[^ ]*' | cut -d "=" -f 2` # If 'aidlist is found in arguments to program then get the next argument which is Signature Limit Diners parameter value'
					equalto_position=`grep "^[^#]" AIDList.ini  | grep -i cardabbrv |  grep -aob '=' | grep -oE '[0-9]+' | head -1`
					siglimitspacecount=$(expr $equalto_position - 14)
					siglimitspaces=""
					for count in $(seq 1 $siglimitspacecount)
					do
						siglimitspaces=$siglimitspaces" "
					done


					#echo "All Signature limits :  $siglimitvisa ; $siglimitamex : $siglimitmc : $siglimitdiscover : $siglimitjcb : $siglimitdiners"
					if [ $siglimitvisa != "-" ]; then
						sed -i -e '/VISA/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for VISA is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitamex != "-" ]; then
						sed -i -e '/AMEX/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitamex" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for AMEX is updated to $siglimitamex"
						filechanged=1
					elif [ $siglimitamex == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/AMEX/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for AMEX in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitmc != "-" ]; then
						sed -i -e '/MASTERCARD/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitmc" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for MASTERCARD is updated to $siglimitmc"
						filechanged=1
					elif [ $siglimitmc == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/MASTERCARD/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for MASTERCARD in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitdiscover != "-" ]; then
						sed -i -e '/DISCOVER/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitdiscover" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for DISCOVER is updated to $siglimitdiscover"
						filechanged=1
					elif [ $siglimitdiscover == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/DISCOVER/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for DISCOVER in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitjcb != "-" ]; then
						sed -i -e '/JCB/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitjcb" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for JCB is updated to $siglimitjcb"
						filechanged=1
					elif [ $siglimitjcb == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/JCB/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for JCB in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitdiners != "-" ]; then
						sed -i -e '/DINERS/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitdiners" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"signaturelimit for DINERS is updated to $siglimitdiners"
						filechanged=1
					elif [ $siglimitdiners == "-" -a $siglimitvisa != "-" ]; then
						sed -i -e '/DINERS/!b' -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
						log_details="${log_details}"$'\n'"Since signaturelimit for DINERS in CM Form is blank, it is updated to $siglimitvisa"
						filechanged=1
					fi
					if [ $siglimitvisa != "-" ]; then
						for processor in `grep -i paymentmedia AIDList.ini | tr -d ' ' | grep "^[^#]" | grep -v -i "VISA" | grep -v -i "AMEX" | grep -v -i "DISCOVER" | grep -v -i "MASTERCARD" | grep -v -i "JCB" | grep -v -i "DINERS" | cut -d "=" -f 2`
						do
							sed -i -e "/$processor/!b" -e ':a' -e "/signaturelimit/c\signaturelimit$siglimitspaces= $siglimitvisa" -e 'n;ba' -e ':trail' -e 'n;btrail' AIDList.ini
							filechanged=1
						done
					fi
				fi
			fi


			if [ $filechanged -eq 1 ];
			then
				maintgztobechanged=1
				echo "tarring $filename2 ***************************"
				tar -cvf "$filename2" * >> /dev/null
				mv "$filename2" ../
			fi

			cd ../
			rm -rf tmp
			
		done

		if [ $filechanged -eq 1 ];
		then
			echo "tgzing $filename ========================="
			tar -zcvf "$filename" * >> /dev/null
			mv "$filename" ../
		fi

		cd ../
		rm -rf tmp
		
		echo `pwd`
		echo "#########################"
		
	done


	if [ $maintgztobechanged -eq 1 ];
	then
		echo "tgzing $tgzfile ========================="
		tar -zcvf "$tgzfile" * >> /dev/null
		mv "$tgzfile" ../
	fi

	cd ../
	sleep 3s
	rm -rf tmp

#fi
elif [ "$1" = "VX"  ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=""
	zipfile=""
	if ls *.tgz 2>> /dev/null
	then
		tgzfile=`ls *.tgz`
		echo "tgzfile = $tgzfile"
		tar -zxvf *.tgz -C tmp >> /dev/null
	elif ls *.zip >> /dev/null
	then
		zipfile=`ls *.zip`
		echo "zipfile = $zipfile"
		unzip *.zip -d tmp >> /dev/null
	fi
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	innerziptobechanged=0
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			filechanged=0
			unzip "$zipfile2" -d tmp
			cd tmp
				for zipfile3 in *.zip; do
					filechanged=0
					rm -rf tmp
					mkdir tmp
					unzip "$zipfile3" -d tmp
					cd tmp
					for foldername3 in */ ; do
						if [ "$foldername3" != "*/" ]
						then
							echo $foldername3
							cd "$foldername3"

							update_config_files $@
							
							if [ $filechanged -eq 1 ]
							then
								innerziptobechanged=1
								maintgztobechanged=1
							fi
							cd ../
						fi
					done
					echo "This is for zipfile 3 : $zipfile3"
					if [ $filechanged -eq 1 ]
					then
						if [ -f "../$zipfile3" ]
						then
							7z a "$zipfile3" *
							mv "$zipfile3" ../
						fi
					fi
					cd ../
					rm -rf tmp
				
				done
				for foldername2 in */ ; do
					if [ "$foldername2" != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"

						update_config_files $@

						if [ $filechanged -eq 1 ]
						then
							innerziptobechanged=1
							maintgztobechanged=1
						fi
						cd ../
					fi
				done
			echo "This is for zipfile 2 : $zipfile2"

			if [ $innerziptobechanged -eq 1 ]
			then
				if [ -f "../$zipfile2" ]
				then
					7z a "$zipfile2" *
					mv "$zipfile2" ../
				fi
			fi
			cd ../
			rm -rf tmp
		done
		cd ../
	done
	if [ $maintgztobechanged -eq 1 ]
	then
		echo "This is for zipfile : $zipfile"
	
		if [ "$zipfile" != "" ]
		then
			if [ -f "../$zipfile" ]
			then
				7z a "$zipfile" *
				mv "$zipfile" ../
			fi
		elif [ "$tgzfile" != "" ]
		then
			if [ -f ../$tgzfile ]
			then
				echo "tgzing $tgzfile ========================="
				tar -zcvf "$tgzfile" * >> /dev/null
				mv "$tgzfile" ../
			fi
		fi
	fi
	cd ../
	sleep 3s
	rm -rf tmp
fi
cd ../
echo -e "${log_details}" >> "$logfile"
