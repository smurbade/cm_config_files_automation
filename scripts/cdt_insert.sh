#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
cd extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`
tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
mkdir tmp
tar -zxvf zUSR1F-1.0.0.tgz -C tmp/ >> /dev/null
cd tmp/
mkdir tmp
tar -xvf zUSR1F-1.0.0.tar -C tmp/ >> /dev/null
cd tmp/
cp ../../../cdt.txt .
tar -cvf zUSR1F-1.0.0.tar * >> /dev/null
mv zUSR1F-1.0.0.tar ../
cd ../
rm -rf tmp/
tar -zcvf zUSR1F-1.0.0.tgz * >> /dev/null
mv zUSR1F-1.0.0.tgz ../
cd ../
rm -rf tmp
tar -zcvf $tgzfile * >> /dev/null
mv $tgzfile ../
cd ../
rm -rf tmp/
rm cdt.txt
