#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
if [ "$1" -ne "915" ] && [ "$1" -ne "925" ]
then
	echo "Usage : media.sh 915/925"
	exit
fi
cd extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`

tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
rm PaaSAdvertisements*.tgz

#gunzip *.tgz
#tarfile=`ls *.tar`
#filetodelete=`tar tf *.tar  | grep PaaSAdvertisements | head -1`
#echo $tgzfile
#echo $tarfile
#echo $filetodelete
#tar --delete --file=$tarfile  $filetodelete

if [ "$1" -eq "915" ]
then
	cp ../../MediaFiles/PaaSAdvertisements-1.0.0-915-sample.tgz PaaSAdvertisements-1.0.0.tgz
	#tar --transform='flags=r;s|MediaFiles/PaaSAdvertisements-1.0.0-915-sample.tgz|PaaSAdvertisements-1.0.0.tgz|' -rvf $tarfile ../MediaFiles/PaaSAdvertisements-1.0.0-915-sample.tgz	
	#tar -rvf $tarfile ../MediaFiles/PaaSAdvertisements-1.0.0-915-sample.tgz
elif [ "$1" -eq "925"  ]
then
	cp ../../MediaFiles/PaaSAdvertisements-1.0.0-925-sample.tgz PaaSAdvertisements-1.0.0.tgz
	#tar --transform='flags=r;s|MediaFiles/PaaSAdvertisements-1.0.0-925-sample.tgz|PaaSAdvertisements-1.0.0.tgz|' -rvf $tarfile ../MediaFiles/PaaSAdvertisements-1.0.0-925-sample.tgz	
	#tar -rvf $tarfile ../MediaFiles/PaaSAdvertisements-1.0.0-925-sample.tgz
fi

tar -zcvf $tgzfile * >> /dev/null
mv $tgzfile ../
cd ../
rm -rf tmp/


#gzip $tarfile
#targzfile=`ls *tar.gz`
#echo $targzfile
#mv $targzfile "$(basename "$targzfile" .tar.gz).tgz"
#cd ../

