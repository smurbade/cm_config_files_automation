#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
cd extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`
tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
mkdir tmp
tar -zxvf zUSR1F-1.0.0.tgz -C tmp/ >> /dev/null
cd tmp/
mkdir tmp
tar -xvf zUSR1F-1.0.0.tar -C tmp/ >> /dev/null
cd tmp/
if [ -f "../../../CAPKData.INI" ]
then
    cp ../../../CAPKData.INI .
fi
if [ -f "../../../EMVTables.INI" ]
then
    cp ../../../EMVTables.INI .
fi
if [ -f "../../../CTLSConfig.INI" ]
then
    cp ../../../CTLSConfig.INI .
fi
if [ -f "../../../MSGXPI.txt" ]
then
    cp ../../../MSGXPI.txt .
fi
tar -cvf zUSR1F-1.0.0.tar * >> /dev/null
mv zUSR1F-1.0.0.tar ../
cd ../
rm -rf tmp/
tar -zcvf zUSR1F-1.0.0.tgz * >> /dev/null
mv zUSR1F-1.0.0.tgz ../
cd ../
rm -rf tmp
tar -zcvf $tgzfile * >> /dev/null
mv $tgzfile ../
cd ../
rm -rf tmp/
#rm CAPKData.INI EMVTables.INI CTLSConfig.INI MSGXPI.txt 
if [ -f "CAPKData.INI" ]
then
    rm CAPKData.INI
fi
if [ -f "EMVTables.INI" ]
then
    rm EMVTables.INI
fi
if [ -f "CTLSConfig.INI" ]
then
    rm CTLSConfig.INI
fi
if [ -f "MSGXPI.txt" ]
then
    rm MSGXPI.txt
fi
