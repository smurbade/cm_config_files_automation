#!/bin/bash
logfile="`pwd`/logs/Create_OTB_Build.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
shopt -s nocasematch

filechanged=0

add_missing_parameter()
{
	if [ "$1" == "dayslimit" ]
	then
		dayslimitfound=`grep -i -F "dayslimit" $2 | grep -Fv "safdayslimit" | wc -l`
		#echo $dayslimitfound
		if [ $dayslimitfound -eq "0" ]
		then
			echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
			fi
		fi
	else
		grep -i -q "$1" $2
		if [ $? -ne "0" ]
		then
			echo "$1 Not Found"
			grep -i -q "\[$3\]" $2
			if [ $? -ne "0" ]
			then
				echo "$3 section not found"
				echo "" >> $2
				echo "[$3]" >> $2 #Adding parameter with section to file
				echo "$1=$4" >> $2 #Adding signaturelimit with stb section to config.usr1 file
			else
				sed -i "/\[$3\]/Ia $1=$4" $2
				echo "added $1"
			fi
		fi
	fi
}


process_vhqconfigini()
{
	if [ -f vhqconfig.ini ]
	then
		if [ "$3" != "" ]; then
			echo "Updting VHQconfig.ini in $foldername2 ----------"
			sed -i "/"$2"/c $2=$3" vhqconfig.ini
			add_missing_parameter $2 "vhqconfig.ini" $1 $3
			filechanged=1
			grep $2 VHQconfig.ini
			#cp vhqconfig.ini ../../../../
		fi
	fi
}

if [ "$1" = "MX" ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	for filename in *.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		filechanged=0
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"
		
			if [[ -n `find . -maxdepth 1 -iname vhqconfig.ini` ]]; 
			then 
				if [ "$2" != "" ]; then
					sed -i "/CustomerId/c\CustomerId=$2" vhqconfig.ini
					add_missing_parameter "CustomerId" "vhqconfig.ini" "vhq" "$2"
					filechanged=1
					echo "Found VHQconfig.ini ---------";
					grep CustomerId VHQconfig.ini
					#cp vhqconfig.ini ../../../
				fi
			fi
					
			if [ $filechanged -eq 1 ];
			then
				maintgztobechanged=1
				echo "tarring $filename2 ***************************"
				tar -cvf "$filename2" * >> /dev/null
				mv "$filename2" ../
			fi

			cd ../
			rm -rf tmp
			
		done

		if [ $filechanged -eq 1 ];
		then
			echo "tgzing $filename ========================="
			tar -zcvf "$filename" * >> /dev/null
			mv "$filename" ../
		fi

		cd ../
		rm -rf tmp
		
		echo `pwd`
		echo "#########################"
		
	done

	if [ $maintgztobechanged -eq 1 ];
	then
		echo "tgzing $tgzfile ========================="
		tar -zcvf "$tgzfile" * >> /dev/null
		mv "$tgzfile" ../
	fi

	cd ../
	sleep 3s
	rm -rf tmp

#fi
elif [ "$1" = "VX"  ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=""
	zipfile=""
	if ls *.tgz 2>> /dev/null
	then
		tgzfile=`ls *.tgz`
		echo "tgzfile = $tgzfile"
		tar -zxvf *.tgz -C tmp >> /dev/null
	elif ls *.zip >> /dev/null
	then
		zipfile=`ls *.zip`
		echo "zipfile = $zipfile"
		unzip *.zip -d tmp >> /dev/null
	fi
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	innerziptobechanged=0
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			filechanged=0
			unzip "$zipfile2" -d tmp
			cd tmp
				for zipfile3 in *.zip; do
					filechanged=0
					rm -rf tmp
					mkdir tmp
					unzip "$zipfile3" -d tmp
					cd tmp
					for foldername3 in */ ; do
						if [ "$foldername3" != "*/" ]
						then
							echo $foldername3
							cd "$foldername3"
							
							if [[ -n `find . -maxdepth 1 -iname vhqconfig.ini` ]]; 
							then 
								if [ "$2" != "" ]; then
									sed -i "/CustomerId/c\CustomerId=$2" vhqconfig.ini
									add_missing_parameter "CustomerId" "vhqconfig.ini" "vhq" "$2"
									filechanged=1
									echo "Found VHQconfig.ini ---------";
									grep CustomerId VHQconfig.ini
									#cp vhqconfig.ini ../../../
								fi
							fi
							
							if [ $filechanged -eq 1 ]
							then
								innerziptobechanged=1
								maintgztobechanged=1
							fi
							cd ../
						fi
					done
					echo "This is for zipfile 3 : $zipfile3"
					if [ $filechanged -eq 1 ]
					then
						if [ -f "../$zipfile3" ]
						then
							7z a "$zipfile3" *
							mv "$zipfile3" ../
						fi
					fi
					cd ../
					rm -rf tmp
				
				done
				for foldername2 in */ ; do
					if [ "$foldername2" != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"
						
						#process_vhqconfigini "vhq" "CustomerId" $6
						
						if [[ -n `find . -maxdepth 1 -iname vhqconfig.ini` ]]; 
						then 
							if [ "$2" != "" ]; then
								sed -i "/CustomerId/c\CustomerId=$2" vhqconfig.ini
								add_missing_parameter "CustomerId" "vhqconfig.ini" "vhq" "$2"
								filechanged=1
								echo "Found VHQconfig.ini ---------";
								grep CustomerId VHQconfig.ini
								#cp vhqconfig.ini ../../../
							fi
						fi
						
						if [ $filechanged -eq 1 ]
						then
							innerziptobechanged=1
							maintgztobechanged=1
						fi
						cd ../
					fi
				done
			echo "This is for zipfile 2 : $zipfile2"

			if [ $innerziptobechanged -eq 1 ]
			then
				if [ -f "../$zipfile2" ]
				then
					7z a "$zipfile2" *
					mv "$zipfile2" ../
				fi
			fi
			cd ../
			rm -rf tmp
		done
		cd ../
	done
	if [ $maintgztobechanged -eq 1 ]
	then
		echo "This is for zipfile : $zipfile"
	
		if [ "$zipfile" != "" ]
		then
			if [ -f "../$zipfile" ]
			then
				7z a "$zipfile" *
				mv "$zipfile" ../
			fi
		elif [ "$tgzfile" != "" ]
		then
			if [ -f ../$tgzfile ]
			then
				echo "tgzing $tgzfile ========================="
				tar -zcvf "$tgzfile" * >> /dev/null
				mv "$tgzfile" ../
			fi
		fi
	fi
	cd ../
	sleep 3s
	rm -rf tmp
fi
