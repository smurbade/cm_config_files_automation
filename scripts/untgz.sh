# extract tgz in extract folder and copy all configuration files inside tgz in extract folder

#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s

if [ "$1" = "MX" ]
then
	rm -rf extract/config.usr1 				    >> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.ini 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.txt 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.xml 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.dat 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.data 					 	>> logs/untgz.log 2>> logs/untgz.log
	cd extract
	if [ $? -eq 0 ]; then
		echo "Extracting files"                               > ../logs/untgz.log 2>> ../logs/untgz.log  
		tgzfile=`ls *.tgz`
		#echo "$tgzfile"
		rm -rf tmp
		mkdir tmp
		echo "extracting tgz's"                              >> ../logs/untgz.log 2>> ../logs/untgz.log
		tar -zxvf *.tgz -C tmp        						 >> ../logs/untgz.log 2>> ../logs/untgz.log
		cd tmp
		
		mkdir tmp
		#echo `pwd`
		echo "extracting tgz's"                              >> ../../logs/untgz.log 2>> ../../logs/untgz.log
		for filename in *.tgz; do
			#echo $filename
			tar -zxvf "$filename" -C tmp/ >> /dev/null		 >> ../../logs/untgz.log 2>> ../../logs/untgz.log
		done
		cd tmp
		
		mkdir tmp
		echo "extracting tar's"                              >> ../../../logs/untgz.log 2>> ../../../logs/untgz.log
		for filename2 in *.tar; do
			#echo $filename2
			tar -xvf "$filename2" -C tmp/ 					 >> ../../../logs/untgz.log 2>> ../../../logs/untgz.log
		done
		cd tmp
		
		cp CONTROL/config.usr1 ../../../					 >> ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log
		cp *.ini ../../../					 				 >> ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log
		cp *.txt ../../../					 				 >> ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log
		cp *.xml ../../../					 				 >> ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log
		cp *.dat ../../../					 				 >> ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log
		cp *.data ../../../					 				 >> ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log

		cd ../../../
		rm -rf tmp											 >> ../logs/untgz.log 2>> ../logs/untgz.log
		cd ../
	else
		echo "Couldn't change to 'extract' directory"         > ../../../../logs/untgz.log 2>> ../../../../logs/untgz.log
	fi
elif [ "$1" = "VX"  ]
then
	rm -rf extract/config.usr1 				    >> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.ini 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.txt 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.xml 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.dat 					 	>> logs/untgz.log 2>> logs/untgz.log
	rm -rf extract/*.data 					 	>> logs/untgz.log 2>> logs/untgz.log
	cd extract
	rm -rf tmp
	mkdir tmp
	zipfile=`ls *.zip`
	echo "zipfile = $zipfile"
	unzip *.zip -d tmp >> /dev/null
	cd tmp/
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			unzip "$zipfile2" -d tmp
			cd tmp
				for foldername2 in */ ; do
					if [ $foldername2 != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"
						cp CONTROL/config.usr1 ../../../../					     >> ../../../../../logs/untgz.log 2>> ../../../../../logs/untgz.log
						cp *.ini ../../../../					 				 >> ../../../../../logs/untgz.log 2>> ../../../../../logs/untgz.log
						cp *.txt ../../../../					 				 >> ../../../../../logs/untgz.log 2>> ../../../../../logs/untgz.log
						cp *.xml ../../../../					 				 >> ../../../../../logs/untgz.log 2>> ../../../../../logs/untgz.log
						cp *.dat ../../../../					 				 >> ../../../../../logs/untgz.log 2>> ../../../../../logs/untgz.log
						cp *.data ../../../../					 				 >> ../../../../../logs/untgz.log 2>> ../../../../../logs/untgz.log
						cd ../
						rm -rf $foldername2
					fi
				done
			cd ../
			rm -rf "$zipfile2"
			rm -rf tmp
		done
		cd ../
		rm -rf $foldername
	done
	cd ../
	echo `pwd`
	rm -rf tmp
fi
