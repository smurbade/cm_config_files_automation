#!/bin/bash
logfile="`pwd`/logs/initial_update_configuration.log"
date > "$logfile"
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
shopt -s nocasematch
log_details=""

if [ "$1" = "MX" ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	for filename in *.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		filechanged=0
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"

            if [[ "$@" =~ "insert" ]]
            then
                confiusr1filename=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/insert/)print $(i+1)}'` # If 'insert' is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'

                if [ $confiusr1filename != "" ]
                then
                    if [[ $confiusr1filename == *"config.usr1"* ]]
                    then
                        if [[ -f CONTROL/config.usr1 ]];
                        then
                            pwd
                            cd CONTROL
                            cp ../../../../config.usr1 config.usr1
							
							# If last non empty line is section then delete it
							awk '/./{line=$0} END{print line}' config.usr1 | awk '{$1=$1;print}' | cut -c1 | grep "\["
							if [ $? -eq "0" ] # if last non empty line is not section
							then
								awk '/./{line=$0} END{print NR-0}' config.usr1
								linenumber="$(awk '/./{line=$0} END{print NR-0}' config.usr1)"
								sed -i -e "$linenumber d" config.usr1 # awk command will give the line number of section which is on last non empty line ... sed will delete that line
								log_details="${log_details}"$'\n'"Deleted section on last non blank line"
							fi
							
                            rm ../../../../config.usr1
                            cd ../
                            filechanged=1
                        fi
                    else
                        if [ -f $confiusr1filename ]
                        then
                            cp ../../../$confiusr1filename $confiusr1filename
                            rm ../../../$confiusr1filename
                            filechanged=1
                        fi
                    fi
                fi
            fi

            if [[ "$@" =~ "extract" ]]
            then
                confiusr1filename=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/extract/)print $(i+1)}'` # If 'extract' is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'
                echo $confiusr1filename

                if [ $confiusr1filename != "" ]
                then
                    if [[ $confiusr1filename == *"config.usr1"* ]]
                    then
                        if [[ -f CONTROL/config.usr1 ]];
                        then
                            pwd
                            cd CONTROL
                            cp config.usr1 ../../../../config.usr1
                            cd ../
                        fi
                    else
                        if [ -f $confiusr1filename ]
                        then
                            cp $confiusr1filename ../../../$confiusr1filename
                        fi
                    fi
                fi
            fi


			if [ $filechanged -eq 1 ];
			then
				maintgztobechanged=1
				echo "tarring $filename2 ***************************"
				tar -cvf "$filename2" * >> /dev/null
				mv "$filename2" ../
			fi

			cd ../
			rm -rf tmp

		done

		if [ $filechanged -eq 1 ];
		then
			echo "tgzing $filename ========================="
			tar -zcvf "$filename" * >> /dev/null
			mv "$filename" ../
		fi

		cd ../
		rm -rf tmp

		echo `pwd`
		echo "#########################"

	done


	if [ $maintgztobechanged -eq 1 ];
	then
		echo "tgzing $tgzfile ========================="
		tar -zcvf "$tgzfile" * >> /dev/null
		mv "$tgzfile" ../
	fi

	cd ../
	sleep 3s
	rm -rf tmp

#fi
elif [ "$1" = "VX"  ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=""
	zipfile=""
	if ls *.tgz 2>> /dev/null
	then
		tgzfile=`ls *.tgz`
		echo "tgzfile = $tgzfile"
		tar -zxvf *.tgz -C tmp >> /dev/null
	elif ls *.zip >> /dev/null
	then
		zipfile=`ls *.zip`
		echo "zipfile = $zipfile"
		unzip *.zip -d tmp >> /dev/null
	fi
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	innerziptobechanged=0
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			filechanged=0
			unzip "$zipfile2" -d tmp
			cd tmp
				for zipfile3 in *.zip; do
					filechanged=0
					rm -rf tmp
					mkdir tmp
					unzip "$zipfile3" -d tmp
					cd tmp
					for foldername3 in */ ; do
						if [ "$foldername3" != "*/" ]
						then
							echo $foldername3
							cd "$foldername3"


                            if [[ "$@" =~ "insert" ]]
                            then
                                confiusr1filename=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/insert/)print $(i+1)}'` # If 'insert' is found in arguments to program then get the next argument which is filename'

                                if [ $confiusr1filename != "" ]
                                then
                                    if [[ $confiusr1filename == *"config.usr1"* ]]
                                    then
                                        if [[ -f CONTROL/config.usr1 ]];
                                        then
                                            pwd
                                            cd CONTROL
                                            cp ../../../../../../config.usr1 config.usr1

											# If last non empty line is section then delete it
											awk '/./{line=$0} END{print line}' config.usr1 | awk '{$1=$1;print}' | cut -c1 | grep "\["
											if [ $? -eq "0" ] # if last non empty line is not section
											then
												awk '/./{line=$0} END{print NR-0}' config.usr1
												linenumber="$(awk '/./{line=$0} END{print NR-0}' config.usr1)"
												sed -i -e "$linenumber d" config.usr1 # awk command will give the line number of section which is on last non empty line ... sed will delete that line
												log_details="${log_details}"$'\n'"Deleted section on last non blank line"
											fi

                                            rm ../../../../../../config.usr1
                                            cd ../
                                            filechanged=1
                                        fi
                                    else
                                        if [ -f $confiusr1filename ]
                                        then
                                            cp ../../../../../$confiusr1filename $confiusr1filename
                                            rm ../../../../../$confiusr1filename
                                            filechanged=1
                                        fi
                                    fi
                                fi
                            fi

                            if [[ "$@" =~ "extract" ]]
                            then
                                confiusr1filename=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/extract/)print $(i+1)}'` # If 'insert' is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'
                                echo $confiusr1filename

                                if [ $confiusr1filename != "" ]
                                then
                                    if [[ $confiusr1filename == *"config.usr1"* ]]
                                    then
                                        if [[ -f CONTROL/config.usr1 ]];
                                        then
                                            pwd
                                            cd CONTROL
                                            cp config.usr1 ../../../../../../config.usr1
                                            cd ../
                                        fi
                                    else
                                        if [ -f $confiusr1filename ]
                                        then
                                            cp $confiusr1filename ../../../../../$confiusr1filename
                                        fi
                                    fi
                                fi
                            fi

							if [ $filechanged -eq 1 ]
							then
								innerziptobechanged=1
								maintgztobechanged=1
							fi
							cd ../
						fi
					done
					echo "This is for zipfile 3 : $zipfile3"
					if [ $filechanged -eq 1 ]
					then
						if [ -f "../$zipfile3" ]
						then
							7z a "$zipfile3" *
							mv "$zipfile3" ../
						fi
					fi
					cd ../
					rm -rf tmp

				done
				for foldername2 in */ ; do
					if [ "$foldername2" != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"


                        if [[ "$@" =~ "insert" ]]
                        then
                            confiusr1filename=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/insert/)print $(i+1)}'` # If 'insert' is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'

                            if [ $confiusr1filename != "" ]
                            then
                                if [[ $confiusr1filename == *"config.usr1"* ]]
                                then
                                    if [[ -f CONTROL/config.usr1 ]];
                                    then
                                        pwd
                                        cd CONTROL
                                        cp ../../../../../config.usr1 config.usr1
                                        rm ../../../../../config.usr1
                                        cd ../
                                        filechanged=1
                                    fi
                                else
                                    if [ -f $confiusr1filename ]
                                    then
                                        cp ../../../../$confiusr1filename $confiusr1filename
                                        rm ../../../../$confiusr1filename
                                        filechanged=1
                                    fi
                                fi
                            fi
                        fi

                        if [[ "$@" =~ "extract" ]]
                        then
                            confiusr1filename=`echo $@  | awk '{for(i=1;i<=NF;i++)if($i~/extract/)print $(i+1)}'` # If 'insert' is found in arguments to program then get the next argument which is Signature Limit Visa parameter value'
                            echo $confiusr1filename

                            if [ $confiusr1filename != "" ]
                            then
                                if [[ $confiusr1filename == *"config.usr1"* ]]
                                then
                                    if [[ -f CONTROL/config.usr1 ]];
                                    then
                                        pwd
                                        cd CONTROL
                                        cp config.usr1 ../../../../../config.usr1
                                        cd ../
                                    fi
                                else
                                    if [ -f $confiusr1filename ]
                                    then
                                        cp $confiusr1filename ../../../../$confiusr1filename
                                    fi
                                fi
                            fi
                        fi

						if [ $filechanged -eq 1 ]
						then
							innerziptobechanged=1
							maintgztobechanged=1
						fi
						cd ../
					fi
				done
			echo "This is for zipfile 2 : $zipfile2"

			if [ $innerziptobechanged -eq 1 ]
			then
				if [ -f "../$zipfile2" ]
				then
					7z a "$zipfile2" *
					mv "$zipfile2" ../
				fi
			fi
			cd ../
			rm -rf tmp
		done
		cd ../
	done
	if [ $maintgztobechanged -eq 1 ]
	then
		echo "This is for zipfile : $zipfile"

		if [ "$zipfile" != "" ]
		then
			if [ -f "../$zipfile" ]
			then
				7z a "$zipfile" *
				mv "$zipfile" ../
			fi
		elif [ "$tgzfile" != "" ]
		then
			if [ -f ../$tgzfile ]
			then
				echo "tgzing $tgzfile ========================="
				tar -zcvf "$tgzfile" * >> /dev/null
				mv "$tgzfile" ../
			fi
		fi
	fi
	cd ../
	sleep 3s
	rm -rf tmp
fi
cd ../
echo -e "${log_details}" >> "$logfile"
