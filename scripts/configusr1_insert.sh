#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
cd extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`
echo $tgzfile > ../logs/configusr1_insert.log
tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
mkdir tmp
zusr1tgzfile=`ls zUSR1-*.tgz`
echo $zusr1tgzfile >> ../../logs/configusr1_insert.log
tar -zxvf zUSR1-*.tgz -C tmp/ >> /dev/null
cd tmp/
mkdir tmp
zusr1tarfile=`ls zUSR1-*.tar`
tar -xvf zUSR1-*.tar -C tmp/ >> /dev/null
cd tmp
rm CONTROL/config.usr1
cp ../../../config.usr1 CONTROL/ 
tar -cvf $zusr1tarfile * >> /dev/null
echo $zusr1tarfile >> ../../../../logs/configusr1_insert.log
#tar -zxvf *.tgz -C tmp >> /dev/null
mv zUSR1-*.tar ../
cd ../
rm -rf tmp/
tar -zcvf $zusr1tgzfile * >> /dev/null
#tar -zxvf *.tgz -C tmp >> /dev/null
mv zUSR1-*.tgz ../
cd ../
rm -rf tmp
tar -zcvf $tgzfile * >> /dev/null
#tar -zxvf *.tgz -C tmp >> /dev/null
mv $tgzfile ../
cd ../
rm -rf tmp/
rm config.usr1 
