#!/bin/bash
logfile="`pwd`/logs/initial_update_configuration.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
shopt -s nocasematch
log_details=""


if [ "$1" = "MX" ]
then
	pwd
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	for filename in *.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		filechanged=0
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"
			if [ -f xmlCfgFile.xml ]
			then
				cp xmlCfgFile.xml ../../../xmlCfgFile.xml
			fi
			if [ -f CONTROL/config.usr1 ]
			then
				cp CONTROL/config.usr1 ../../../config.usr1
			fi
			if [ -f cdt.txt ]
			then
				cp cdt.txt ../../../cdt.txt
			fi
			if [ -f cdt.ini ]
			then
				cp cdt.ini ../../../cdt.ini
			fi
			if [ -f aidlist.txt ]
			then
				cp aidlist.txt ../../../aidlist.txt
			fi
			if [ -f aidlist.ini ]
			then
				cp aidlist.ini ../../../aidlist.ini
			fi

			cd ../
			rm -rf tmp
			
		done

		cd ../
		rm -rf tmp
		
		echo `pwd`
		echo "#########################"
		
	done

	cd ../
	sleep 3s
	rm -rf tmp

#fi
elif [ "$1" = "VX"  ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=""
	zipfile=""
	if ls *.tgz 2>> /dev/null
	then
		tgzfile=`ls *.tgz`
		echo "tgzfile = $tgzfile"
		tar -zxvf *.tgz -C tmp >> /dev/null
	elif ls *.zip >> /dev/null
	then
		zipfile=`ls *.zip`
		echo "zipfile = $zipfile"
		unzip *.zip -d tmp >> /dev/null
	fi
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	innerziptobechanged=0
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			filechanged=0
			unzip "$zipfile2" -d tmp
			cd tmp
				for zipfile3 in *.zip; do
					filechanged=0
					rm -rf tmp
					mkdir tmp
					unzip "$zipfile3" -d tmp
					cd tmp
					for foldername3 in */ ; do
						if [ "$foldername3" != "*/" ]
						then
							echo $foldername3
							cd "$foldername3"

							if [ -f xmlCfgFile.xml ]
							then
								cp xmlCfgFile.xml ../../../../../xmlCfgFile.xml
							fi
							if [ -f CONTROL/config.usr1 ]
							then
								cp CONTROL/config.usr1 ../../../../../config.usr1
							fi
							if [ -f cdt.txt ]
							then
								cp cdt.txt ../../../../../cdt.txt
							fi
							if [ -f cdt.ini ]
							then
								cp cdt.ini ../../../../../cdt.ini
							fi
							if [ -f aidlist.txt ]
							then
								cp aidlist.txt ../../../../../aidlist.txt
							fi
							if [ -f aidlist.ini ]
							then
								cp aidlist.ini ../../../../../aidlist.ini
							fi
							
							cd ../
						fi
					done
					cd ../
					rm -rf tmp
				
				done
				for foldername2 in */ ; do
					if [ "$foldername2" != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"

						if [ -f xmlCfgFile.xml ]
						then
							cp xmlCfgFile.xml ../../../../xmlCfgFile.xml
						fi
						if [ -f CONTROL/config.usr1 ]
						then
							cp CONTROL/config.usr1 ../../../../config.usr1
						fi
						if [ -f cdt.txt ]
						then
							cp cdt.txt ../../../../cdt.txt
						fi
						if [ -f cdt.ini ]
						then
							cp cdt.ini ../../../../cdt.ini
						fi
						if [ -f aidlist.txt ]
						then
							cp aidlist.txt ../../../../aidlist.txt
						fi
						if [ -f aidlist.ini ]
						then
							cp aidlist.ini ../../../../aidlist.ini
						fi

						cd ../
					fi
				done
			echo "This is for zipfile 2 : $zipfile2"

			cd ../
			rm -rf tmp
		done
		cd ../
	done
	cd ../
	sleep 3s
	rm -rf tmp
fi
cd ../
echo -e "${log_details}" >> "$logfile"
