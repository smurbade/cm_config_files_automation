# 1. Delete existing *EMV* files from package
# 2. delete capkdata.ini, ctlsconfig.ini, emvtables.ini from package
# 3. Copy EMV template package from Sharepoint to package and rename it to zemv.tgz

#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
#pwd
logfile="`pwd`/logs/emv.log"
date > "$logfile" 
#cd EMV/$1
#cd  ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1
rm -rf tmp
mkdir tmp
#tgzfile=`ls *.tgz`
log_details=""
if [ "$2" = "MX" ]
then
	if [ -f ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/*.tgz ]
	then
		log_details="${log_details}"$'\n\n'"#######################################################################"
		log_details="${log_details}"$'\n'"EMV Template Insertion :"
		tar -zxvf ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/*.tgz -C tmp >> /dev/null
		cd extract
		rm -rf tmp
		mkdir tmp
		tgzfile="`ls *.tgz`"
		tar -zxvf *.tgz -C tmp >> /dev/null
		cd tmp/
		if [[ -n `find . -maxdepth 1 -iname "*EMV*.tgz"` ]];
		then
			log_details="${log_details}"$'\n'"Deleting existing "*EMV*.tgz" package"
			rm -f *EMV*
			#echo "Deleting existing EMV package"
		fi

		filechanged=0
		for filename in *.tgz; do
			echo $filename
			rm -rf tmp
			mkdir tmp
			tar -zxvf "$filename" -C tmp/ >> /dev/null
			cd tmp/
			filechanged=0
			for filename2 in *.tar; do
				echo $filename2
				rm -rf tmp
				mkdir tmp
				tar -xvf "$filename2" -C tmp/ >> /dev/null
				cd tmp/
				echo `ls *`
				echo "#############"
				if [[ -n `find . -maxdepth 1 -iname CAPKData.INI` ]]; 
				then
					filechanged=1
					rm CAPKData.INI
					#echo "Deleting CAPKData.INI   from $filename2 -----------"
					log_details="${log_details}"$'\n'"Deleting CAPKData.INI from $filename2"
				fi
				if [[ -n `find . -maxdepth 1 -iname CTLSConfig.INI` ]]; 
				then
					filechanged=1
					rm CTLSConfig.INI
					#echo "Deleting CTLSConfig.INI from $filename2 -----------"
					log_details="${log_details}"$'\n'"Deleting CTLSConfig.INI from $filename2"
				fi
				if [[ -n `find . -maxdepth 1 -iname EMVTables.INI` ]]; 
				then
					filechanged=1
					rm EMVTables.INI
					#echo "Deleting EMVTables.INI  from $filename2 -----------"
					log_details="${log_details}"$'\n'"Deleting EMVTables.INI from $filename2"
				fi

				if [ $filechanged -eq 1 ];
				then
					echo "tarring $filename2 ***************************"
					tar -cvf "$filename2" * >> /dev/null
					mv "$filename2" ../
				fi

				cd ../
				rm -rf tmp
				
			done

			if [ $filechanged -eq 1 ];
			then
				echo "tgzing $filename ========================="
				tar -zcvf "$filename" * >> /dev/null
				mv "$filename" ../
			fi

			cd ../
			rm -rf tmp
			
			echo `pwd`
			echo "#########################"
			
		done

		#echo "copying EMV sample package to main package"  >> "$logfile" 
		log_details="${log_details}"$'\n'"copying EMV sample package from "$1" to config package"
		cp ../../tmp/*EMV* .
		#mv *EMV*.tgz zemv.tgz
		#mv *EMV*.p7s zemv.tgz.p7s
		echo "tgzing $tgzfile ========================="
		tar -zcvf "$tgzfile" * >> /dev/null
		mv "$tgzfile" ../
		cd ../
		rm -rf tmp/
		rm -rf ../tmp
		cd ../
	else
		echo "EMV Template package not present @ "$1""
		log_details="${log_details}"$'\n'"EMV Template package not present @ "$1""
	fi

elif [ "$2" = "VX"  ]
then
	if [ -f ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/*.zip ]
	then
		log_details="${log_details}"$'\n\n'"#######################################################################"
		log_details="${log_details}"$'\n'"EMV Template Insertion :"
		rm -rf ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/tmp
		mkdir ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/tmp
		unzip ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/*.zip -d ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/tmp >> /dev/null
		cd extract
		rm -rf tmp
		mkdir tmp
		zipfile=`ls *.zip` >> /dev/null
		echo "zipfile = $zipfile"
		echo "Please wait..."
		unzip *.zip -d tmp >> /dev/null 2>> /dev/null
		#cp -rf ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/*.zip tmp/ >> /dev/null 2>> /dev/null
		cd tmp/
		#echo `ls *`
		filechanged=0
		maintgztobechanged=0
		innerziptobechanged=0
		for foldername in */ ; do
			#echo $foldername
			cd "$foldername"
			rm -rf tmp
			mkdir tmp
			for zipfile2 in *.zip; do
				filechanged=0
				unzip "$zipfile2" -d tmp >> /dev/null 2>> /dev/null
				cd tmp
					for zipfile3 in *.zip; do
						filechanged=0
						rm -rf tmp
						mkdir tmp
						unzip "$zipfile3" -d tmp >> /dev/null 2>> /dev/null
						cd tmp
						for foldername3 in */ ; do
							if [ "$foldername3" != "*/" ]
							then
								#echo $foldername3
								cd "$foldername3"

								for emvtemplatefilefullpath in ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/tmp/* ; do
									emvtemplatefile=$(basename "$emvtemplatefilefullpath")
									if [[ ( $emvtemplatefile != *"dld"* ) && ( $emvtemplatefile != *"ddl"* ) && ( $emvtemplatefile != *"exe"* ) && ( $emvtemplatefile != *"bat"* ) ]]; then
										if [[ -f $emvtemplatefile ]]; then
											cp -p "$emvtemplatefilefullpath" .
											echo "Copied $emvtemplatefile from EMV Template to $zipfile/$foldername$zipfile2/$zipfile3/$foldername3" >> ../../../../../../logs/emv.log
											log_details="${log_details}"$'\n'"Copied $emvtemplatefile from "$1" to config package"
											filechanged=1
										fi
									fi
								done

								if [ $filechanged -eq 1 ]
								then
									innerziptobechanged=1
									maintgztobechanged=1
								fi
								cd ../
							fi
						done
						#echo "$zipfile3"
						if [ $filechanged -eq 1 ]
						then
							if [ -f "../$zipfile3" ]
							then
								7z a "$zipfile3" * >> /dev/null 2>> /dev/null
								mv "$zipfile3" ../
							fi
						fi
						cd ../
						rm -rf tmp
					
					done
					for foldername2 in */ ; do
						if [ "$foldername2" != "*/" ]
						then
							#echo $foldername2
							cd "$foldername2"
							
							for emvtemplatefilefullpath in ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/tmp/* ; do
								emvtemplatefile=$(basename "$emvtemplatefilefullpath")
								if [[ ( $emvtemplatefile != *"dld"* ) && ( $emvtemplatefile != *"ddl"* ) && ( $emvtemplatefile != *"exe"* ) && ( $emvtemplatefile != *"bat"* ) ]]; then
									#echo "hehe $emvtemplatefilefullpath"
									if [[ -f $emvtemplatefile ]]; then
										cp -p "$emvtemplatefilefullpath" .
										#echo "Copied $emvtemplatefile from EMV Template to $zipfile/$foldername$zipfile2/$zipfile3/$foldername3" >> ../../../../../logs/emv.log
										log_details="${log_details}"$'\n'"Copied $emvtemplatefile from "$1" to config package"
										filechanged=1
									fi
								fi
							done
							if [ $filechanged -eq 1 ]
							then
								innerziptobechanged=1
								maintgztobechanged=1
							fi
							cd ../
						fi
					done
				#echo "$zipfile2"

				if [ $innerziptobechanged -eq 1 ]
				then
					if [ -f "../$zipfile2" ]
					then
						7z a "$zipfile2" * >> /dev/null 2>> /dev/null
						mv "$zipfile2" ../
					fi
				fi
				cd ../
				rm -rf tmp
			done
			cd ../
		done
		if [ $maintgztobechanged -eq 1 ]
		then
			#echo "$zipfile"
		
			if [ "$zipfile" != "" ]
			then
				if [ -f "../$zipfile" ]
				then
					7z a "$zipfile" * >> /dev/null 2>> /dev/null
					mv "$zipfile" ../
				fi
			elif [ "$tgzfile" != "" ]
			then
				if [ -f ../$tgzfile ]
				then
					echo "tgzing $tgzfile ========================="
					tar -zcvf "$tgzfile" * >> /dev/null 2>> /dev/null
					mv "$tgzfile" ../
				fi
			fi
		fi
		cd ../
		sleep 3s
		rm -rf tmp
		#7z a "$zipfile" * >> /dev/null 2>> /dev/null
		#mv "$zipfile" ../
		rm -rf ~/SharePoint/North\ America\ Config\ Manageme\ -\ Doc/EMV/$1/tmp
	fi
fi

echo -e "${log_details}" >> "$logfile"
