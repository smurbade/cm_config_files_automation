#!/bin/bash
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
pwd
logfile="`pwd`/logs/emv.log"
date > "$logfile" 
cd EMV/$1
rm -rf tmp
mkdir tmp
#tgzfile=`ls *.tgz`
tar -zxvf *.tgz -C tmp >> /dev/null
cp *.zip tmp/ >> /dev/null
cp *.ZIP tmp/ >> /dev/null


cd ../../../../../extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`
tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
rm -f *EMV*
mkdir tmp
echo "copying EMV sample package to main package"  >> "$logfile" 
cp ../../EMV/$1/tmp/* .
tar -zxvf zUSR1F-1.0.0.tgz -C tmp/ >> /dev/null
cd tmp/
mkdir tmp
tar -xvf zUSR1F-1.0.0.tar -C tmp/ >> /dev/null
cd tmp/
echo "Removing CAPK, CTLS, EMVTables files from USR1F tar" >> "$logfile" 
rm CAPKData.INI CTLSConfig.INI EMVTables.INI
echo "packing zUSR1F-1.0.0.tar" >> "$logfile" 
tar -cvf zUSR1F-1.0.0.tar * >> /dev/null
mv zUSR1F-1.0.0.tar ../
cd ../
rm -rf tmp/
echo "packing zUSR1F-1.0.0.tgz" >> "$logfile" 
tar -zcvf zUSR1F-1.0.0.tgz * >> /dev/null
mv zUSR1F-1.0.0.tgz ../
cd ../
rm -rf tmp
echo "Packing $tgzfile" >> "$logfile" 
tar -zcvf $tgzfile * >> /dev/null
mv $tgzfile ../
cd ../
rm -rf tmp/
rm -rf ../EMV/$1/tmp
cd ../
