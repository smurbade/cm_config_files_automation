#!/bin/bash
logfile="`pwd`/logs/cybersource.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s

if [ "$1" = "MX" ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	cd tmp/
	for filename in *.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"
			if [[ -n `find . -maxdepth 1 -iname cdt.txt` ]]; 
			then
				cp cdt.txt ../../../cdt.txt
				echo "Found cdt.txt -----------"
			fi
			if [[ -n `find . -maxdepth 1 -iname cdt.ini` ]]; 
			then
				cp cdt.ini ../../../cdt.ini
				echo "Found cdt.ini -----------"
			fi

			cd ../
			rm -rf tmp
			
		done

		cd ../
		rm -rf tmp
		echo `pwd`
		echo "#########################"
	done

	cd ../
	rm -rf tmp
elif [ "$1" = "VX"  ]
then
	cd extract
	rm -rf tmp
	mkdir tmp
	zipfile=`ls *.zip`
	echo "zipfile = $zipfile"
	unzip *.zip -d tmp >> /dev/null
	cd tmp/
	for foldername in */ ; do
		echo $foldername
		cd "$foldername"
		rm -rf tmp
		mkdir tmp
		for zipfile2 in *.zip; do
			unzip "$zipfile2" -d tmp
			cd tmp
				for foldername2 in */ ; do
					if [ $foldername2 != "*/" ]
					then
						echo $foldername2
						cd "$foldername2"
						if [[ -n `find . -maxdepth 1 -iname cdt.txt` ]]; 
						then
							cp -p cdt.txt ../../../../cdt.txt
						fi
						if [[ -n `find . -maxdepth 1 -iname cdt.ini` ]]; 
						then
							cp -p cdt.ini ../../../../cdt.ini
						fi
						cd ../
					fi
				done
			cd ../
			rm -rf tmp
		done
		cd ../
	done
	cd ../
	rm -rf tmp
fi
