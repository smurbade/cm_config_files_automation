#!/bin/bash
logfile="`pwd`/logs/cybersource.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s

cd extract
rm -rf tmp
mkdir tmp
tgzfile=`ls *.tgz`
echo "tgzfile = $tgzfile"
tar -zxvf *.tgz -C tmp >> /dev/null
cd tmp/
for filename in *.tgz; do
	echo $filename
	rm -rf tmp
	mkdir tmp
	tar -zxvf "$filename" -C tmp/ >> /dev/null
	cd tmp/
	for filename2 in *.tar; do
		echo $filename2
		rm -rf tmp
		mkdir tmp
		tar -xvf "$filename2" -C tmp/ >> /dev/null
		cd tmp/
		echo `ls *`
		echo "#############"
		if [[ -n `find . -maxdepth 2 -iname config.usr1` ]]; 
		then
			cp CONTROL/config.usr1 ../../../config.usr1
			echo "Found config.usr1 -----------"
		fi

		cd ../
		rm -rf tmp
		
	done

	cd ../
	rm -rf tmp
	echo `pwd`
	echo "#########################"
done
cd ../
rm -rf tmp
