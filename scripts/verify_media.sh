#!/bin/bash
logfile="`pwd`/logs/updatepackage_with_configusr1_from_form.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s
shopt -s nocasematch

validate()
{
	#$1 - Device ( 915 or 925 )
	#$2 - SCA version passed from GUI
	#$3 - filename in control file (imagelist1.dat, imagelist2.dat, videoloist.dat)
	#$4 - media area

	width=$(../../../../scripts/MediaInfo_CLI_0.7.97_Windows_x64/MediaInfo.exe $3 | grep -i width | cut -d ":" -f 2 | cut -d " " -f 2)
	height=$(../../../../scripts/MediaInfo_CLI_0.7.97_Windows_x64/MediaInfo.exe $3 | grep -i height | cut -d ":" -f 2 | cut -d " " -f 2)
	filetype=$(../../../../scripts/MediaInfo_CLI_0.7.97_Windows_x64/MediaInfo.exe $3 | grep -i format | head -1 | cut -d ":" -f 2 | cut -d " " -f 2)
	sizebytes=$(ls -lrt $3 | cut -d " " -f 5)
	sizekb=$((sizebytes / 1024))
	sizemb=$((sizekb / 1024))
	mediarea=$(grep $1 ../../../../logs/media_guide_table.txt | grep "$4" | cut -d "," -f 2 | sed "s/^[ \t']*//" | sed s'/.$//')
	widthlimit=$(grep $1 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 3 | sed "s/^[ \t]*//")
	heigthlimit=$(grep $1 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 4 | sed "s/^[ \t]*//")
	filetypelimit=$(grep $1 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 5 | sed "s/^[ \t]*//")
	filesizelimit=$(grep $1 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 6 | sed "s/^[ \t]*//")
	extension="${3##*.}"
	minversion=$(grep $1 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 7 | cut -d "'" -f 2 )
	if [[ ( $minversion != "None" ) && "$2" < "$minversion" ]]; then
		echo "Minimum SCA version required for \"$4\" is $minversion whereas package version specified in program GUI is $2" >> ../../../../logs/verify_media.log
	fi
	echo "$3 : Resolution - $width * $height; Filetype - $filetype; Size - $sizebytes"
	if [ "$height" != "$heigthlimit" -o "$width" != "$widthlimit" ]; then
		echo "Resolution for $3 ( $width x $height ) doesn't match with Media guide specification ( $widthlimit x $heigthlimit ) for \"$4\"" >> ../../../../logs/verify_media.log
		echo "" >> ../../../../logs/verify_media.log
	fi
	ifextensioncorrect=$(echo $filetypelimit | grep -i $extension)
	if [[ -z "$ifextensioncorrect" ]]; then
		echo "$3 file type is $extension. As per Media guide specification, allowed file types are $filetypelimit" >> ../../../../logs/verify_media.log
		echo "" >> ../../../../logs/verify_media.log
	fi
	if [ $sizemb -gt $filesizelimit ]; then
		echo "$3 file size ( $sizebytes bytes ) is more than allowed ( $filesizelimit MB ) as per Media Guide" >> ../../../../logs/verify_media.log
		echo "" >> ../../../../logs/verify_media.log
	fi

}

test() # ignore this function
{
	width=$(../../../../scripts/MediaInfo_CLI_0.7.97_Windows_x64/MediaInfo.exe $imglist1file | grep -i width | cut -d ":" -f 2 | cut -d " " -f 2)
	height=$(../../../../scripts/MediaInfo_CLI_0.7.97_Windows_x64/MediaInfo.exe $imglist1file | grep -i height | cut -d ":" -f 2 | cut -d " " -f 2)
	filetype=$(../../../../scripts/MediaInfo_CLI_0.7.97_Windows_x64/MediaInfo.exe $imglist1file | grep -i format | head -1 | cut -d ":" -f 2 | cut -d " " -f 2)
	sizebytes=$(ls -lrt $imglist1file | cut -d " " -f 5)
	sizekb=$((sizebytes / 1024))
	sizemb=$((sizekb / 1024))
	mediarea=$(grep $2 ../../../../logs/media_guide_table.txt | grep "Idle Screen Banner" | cut -d "," -f 2 | sed "s/^[ \t']*//" | sed s'/.$//')
	widthlimit=$(grep $2 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 3 | sed "s/^[ \t]*//")
	heigthlimit=$(grep $2 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 4 | sed "s/^[ \t]*//")
	filetypelimit=$(grep $2 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 5 | sed "s/^[ \t]*//")
	filesizelimit=$(grep $2 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 6 | sed "s/^[ \t]*//")
	extension="${imglist1file##*.}"
	minversion=$(grep $2 ../../../../logs/media_guide_table.txt | grep "$mediarea" | cut -d "," -f 7 | cut -d "'" -f 2 )
	if [[ ( $minversion != "None" ) && "$3" < "$minversion" ]]; then
		echo "idlescreenmode is \"$idlescreenmode\". Minimum SCA version required for \"Idle Screen Banner\" is $minversion whereas entered version is $3" >> ../../../../logs/verify_media.log
	fi
	echo "$imglist1file : Resolution - $width * $height; Filetype - $filetype; Size - $sizebytes"
	if [ "$height" != "$heigthlimit" -o "$width" != "$widthlimit" ]; then
		echo "Resolution for $imglist1file ( $width x $height ) doesn't match with Media guide specification ( $widthlimit x $heigthlimit ) for \"Idle Screen Banner\"" >> ../../../../logs/verify_media.log
		echo "" >> ../../../../logs/verify_media.log
	fi
	ifextensioncorrect=$(echo $filetypelimit | grep -i $extension)
	if [[ -z "$ifextensioncorrect" ]]; then
		echo "$imglist1file file type is $extension. As per Media guide specification, allowed file types are $filetypelimit" >> ../../../../logs/verify_media.log
		echo "" >> ../../../../logs/verify_media.log
	fi
	if [ $sizemb -gt $filesizelimit ]; then
		echo "$imglist1file file size ( $sizebytes bytes ) is more than allowed ( $filesizelimit MB ) as per Media Guide" >> ../../../../logs/verify_media.log
		echo "" >> ../../../../logs/verify_media.log
	fi
}


if [ "$1" = "MX" ]
then
	rm -rf logs/verify_media.log
	sed -e s/"\.0"//g -i logs/media_guide_table.txt
	sed 's/[()]//g' -i logs/media_guide_table.txt
	cd extract
	rm -rf tmp
	mkdir tmp
	tgzfile=`ls *.tgz`
	echo "tgzfile = $tgzfile"
	tar -zxvf *.tgz -C tmp >> /dev/null
	#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
	cd tmp/
	filechanged=0
	maintgztobechanged=0
	idlescreenmode=""
	for filename in *adv*.tgz; do
		echo $filename
		rm -rf tmp
		mkdir tmp
		tar -zxvf "$filename" -C tmp/ >> /dev/null
		cd tmp/
		filechanged=0
		for filename2 in *.tar; do
			echo $filename2
			rm -rf tmp
			mkdir tmp
			tar -xvf "$filename2" -C tmp/ >> /dev/null
			cd tmp/
			echo `ls *`
			echo "#############"
			echo $filename2 | grep -i -q "image"
			if [ $? -eq "0" ]
			then
				if [ ! -f idlescreenmode.dat ]; then
					echo "idlescreenmode.dat does not exist in $filename2" >> ../../../../logs/verify_media.log
					echo "" >> ../../../../logs/verify_media.log
				else
					idlescreenvalid=`awk '/VIDEO_BANNER|IMAGE|VIDEO|ANIMATION/' idlescreenmode.dat | wc -l`
					if [ $idlescreenvalid -eq "0" ]; then
						echo "idlescreenmode.dat does not contain any of these valid parameters ( VIDEO_BANNER, IMAGE, VIDEO, ANIMATION )" >> ../../../../logs/verify_media.log
						echo "" >> ../../../../logs/verify_media.log
					elif grep -q "VIDEO_BANNER" idlescreenmode.dat; then
						idlescreenmode="VIDEO_BANNER"
						echo "idle screen mode set - $idlescreenmode"
						if [ ! -f imageList1.dat ]; then
							echo "idlescreenmode set to idlescreenmode but imageList1.dat does not exist in $filename2" >> ../../../../logs/verify_media.log
							echo "" >> ../../../../logs/verify_media.log
						else
							sed 1d imageList1.dat | while read imglist1file; do
								#echo $imglist1file
								if [ ! -f $imglist1file ]; then
									echo "$imglist1file is mentioned in imageList1.dat but does not exist in $filename2 " >> ../../../../logs/verify_media.log
									echo "" >> ../../../../logs/verify_media.log
								else
									validate $2 $3 "$imglist1file" "Idle Screen Banner"
								fi
							done
						fi
					elif grep -q "IMAGE" idlescreenmode.dat; then
						idlescreenmode="IMAGE"
						if [ ! -f $2_scaidleimage.png ]; then
							echo "idlescreenmode is \"IMAGE\" but $2_scaidleimage.png does not exist in $filename2" >> ../../../../logs/verify_media.log
							echo "" >> ../../../../logs/verify_media.log
						else
							validate $2 $3 "$2_scaidleimage.png" "Idle Screen - Full Image"
						fi
					elif grep -q "VIDEO" idlescreenmode.dat; then
						idlescreenmode="VIDEO"
						echo "idle screen mode set - $idlescreenmode"
					elif grep -q "ANIMATION" idlescreenmode.dat; then
						idlescreenmode="ANIMATION"
						if [ ! -f $2_scaidleanimation.eet ]; then
							echo "idlescreenmode is \"ANIMATION\" but $2_scaidleanimation.eet does not exist in $filename2" >> ../../../../logs/verify_media.log
							echo "" >> ../../../../logs/verify_media.log
						else
							validate $2 $3 "$2_scaidleanimation.eet" "Idle Screen - Full Animation"
						fi
					fi
				fi
				if [ ! -f imageList2.dat ]; then
					echo "imageList2.dat does not exist in $filename2" >> ../../../../logs/verify_media.log
					echo "" >> ../../../../logs/verify_media.log
				else
					sed 1d imageList2.dat | while read imglist2file; do
						#echo $imglist2file
						if [ ! -f $imglist2file ]; then
							echo "$imglist2file is mentioned in imageList2.dat but does not exist in $filename2 " >> ../../../../logs/verify_media.log
							echo "" >> ../../../../logs/verify_media.log
						else
							validate $2 $3 "$imglist2file" "Line Item Screen Ad Area" 
						fi
					done
				fi
			fi
			echo $filename2 | grep -i -q "video"
			if [ $? -eq "0" ]
			then
				echo "idle screen mode - $idlescreenmode"
				if [ "$idlescreenmode" == "VIDEO" ]; then
					if [ ! -f $2_scaidlevideo.avi ]; then
						echo "idlescreenmode is \"VIDEO\" but $2_scaidlevideo.avi does not exist in $filename2" >> ../../../../logs/verify_media.log
						echo "" >> ../../../../logs/verify_media.log
					else
						validate $2 $3 "$2_scaidlevideo.avi" "Idle Screen - Full Video"
					fi
				elif [ "$idlescreenmode" == "VIDEO_BANNER" ]; then
					if [ ! -f videoList.dat ]; then
						echo "videoList.dat does not exist in $filename2" >> ../../../../logs/verify_media.log
						echo "" >> ../../../../logs/verify_media.log
					else
						while read vidfile; do
							echo $vidfile
							if [ ! -f $vidfile ]; then
								echo "$vidfile is mentioned in videoList.dat but does not exist in $filename2 " >> ../../../../logs/verify_media.log
								echo "" >> ../../../../logs/verify_media.log
							else
								validate $2 $3 "$vidfile" "Idle Screen Video"
							fi
						done < videoList.dat
					fi				
				fi
			fi
			
			if [ $filechanged -eq 1 ];
			then
				maintgztobechanged=1
				echo "tarring $filename2 ***************************"
				tar -cvf "$filename2" * >> /dev/null
				mv "$filename2" ../
			fi

			cd ../
			rm -rf tmp
			
		done

		if [ $filechanged -eq 1 ];
		then
			echo "tgzing $filename ========================="
			tar -zcvf "$filename" * >> /dev/null
			mv "$filename" ../
		fi

		cd ../
		rm -rf tmp
		
		echo `pwd`
		echo "#########################"
		
	done

	if [ $maintgztobechanged -eq 1 ];
	then
		echo "tgzing $tgzfile ========================="
		tar -zcvf "$tgzfile" * >> /dev/null
		mv "$tgzfile" ../
	fi

	cd ../
	sleep 3s
	rm -rf tmp
	if [ -f ../logs/verify_media.log ]; then
		sort ../logs/verify_media.log | uniq > ../vermed.txt
		cp ../vermed.txt ../logs/verify_media.log
		rm ../vermed.txt
		awk ' {print;} NR % 1 == 0 { print ""; }' ../logs/verify_media.log > ../vermed.txt
		cp ../vermed.txt ../logs/verify_media.log
		rm ../vermed.txt
	fi
fi
