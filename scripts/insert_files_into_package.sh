# accepts argument as Device type ( MX/VX ) and name of config files to be inserted into package
# For example ./insert_files_into_package.sh MX config.usr1 cdt.txt aidlist.txt aidlist.ini
# inserts config files from extract folder into package as per the arguments passed

#!/bin/bash
logfile="`pwd`/logs/insert_files_into_package.log"
date > "$logfile" 
shopt -s nocaseglob  # Make file search case insensitive ... To disable this option use -u instead of -s

arguments=$* 
filechanged=0
maintgztobechanged=0

copy_config_file()
{
	if [ -z `echo "$arguments" | grep -q "$1"` ]
	then
		if [ \( -f ../../../$1 \) ]
		then
			if [ -f $1 ]
			then
				filechanged=1
				cp ../../../$1 .
				echo "Copied $1 -----------"
			fi
		fi
	fi
}

copy_config_file_vx()
{
	if [ -z `echo "$arguments" | grep -q "$1"` ]
	then
		if [ \( -f ../../../../$1 \) ]
		then
			if [ -f $1 ]
			then
				filechanged=1
				cp ../../../../$1 .
				echo "Copied $1 -----------"
			fi
		fi
	fi
}


if [ $# -gt 1 ]
then
	if [ "$1" = "MX" ]
	then
		cd extract
		rm -rf tmp
		mkdir tmp
		tgzfile=`ls *.tgz`
		echo "tgzfile = $tgzfile"
		tar -zxvf *.tgz -C tmp >> /dev/null
		#tar -zxvf "`find . -maxdepth 1 -iname *.Tgz`" -C tmp >> /dev/null
		cd tmp/
		filechanged=0
		maintgztobechanged=0
		for filename in *.tgz; do
			#echo $filename
			rm -rf tmp
			mkdir tmp
			tar -zxvf "$filename" -C tmp/ >> /dev/null
			cd tmp/
			filechanged=0
			for filename2 in *.tar; do
				#echo $filename2
				rm -rf tmp
				mkdir tmp
				tar -xvf "$filename2" -C tmp/ >> /dev/null
				cd tmp/
				#echo `ls *`
				#echo "#############"
				#if [ \( -f ../../../config.usr1 -a "${arguments#*config.usr1}" != "$arguments" \) -a \( "${filename#*zcfg}" != "$filename" -o "${filename#*usr1}" != "$filename" -o "${filename#*USR1}" != "$filename" \) ]
				if [ \( -f ../../../config.usr1 -a "${arguments#*config.usr1}" != "$arguments" \) ]
				then
					if [ \( "${filename#*zcfg}" != "$filename" \) ]
					then
						filechanged=1
						cp ../../../config.usr1 CONTROL/
						#echo "Inserted config.usr1 into $filename -----------"
						echo "Inserted config.usr1 -----------"
					else
						if [ -f CONTROL/config.usr1 ]
						then
							filechanged=1
							cp ../../../config.usr1 CONTROL/
							#echo "Copied config.usr1 into $filename -----------"
							echo "Copied config.usr1 -----------"
						fi
					fi
				fi
				
				for fileargument in $arguments
				do
					if [ $fileargument != "config.usr1" ]
					then
						copy_config_file $fileargument
					fi
				done
				
				if [ $filechanged -eq 1 ];
				then
					maintgztobechanged=1
					#echo "tarring $filename2 ***************************"
					tar -cvf "$filename2" * >> /dev/null
					mv "$filename2" ../
				fi

				cd ../
				rm -rf tmp
				
			done

			if [ $filechanged -eq 1 ];
			then
				#echo "tgzing $filename ========================="
				tar -zcvf "$filename" * >> /dev/null
				mv "$filename" ../
			fi

			cd ../
			sleep 3s
			rm -rf tmp
			
			#echo `pwd`
			#echo "#########################"
			
		done

		if [ $maintgztobechanged -eq 1 ];
		then
			#echo "tgzing $tgzfile ========================="
			tar -zcvf "$tgzfile" * >> /dev/null
			mv "$tgzfile" ../
		fi

		cd ../
		sleep 3s
		rm -rf tmp

	#fi
	elif [ "$1" = "VX"  ]
	then
		cd extract
		rm -rf tmp
		mkdir tmp
		zipfile=`ls *.zip`
		echo "zipfile = $zipfile"
		unzip *.zip -d tmp >> /dev/null
		cd tmp/
		filechanged=0
		maintgztobechanged=0
		for foldername in */ ; do
			#echo $foldername
			cd "$foldername"
			rm -rf tmp
			mkdir tmp
			for zipfile2 in *.zip; do
				unzip "$zipfile2" -d tmp >> /dev/null
				cd tmp
				filechanged=0
					for foldername2 in */ ; do
						if [ $foldername2 != "*/" ]
						then
							#echo $foldername2
							cd "$foldername2"

							for fileargument in $arguments
							do
								copy_config_file_vx $fileargument
							done
							
							if [ $filechanged -eq 1 ]
							then
								maintgztobechanged=1
							fi
							cd ../
						fi
					done
				if [ $filechanged -eq 1 ]
				then
					7z a "$zipfile2" * >> /dev/null
					mv "$zipfile2" ../
				fi
				cd ../
				rm -rf tmp
			done
			cd ../
		done
		if [ $maintgztobechanged -eq 1 ]
		then
			7z a "$zipfile" * >> /dev/null
			mv "$zipfile" ../
		fi
		cd ../
		sleep 4s
		rm -rf tmp
	fi
else
	echo "Insufficient number of arguments to program"
fi
