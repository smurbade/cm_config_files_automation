import pypyodbc, os, re
from xlrd import open_workbook

client_prodtype_device_list_filename = "C:\Verifone\client_prodtype_device_list.xlsx"
workbook = open_workbook(client_prodtype_device_list_filename)
wb = workbook.sheet_by_name("Sheet1")
global queryfieldlist
queryfieldlist = ""
global custsurveyfieldlist
custsurveyfieldlist = []
global fieldexclude_lower
global fieldlist


def getid_addclientname(clientname): # returns ID for 'Client name' from 'Add Client Name' list ; Returns -1 if client is not found
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Add Client Name.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Add Client Name.accdb;\"")

    #query = "SELECT * FROM \"Add Client Name\" WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.lower() + "'"
    #query = "SELECT * FROM \"Add Client Name\" WHERE ([Add Client Name].[Client Name]) = '" + clientname + "'"

    #print(query)
    #print(clientname)

    cur = conn.cursor()
    cur.execute("SELECT [Add Client Name].[ID] FROM [Add Client Name] WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.replace("'", "''").lower() + "'" )
    #cur.execute(query);

    while True:
        row = cur.fetchone()
        if row is None:
            break
        else:
            ID = row[0]
            cur.close()
            conn.close()
            #print(row)
            return ID

    return -1
    cur.close()
    conn.close()


def create_survey(client, prodtype, device):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Form.accdb;\"")

    global queryfieldlist

    cur = conn.cursor()
    WHERE = "[CM Form].[Client name] = " + str(getid_addclientname(client)) + " and  [CM Form].[Product Type] = '" + str(prodtype) + "' and [CM Form].[Device Model] = '" + str(device) + "' and [CM Form].[Device Model] is Not Null"
    #WHERE = "[CM Form].[Product Type] = '" + str(prodtype) + "' and [CM Form].[Device Model] = '" + str(device) + "'"
    #query = "SELECT " + queryfieldlist + " FROM [CM Form] WHERE " + WHERE
    query = "SELECT " + queryfieldlist + " FROM [CM Form] WHERE " + WHERE + " ORDER BY [CM Form].[ID]"
    #query = "SELECT [CM Form].[ID], [CM Form].[Client name], [CM Form].[Product Type], [CM Form].[Device Model] FROM [CM Form] WHERE " + WHERE + " ORDER BY [CM Form].[ID]"
    print(query)
    cur.execute(query)
    #fieldlist = [tuple[0] for tuple in cur.description]


    while True:
        row = cur.fetchone()

        if row is None:
            return False
            #break
        else:
            #print(row)
            if ( check_if_cm_ticket_exists(client, prodtype, device) ):
                print("update")
                create_update_customer_survey(client, prodtype, device, row, "update")
                #print("Customer Survey exists")
            else:
                #print("Customer Survey does not exists")
                print("create")
                create_update_customer_survey(client, prodtype, device, row, "create")

            '''
            for i in range(0, len(fieldlist)):
                if not row.get(fieldlist[i]):
                    dict[fieldlist[i]] = ""
                else:
                    dict[fieldlist[i]] = row.get(fieldlist[i])
            #print(dict)
            '''

    cur.close()
    conn.close()
    return True

def read_client_prodtype_device_list_file():
    values = []
    for row in range(1, wb.nrows):
        clint = wb.cell(row ,0).value
        prdtype = wb.cell(row ,1).value
        devmod = wb.cell(row ,2).value
        print("\n----------------------------###########################--------------------------------")
        print(clint + " : " + prdtype + " : " + devmod)
        if ( clint != "" and prdtype != "" and ( devmod != "" and devmod != "None") ):
            create_survey(clint, prdtype, devmod)


def check_if_cm_ticket_exists(client, prodtype, device):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")

    cur = conn.cursor()
    WHERE = "[Customer Survey].[Client Name] = " + str(getid_addclientname(client)) + " and  [Customer Survey].[Product Type] = '" + str(prodtype) + "' and [Customer Survey].[Device Model] = '" + str(device) + "'"
    query = "SELECT [Customer Survey].[ID], [Customer Survey].[Client Name], [Customer Survey].[Product Type], [Customer Survey].[Device Model] FROM [Customer Survey] WHERE " + WHERE
    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            return False
            #break
        else:
            return True

    cur.close()
    conn.close()
    return True

def create_update_customer_survey(client, prodtype, device, row, create_update):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")
    tablename = "Customer Survey"

    print("CM Form Row : " + str(row))
    global custsurveyfieldlist

    if create_update == "update":
        print("Updating Customer Survey")
        try:
            query = "UPDATE [" + tablename + "] SET "
            custsurvey_insert_fieldlist = "(replace"
            custsurvey_insert_valuelist = "(replace"
            for index in range(2, len(row)): # ignore ID and Title fields which are first two fields
                if row[index] != "" and row[index] != "None" and row[index] is not None :
                    value = row[index]
                    if isinstance(value, str):
                        if custsurveyfieldlist[index].lower() == "Device Network Configuration (Static/DHCP)".lower() and ( value == "Verifone" or value == "Client" or value == "3rd Party" ):
                            value = ""
                        elif custsurveyfieldlist[index].lower() == "Revision".lower():
                            value = "Initial Revision"
                        elif custsurveyfieldlist[index].lower() == "Revision Number".lower():
                            value = "1"
                        elif custsurveyfieldlist[index].lower() == "VSP Key".lower():
                            value = re.sub(' +',' ', value)
                            if "A-KEYVSP-PWCPRODREMOTEKEK" in value:
                                value = "Classic Elm & Ficus : Prod : A-KEYVSP-PWCPRODREMOTEKEK"
                            elif "A-KEYTST-PWCDEMOREMOTEKEK" in value:
                                value = "Classic Elm & Ficus : Test : A-KEYTST-PWCDEMOREMOTEKEK"
                            elif "A-KEYVSP-VNTVPRODUCTIONRK" in value:
                                value = "Vantive : Prod : A-KEYVSP-VNTVPRODUCTIONRKK"
                            elif "A-KEYVSP-FDVRK-01" in value:
                                value = "First Data - TransArmor : Prod : A-KEYVSP-FDVRK-01"
                            elif "A-KEYVSP-CPKIF04PRDRMTKEK" in value:
                                value = "Chase Paymentech - SafeTech : Prod : A-KEYVSP-CPKIF04PRDRMTKEK"
                            elif "A-KEYVSP-ELVPRODRMTKEK" in value:
                                value = "Elavon/Simplify Direct Prod/Pilot : A-KEYVSP-ELVPRODRMTKEK" # this may fail since vsp key doesn't have this option
                        elif ( custsurveyfieldlist[index].lower() == "Credit/Debit/EBT Processors".lower() or custsurveyfieldlist[index].lower() == "Gift Processors".lower() or custsurveyfieldlist[index].lower() == "Check Processors".lower() or custsurveyfieldlist[index].lower() == "SCA Point Enterprise Processor".lower() ) and ( value.lower() == "none" ):
                            value = ""
                        if "Classic" in row[3] and value == "First Data":
                            value = value.replace("First Data", "First Data Rapid Connect Classic")
                        elif "Classic" in row[3] and value == "First Data Rapid Connect":
                            value = value.replace("First Data Rapid Connect", "First Data Rapid Connect Classic")
                        elif "Passthrough" in row[3] and value == "First Data":
                            value = value.replace("First Data", "First Data Rapid Connect Pass Through")
                        elif "Passthrough" in row[3] and value == "First Data Rapid Connect":
                            value = value.replace("First Data Rapid Connect", "First Data Rapid Connect Pass Through")
                        value = value.replace("-Disable", "")
                        value = value.replace("-Gift Receipt", "")
                        value = value.replace("-Private Label Card", "")
                        value = value.replace("-Email Offer", "")
                        value = value.replace("-Email Receipt", "")
                    if custsurveyfieldlist[index].lower() == "vhq company id".lower():
                        if str(value) == "POINT-NA for now":
                            query = query + " [VHQ Company ID - Lab] = 'POINT-NA', "
                        else:
                            query = query + " [VHQ Company ID - Lab] = '" + str(value) + "', "
                    elif custsurveyfieldlist[index].lower() == "debit key part number / ksn".lower():
                        query = query + " [Debit Key Part Number / KSN - Lab] = '" + str(value) + "', "
                    elif custsurveyfieldlist[index].lower() == "netconnect username".lower():
                        query = query + " [netconnect username - Lab] = '" + str(value) + "', "
                    elif custsurveyfieldlist[index].lower() == "netconnect password".lower():
                        query = query + " [netconnect password - Lab] = '" + str(value) + "', "
                    else:
                        query = query + " [" + custsurveyfieldlist[index] + "] = '" + str(value) + "', "
            query = query[:-2] + " WHERE [Client Name] = " + str(row[2]) + " and [Product Type] = '" + str(row[3]) + "' and [Device Model] = '" + str(row[8]) + "' ;"

            print(query)
            cur = conn.cursor()
            cur.execute(query)
            cur.commit()
        except Exception as e:
            print('Error : ' + str(e))
            print("\nError updating records into " + tablename + " list. Exiting...")
            print(query)
            return False
    else:
        print("Creating Customer Survey")

        try:
            query = "INSERT INTO [" + tablename + "] "
            custsurvey_insert_fieldlist = "(replace"
            custsurvey_insert_valuelist = "(replace"
            for index in range(2, len(row)): # ignore ID and Title fields which are first two fields
                if row[index] != "None" and row[index] is not None :
                    #print(str(custsurveyfieldlist[index]) + " : " + str(row[index]) + " is Not None")
                    value = row[index]
                    if isinstance(value, str):
                        if custsurveyfieldlist[index].lower() == "Device Network Configuration (Static/DHCP)".lower() and ( value == "Verifone" or value == "Client" or value == "3rd Party" ):
                            value = ""
                        elif custsurveyfieldlist[index].lower() == "VSP Key".lower():
                            value = re.sub(' +',' ', value)
                            if "A-KEYVSP-PWCPRODREMOTEKEK" in value:
                                value = "Classic Elm & Ficus : Prod : A-KEYVSP-PWCPRODREMOTEKEK"
                            elif "A-KEYTST-PWCDEMOREMOTEKEK" in value:
                                value = "Classic Elm & Ficus : Test : A-KEYTST-PWCDEMOREMOTEKEK"
                            elif "A-KEYVSP-VNTVPRODUCTIONRK" in value:
                                value = "Vantive : Prod : A-KEYVSP-VNTVPRODUCTIONRKK"
                            elif "A-KEYVSP-FDVRK-01" in value:
                                value = "First Data - TransArmor : Prod : A-KEYVSP-FDVRK-01"
                            elif "A-KEYVSP-CPKIF04PRDRMTKEK" in value:
                                value = "Chase Paymentech - SafeTech : Prod : A-KEYVSP-CPKIF04PRDRMTKEK"
                            elif "A-KEYVSP-ELVPRODRMTKEK" in value:
                                value = "Elavon/Simplify Direct Prod/Pilot : A-KEYVSP-ELVPRODRMTKEK" # this may fail since vsp key doesn't have this option
                        elif ( custsurveyfieldlist[index].lower() == "Credit/Debit/EBT Processors".lower() or custsurveyfieldlist[index].lower() == "Gift Processors".lower() or custsurveyfieldlist[index].lower() == "Check Processors".lower() or custsurveyfieldlist[index].lower() == "SCA Point Enterprise Processor".lower() ) and ( value.lower() == "none" ):
                            value = ""
                        if "Classic" in row[3] and value == "First Data":
                            value = value.replace("First Data", "First Data Rapid Connect Classic")
                        elif "Classic" in row[3] and value == "First Data Rapid Connect":
                            value = value.replace("First Data Rapid Connect", "First Data Rapid Connect Classic")
                        elif "Passthrough" in row[3] and value == "First Data":
                            value = value.replace("First Data", "First Data Rapid Connect Pass Through")
                        elif "Passthrough" in row[3] and value == "First Data Rapid Connect":
                            value = value.replace("First Data Rapid Connect", "First Data Rapid Connect Pass Through")
                        value = value.replace("-Disable", "")
                        value = value.replace("-Gift Receipt", "")
                        value = value.replace("-Private Label Card", "")
                        value = value.replace("-Email Offer", "")
                        value = value.replace("-Email Receipt", "")
                    if custsurveyfieldlist[index].lower() == "vhq company id".lower():
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [VHQ Company ID - Lab], "
                        if str(value) == "POINT-NA for now":
                            custsurvey_insert_valuelist = custsurvey_insert_valuelist + " 'POINT-NA', "
                        else:
                            custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                    elif custsurveyfieldlist[index].lower() == "debit key part number / ksn".lower():
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [Debit Key Part Number / KSN - Lab], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                    elif custsurveyfieldlist[index].lower() == "netconnect username".lower():
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [netconnect username - Lab], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                    elif custsurveyfieldlist[index].lower() == "netconnect password".lower():
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [netconnect password - Lab], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
                    else:
                        custsurvey_insert_fieldlist = custsurvey_insert_fieldlist + " [" + custsurveyfieldlist[index] + "], "
                        custsurvey_insert_valuelist = custsurvey_insert_valuelist + " '" + str(value) + "', "
            custsurvey_insert_fieldlist = custsurvey_insert_fieldlist[:-2] + " )"
            custsurvey_insert_fieldlist = custsurvey_insert_fieldlist.replace("(replace", "( [Revision], [Revision Number], [How would you like to create Customer Survey Revision], ")
            custsurvey_insert_valuelist = custsurvey_insert_valuelist[:-2] + " );"
            custsurvey_insert_valuelist = custsurvey_insert_valuelist.replace("(replace", "( 'Initial Revision', '1', 'Create from scratch', ")

            #print("Insert field list : " + custsurvey_insert_fieldlist)
            #print("Insert value list : " + custsurvey_insert_valuelist)
            query = query + " " + custsurvey_insert_fieldlist + " VALUES " + custsurvey_insert_valuelist
            print(query)
            cur = conn.cursor()
            cur.execute(query)
            cur.commit()
        except Exception as e:
            print('Error : ' + str(e))
            print("\nError inserting records into " + tablename + " list. Exiting...")
            print(query)
            return False


def get_fieldlist():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Form.accdb;\"")

    global queryfieldlist
    global custsurveyfieldlist
    global fieldexclude_lower
    global fieldlist

    crsr = conn.cursor()

    try:
        # Get field names in CM Form
        res = crsr.execute("SELECT * FROM [CM Form] WHERE 1=0")
        fieldlist = [tuple[0] for tuple in res.description]
        crsr.close()
        #print(fieldlist)
    except pypyodbc.Error:
        crsr.close()
        return 5

    fieldexclude = [ "Priority", "Type of Request", "cm workflow new1", "Select Bid Options", "Environment","Please confirm that Client approval has been received for this r", "Request Status", "Change_Perm", "Total Time Spent on CM Request in Hrs", "Product version", "Who is Boarding the Devices?", "Update_Graphs_Requests_Status", "Update_Graph_Product_Type", "Update_Graphs_Priority", "update_graphs", "Inform_CM_Form_Updates", "Creation time", "DeliveryDate", "Assignee", "Comments", "Delivery instructions", "CM Workflow new", "Delivered_date", "SLA_Stop_Date", "Delivery Date", "Approver on Client Side", "Package", "VCL Configuration Package", "Key File", "VHQ Config File", "Cert to use", "files to be uploaded for sca", "LANEs (one for each device)", "Bid #", "Login Name", "Contact Name", "Contact Phone", "Contact Email Address", "Contact Fax Number", "Key File", "VHQ Config File", "Cert to use", "Serial number", "Files to be uploaded for SCA", "Delivery instructions", "Comments", "Assignee", "CM Workflow", "DeliveryDate", "Creation time", "Inform_CM_Form_Updates", "Update_Graphs", "Update_Internal_Workflow_Status", "Update_Graphs_Requests_Status", "Update_Graphs_Priority", "Update_Graph_Product_Type", "Update_Graphs_Weekly_Requests", "Update_Graphs_Requests_Distribution", "Create_CM_Checklist", "Update_CM_Checklist", "Did you input all requested parameters?", "Did you embed correct VCL file for either production or Demo or ", "What VCL is chosen?", "Did you sign all packages with Verifone Demo, Point or customer ", "What is the signature?", "Did you embed proper media files (MX915 or MX925 or Customized m", "Which media chosen?", "Did you check to see if customer requested EMV and provided prop", "Which EMV package did you chose?", "Did you test and install your package on a device and got succes", "Install successful?", "Did you choose correct SCA package for the specified VX device?", "Please answer which SCA package was chosen?", "Do You confirm all above Checklist information is correct?", "Update_Request_Status_one_time", "Update_Internal_workflow_status_from_Request_status", "CM Workflow new", "Update_CMChecklist_Field", "Update_Graphs_Requests_Distribution_update", "Checklist_Validation_in_CM_Form", "Update_CM_report", "Create_CM_report", "Assignee_Validation", "Content Type", "App Created By", "App Modified By", "“Deliver to” person", "Customer Email", "Attachments", "Workflow Instance ID", "File Type", "Modified", "Created", "Created By", "Modified By", "URL Path", "Path", "Item Type", "Encoded Absolute URL", "internal workflow status", "createdthismonth"]
    fieldexclude_lower = [x.lower() for x in fieldexclude]
    custsurveyfieldlist = []
    for i in range(0, len(fieldlist)):
        if fieldlist[i] not in fieldexclude_lower:
            queryfieldlist = queryfieldlist + ", [CM Form].[" + fieldlist[i] + "]"
            custsurveyfieldlist.append(fieldlist[i])
    queryfieldlist = queryfieldlist[2:]

    #print(queryfieldlist)
    #print(custsurveyfieldlist)

get_fieldlist()
read_client_prodtype_device_list_file()
