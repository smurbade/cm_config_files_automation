import pypyodbc
import os
import subprocess
from pathlib import Path
import glob
from appJar import gui
pypyodbc.lowercase = False
from time import sleep
from colorama import init, Fore, Back, Style
init()
#import gui
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"

import read_access_db


#---------------------------------------------------------------------------------------------------------------

def insert_cdt(cdtorder):             # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    cdt_lines = 0
    cdt_row_params = 0
    cdt_row_ind = 0

    try:
        with open('extract\\cdt.txt') as cdtfile:
            for line in cdtfile:
                line = line.strip()
                if (len(line) > 1) and line[0] != ("\\") and line[0] != ("/") and line[0] != ("#"):
                    cdt_lines+=1
                    param = line.split(' ', 1)[0]

                    if ( cdt_row_params > 1 and cdt_row_params != len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')) ):
                        #print(cdt_row_params)
                        #print(len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')))
                        print(Fore.RED + "cdt.txt is not in standard format. Below line has more values than other lines above it")
                        print(Style.RESET_ALL)
                        print(line)
                        return 1
                    cdt_row_params= len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(','))
                    #print(line)
                    #print(cdt_row_params)
        cdtfile.close()
    except FileNotFoundError:
        print("File Not Found - extract\\cdt.txt. Please make sure file is present")
        return 2
    except IOError:
        print("Error opening extract\\cdt.txt")
        return 3


    #print(cdt_lines)
    #print(cdt_row_params)

    cdt_dict = [[0 for x in range(2)] for y in range(cdt_lines)]
    #cdt_dict = [cdt_lines][cdt_row_params]
    row_ind = 0
    col_ind = 0

    with open('extract\\cdt.txt') as cdtfile:
        for line in cdtfile:
            line = line.strip()
            if (len(line) > 1) and line[0] != ("\\") and line[0] != ("/") and line[0] != ("#"):
                param = line.split(' ', 1)[0]
                if line.split(' ', 1)[0] not in cdtorder[1]:
                    print(Fore.RED + line.split(' ', 1)[0] + " parameter doesn't exist in Sharepoint. Please check cdt.txt for correctness. Exiting ...")
                    print(Style.RESET_ALL)
                    return 8
                #print(cdtorder[0][cdtorder[1].index(line.split(' ', 1)[0])])
                cdt_dict[row_ind][0] = cdtorder[0][cdtorder[1].index(line.split(' ', 1)[0])]
                cdt_dict[row_ind][1] = (line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')
                #print(param)
                row_ind+=1
    cdtfile.close()

    print(cdt_dict)
    print(cdt_dict[0])
    print(cdt_dict[0][1])
    print(cdt_dict[0][1][2])
    #print("# of records : " + str(len(cdt_dict[0][1])))
    print(len(cdt_dict))

    configuserfilename = "extract\\cdt.ini"
    #configuserfile = open(configuserfilename, 'w')

    line = "RecCnt=" + str(len(cdt_dict[0][1])) + "\n\n"

    for reccount in range (0, len(cdt_dict[0][1])):
        line = line + "[Rec_" + str(reccount) + "]\n"

    print(line)
    #configuserfile.write(line)
    #configuserfile.close()

    return 0

#---------------------------------------------------------------------------------------------------------------

def insert_cdtini(id, prodtype, device, environment, cdtvxorder):             # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\cdtvx.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\cdtvx.accdb;\"")

    cur = conn.cursor()
    WHERE = "[cdt vx].[Client Name] = " + str(id) + " AND lcase([cdt vx].[Product Type]) = \'" + prodtype.lower() + "\' AND lcase([cdt vx].[Device]) = \'" + device.lower() + "\' AND lcase([cdt vx].[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM [cdt vx] WHERE " + WHERE
    cur.execute(query)
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        return 7
        #print(Fore.WHITE + "\nSharepoint has cdt records for Client/Prod Type/Device/Environment combination entered")
        #replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        #if ( replace.lower() == 'y' ):
        #    query = "DELETE * FROM [cdt vx] WHERE " + WHERE
        #    try:
        #        cur = conn.cursor()
        #        cur.execute(query);
        #        cur.commit()
        #        print(Fore.GREEN + "Existing records are deleted from Sharepoint")
        #    except:
        #        print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
        #        return 4
        #else:
        #    print(Fore.YELLOW + "\nExiting")
        #    return 5


    cdtini_dict = [""]*len(cdtvxorder[0])
    #print(cdtini_dict)
    row_ind = 0
    queries = []

    try:
        with open('extract\\cdt.ini') as cdtfile:
            for line in cdtfile:
                line = line.strip()
                if (len(line) > 1 and line[0] != ("\\") and line[0] != ("/") and line[0] != ("#")):
                    line = line.replace("\r\n", "\n")
                    line = line.replace("\n", "")
                    line = line.strip()
                    if line.find("RecCnt") != -1:
                        pass
                    else:
                        if line.find("Rec_") != -1:
                            #print(cdtini_dict)

                            if cdtini_dict[0] != "":
                                query = "INSERT INTO [cdt vx] ([Title], [Client Name], [Product Type], [Device], [Environment],"
                                for i in range(0, len(cdtvxorder[0])):
                                    query = query + " [" + cdtvxorder[0][i] + "],"

                                query = query[:-1] + ") VALUES ( 'cdt ini', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '"
                                for i in range(0, len(cdtini_dict)):
                                    query = query + cdtini_dict[i].strip() + "', '"
                                query = query[:-3] + " )"
                                #print(query)
                                queries.append(query)

                            for i in range(0, len(cdtini_dict)):
                                cdtini_dict[i] = ''
                            #print(cdtini_dict)
                        else:
                            param = line.split('=')
                            param[0] = param[0].strip()
                            param[1] = param[1].strip()
                            if param[0] not in cdtvxorder[1]:
                                print(Fore.RED + line.split(' ', 1)[0] + " parameter doesn't exist in Sharepoint. Please check cdt.ini for correctness. Exiting ...")
                                print(Style.RESET_ALL)
                                return 8
                            cdtini_dict[cdtvxorder[1].index(param[0])] = param[1]
                            row_ind+=1

        if cdtini_dict[0] != "":
            query = "INSERT INTO [cdt vx] ([Title], [Client Name], [Product Type], [Device], [Environment],"
            for i in range(0, len(cdtvxorder[0])):
                query = query + " [" + cdtvxorder[0][i] + "],"

            query = query[:-1] + ") VALUES ( 'cdt ini', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '"
            for i in range(0, len(cdtini_dict)):
                query = query + cdtini_dict[i] + "', '"
            query = query[:-3] + " )"
            #print(query)
            queries.append(query)

        cdtfile.close()

    except FileNotFoundError:
        print("File Not Found - extract\\cdt.ini. Please make sure file is present")
        return 2
    except IOError:
        print("Error opening extract\\cdt.ini")
        return 3


    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i])
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(Style.RESET_ALL)
            return 6

    print(Fore.GREEN + "Records inserted successfully from cdt.ini to Sharepoint")
    print(Style.RESET_ALL)

    conn.close()
    return 0


insert_cdt(read_access_db.cdtorder)
