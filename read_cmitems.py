import os
import logging
import pypyodbc
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"

logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
from xlrd import open_workbook
from colorama import init, Fore, Back, Style
init() # initialize color output priting to console through colorama

class read_cmitems():
    def __init__(self):
        self.formnumber = 0
        self.dict = {}
        self.cust_dict = {}
        #self.cmitems_filename = "cmitems.xlsx"
        #self.workbook = open_workbook(self.cmitems_filename)
        #self.wb = self.workbook.sheet_by_name("cmitems")

    def kickstart_cmitems_processing(self):
        self.formnumber = input('Enter Form Number : ')
        #print("Form " + str(self.formnumber) + " processing started")
        #logging.info("Form " + str(self.formnumber) + " processing started")
        #self.read_cm_form_file()
        if len(self.formnumber) > 0:
            if (self.create_dict() == False):
            #if not self.dict:  # if form number is not found in sharepoint site excel then this dictionary will be empty
                #print(Fore.RED + "Form " + str(self.formnumber) + " doesnt exist in " + os.getcwd() + '\\' + self.cmitems_filename + ". \nEither enter correct Form number or refresh and save cmitems.xlsx file and enter Form number")
                print(Fore.RED + "\nForm " + str(self.formnumber) + " doesn't exist in Sharepoint")
                print(Style.RESET_ALL)
                #logging.warning("Form " + str(self.formnumber) + " doesnt exist in " + os.getcwd() + '\\' + self.cmitems_filename + ". Please enter correct Form number")
                return False
            else:
                return True
        else:
            return False

    def kickstart_cmitems_processing_gui(self, formno):
        self.formnumber = int(formno)
        #print("Form " + str(self.formnumber) + " processing started")
        #logging.info("Form " + str(self.formnumber) + " processing started")
        #self.read_cm_form_file()
        if self.formnumber > 0:
            if (self.create_dict() == False):
            #if not self.dict:  # if form number is not found in sharepoint site excel then this dictionary will be empty
                #print(Fore.RED + "Form " + str(self.formnumber) + " doesnt exist in " + os.getcwd() + '\\' + self.cmitems_filename + ". \nEither enter correct Form number or refresh and save cmitems.xlsx file and enter Form number")
                print(Fore.RED + "\nForm " + str(self.formnumber) + " doesn't exist in Sharepoint")
                print(Style.RESET_ALL)
                #logging.warning("Form " + str(self.formnumber) + " doesnt exist in " + os.getcwd() + '\\' + self.cmitems_filename + ". Please enter correct Form number")
                return False
            else:
                return True
        else:
            return False

    def read_cm_form_file(self):
        values = []
        for row in range(self.wb.nrows):
            col_value = []
            if self.wb.cell(row,0).value == 'ID' or self.wb.cell(row,0).value == self.formnumber:  # select only first row and row which has form id from excel
                for col in range(self.wb.ncols):
                    value  = self.wb.cell(row,col).value
                    if self.wb.cell(row, 0).value == self.formnumber:  # if row is for the form number then fill dictionary values
                        self.dict[self.wb.cell(0, col).value.strip()] = value
                    try :
                        value = str(int(value))
                    except : pass
                    col_value.append(value.strip())
                values.append(col_value)

        #print(values)
        #print(self.dict)


    def create_dict(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
            #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")
        # Get field names in CM Form
        #crsr = conn.cursor()
        #res = crsr.execute("SELECT * FROM [CM Form] WHERE 1=0")
        #fieldlist = [tuple[0] for tuple in res.description]
        #crsr.close()
        #print(fieldlist)

        try:
            # Get field names in CM Form
            crsr = conn.cursor()
            res = crsr.execute("SELECT * FROM [CM Ticket] WHERE 1=0")
            fieldlist = [tuple[0] for tuple in res.description]
            crsr.close()
            #print(fieldlist)
        except pypyodbc.Error:
            crsr.close()
            return 5

        queryfieldlist = ""
        #fieldexclude = ["id", "title", "product version", "contact name", "contact phone", "contact email address", "contact fax number", "who is boarding the devices?", "files to be uploaded for sca", 'delivery instructions', 'comments', 'assignee', 'internal workflow status', 'cm workflow', 'deliverydate', 'creation time', 'inform_cm_form_updates', 'update_graphs', 'update_internal_workflow_status', 'update_graphs_requests_status', 'update_graphs_priority', 'update_graph_product_type', 'update_graphs_weekly_requests', 'update_graphs_requests_distribution', 'create_cm_checklist', 'update_cm_checklist', 'did you input all requested parameters?', 'did you embed correct vcl file for either production or demo or ', 'what vcl is chosen?', 'did you sign all packages with verifone demo, point or customer ', 'what is the signature?', 'did you embed proper media files (mx915 or mx925 or customized m', 'which media chosen?', 'did you check to see if customer requested emv and provided prop', 'which emv package did you chose?', 'did you test and install your package on a device and got succes', 'install successful?', 'did you choose correct sca package for the specified vx device?', 'please answer which sca package was chosen?', 'do you confirm all above checklist information is correct?', 'request status', 'update_request_status_one_time', 'update_internal_workflow_status_from_request_status', 'cm workflow new', 'update_cmchecklist_field', 'update_graphs_requests_distribution_update', 'checklist_validation_in_cm_form', 'createdthismonth', 'update_cm_report', 'create_cm_report', 'assignee_validation', 'content type', 'app created by', 'app modified by', '“deliver to” person', 'customer email', 'attachments', 'workflow instance id', 'file type', 'modified', 'created', 'created by', 'modified by', 'url path', 'path', 'item type', 'encoded absolute url']
        fieldexclude = ["Is this a Project ?", "CM Ticket - Daily Mail SLA Miss", "Salesforce Project Number", "CM Ticket - Update Graph - Product Type", "Please confirm that Client approval has been received for this r", "CM Ticket - Change Folder Permissions on Deliver", "CM Ticket - Create CM Report", "VCL Configuration Package", "Ticket Created on", "LANEs (one for each device)", "SLA_Stop_Date", "CM Ticket - Update Email", "Days_to_exclude_detmissing_onhold", "Delivered_date", "Approver on Client Side", "CM Ticket - New Email", "Choose Customer Survey Revision", "Delivery Date", "CM Ticket - Update CM Report", "Select Bid Options", "Ticket Created By", "tktnumbertxtcopy", "Compliance Asset Id", "Total Time Spent on CM Request in Hrs", "ID", "Title", "Priority", "Who is Boarding the Devices?", "Key File", "VHQ Config File", "Cert to use", "Delivery instructions", "Comments", "CM Assigned for the request", "Creation time", "Did you input all requested parameters?", "Did you embed correct VCL file for either production or Demo or ", "What VCL is chosen?", "Did you sign all packages with Verifone Demo, Point or customer ", "What is the signature?", "Did you embed proper media files (MX915 or MX925 or Customized m", "Which media chosen?", "Did you check to see if customer requested EMV and provided prop", "Which EMV package did you chose?", "Did you test and install your package on a device and got succes", "Install successful?", "Did you choose correct SCA package for the specified VX device?", "Please answer which SCA package was chosen?", "Do You confirm all above Checklist information is correct?", "Update_Request_Status_one_time", "Update_Internal_workflow_status_from_Request_status", "Content Type", "App Created By", "App Modified By", "“Deliver to” person", "Customer Email", "Attachments", "Workflow Instance ID", "File Type", "Modified", "Created", "Created By", "Modified By", "URL Path", "Path", "Item Type", "Encoded Absolute URL", "internal workflow status"]
        for i in range(0, len(fieldlist)):
            if fieldlist[i] not in fieldexclude:
                queryfieldlist = queryfieldlist + ", [CM Ticket].[" + fieldlist[i] + "]"
        queryfieldlist = queryfieldlist[2:]

        cur = conn.cursor()
        WHERE = "[CM Ticket].[Ticket Number] = " + str(self.formnumber)
        query = "SELECT " + queryfieldlist + " FROM [CM Ticket] WHERE " + WHERE
        #print(query)
        cur.execute(query)
        fieldlist = [tuple[0] for tuple in cur.description]


        while True:
            row = cur.fetchone()

            if row is None:
                cur.close()
                conn.close()
                return False
                #break
            else:
                for i in range(0, len(fieldlist)):
                    if not row.get(fieldlist[i]):
                        self.dict[fieldlist[i]] = ""
                    else:
                        self.dict[fieldlist[i]] = row.get(fieldlist[i])
                #print(self.dict)
                cur.close()
                conn.close()
                return True


    def create_cust_dict(self, id):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")

        try:
            # Get field names in Customer Survey
            crsr = conn.cursor()
            res = crsr.execute("SELECT * FROM [Customer Survey] WHERE 1=0")
            fieldlist = [tuple[0] for tuple in res.description]
            crsr.close()
            #print(fieldlist)
        except pypyodbc.Error:
            crsr.close()
            print("Error")
            return 5

        #print(id)
        queryfieldlist = ""
        fieldexclude = ["How would you like to create Customer Survey Revision", "Revision Number", "Revision", "Select Revision", "LANEs (one for each device)", "ID", "Title", "Content Type", "App Created By", "App Modified By", "Attachments", "Workflow Instance ID", "File Type", "Modified", "Created", "Created By", "Modified By", "URL Path", "Path", "Item Type", "Encoded Absolute URL"]
        for i in range(0, len(fieldlist)):
            if fieldlist[i] not in fieldexclude:
                queryfieldlist = queryfieldlist + ", [Customer Survey].[" + fieldlist[i] + "]"
        queryfieldlist = queryfieldlist[2:]

        cur = conn.cursor()
        WHERE = "[Customer Survey].[ID] = " + str(id)
        query = "SELECT " + queryfieldlist + " FROM [Customer Survey] WHERE " + WHERE
        #print(query)
        cur.execute(query)
        fieldlist = [tuple[0] for tuple in cur.description]


        while True:
            row = cur.fetchone()

            if row is None:
                cur.close()
                conn.close()
                return False
                #break
            else:
                for i in range(0, len(fieldlist)):
                    if not row.get(fieldlist[i]):
                        self.cust_dict[fieldlist[i]] = ""
                    else:
                        self.cust_dict[fieldlist[i]] = row.get(fieldlist[i])
                #print(self.cust_dict)
                cur.close()
                conn.close()
                return True


    def get_tgz_path(self):
        self.client = (list(self.dict.values())[list(self.dict.keys()).index('Client name')])
        self.prodtype = (list(self.dict.values())[list(self.dict.keys()).index('Product Type')])
        self.device = (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])
        self.environment = (list(self.dict.values())[list(self.dict.keys()).index('Environment')])
        path = self.client + "/" + self.prodtype + "/" + self.device + "/" + self.environment
        #print(path)
        return path

#Uncomment below lines if you need to run this program separately
#cmitems = read_cmitems()
#cmitems.kickstart_cmitems_processing()
#cmitems.get_tgz_path()
