import read_cmitems
import Artifactory_Processing
import copy_final_tgz
import read_conf

import os, glob, re
import subprocess
import logging
logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
from colorama import init, Fore, Back, Style
init() # initialize color output priting to console through colorama
read_conf.read_conf_file()

class cdttxt():
    def __init__(self):
        self.cdt_dict = {}
        self.cardlablelist = []
        self.cardfloorlimit = []
        self.cardsiglimit = []
        self.cmitems = read_cmitems.read_cmitems()
        self.final_tgz_path = ""

    def kickstart_cdt_processing(self):
        formexists = self.cmitems.kickstart_cmitems_processing()
        if formexists == 0:
            Artifactory_file_local_path = "extract"
            #extractsuccess = self.extract(Artifactory_file_local_path)

            #uncomment below lines when Config tgz packages are uploaded to artifactory
            self.final_tgz_path = self.cmitems.get_tgz_path()
            if ((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index('Type of request')]) == 'Update configuration'):
                filelist = glob.glob("extract\*.tgz")
                for f in filelist:
                    os.remove(f)
                filelist = glob.glob("extract\*.zip")
                for f in filelist:
                    os.remove(f)
                Artifactory_Processing.Articatory_Processing(self.cmitems.get_tgz_path())

            device = (list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index('Device Model')])
            devtype = ""
            if "MX" in device or "mx" in device:
                devtype = "MX"
            elif "VX" in device or "vx" in device:
                devtype = "VX"

            #command = "C:\\msys\\1.0\\bin\\bash.exe cdt_extract.sh"
            #command = "\"" + read_conf.git_bash_path[0] + "\" cdt_extract.sh"
            #command = "\"" + read_conf.git_bash_path[0] + "\" untgz.sh " + devtype
            command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\untgz.sh " + devtype
            print("Extracting cdt.txt from existing package. Please wait ...")
            # print(command)
            FNULL = open(os.devnull, 'w')
            result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
            if result.returncode != 0:
                print(result.stderr)
                print(result)
                print("Execution of untgz.sh failed")
            else:
                #print(os.getcwd())
                if os.path.isfile(os.getcwd() + "\\" + 'extract\\cdt.txt'):
                    #print("cdt.txt exists")
                    cdtsuccess = self.Process_cdtFile("cdt.txt")
                    if cdtsuccess == 0:
                        #print("cdt.txt file is created at : " + os.getcwd() + "\extract")
                        self.insert_cdt_to_tgz("extract")
                    else:
                        print("Error creating cdt.txt")
                else:
                    print("cdt.txt is not present in extract folder after untgz.sh is executed")


    def Process_cdtFile(self, cdt_file):
        try:
            f = open('extract\\cdt.txt', 'r')
            cdtfile = open(cdt_file, 'w')
            #print("cdt.txt file opened for reading")
            for line in f:
                first_word = line.split(' ', 1)[0]
                #print("First word : " + first_word)
                if (first_word ==  "CardLabel"):
                    self.cardlablelist = line.replace("\"","").replace(",","").split()
                    cdtfile.write(line)
                    #print("Card label list : " + str(self.cardlablelist))
                    #self.Process_CardLabel()
                    #break
                elif (first_word ==  "BatchAuthFloorLimitAmt"):
                    self.cardfloorlimit = line.replace(",","").split()
                    if ((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index("SAF Enabled")]) == 'Yes'):
                        #print("SAF enabled Yes")
                        self.ProcessTotalFloor()
                        BatchAuthFloorLimitAmt_line = ''
                        BatchAuthFloorLimitAmt_line = BatchAuthFloorLimitAmt_line +  str(self.cardfloorlimit[0]) + " " + str(self.cardfloorlimit[1])
                        for ind, item in enumerate(self.cardfloorlimit):
                            if ind > 1:
                                BatchAuthFloorLimitAmt_line = BatchAuthFloorLimitAmt_line + ", " + str(self.cardfloorlimit[ind])
                        BatchAuthFloorLimitAmt_line = BatchAuthFloorLimitAmt_line + "\n"
                        #print("BatchAuthFloorLimitAmt_line : " + BatchAuthFloorLimitAmt_line)
                        cdtfile.write(BatchAuthFloorLimitAmt_line)
                    else:
                        #print("SAF enabled No")
                        cdtfile.write(line)
                    #print("BatchAuthFloorLimitAmtlist : " + str(self.cardfloorlimit))
                    #self.Process_BatchAuthFloorLimitAmt()
                    #break
                elif (first_word ==  "SignLimitAmount"):
                    self.cardsiglimit = line.replace(",","").split()
                    self.ProcessSigLimit("Sig Limit-Visa", "VISA")
                    self.ProcessSigLimit("Sig Limit-MC", "MASTERCARD")
                    self.ProcessSigLimit("Sig Limit-Amex", "AMEX")
                    self.ProcessSigLimit("Sig Limit-Discover", "DISCOVER")
                    self.ProcessSigLimit("Sig Limit-JCB", "JCB")
                    SignLimitAmount_line = ''
                    SignLimitAmount_line = SignLimitAmount_line +  str(self.cardsiglimit[0]) + " " + str(self.cardsiglimit[1])
                    for ind, item in enumerate(self.cardsiglimit):
                        if ind > 1:
                            SignLimitAmount_line = SignLimitAmount_line + ", " + str(self.cardsiglimit[ind])
                    SignLimitAmount_line = SignLimitAmount_line + "\n"
                    #print("SignLimitAmount_line : " + SignLimitAmount_line)
                    cdtfile.write(SignLimitAmount_line)
                    #print("SignLimitAmountlist : " + str(self.cardsiglimit))
                    #self.Process_SignLimitAmount()
                    #break
                else:
                    #print(line)
                    cdtfile.write(line)
                    #break
                #self.cdt_dict[line.split(None, 1)[0]].
            #print("Cardlabellist : " + str(self.cardlablelist))
            #print("BatchAuthFloorLimitAmtlist : " + str(self.cardfloorlimit))
            #print("SignLimitAmountlist : " + str(self.cardsiglimit))
            #print(self.cdt_dict)
            cdtfile.close()
            return 0
        except:
            print("Error opening extract\\cdt.txt")
            return 1

    def ProcessSigLimit(self, Form_Param, Processor):
        if ((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index(Form_Param)]) != '') and ((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index(Form_Param)]) != '0'):
            #print(Form_Param + " not blank or 0")
            ind = 0
            for parameter in self.cardlablelist:
                if parameter == Processor:
                    #print("index = " + str(ind))
                    self.cardsiglimit[ind] = int((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index(Form_Param)]))
                ind = ind + 1
        #print("SignLimitAmountlist : " + str(self.cardsiglimit))

    def ProcessTotalFloor(self):
        if ((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index("Transaction Floor Limit")]) != '') and ((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index("Transaction Floor Limit")]) != '0'):
            #print("Transaction Floor Limit not blank or 0")
            for ind, item in enumerate(self.cardfloorlimit):
                if ind > 0:
                    self.cardfloorlimit[ind] = int((list(self.cmitems.dict.values())[list(self.cmitems.dict.keys()).index("Transaction Floor Limit")]))
        #print("Transaction Floor Limit List : " + str(self.cardfloorlimit))

    def insert_cdt_to_tgz(self, Artifactory_file_local_path):
        #command = "C:\\msys\\1.0\\bin\\bash.exe cdt_insert.sh"
        #command = "\"" + read_conf.git_bash_path[0] + "\" cdt_insert.sh"
        command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\cdt_insert.sh"
        print("Inserting updated cdt file into tgz file. Please wait ...")
        # print(command)
        FNULL = open(os.devnull, 'w')
        result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
            print("Execution of insert_cdt.sh failed")
        else:
            #print("final tgz path : " + self.final_tgz_path)
            copy_final_tgz.check_directory_and_copy_tgz("extract",self.final_tgz_path)
            print("Process completed. Please check updated tgz file @ " + os.getcwd() + "\\" + self.cmitems.client + "\\" + self.cmitems.prodtype + "\\" + self.cmitems.device + "\\" + self.cmitems.environment)
            # print("Process completed. Please check updated tgz file @ " + os.getcwd() + "\\extract folder")

### Uncomment below lines if you want to run this program separately
#artifact = Artifactory_Processing.Articatory_Processing("RMS_Release_CLW/GTO-Retail/MxPoint/SCA_2.19.27B5/MxPoint-SCA_2.19.27B5-20161227.zip")
#Artifactory_file_local_path = "extract"
#cdtobj = cdt.cdttxt()
#cdtobj.kickstart_cdt_processing()

