import Git_operations
import Artifactory_Processing
import Extract_tgz
import copy_final_tgz
import read_conf
read_conf.read_conf_file()

from xlrd import open_workbook
import subprocess
import re
import os, os.path
import pypyodbc
from pygit2 import clone_repository
import logging
logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
from colorama import init, Fore, Back, Style
init() # initialize color output priting to console through colorama

import csv
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"

mapping_filename = base_path + "CMAutomation\\CM_Portal_To_Customer_Portal_Mapping.xlsx"
workbook = open_workbook(mapping_filename)
wb = workbook.sheet_by_name("Mapping")
CMPortal_to_CustomerPortal = {}
value_mapping = {}

def read_mapping_file():
    for row in range(wb.nrows):
        custportalfield = wb.cell(row, 0).value.strip()
        cmportalfield = wb.cell(row, 1).value.strip()
        if custportalfield != "" and cmportalfield != "":
            CMPortal_to_CustomerPortal[custportalfield] = cmportalfield

    print("field mapping : " + str(CMPortal_to_CustomerPortal))

def read_csv_and_map_value():
    try:
        with open(base_path + 'CMAutomation\\CM_Portal_To_Customer_Portal_Mapping.csv') as csvfile:
            rows = csv.reader(csvfile)
            CustomerPortal_to_Value = list(zip(*rows))
            print("input csv : " + str(CustomerPortal_to_Value))
            for i in range(1, len(CustomerPortal_to_Value[0])):
                value_mapping[CMPortal_to_CustomerPortal[CustomerPortal_to_Value[0][i]]] = CustomerPortal_to_Value[1][i]
            print("Value Mapping : " + str(value_mapping))
        return True
    except:
        print("Error in read_csv function")
        return False


def create_cm_request():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\CM Form.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\CM Form.accdb;\"")

    tablename = "CM Form"
    cur = conn.cursor()

    query = "INSERT INTO [" + tablename + "] ( "
    for i in sorted(value_mapping):
        query = query + "[" + i +  "], "
    query = query[:-2]
    query = query + " ) VALUES ( "
    for i in sorted(value_mapping):
        query = query + "'" + str(value_mapping[i]) + "', "
    query = query[:-2]
    query = query + " ) ;"
    print(query)

    #cur.execute(query)
    #cur.commit()
    cur.close()


read_mapping_file()
if read_csv_and_map_value():
    create_cm_request()


