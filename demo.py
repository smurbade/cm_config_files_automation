import random
import django

import sharepoint

from sharepoint import SharePointSite, basic_auth_opener

server_url = "http://sharepoint.example.org/"
site_url = server_url + "sites/foo/bar"

opener = basic_auth_opener(server_url, "username", "password")

site = SharePointSite(site_url, opener)


username = "T_SachinM2"
password = "SSSSNN5nN"

# Try block
print("try block section")
try:
    for x in range(5):
        rand_number=random.randint(1,6)
        print(rand_number/0)
except ZeroDivisionError:
    print('divide by 0 error')
finally:
    print('This is going to execute no matter what')


# for loop
print()
print("for loop section")
for x in range(5):
    rand_number=random.randint(1,6)
    print(rand_number)

# While loop
print()
print("while loop section")
counter = 0
while(counter<10):
    counter+=1

# function
print()
print("function section section")
def add(a,b):
    return a + b

def square(c):
    return c * c

print(square(add(4,5)))

# if else
print()
print("if else section")
username = "admin"
password = "admin123"

if username=="admin" and password == "admin123":
    print("Valid user")
else:
    print("invalid user")

# Range
print()
print("Range section")
for x in range(1,3):
    print("Sachin")

numbersrange = list(range(10))
numbersrange2 = list(range(3,8))
numbersrange3 = list(range(3,30,5))
print(numbersrange)
print(numbersrange2)
print(numbersrange3)

# List
print()
print("List Section")
flights = ["Emirates", "Delta", "American Airlines", "British Airways", "Air India"]
for x in flights:
    print(x)

for x in range(0,9,2):
    print(x)

def function1():
    print("Hi")
    print("Bye")

function1()

fruits = ["Apple","Mango","Peach"]
print("Apple" in fruits)
print("Orange" in fruits)

fruits.append("Banana")
fruits.insert(1, "Pear")
print(len(fruits))
print(fruits)
print(fruits.index("Peach"))

names =["Mike", "Sachin", "Rob", "Tony"]
print(names[0])

numbers = [1,2,3,4,5]
numbers[2] = 5
print(numbers[2])

new_numbers = [2,3,2,3,2,3]
print(numbers+new_numbers)
print(numbers*3)

abc = []
print(abc)

# list slicing
print()
print("List slicing")
numbers = [0, 100, 200, 400, 300, 500, 600]
print(numbers[2:5])
print(numbers[:3])
print(numbers[3:])
print(numbers[1:6:2]) #print list from index 1 to 6 with interval of 2 index

#List comprehension - if you need a list which follow certain rule
print()
print("List comprehension")

list_square = [x**2 for x in range(5)] # list of squares of numbers ranging from 0 to 4
print(list_square)

list_square_even = [x**2 for x in range(5) if x**2 % 2 == 0] # list of squares of numbers ranging from 0 to 4 only if suare is even
print(list_square_even)


# tuple section - it's a list for which value can not be changed once defined ... value are mentioned in ( ) or without ( ) instead of [] as for like list
print()
print("tuple section")
fruits = ("Apple", "Mango", "Peach")  # tuple with ( )
print(fruits[0])
# fruits[0]="pineapple" - This statement will fail since in tuple, we can't reassign a value
cricketer = "Sachin", "Dravid", "Sunil"  # tuple without ( )
print(cricketer[1])


# Dictionary - key value pair
print()
print("Dictionary section")
people = {"John":35, "Sachin":34, "Mike":40}
print(people["Sachin"])

numbers ={
    1: "one",
    2: "two",
    3: "three"
}

print(1 in numbers)
print(5 in numbers)
print(numbers.get(2))
print(numbers.get(5))
print(numbers.get(5,"Key not found"))

# file operations
print()
print("file operations section")
demofile = open("demo.txt",'r')
content = demofile.read()
print(content)
demofile.close()

demofile = open("demo.txt",'r')
content = demofile.read(10)
print(content)
demofile.close()

demofile = open("demo.txt",'r')
content = demofile.readline()
print(content)
demofile.close()

demofile = open("demo.txt",'w')
demofile.write("written from program\n")
demofile.close()

demofile = open("demo.txt",'a')
demofile.write("second line written from program")
demofile.close()

# String formatting
print()
print("String formatting section")
numbers = [4, 5, 6]
newstring = "Numbers:{0}{1}{2}".format(numbers[0], numbers[1], numbers[2])
print(newstring)

a = "{x}/{y}".format(x=100, y=200)
print(a)

# String Functions
print()
print("String Functions section")

print(",".join(["Apple", "Banana", "Peach"]))
print("Hello there".replace("there", "Sachin"))

newstring = "This is a string"
print(newstring.startswith("This"))
print(newstring.endswith("str"))
print(newstring.upper())

# Numeric functions
print()
print("Numeric functions section")

print(min(1, 2, 3, 4, 0))
print(max(1, 2, 3, 4, 0))
print(abs(-203)) # value of number without sign + or -

# Functional programming
print()
print("Functional programming section")

def add_ten(x):
    return x+10

def twice(func, arg):
    return func(func(arg))

print(twice(add_ten, 5))


# Lambdas - similar to function but dont have to name. they dont have return statements
print()
print("Lambdas section")

print((lambda x: x**2)(30)) # this will calculate square of 30 and store in result


# map
print()
print("map section")

def add(x):
    return x+2

newlist = [10, 20, 30, 40, 50]
result = list(map(add, newlist)) # map function will run add function on each newlist item and resultant list will be saved in result list
print(result)

# other way for same using lambda
result = list(map(lambda x: x+2, newlist))
print(result)


# filters
print()
print("Filters section")

newlist = [1, 4, 5, 7, 9, 10, 13]
result = list(filter(lambda x: x % 2 == 0, newlist))
print(result)


# generators - different than list in which case their items cant be referenced by index numbers
# generators can be used to certain type of list
print()
print("Generators section")

def function():
    counter = 0
    while counter < 5:
        yield counter
        counter+=1

for x in function():
    print(x)

def even_numbers(x):
    for i in range(x):
        if i % 2 == 0:
            yield i

print(list(even_numbers(20)))


# Object oriented programming
print()
print("Object oriencted programming")

class students:

    def __init__(self, name, contact):
        self.name = name
        self.contact = contact


    def getdata(self):
        print("Accepting Data:")
        self.name = "Sachin"
        self.contact = '5364536354'

    def putdata(self):
        print("The name is : "+self.name+" Contact is : "+self.contact)


john = students("",0)
john.getdata()
john.putdata()


# inheritance
print()
print("Inheritance section")

class sciencestudents(students):
    def __init__(self, age):
        self.age = age

    def science(self):
        print("I am a science student")


rob = sciencestudents(20)
rob.science()
rob.getdata()
rob.putdata()


# Recursion
print()
print("Recursion section")
def factorial(x):
    if x == 1:
        return 1
    else:
        return x * factorial(x -1)

print(factorial(45))


# Sets
print()
print("Sets section")

numbers = {1, 2, 3, 4, 5, 6}
print(numbers)
print(5 in numbers)

numbers.add(9)
print(numbers)
numbers.remove(4)
print(numbers)

seta = {1, 2, 3, 4, 5}
setb = {4, 5, 6, 7, 8}

print(seta | setb)  # union of two sets means combining two sets excluding duplicate elements
print(seta & setb)  # intersection of elements means common elements in two sets
print(seta - setb)  # subtraction means elements remove elements from first set which are in second set


# itertools
print()
print("itertools section")

from itertools import count

for i in count(3):
    print(i)
    if i >= 20:   # if this check is not there then for loop will print numbers from 3 till infinity
        break
from itertools import accumulate

print(list(accumulate(range(8))))


# Data hiding
print()
print("Data hiding section")

class myclass:
    __hiddenvariable = 0  # __ before a variable name makes a variable private or hidden

    def add(self, increment):
        self.__hiddenvariable+=increment
        print(self.__hiddenvariable)

objectone = myclass()
objectone.add(5)

# Regular expressions
print()
print("Regular Expressions section")

import re

pattern = r"eggs"

if re.match(pattern,"eggseggseggs"):
    print("Match found")
else:
    print("No match found")

if re.match(pattern,"eggseggsbacon"):
    print("Match found")
else:
    print("No match found")

if re.match(pattern,"baconeggseggsbacon"):
    print("Match found")
else:
    print("No match found")


if re.search(pattern,"baconeggseggsbacon"):
    print("Match search found")
else:
    print("Match search not found")

if re.findall(pattern,"baconeggseggsbacon"):
    print("Match findall found")
else:
    print("Match findall not found")

print(re.findall(pattern,"baconeggseggsbacon" ))

string = "My name is John, Hi I'm John"
pattern = r"John"
print(re.sub(pattern, "Rob", string))

pattern = r"gr.y"
if re.match(pattern, "gray"):
    print(". charachter matched")
else:
    print(". character not matched")

pattern = r"^gr.y$"  # ^ specifies start of string and $ signifies end of string
if re.match(pattern, "gray"):
    print("^$ Matched")
else:
    print("^$ not matched")

if re.match(pattern, "arby"):
    print("^$ Matched")
else:
    print("^$ not matched")

pattern = r"[A-Z][A-P][0-9]"
if re.match(pattern, "AA1"):
    print("Pattern Matched")
else:
    print("Pattern not matched")

if re.match(pattern, "2A1"):
    print("Pattern Matched")
else:
    print("Pattern not matched")

pattern = r"eggs(bacon)*" # () creates a group ... this pattern matches eggs followed by bacon any number of times
if re.match(pattern, "eggsbacon"):
    print("Pattern * Matched")
else:
    print("Pattern * not matched")
if re.match(pattern, "eggsbaconbacon"):
    print("Pattern * Matched")
else:
    print("Pattern * not matched")
if re.match(pattern, "eggs"):
    print("Pattern * Matched")
else:
    print("Pattern * not matched")


pattern = r"bread(eggs)*bread"


# tkinter
print()
print("tkinter section")
'''
from tkinter import *

root = Tk()
'''
'''
label1 = Label(root, text="First name")
label2 = Label(root, text="Last name")

entry1 = Entry(root)
entry2 = Entry(root)

label1.grid(row=0, column=0)
label2.grid(row=1, column=0)

entry1.grid(row=0, column=1)
entry2.grid(row=1, column=1)
'''

'''
frame1 = Frame(root)
frame1.pack()

frame2 = Frame(root)
frame2.pack(side=BOTTOM)

button1 = Button(frame1, text="Click Here", fg="RED")
button2 = Button(frame2, text="Click Here", fg="Blue")

button1.pack()
button2.pack()
'''

'''
label3 = Label(root, text="Third", bg="Red", fg="White")
label3.pack(fill=X) # this will adjust label to width of window making it self adjustable

label4 = Label(root, text="Fourth", bg="Blue", fg="Green")
label4.pack(side=LEFT, fill=Y)  # this will adjust label to height of window making it self adjustable
'''


'''
def dosomething():
    print("You clicked the button")

button3 = Button(root, text="Click button 3", command=dosomething)
button3.pack()
'''


'''
class mybuttons():
    def __init__(self, rootone):
        frame = Frame(rootone)
        frame.pack()

        self.printbutton = Button(frame, text="Click Here", command=self.printmessage)
        self.printbutton.pack()

        self.quitbutton = Button(frame, text="Exit", command=frame.quit)
        self.quitbutton.pack(side=LEFT)

    def printmessage(self):
        print("Button Clicked")

b = mybuttons(root)
'''


'''
def function1():
    print("Menu Item Project clicked")

def function2():
    print("Menu Item Save clicked")

mymenu = Menu(root)
root.config(menu=mymenu)

submenu = Menu(mymenu)

mymenu.add_cascade(label="File", menu=submenu)
submenu.add_command(label="Project", command=function1)
submenu.add_command(label="Save", command=function2)

submenu.add_separator()
submenu.add_command(label="Exit", command=quit)

newmenu = Menu(mymenu)
mymenu.add_cascade(label="Edit", menu=newmenu)
newmenu.add_command(label="Undo", command=function1)
'''


# Toolbar
'''
toolbar = Frame(root, bg="pink")
insertbutton = Button(toolbar, text="Insert Files", command=function1)
insertbutton.pack(side=LEFT, padx=2, pady=3)

printbutton = Button(toolbar, text="Print", command=function1)
printbutton.pack(side=LEFT, padx=2, pady=3)

toolbar.pack(side=TOP, fill=X)

status = Label(root, text="Status Bar", border=1, relief=SUNKEN, anchor=W)
status.pack(side=BOTTOM, fill=X)
'''

'''
import tkinter.messagebox

tkinter.messagebox.showinfo("Title","This is awesome")
response = tkinter.messagebox.askquestion("Question 1","Do you like coffee ?")
if response == 'yes':
    print("Here is a coffee for you")

'''
'''
canvas = Canvas(root, width=200, height=100)
canvas.pack()
newline = canvas.create_line(0, 0, 50, 100)
otherline = canvas.create_line(10, 10, 100, 100, fill="Green")


root.mainloop()
'''

# Read sharepoint site
'''
import requests
from requests.auth import HTTPBasicAuth

headers = {'accept': 'application/json;odata=verbose'}
r = requests.get("https://verifone365.sharepoint.com/sites/NAConfigMgmt/_api/web", auth=HTTPBasicAuth('VERIFONE\\T_SachinM2', 'SSSSNN5nN'), headers=headers)

print(r)
'''

'''
import requests
from requests_ntlm import HttpNtlmAuth

session = requests.Session()
session.auth = HttpNtlmAuth('VERIFONE\\T_SachinM2', 'SSSSNN5nN', session)
rr = session.get('https://verifone365.sharepoint.com/sites/NAConfigMgmt/Lists/CM%20Form/Open%20Requests.aspx')
print(rr)
'''
#############################
'''
import requests
from requests_ntlm import HttpNtlmAuth

r = requests.get("https://verifone365.sharepoint.com/sites/NAConfigMgmt/Lists/CM%20Form/Open%20Requests.aspx",auth=HttpNtlmAuth('VERIFONE\\T_SachinM2', 'SSSSNN5nN'))
print(r)
'''
#########################

'''
import requests
from requests.auth import HTTPDigestAuth
url = 'https://verifone365.sharepoint.com/sites/NAConfigMgmt/Lists/CM%20Form/Open%20Requests.aspx'
r = requests.get(url, auth=HTTPDigestAuth('VERIFONE\\T_SachinM2', 'SSSSNN5nN'))
print(r)
'''

'''
#r = requests.get('https://verifone365.sharepoint.com/sites/NAConfigMgmt/Lists/CM Form/', auth=('T_SachinM2', 'SSSSNN5nN'))
#print(r.text)

from requests.auth import HTTPBasicAuth
r = requests.get('https://verifone365.sharepoint.com/sites/NAConfigMgmt/Lists/CM Form/', auth=HTTPBasicAuth('T_SachinM2', 'SSSSNN5nN'))
print(r)
'''


# read excel

####################################################
# Read sharepoint site excel into dictionary

import xlrd

#workbook = xlrd.open_workbook('C:\Verifone\ConfigFilesAutomation\cmitems.xlsx')
#worksheet = workbook.sheet_by_index(0)
#print(worksheet.cell(0, 0).value)
wb = xlrd.open_workbook('C:\Verifone\ConfigFilesAutomation\cmitems.xlsx')
dict = {}
formnumber = -9876 # setting form number to some random number so as to identify later
formnumber = int(input('Enter Form Number : '))
for s in wb.sheets():
    #print 'Sheet:',s.name
    values = []
    for row in range(s.nrows):
        col_value = []
        if s.cell(row,0).value == 'ID' or s.cell(row,0).value == formnumber:  # select only first row and row which has form id from excel
            for col in range(s.ncols):
                value  = (s.cell(row,col).value)
                if s.cell(row, 0).value == formnumber:  # if row is for the form number then fill dictionary values
                    dict[s.cell(0, col).value] = value
                try :
                    value = str(int(value))
                except : pass
                col_value.append(value)
            values.append(col_value)
print(values)
print(dict)

if not dict:  # if form number is not found in sharepoint site excel then this dictionary will be empty
    print("Form record doesnt exist. Please enter correct Form number")
else:

    ####################################################
    # Read Point parameter file into dictionary

    pointparam = xlrd.open_workbook('C:\Verifone\ConfigFilesAutomation\pointparameters.xlsx')
    pointparam_dict = {} # this dictionary has key value as parameter name from point parameters file

    for s in pointparam.sheets():
        for row in range(s.nrows):
            if row != 0:
                for col in range(s.ncols):
                    param = s.cell(row, 0).value
                    if col != 0:
                        pointparam_dict.setdefault(param, [])
                        pointparam_dict[param].append(s.cell(row, col).value)
    print("Point parameters")
    print(pointparam_dict)
    #print("Point form parameters")
    #print(pointparam_dict_form)


    ####################################################

    devmodel_mx_pattern = r"MX"
    devmodel_vx_pattern = r"VX"
    devmodel_both_pattern = r"Both"

    Config_User1_Dict = {}
    Config_User1_Dict['CBT'] = {}
    Config_User1_Dict['CTT'] = {}
    Config_User1_Dict['DCT'] = {}
    Config_User1_Dict['DHI'] = {}
    Config_User1_Dict['HDT'] = {}
    Config_User1_Dict['MCT'] = {}
    Config_User1_Dict['MID'] = {}
    Config_User1_Dict['PDT'] = {}
    Config_User1_Dict['RCHI'] = {}
    Config_User1_Dict['RCT'] = {}
    Config_User1_Dict['SAF'] = {}
    Config_User1_Dict['SCT'] = {}
    Config_User1_Dict['STB'] = {}
    Config_User1_Dict['TDT'] = {}
    Config_User1_Dict['TID'] = {}

    CBT  = {}
    CTT  = {}
    DCT  = {}
    DHI  = {}
    HDT  = {}
    MCT  = {}
    MID  = {}
    PDT  = {}
    RCHI = {}
    RCT  = {}
    SAF  = {}
    SCT  = {}
    STB  = {}
    TDT  = {}
    TID  = {}
    ####################################################
    # Function to set config user parameters

    def set_config_user1_Parameters(param_from_form):
        if (list(dict.values())[list(dict.keys()).index(param_from_form)]) == 'Yes':
            parametername2 = ''
            if (re.match(devmodel_mx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])) or re.match(devmodel_both_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')]))):
                for parametername2, values in pointparam_dict.items():
                    if (param_from_form in values):
                        if ('MX' in values or 'Both' in values):
                            print("parameter name is", parametername2)
                            break
            if (re.match(devmodel_vx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])) or re.match(devmodel_both_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')]))):
                for parametername2, values in pointparam_dict.items():
                    if (param_from_form in values):
                        if ('VX' in values or 'Both' in values):
                            print("parameter name is", parametername2)
                            break
            exec("%s%s%s" % ("Config_User1_Dict['", (list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)

    '''
    def set_config_user1_Parameters2(param_from_form, mx_parameter_name, vx_parameter_name):
        if re.match(devmodel_mx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])):
            print(param_from_form," is yes for MX")
            parametername=mx_parameter_name
        if re.match(devmodel_vx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])):
            print("Counter Tip is yes for VX")
            parametername=vx_parameter_name
        exec("%s%s%s" % ("Config_User1_Dict['", (list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0]),"'][parametername]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
    '''



    ####################################################

    if (list(dict.values())[list(dict.keys()).index('Product Type')]) == 'SCA Point Classic':
        print("This form is for SCA Point Classic")
        #set_config_user1_Parameters2("Split Tender","SPLITTENDER","SPLIT")
        set_config_user1_Parameters("Email Capture")
        set_config_user1_Parameters("Loyalty Capture")
        set_config_user1_Parameters("Cashback (only if Debit = Y)")
        set_config_user1_Parameters("Counter Tip")
        set_config_user1_Parameters("Credit")
        set_config_user1_Parameters("EBT")
        set_config_user1_Parameters("Gift")
        set_config_user1_Parameters("MID")
        set_config_user1_Parameters("Partial Auth")
        set_config_user1_Parameters("Debit")
        set_config_user1_Parameters("SAF Enabled")
        set_config_user1_Parameters("Signature Capture (MX Only)")
        set_config_user1_Parameters("Split Tender")
        set_config_user1_Parameters("TIDs (one for each device)")

    '''
        if (list(dict.values())[list(dict.keys()).index('Signature Capture (MX Only)')]) == 'Yes':
            if re.match(devmodel_mx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])):
                print("Signature Capture is yes for MX")
                parametername='sigcapture'
            #print(list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0])
            #exec("%s%s" % (list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0], ".setdefault(parametername, [])")) # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
            #exec("%s%s" % (list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0], "[parametername].append('y')"))    # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
            #exec("%s%s" % (list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0], "[parametername]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
            print(list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)])
            exec("%s%s%s" % ("Config_User1_Dict['",(list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0]), "'][parametername]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
            #Config_User1_Dict['PDT'][parametername] = 'y'
            #PDT.setdefault('sigcapture', [])
            #PDT['sigcapture'].append('y')
        if (list(dict.values())[list(dict.keys()).index('Counter Tip')]) == 'Yes':
            if re.match(devmodel_mx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])):
                print("Counter Tip is yes for MX")
                parametername='countertip'
            if re.match(devmodel_vx_pattern, (list(dict.values())[list(dict.keys()).index('Device Model')])):
                print("Counter Tip is yes for VX")
                parametername='CPTIP'
            print(list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)])
            exec("%s%s%s" % ("Config_User1_Dict['", (list(pointparam_dict.values())[list(pointparam_dict.keys()).index(parametername)][0]),"'][parametername]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
    '''

    '''
        if (list(dict.values())[list(dict.keys()).index('Credit')]) == 'Yes':
            set_config_user1_Parameters("Credit", "credit", "credit")
        if (list(dict.values())[list(dict.keys()).index('EBT')]) == 'Yes':
            set_config_user1_Parameters("EBT", "EBT", "EBT")
        if (list(dict.values())[list(dict.keys()).index('Gift')]) == 'Yes':
            set_config_user1_Parameters("Gift", "GIFT", "GIFT")
        if (list(dict.values())[list(dict.keys()).index('Email Capture')]) == 'Yes':
            set_config_user1_Parameters("Email Capture", "CptrEmail", "emailcapture")
        if (list(dict.values())[list(dict.keys()).index('Loyalty Capture')]) == 'Yes':
            set_config_user1_Parameters("Loyalty Capture", "loyaltycapture", "CPTRLYLTY")
        if (list(dict.values())[list(dict.keys()).index('MID')]) == 'Yes':
            set_config_user1_Parameters("MID", "mid", "mid")
        if (list(dict.values())[list(dict.keys()).index('Partial Auth')]) == 'Yes':
            set_config_user1_Parameters("Partial Auth", "partialauth", "PARTATH1")
        if (list(dict.values())[list(dict.keys()).index('SAF Enabled')]) == 'Yes':
            set_config_user1_Parameters("SAF Enabled", "safenabled", "BAUTH")
    '''

        #print(PDT)
        #print(Config_User1_Dict)

    #################################################
    # Create config user1 files

    #for x in PDT.items():
    #    print(x[0],'=',x[1])

    #for tt in pointparam_dict:
    #    print(list(pointparam_dict.values())[list(pointparam_dict.keys()).index(tt)][3])

    import os, os.path
    os.chdir(os.path.dirname(__file__))
    print(os.getcwd())

    formnumber_dir = os.getcwd()+ '\\' + str(formnumber)

    if not os.path.isdir(formnumber_dir):
        os.mkdir(formnumber_dir)

    print("Formnumber dir " + formnumber_dir)
    configuserfilename = formnumber_dir + "\\config.usr1"
    print(configuserfilename)
    configuserfile = open(configuserfilename,'w')
    #with open(formnumber_dir + "\\config.usr1", 'w') as configuserfile:
    print()
    print(configuserfile)
    for x in Config_User1_Dict:
        if Config_User1_Dict[x]:
            header = "[" + x + "]\n"
            print ("[",x,"]")
            configuserfile.write(header)
            for y in Config_User1_Dict[x]:
                para = y + "=" + Config_User1_Dict[x][y] + "\n"
                print (y,'=',Config_User1_Dict[x][y])
                configuserfile.write(para)

    configuserfile.close()

######################################################

# input from command line
print()
print("input from command line section")
marks = int(input('Enter marks : '))

if marks>=90:
    print('Grade A')
elif marks>=70:
    print('Gade B')
elif marks>=60:
    print('Grade C')

add(input("enter a"),input("enter b4"))

