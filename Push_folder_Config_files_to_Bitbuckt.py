
import xlrd
import os

class Push_folder_Config_files_to_Bitbucket():
    def __init__(self):
        self.workbook = xlrd.open_workbook('cmitems.xlsx')
        self.cmitems = self.workbook.sheet_by_name("cmitems")
        self.folder_names_dict = {}
        self.folder_names = []

    def read_client_names_from_cm_form_file(self):
        print(self.cmitems.nrows)
        for row in range(1, self.cmitems.nrows):
            if self.cmitems.cell(row,6).value != 'None': # If device model is not None
                self.folder_names_dict.setdefault(self.cmitems.cell(row, 1).value, [])  # Client Name
                self.folder_names_dict[self.cmitems.cell(row, 1).value].append(self.cmitems.cell(row, 6).value)  # Device Model
                self.folder_names_dict[self.cmitems.cell(row, 1).value].append(self.cmitems.cell(row, 4).value)  # Product Type
                self.folder_names_dict[self.cmitems.cell(row, 1).value].append(self.cmitems.cell(row, 3).value)  # Environment
        print(self.folder_names_dict)
        self.create_folder_name()

    def create_folder_name(self):
        x = 0
        for client_name in self.folder_names_dict:
            #print(client_name)
            x = int(len(self.folder_names_dict[client_name]))
            #print("Total Elements : " + str(x))
            #for iter_no in range(0, x) :
                #print(iter_no)
                #print(self.folder_names_dict[client_name][iter_no])
            #print("Start For #########################")
            #print(str(x))
            #print(str(int(x / 3)))
            y = 0
            #print("Start while")
            while (y < x):
                Device_model = ""
                Product_type = ""
                Environment = ""
                for i in range(0, 3):
                    #print("i = " + str(i))
                    if i == 0:
                        Device_model = self.folder_names_dict[client_name][y]
                    elif i == 1:
                        Product_type = self.folder_names_dict[client_name][y]
                    else:
                        Environment = self.folder_names_dict[client_name][y]
                        path = client_name.replace("/","") + "\\" + Product_type.replace("/","")  + "\\" + Device_model + "\\" + Environment
                        #print(path)
                        self.create_folder(path)
                    #print("param no = " + str(y))
                    y+=1
            #print("End while")
            #print("End For #########################")

    def create_folder(self, path):
        #print(os.getcwd())
        path_to_create = "From_git\\" + path
        #print(path_to_create)
        if not os.path.exists(path_to_create):
            print("Creating Directory : " + path_to_create)
            os.makedirs(path_to_create)
            with open(os.path.join(path_to_create, '.gitkeep'), 'w'):
                pass
            #empty_file =path_to_create + "\\.gitkeep"
            #os.mknod(empty_file)
        else:
            print("Not creating directory since it exists : " + path_to_create)

Push_folder_Config_files_to_Bitbucket1 = Push_folder_Config_files_to_Bitbucket()
Push_folder_Config_files_to_Bitbucket1.read_client_names_from_cm_form_file()

