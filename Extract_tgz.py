import os, glob
import subprocess, shutil

def extract(Artifactory_file_local_path):
    os.chdir(Artifactory_file_local_path)

    if (any(File.endswith(".tgz") for File in os.listdir("."))) or (any(File.endswith(".TGZ") for File in os.listdir("."))) or (any(File.endswith(".tar") for File in os.listdir("."))) or (any(File.endswith(".TAR") for File in os.listdir("."))) or (any(File.endswith(".zip") for File in os.listdir("."))) or (any(File.endswith(".ZIP") for File in os.listdir("."))):
        print("tgz/zip Extraction in progress ... Please wait")
        #print("Extraction iteration - 1")
        # extract tgz file to tar files

        extract_command = "FOR /R \"C:\" %I IN (*.zip) DO 7z x \"%I\" -aou -o\"%~dpI\""
        FNULL = open(os.devnull, 'w')
        result = subprocess.run(extract_command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
        # remove original tgz file
        filelist = glob.glob("*.zip")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("*.doc*")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("*.txt")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("*.pdf")
        for f in filelist:
            os.remove(f)
        # remove above section

        #print("Extraction iteration - 2")
        # extract tgz file to tar files
        extract_command = "FOR /R \"C:\" %I IN (*.tgz) DO 7z x \"%I\" -aou -o\"%~dpI\""
        result = subprocess.run(extract_command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
        # remove original tgz file
        filelist = glob.glob("*.tgz")
        for f in filelist:
            os.remove(f)

        #print("Extraction iteration - 3")
        extract_command = "FOR /R \"C:\" %I IN (*.tar) DO 7z x \"%I\" -aou -o\"%~dpI\""
        result = subprocess.run(extract_command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
        # remove original tgz file
        filelist = glob.glob("*.p7s")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("*.tar")
        for f in filelist:
            os.remove(f)

        #print("Extraction iteration - 4")
        extract_command = "FOR /R \"C:\" %I IN (*.tgz) DO 7z x \"%I\" -aou -o\"%~dpI\""
        result = subprocess.run(extract_command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
        # remove original tgz file
        filelist = glob.glob("*.tgz")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("*.p7s")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("remove*")
        for f in filelist:
            os.remove(f)
        #shutil.rmtree('CONTROL')
        #shutil.rmtree('CRT')

        newpath = r'Artifactory_Extract'
        if os.path.exists(newpath):
            shutil.rmtree('Artifactory_Extract')
        if not os.path.exists(newpath):
            os.makedirs(newpath)

        #print("Extraction iteration - 5")
        extract_command = "FOR /R \"C:\" %I IN (*.tar) DO 7z x \"%I\" -aou -oArtifactory_Extract"
        result = subprocess.run(extract_command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
        # remove original tgz file
        filelist = glob.glob("*.tar")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("*.p7s")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("remove*")
        for f in filelist:
            os.remove(f)

        os.chdir("..\\")
        print("Extraction completed")
    else:
        print("tgz/zip files don't exist in extract folder")
'''
    config.usr1 will be in 'new\CONTROL' folder
'''

### Uncomment below two lines if you want to run this program separately
#Artifactory_file_local_path = "extract"
#extract(Artifactory_file_local_path)

