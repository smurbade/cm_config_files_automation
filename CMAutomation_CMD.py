import logging, time, glob
logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
from time import strftime
import subprocess, os
import msvcrt
import win32api
import win32con
from colorama import init, Fore, Back, Style
init()


import ConfigUsr1
import Add_Media_Files
import cdt
import emv
import cdt_sharepoint
import read_access_db
import insert_access_db
#import Git_operations
import read_conf
read_conf.read_conf_file()


'''
print(Fore.RED + 'some red text')
print(Back.GREEN + 'and with a green background')
print(Style.DIM + 'and in dim text')
print(Style.RESET_ALL)
print('back to normal now')
'''

def Enter_choice():
    print()
    print()
    print(Fore.MAGENTA + "###############################################################################")
    #print(Fore.CYAN + "Press 1  to create config.usr1 file from CM Request")
    print(Fore.CYAN + "Press 1  to insert Sample Media files (915/925) to local package")
    print(Fore.CYAN + "Press 2  to insert Sample EMV files to loacl package")
    print(Fore.MAGENTA + "----------------- Sharepoint ----------------------------------------")
    print(Fore.CYAN + "Press 3  to Upload / Download cdt.txt        Sharepoint <===> local")
    print(Fore.CYAN + "Press 4  to Upload / Download config.usr1    Sharepoint <===> local")
    print(Fore.CYAN + "Press 5  to Upload / Download EMVTables.INI  Sharepoint <===> local")
    print(Fore.CYAN + "Press 6  to Upload / Download VHQconfig.ini  Sharepoint <===> local")
    print(Fore.CYAN + "Press 7  to Upload / Download CAPKData.INI   Sharepoint <===> local")
    print(Fore.CYAN + "Press 8  to Upload / Download CTLSConfig.INI Sharepoint <===> local")
    print(Fore.CYAN + "Press 9  to Upload / Download MSGXPI.txt     Sharepoint <===> local")
    print(Fore.CYAN + "Press 10 to Download Multiple files from     Sharepoint ")
    print(Fore.CYAN + "Press 11 to Upload Multiple files from Local tgz to Sharepoint")
    #print(Fore.CYAN + "Press 2 to pull files from Bitbucket to your local")
    #print(Fore.CYAN + "Press 3 to upload modified files back to bitbucket")
    #print(Fore.CYAN + "Press 4  to create cdt.txt from Artifactory")
    print(Fore.MAGENTA + "---------------------------------------------------------------------")
    print(Fore.YELLOW + "Press any other key to quit")
    print(Style.RESET_ALL)
    print()
    ch = input('Enter your choice here : ')
    print(Fore.MAGENTA + strftime("%m/%d/%Y : %H:%M"))
    print(Style.RESET_ALL)
    logging.info('\n\n\n\n\n###############################################################################')
    logging.info('\nChoice Entered : ' + ch)
    choice_execute(ch)

def sharepoint_processing(file):
    clientID = read_access_db.getid_addclientname(input('\nEnter Client Name  : '))
    if (clientID == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
    else:
        prodtype = input('\nEnter Product Type : ')
        dev = input('\nEnter Device       : ')
        env = input('\nEnter Environment  : ')
        if prodtype and dev and env:
            updown = input('\nUpload to Sharepoint or Download from Sharepoint ? u / d : ')
            if updown.lower() == "d":
                insertdown = input('\nDownload as individual file or download and Insert into Local package?  d / i : ')
                if insertdown.lower() == "i":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when package is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if file == "cdt":
                        if (read_access_db.create_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)):
                            read_access_db.insert_into_local_package("scripts\\cdt_insert.sh", "cdt.txt")
                    elif file == "configusr1":
                        if (read_access_db.create_configusr1(clientID, prodtype, dev, env)):
                            read_access_db.insert_into_local_package("scripts\\configusr1_insert.sh", "config.usr1")
                    elif file == "emvtables":
                        if (read_access_db.create_emvtables(clientID, prodtype, dev, env)):
                            read_access_db.insert_into_local_package("scripts\\mx_insert.sh", "EMVTables.INI")
                    elif file == "vhqconfig":
                        if (read_access_db.create_VHQconfig(clientID, prodtype, dev, env)):
                            read_access_db.insert_into_local_package("scripts\\vhq_insert.sh", "VHQconfig.ini")
                    elif file == "CAPKData":
                        if (read_access_db.create_CAPKData(clientID, prodtype, dev, env)):
                            read_access_db.insert_into_local_package("scripts\\mx_insert.sh", "CAPKData.INI")
                    elif file == "CTLSConfig":
                        if (read_access_db.create_CTLSConfig(clientID, prodtype, dev, env)):
                            read_access_db.insert_into_local_package("scripts\\mx_insert.sh", "CTLSConfig.INI")
                    elif file == "msgxpi":
                        if (read_access_db.create_MSGXPI(clientID, prodtype, dev, env)):
                            read_access_db.insert_into_local_package("scripts\\mx_insert.sh", "MSGXPI.txt")
                    else:
                        print(Fore.RED + "\nInvalid option")
                elif insertdown.lower() == "d":
                    if file == "cdt":
                        if read_access_db.create_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    elif file == "configusr1":
                        if (read_access_db.create_configusr1(clientID, prodtype, dev, env)):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    elif file == "emvtables":
                        if (read_access_db.create_emvtables(clientID, prodtype, dev, env)):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    elif file == "vhqconfig":
                        if (read_access_db.create_VHQconfig(clientID, prodtype, dev, env)):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    elif file == "CAPKData":
                        if (read_access_db.create_CAPKData(clientID, prodtype, dev, env)):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    elif file == "CTLSConfig":
                        if (read_access_db.create_CTLSConfig(clientID, prodtype, dev, env)):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    elif file == "msgxpi":
                        if (read_access_db.create_MSGXPI(clientID, prodtype, dev, env)):
                            print(Fore.GREEN + "\n" + file + " is available @ extract folder")
                    else:
                        print(Fore.RED + "\nInvalid option")
            elif updown.lower() == "u":
                if file == "cdt":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when cdt.txt is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if ( os.path.isfile("extract\\cdt.txt") ):
                        insert_access_db.insert_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)
                    else:
                        print(Fore.RED + "File Not Found - extract\\cdt.txt  ... Please make sure file is present")
                elif file == "configusr1":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when config.usr1 is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if (os.path.isfile("extract\\config.usr1")):
                        insert_access_db.insert_configusr1(clientID, prodtype, dev, env)
                    else:
                        print(Fore.RED + "File Not Found - extract\\config.usr1  ... Please make sure file is present")
                elif file == "emvtables":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when EMVTables.INI is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if (os.path.isfile("extract\\EMVTables.INI")):
                        insert_access_db.insert_EMVTablesINI(clientID, prodtype, dev, env)
                    else:
                        print(Fore.RED + "File Not Found - extract\\EMVTables.INI  ... Please make sure file is present")
                elif file == "vhqconfig":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when VHQconfig.ini is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if (os.path.isfile("extract\\VHQconfig.ini")):
                        insert_access_db.insert_vhqconfig(clientID, prodtype, dev, env)
                    else:
                        print(Fore.RED + "File Not Found - extract\\VHQconfig.ini   ... Please make sure file is present")
                elif file == "CAPKData":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when CAPKData.INI is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if (os.path.isfile("extract\\CAPKData.INI")):
                        insert_access_db.insert_CAPKData(clientID, prodtype, dev, env)
                    else:
                        print(Fore.RED + "File Not Found - extract\\CAPKData.INI   ... Please make sure file is present")
                elif file == "CTLSConfig":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when CTLSConfig.INI is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if (os.path.isfile("extract\\CTLSConfig.INI")):
                        insert_access_db.insert_CTLSConfigini(clientID, prodtype, dev, env)
                    else:
                        print(Fore.RED + "File Not Found - extract\\CTLSConfig.INI   ... Please make sure file is present")
                elif file == "msgxpi":
                    print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when MSGXPI.txt is kept in \'extract\' folder" + Style.RESET_ALL)
                    input()
                    if (os.path.isfile("extract\\MSGXPI.txt")):
                        insert_access_db.insert_msgxpi(clientID, prodtype, dev, env)
                    else:
                        print(Fore.RED + "File Not Found - extract\\MSGXPI.txt   ... Please make sure file is present")
                else:
                    print(Fore.RED + "\nInvalid option")
        else:
            print(Fore.RED + "\nPlease check if you have entered correct inputs")

def sharepoint_multiple_download():
    print("Option chosen for Downloading multiple files")
    clientID = read_access_db.getid_addclientname(input('\nEnter Client Name             : '))
    if (clientID == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
    else:
        prodtype = input('\nEnter Product Type            : ')
        dev = input('\nEnter Device                  : ')
        env = input('\nEnter Environment             : ')
        if prodtype and dev and env:
            insertdown = input('\nDownload as individual file or download and Insert into Local package?  d / i : ')
            if insertdown.lower() == "i":
                print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when package is kept in \'extract\' folder" + Style.RESET_ALL)
                input()
                filelist = glob.glob("extract\\*.tgz")
                if not filelist:
                    print(Fore.RED + "\nPackage is not present in extract folder")
                    return False

                conf       = input('\nDownload & Insert config.usr1 ? y/n      : ')
                vhq        = input('\nDownload & Insert VHQConfig ? y/n        : ')
                capk       = input('\nDownload & Insert CAPKData ? y/n         : ')
                emvtables  = input('\nDownload & Insert EMVTables ? y/n        : ')
                ctlsconfig = input('\nDownload & Insert CTLSConfig ? y/n       : ')
                msgxpi     = input('\nDownload & Insert MSGXPI ? y/n           : ')
                cdtt       = input('\nDownload & Insert cdt ? y/n              : ')

                if (conf.lower() != 'y' and conf.lower() != 'yes' and vhq.lower() != 'y' and vhq.lower() != 'yes' and cdtt.lower() != 'y' and cdtt.lower() != 'yes' and capk.lower() != 'y' and capk.lower() != 'yes' and emvtables.lower() != 'y' and emvtables.lower() != 'yes' and ctlsconfig.lower() != 'y' and ctlsconfig.lower() != 'yes' and msgxpi.lower() != 'y' and msgxpi.lower() != 'yes'):
                    print(Fore.YELLOW + "\nConfiguration files insertion skipped since none of the option selected")
                else:
                    if (conf.lower() == 'y' or conf.lower() == 'yes'):
                        if read_access_db.create_configusr1(clientID, prodtype, dev, env):
                            if read_access_db.insert_into_local_package("scripts\\configusr1_insert.sh", "config.usr1") == False:
                                return False
                    if (vhq.lower() == 'y' or vhq.lower() == 'yes'):
                        if read_access_db.create_VHQconfig(clientID, prodtype, dev, env):
                            read_access_db.insert_into_local_package("scripts\\vhq_insert.sh", "VHQconfig.ini")
                    if (cdtt.lower() == 'y' or cdtt.lower() == 'yes'):
                        if (read_access_db.create_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)):
                            read_access_db.insert_into_local_package("scripts\\cdt_insert.sh", "cdt.txt")
                    if (capk.lower() == 'y' or capk.lower() == 'yes'):
                        read_access_db.create_CAPKData(clientID, prodtype, dev, env)
                    if (emvtables.lower() == 'y' or emvtables.lower() == 'yes'):
                        read_access_db.create_emvtables(clientID, prodtype, dev, env)
                    if (ctlsconfig.lower() == 'y' or ctlsconfig.lower() == 'yes'):
                        read_access_db.create_CTLSConfig(clientID, prodtype, dev, env)
                    if (msgxpi.lower() == 'y' or msgxpi.lower() == 'yes'):
                        read_access_db.create_MSGXPI(clientID, prodtype, dev, env)

                    read_access_db.insert_into_local_package("scripts\\all_mx_insert.sh", "All")
                    #print("\nConfiguration files are inserted into local package from Sharepoint as per the selections above")

            elif insertdown.lower() == "d":
                conf       = input('\nDownload config.usr1 ? y/n      : ')
                vhq        = input('\nDownload VHQConfig ? y/n        : ')
                capk       = input('\nDownload CAPKData ? y/n         : ')
                emvtables  = input('\nDownload EMVTables ? y/n        : ')
                ctlsconfig = input('\nDownload CTLSConfig ? y/n       : ')
                msgxpi     = input('\nDownload MSGXPI ? y/n           : ')
                cdtt       = input('\nDownload cdt ? y/n              : ')

                if (conf.lower() == 'y' or conf.lower() == 'yes'):
                    if read_access_db.create_configusr1(clientID, prodtype, dev, env):
                        print(Fore.GREEN + "\nconfig.usr1    is available @ extract folder")
                if (vhq.lower() == 'y' or vhq.lower() == 'yes'):
                    if read_access_db.create_VHQconfig(clientID, prodtype, dev, env):
                        print(Fore.GREEN + "\nVHQconfig.ini  is available @ extract folder")
                if (cdtt.lower() == 'y' or cdtt.lower() == 'yes'):
                    if read_access_db.create_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder):
                        print(Fore.GREEN + "\ncdt.txt        is available @ extract folder")
                if (capk.lower() == 'y' or capk.lower() == 'yes'):
                    if read_access_db.create_CAPKData(clientID, prodtype, dev, env):
                        print(Fore.GREEN + "\nCAPKData.INI   is available @ extract folder")
                if (emvtables.lower() == 'y' or emvtables.lower() == 'yes'):
                    if read_access_db.create_emvtables(clientID, prodtype, dev, env):
                        print(Fore.GREEN + "\nEMVTables.INI  is available @ extract folder")
                if (ctlsconfig.lower() == 'y' or ctlsconfig.lower() == 'yes'):
                    if read_access_db.create_CTLSConfig(clientID, prodtype, dev, env):
                        print(Fore.GREEN + "\nCTLSConfig.INI is available @ extract folder")
                if (msgxpi.lower() == 'y' or msgxpi.lower() == 'yes'):
                    if read_access_db.create_MSGXPI(clientID, prodtype, dev, env):
                        print(Fore.GREEN + "\nMSGXPI.txt     is available @ extract folder")
        else:
            print(Fore.RED + "\nPlease check if you have entered correct inputs")

def sharepoint_multiple_upload():
    print("Option chosen for Uploading multiple files")
    clientID = read_access_db.getid_addclientname(input('\nEnter Client Name             : '))
    if (clientID == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
    else:
        prodtype = input('\nEnter Product Type            : ')
        dev = input('\nEnter Device                  : ')
        env = input('\nEnter Environment             : ')
        if prodtype and dev and env:
            insertdown = input('\nUpload individual files or from Local package?  i / p : ')
            if insertdown.lower() == "i":
                conf       = input('\nUpload config.usr1 ?     y/n  : ')
                vhq        = input('\nUpload VHQConfig ?       y/n  : ')
                capk       = input('\nUpload CAPKData ?        y/n  : ')
                emvtables  = input('\nUpload EMVTables ?       y/n  : ')
                ctlsconfig = input('\nUpload CTLSConfig ?      y/n  : ')
                msgxpi     = input('\nUpload MSGXPI ?          y/n  : ')
                cdtt       = input('\nUpload cdt ?             y/n  : ')

                if (conf.lower() != 'y' and conf.lower() != 'yes' and vhq.lower() != 'y' and vhq.lower() != 'yes' and cdtt.lower() != 'y' and cdtt.lower() != 'yes' and capk.lower() != 'y' and capk.lower() != 'yes' and emvtables.lower() != 'y' and emvtables.lower() != 'yes' and ctlsconfig.lower() != 'y' and ctlsconfig.lower() != 'yes' and msgxpi.lower() != 'y' and msgxpi.lower() != 'yes'):
                    print(Fore.YELLOW + "\nConfiguration files Upload to Sharepoint skipped since none of the option selected")
                else:
                    try:
                        if (conf.lower() == 'y' or conf.lower() == 'yes'):
                            insert_access_db.insert_configusr1(clientID, prodtype, dev, env)
                        if (vhq.lower() == 'y' or vhq.lower() == 'yes'):
                            insert_access_db.insert_vhqconfig(clientID, prodtype, dev, env)
                        if (cdtt.lower() == 'y' or cdtt.lower() == 'yes'):
                            insert_access_db.insert_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)
                        if (capk.lower() == 'y' or capk.lower() == 'yes'):
                            insert_access_db.insert_CAPKData(clientID, prodtype, dev, env)
                        if (emvtables.lower() == 'y' or emvtables.lower() == 'yes'):
                            insert_access_db.insert_EMVTablesINI(clientID, prodtype, dev, env)
                        if (ctlsconfig.lower() == 'y' or ctlsconfig.lower() == 'yes'):
                            insert_access_db.insert_CTLSConfigini(clientID, prodtype, dev, env)
                        if (msgxpi.lower() == 'y' or msgxpi.lower() == 'yes'):
                            insert_access_db.insert_msgxpi(clientID, prodtype, dev, env)
                    except FileNotFoundError:
                        pass
                    print(Fore.GREEN + "\nConfiguration files are Uploaded to Sharepoint as per the selections above")

            elif insertdown.lower() == "p":
                print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when package is kept in \'extract\' folder" + Style.RESET_ALL)
                input()
                filelist = glob.glob("extract\\*.tgz")
                if not filelist:
                    print(Fore.RED + "\nPackage is not present in extract folder")
                    return False

                print(Fore.WHITE + "Configuration files are being extracted from package in extract folder and will be uploaded to Sharepoint one by one...")

                #command = "C:\\msys\\1.0\\bin\\bash.exe untgz.sh"
                command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\untgz.sh"
                print("Extracting Configuration files from package in extract folder. Please wait ...")
                FNULL = open(os.devnull, 'w')
                result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                if result.returncode != 0:
                    print(result.stderr)
                    print(result)
                    print(Fore.RED + "\nExecution of untgz.sh failed")
                else:
                    conf = ''
                    vhq = ''
                    capk = ''
                    emvtables = ''
                    ctlsconfig = ''
                    msgxpi = ''
                    cdtt = ''

                    if os.path.isfile("extract\\config.usr1"):
                        conf       = input('\nUpload config.usr1    from Local package to Sharepoint ?  y/n   : ')
                    if os.path.isfile("extract\\VHQconfig.ini"):
                        vhq        = input('\nUpload VHQconfig.ini  from Local package to Sharepoint ?  y/n   : ')
                    if os.path.isfile("extract\\CAPKData.INI"):
                        capk       = input('\nUpload CAPKData.INI   from Local package to Sharepoint ?  y/n   : ')
                    if os.path.isfile("extract\\EMVTables.INI"):
                        emvtables  = input('\nUpload EMVTables.INI  from Local package to Sharepoint ?  y/n   : ')
                    if os.path.isfile("extract\\CTLSConfig.INI"):
                        ctlsconfig = input('\nUpload CTLSConfig.INI from Local package to Sharepoint ?  y/n   : ')
                    if os.path.isfile("extract\\MSGXPI.txt"):
                        msgxpi     = input('\nUpload MSGXPI.txt     from Local package to Sharepoint ?  y/n   : ')
                    if os.path.isfile("extract\\cdt.txt"):
                        cdtt       = input('\nUpload cdt.txt        from Local package to Sharepoint ?  y/n   : ')

                    if (conf.lower() == 'y' or conf.lower() == 'yes'):
                        insert_access_db.insert_configusr1(clientID, prodtype, dev, env)
                    if (vhq.lower() == 'y' or vhq.lower() == 'yes'):
                        insert_access_db.insert_vhqconfig(clientID, prodtype, dev, env)
                    if (cdtt.lower() == 'y' or cdtt.lower() == 'yes'):
                        insert_access_db.insert_cdt(clientID, prodtype, dev, env, read_access_db.cdtorder)
                    if (capk.lower() == 'y' or capk.lower() == 'yes'):
                        insert_access_db.insert_CAPKData(clientID, prodtype, dev, env)
                    if (emvtables.lower() == 'y' or emvtables.lower() == 'yes'):
                        insert_access_db.insert_EMVTablesINI(clientID, prodtype, dev, env)
                    if (ctlsconfig.lower() == 'y' or ctlsconfig.lower() == 'yes'):
                        insert_access_db.insert_CTLSConfigini(clientID, prodtype, dev, env)
                    if (msgxpi.lower() == 'y' or msgxpi.lower() == 'yes'):
                        insert_access_db.insert_msgxpi(clientID, prodtype, dev, env)

                    if (conf.lower() != 'y' and conf.lower() != 'yes' and vhq.lower() != 'y' and vhq.lower() != 'yes' and cdtt.lower() != 'y' and cdtt.lower() != 'yes' and capk.lower() != 'y' and capk.lower() != 'yes' and emvtables.lower() != 'y' and emvtables.lower() != 'yes' and ctlsconfig.lower() != 'y' and ctlsconfig.lower() != 'yes' and msgxpi.lower() != 'y' and msgxpi.lower() != 'yes'):
                        print(Fore.YELLOW + "\nConfiguration files Upload to Sharepoint skipped since none of the option selected")
                    else:
                        print(Fore.GREEN + "Configuration files are Uploaded to Sharepoint as per the selections above")
        else:
            print(Fore.RED + "\nPlease check if you have entered correct inputs")



def choice_execute(choice):
    if choice == '107867':
        ##########################################################################

        # Create Config Usr1 object to create / update config.usr1 file.
        # Applicable only for SCA Point Classic / Enterprise
        configuser1 = ConfigUsr1.config_usr1(int(input('Enter Form Number : ')))
        configuser1.kickstart_config_usr1_processing()
        Enter_choice()
        ##########################################################################
    elif choice == '1':
        print("\nOption chosen for Template Media Files Insertion into Local package")
        print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when package is kept in \'extract\' folder" + Style.RESET_ALL)
        input()
        Add_Media_Files.insert_sample_media()
        Enter_choice()
    elif choice == '2':
        print("\nOption chosen for Template EMV Files Insertion into Local package")
        print(Style.BRIGHT + Fore.YELLOW + "\nHit Enter when package is kept in \'extract\' folder" + Style.RESET_ALL)
        input()
        emv.insert_emv("nongui", 0)
        Enter_choice()
    elif choice == '3':
        print("Option chosen for cdt processing")
        sharepoint_processing("cdt")
        Enter_choice()
    elif choice == '4':
        print("Option chosen for configusr1 processing")
        sharepoint_processing("configusr1")
        Enter_choice()
    elif choice == '5':
        print("Option chosen for emvtables processing")
        sharepoint_processing("emvtables")
        Enter_choice()
    elif choice == '6':
        print("Option chosen for vhqconfig processing")
        sharepoint_processing("vhqconfig")
        Enter_choice()
    elif choice == '7':
        print("Option chosen for CAPKData processing")
        sharepoint_processing("CAPKData")
        Enter_choice()
    elif choice == '8':
        print("Option chosen for CTLSConfig processing")
        sharepoint_processing("CTLSConfig")
        Enter_choice()
    elif choice == '9':
        print("Option chosen for msgxpi processing")
        sharepoint_processing("msgxpi")
        Enter_choice()
    elif choice == '10':
        sharepoint_multiple_download()
        Enter_choice()
    elif choice == '11':
        sharepoint_multiple_upload()
        Enter_choice()
    if not choice:
        pass
'''
    elif choice == '931':
        cdtsharepoint = cdt_sharepoint.read_cmitems()
        cdtsharepoint.kickstart_cmitems_processing()
        Enter_choice()
    elif choice == '925':
        git_obj = Git_operations.git_processing()
        git_obj.pull_from_git()
        Enter_choice()
    elif choice == '601':
        git_obj = Git_operations.git_processing()
        git_obj.pull_from_git()
        git_obj.add_file_to_git()
        Enter_choice()
    elif choice == '4':
        cdtobj = cdt.cdttxt()
        cdtobj.kickstart_cdt_processing()
        Enter_choice()

'''


#encoding = os.popen('chcp 65001')
#encoding.read()
Enter_choice()
#encoding.close()


