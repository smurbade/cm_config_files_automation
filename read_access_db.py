import pypyodbc
import os
import subprocess
from pathlib import Path
import glob

from xlrd import open_workbook

pypyodbc.lowercase = False
from colorama import init, Fore, Back, Style
init()
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"

import read_conf
read_conf.read_conf_file()

cdt_dict = {}
cdt_dict_organised = {}

#---------------------------------------------------------------------------------------------------------------

def read_amdocsid():
        conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Amdocs_Ids.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Amdocs_Ids.accdb;\"")
        #r"Dbq=C:\Users\T_SachinM2\PycharmProjects\First Project\Amdocs_Ids.accdb;")

        cur = conn.cursor()
        cur.execute("SELECT Amdocs_id, choice FROM Amdocs_Ids WHERE Amdocs_id = '718'")

        while True:
            row = cur.fetchone()
            if row is None:
                break
            print(u"Creature with ID {0} is {1} ".format(row.get("Amdocs_id"), row.get("choice")))

        cur.close()
        conn.close()

#---------------------------------------------------------------------------------------------------------------

def read_cdt(id, prodtype, device, environment, cdtorder):             # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\cdt.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\cdt.accdb;\"")

    #clientname = "AM Retail"
    #prodtype = "SCA Point Enterprise"
    #device = "MX870"
    #environment = "UAT"

    #query = "SELECT * FROM cdt WHERE 'Client Name' = '" + clientname + "'" + " AND 'Product Type' = '" + prodtype + "'" + " AND 'Device' = '" + device + "'" + " AND 'Environment' = '" + environment + "'"

    #print(query)
    cur = conn.cursor()
    cur.execute("SELECT * FROM cdt");
    #cur.execute(query);

    cdt_row_index = 0
    cdt_dict[cdt_row_index] = []
    for key_cdt_mapping in cdtorder:
        cdt_dict[cdt_row_index].insert(0, key_cdt_mapping)
    #print(cdt_dict)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type") == prodtype) and (row.get("Device") == device) and (row.get("Environment") == environment):
            #print(u"Creature with ID {0} is {1} ".format(row.get("Client Name"), row.get("Device")))
            cdt_row_index += 1
            cdt_dict[cdt_row_index] = []
            for key_cdt_mapping in cdtorder:
                cdt_dict[cdt_row_index].insert(0, row.get(key_cdt_mapping))

    cur.close()
    conn.close()
    return cdt_dict

#---------------------------------------------------------------------------------------------------------------

def getid_addclientname(clientname): # returns ID for 'Client name' from 'Add Client Name' list ; Returns -1 if client is not found
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Add Client Name.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Add Client Name.accdb;\"")

    #query = "SELECT * FROM \"Add Client Name\" WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.lower() + "'"
    #query = "SELECT * FROM \"Add Client Name\" WHERE ([Add Client Name].[Client Name]) = '" + clientname + "'"

    #print(query)
    #print(clientname)

    cur = conn.cursor()
    cur.execute("SELECT [Add Client Name].[ID] FROM [Add Client Name] WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.replace("'", "''").lower() + "'" )
    #cur.execute(query);

    while True:
        row = cur.fetchone()
        if row is None:
            break
        else:
            ID = row[0]
            cur.close()
            conn.close()
            #print(row)
            return ID

    return -1
    cur.close()
    conn.close()



'''
def getid_addclientname(clientname): # returns ID for 'Client name' from 'Add Client Name' list ; Returns -1 if client is not found
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Add Client Name.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Add Client Name.accdb;\"")

    #query = "SELECT * FROM \"Add Client Name\" WHERE lcase([Add Client Name].[Client Name]) = '" + clientname.lower() + "'"
    #query = "SELECT * FROM \"Add Client Name\" WHERE ([Add Client Name].[Client Name]) = '" + clientname + "'"

    #print(query)
    #print(clientname)

    cur = conn.cursor()
    cur.execute("SELECT * FROM \"Add Client Name\"");
    #cur.execute(query);

    while True:
        row = cur.fetchone()
        if row is None:
            break
        if (row.get("Title").lower() == clientname.lower()):
            #print(row)
            #print(u"Creature with ID {0} is {1} ".format(row.get("ID"), row.get("Client Name")))
            ID = row.get("ID")
            cur.close()
            conn.close()
            #print(ID)
            return ID
            break

    return -1
    cur.close()
    conn.close()
'''
#---------------------------------------------------------------------------------------------------------------

def create_cdt(id, prodtype, device, environment, cdtorder):             # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1

    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\cdt.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\cdt.accdb;\"")
    cur = conn.cursor()
    WHERE = "cdt.[Client Name] = " + str(id) + " AND lcase(cdt.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(cdt.[Device]) = \'" + device.lower() + "\' AND lcase(cdt.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM cdt WHERE " + WHERE
    cur.execute(query)

    for i in range(0, len(cdtorder[0])):
        cdt_dict_organised[i] = []
        cdt_dict_organised[i].insert(0, cdtorder[1][i])

    records_found = False
    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            for i in range(0, len(cdtorder[0])):
                #cdt_dict_organised[i].insert(i+1, row.get(cdtorder[0][i]))
                #cdt_dict_organised[i].insert(1, row.get(cdtorder[0][i]))
                cdt_dict_organised[i].append(row.get(cdtorder[0][i]))
            #print(cdt_dict_organised)
            records_found = True

    #print(cdt_dict_organised)
    cur.close()
    conn.close()
    if (records_found):
        if (create_cdt_file(cdt_dict_organised)):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for cdt")
        return 3

#---------------------------------------------------------------------------------------------------------------

def create_cdtini(id, prodtype, device, environment):             # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\cdtvx.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\cdtvx.accdb;\"")
    cur = conn.cursor()
    WHERE = "[cdt vx].[Client Name] = " + str(id) + " AND [cdt vx].[Product Type] = \'" + prodtype + "\' AND [cdt vx].[Device] = \'" + device + "\' AND [cdt vx].[Environment] = \'" + environment + "\'"
    query = "SELECT * FROM [cdt vx] WHERE " + WHERE
    cur.execute(query)

    line = ""
    reccount = 0
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type") == prodtype) and (row.get("Device") == device) and (row.get("Environment") == environment):
            if ( reccount < 10 ):
                line = line + "[Rec_0" + str(reccount) + "]\n"
            else:
                line = line + "[Rec_" + str(reccount) + "]\n"
            reccount+=1
            for i in range(0, len(cdtvxorder[0])):
                try:
                    line = line + str(cdtvxorder[0][i]) + "=" + str(int(float(row.get(cdtvxorder[0][i])))) + "\n"
                except ValueError:
                    line = line + str(cdtvxorder[0][i]) + "=" + str(row.get(cdtvxorder[0][i])) + "\n"
            #print(cdt_dict_organised)
            line = line + "\n"
            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        cdtini = "RecCnt=" + str(reccount) + "\n\n" + line
        #print(cdtini)
        cdt_dir = os.getcwd() + '\\extract'
        if not os.path.isdir(cdt_dir):
            os.mkdir(cdt_dir)
        cdtfilename = cdt_dir + "\\CDT.INI"
        try:
            cdtfile = open(cdtfilename, 'w')
            cdtfile.write(cdtini)
        except IOError:
            print(Fore.RED + "\nError creating file @ " + os.getcwd() + "\\extract\\CDT.INI")
            return 4
        else:
            cdtfile.close()
            #print(Fore.GREEN + "\nFile created @ " + os.getcwd() + "\\extract\\CDT.INI")
            return 0
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for cdt.ini")
        return 3


def create_cdt_file(cdt_dict_organised):
        cdt_dir = os.getcwd() + '\\extract'
        if not os.path.isdir(cdt_dir):
            os.mkdir(cdt_dir)
        cdtfilename = cdt_dir + "\\cdt.txt"
        try:
            cdtfile = open(cdtfilename, 'w')
            for i in range(0, len(cdt_dict_organised)):
                for j in range(0, len(cdt_dict_organised[i])):
                    if j == 0:
                        line = str(cdt_dict_organised[i][j]) + " "
                    else:
                        try:
                            line = line + str(int(float(cdt_dict_organised[i][j]))) + ", "
                        except ValueError:
                            line = line + cdt_dict_organised[i][j] + ", "
                line = line[:-2] + "\n" # Remove last two characters(comma and space) and add newline
                cdtfile.write(line)
        except IOError:
            print(Fore.RED + "\nError creating cdt.txt @ " + os.getcwd() + "\\extract\\cdt.txt")
            return False
        else:
            cdtfile.close()
            return True
            #insert_into_local_package("cdt_insert.sh", "cdt.txt")

#---------------------------------------------------------------------------------------------------------------

def create_configusr1(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\config.usr1.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\config.usr1.accdb;\"")
    cur = conn.cursor()
    #cur.execute("SELECT * FROM config_usr1 GROUP BY config_usr1.[Section], config_usr1.[ID], config_usr1.[Client Name], config_usr1.[Product Type], config_usr1.[Device], config_usr1.[Environment], config_usr1.[Parameter], config_usr1.[Value], config_usr1.[Title]");
    #cur.execute("SELECT config_usr1.[Section], config_usr1.[Parameter], config_usr1.[Value], config_usr1.[Client Name], config_usr1.[Product Type], config_usr1.[Device], config_usr1.[Environment], config_usr1.[ID] FROM config_usr1 GROUP BY config_usr1.[Client Name], config_usr1.[Product Type], config_usr1.[Device], config_usr1.[Environment], config_usr1.[Section], config_usr1.[ID], config_usr1.[Parameter], config_usr1.[Value]");
    WHERE = "config_usr1.[Client Name] = " + str(id) + " AND lcase(config_usr1.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(config_usr1.[Device]) = \'" + device.lower() + "\' AND lcase(config_usr1.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT config_usr1.[Section], config_usr1.[Parameter], config_usr1.[Value], config_usr1.[Client Name], config_usr1.[Product Type], config_usr1.[Device], config_usr1.[Environment], config_usr1.[ID] FROM config_usr1 WHERE " + WHERE + " ORDER BY config_usr1.[Client Name], config_usr1.[Product Type], config_usr1.[Device], config_usr1.[Environment], config_usr1.[Section], config_usr1.[ID], config_usr1.[Parameter], config_usr1.[Value]"
    #query = "SELECT config_usr1.[Section], config_usr1.[Parameter], config_usr1.[Value], config_usr1.[Client Name], config_usr1.[Product Type], config_usr1.[Device], config_usr1.[Environment], config_usr1.[ID] FROM config_usr1 WHERE " + WHERE
    cur.execute(query)

    section = ""
    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if (section != row.get("Section")):
                if (section == ""):
                    section = row.get("Section")
                    line = line + "[" + section + "]\n"
                else:
                    section = row.get("Section")
                    line = line + "\n[" + section + "]\n"
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, "config.usr1"):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for config.usr1")
        return 3

# ---------------------------------------------------------------------------------------------------------------

def create_MSGXPI(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\MSGXPI.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\MSGXPI.accdb;\"")
    cur = conn.cursor()
    WHERE = "MSGXPI.[Client Name] = " + str(id) + " AND lcase(MSGXPI.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(MSGXPI.[Device]) = \'" + device.lower() + "\' AND lcase(MSGXPI.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT MSGXPI.[Parameter], MSGXPI.[Value], MSGXPI.[Client Name], MSGXPI.[Product Type], MSGXPI.[Device], MSGXPI.[Environment] FROM MSGXPI WHERE " + WHERE
    cur.execute(query)

    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, "MSGXPI.txt"):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for msgxpi")
        return 3

# ---------------------------------------------------------------------------------------------------------------

def create_VHQconfig(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\VHQconfigini.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\VHQconfigini.accdb;\"")
    cur = conn.cursor()
    WHERE = "VHQconfigini.[Client Name] = " + str(id) + " AND lcase(VHQconfigini.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(VHQconfigini.[Device]) = \'" + device.lower() + "\' AND lcase(VHQconfigini.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT VHQconfigini.[Section], VHQconfigini.[Parameter], VHQconfigini.[Value], VHQconfigini.[Client Name], VHQconfigini.[Product Type], VHQconfigini.[Device], VHQconfigini.[Environment], VHQconfigini.[ID] FROM VHQconfigini WHERE " + WHERE
    cur.execute(query)

    section = ""
    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if (section != row.get("Section")):
                if (section == ""):
                    section = row.get("Section")
                    line = line + "[" + section + "]\n"
                else:
                    section = row.get("Section")
                    line = line + "\n[" + section + "]\n"
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if not (create_file(line, "VHQconfig.ini")):
            return 2
        else:
            return 0
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for VHQconfig")
        return 3

#---------------------------------------------------------------------------------------------------------------

def create_file(line, filename):
    dir = os.getcwd() + '\\extract'
    if not os.path.isdir(dir):
        os.mkdir(dir)
    fullfilename = dir + "\\" + filename
    try:
        file = open(fullfilename, 'w')
        file.write(line)
    except IOError:
        print(Fore.RED + "\nError Creating " + filename)
        return False
    else:
        file.close()
        return True

#---------------------------------------------------------------------------------------------------------------

def create_emvtables(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\EMVTablesINI.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\EMVTablesINI.accdb;\"")
    cur = conn.cursor()
    WHERE = "EMVTablesINI.[Client Name] = " + str(id) + " AND lcase(EMVTablesINI.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(EMVTablesINI.[Device]) = \'" + device.lower() + "\' AND lcase(EMVTablesINI.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT EMVTablesINI.[Scheme], EMVTablesINI.[Parameter Type], EMVTablesINI.[Parameter], EMVTablesINI.[Value], EMVTablesINI.[Client Name], EMVTablesINI.[Product Type], EMVTablesINI.[Device], EMVTablesINI.[Environment], EMVTablesINI.[ID] FROM EMVTablesINI WHERE " + WHERE
    cur.execute(query)

    section = [""]
    sectionind= 0
    sectionprev = ""
    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            sectionprev = row.get("Scheme") + "." + row.get("Parameter Type")
            if ( section[sectionind] != sectionprev ):
                section.append(row.get("Scheme") + "." + row.get("Parameter Type"))
                #section.insert(sectionind, row.get("Scheme") + "." + row.get("Parameter Type"))
                line = line + "\n[" + section[sectionind+1] + "]\n"
                sectionind+= 1
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        uniqscheme = [""]

        for scheme_partype in section:
            if (scheme_partype.split(".")[0] not in uniqscheme):
                uniqscheme.append(scheme_partype.split(".")[0])
        #print(uniqscheme)

        supportedschemeind = 1
        supportedschemes = "[SupportedSchemes]\n"
        for i in range (1, len(uniqscheme)):
            supportedschemes = supportedschemes + "Scheme" + str(i) + "=" + uniqscheme[i] + "\n"

        line = supportedschemes + line

        #print(line)
        #print(supportedschemes)
        if create_file(line, "EMVTables.INI"):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for EMVTables")
        return 3

# ---------------------------------------------------------------------------------------------------------------

def create_CAPKData(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CAPKData.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CAPKData.accdb;\"")
    cur = conn.cursor()
    WHERE = "CAPKData.[Client Name] = " + str(id) + " AND lcase(CAPKData.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(CAPKData.[Device]) = \'" + device.lower() + "\' AND lcase(CAPKData.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT CAPKData.[Filename], CAPKData.[Parameter Type], CAPKData.[Parameter], CAPKData.[Value], CAPKData.[Client Name], CAPKData.[Product Type], CAPKData.[Device], CAPKData.[Environment], CAPKData.[ID] FROM CAPKData WHERE " + WHERE
    cur.execute(query)

    section = [""]
    sectionind = 0
    sectionprev = ""
    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            sectionprev = row.get("Filename").replace(".", "") + "." + row.get("Parameter Type")
            if (section[sectionind] != sectionprev):
                section.append(row.get("Filename").replace(".", "") + "." + row.get("Parameter Type"))
                # section.insert(sectionind, row.get("Scheme") + "." + row.get("Parameter Type"))
                line = line + "\n[" + section[sectionind + 1] + "]\n"
                sectionind += 1
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        uniqscheme = [""]

        for scheme_partype in section:
            if (scheme_partype.split(".")[0] not in uniqscheme):
                uniqscheme.append(scheme_partype.split(".")[0])
        #print(uniqscheme)

        supportedschemeind = 1
        supportedschemes = "[CAPKFiles]\n"
        for i in range(1, len(uniqscheme)):
            supportedschemes = supportedschemes + "FILENAME" + str(i-1) + "= " + uniqscheme[i][:-2] + "." + uniqscheme[i][-2:] + "\n"

        line = supportedschemes + line

        #print(line)
        #print(supportedschemes)
        if create_file(line, "CAPKData.INI"):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for CAPKData")
        return 3

# ---------------------------------------------------------------------------------------------------------------

def create_optflag(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\OptFlag.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\OptFlag.accdb;\"")
    cur = conn.cursor()
    WHERE = "OptFlag.[Client Name] = " + str(id) + " AND lcase(OptFlag.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(OptFlag.[Device]) = \'" + device.lower() + "\' AND lcase(OptFlag.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT OptFlag.[Parameter], OptFlag.[Value], OptFlag.[Client Name], OptFlag.[Product Type], OptFlag.[Device], OptFlag.[Environment], OptFlag.[ID] FROM OptFlag WHERE " + WHERE
    cur.execute(query)

    line = "RecCnt=1\n\n[Rec_00]\n"
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=\"" + str(row.get("Value")) + "\"\n"
            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        # print(line)
        if create_file(line, "OptFlag.INI"):
            return 0
        else:
            return 2
    else:
        print(
            Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for OptFlag")
        return 3

# ---------------------------------------------------------------------------------------------------------------

def create_EmvTagsReqbyHost(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\EmvTagsReqbyHost.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\EmvTagsReqbyHost.accdb;\"")
    cur = conn.cursor()
    WHERE = "EmvTagsReqbyHost.[Client Name] = " + str(id) + " AND lcase(EmvTagsReqbyHost.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(EmvTagsReqbyHost.[Device]) = \'" + device.lower() + "\' AND lcase(EmvTagsReqbyHost.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT EmvTagsReqbyHost.[Value], EmvTagsReqbyHost.[Client Name], EmvTagsReqbyHost.[Product Type], EmvTagsReqbyHost.[Device], EmvTagsReqbyHost.[Environment], EmvTagsReqbyHost.[ID] FROM EmvTagsReqbyHost WHERE " + WHERE
    cur.execute(query)

    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            line = line + str(row.get("Value")) + "\n"
            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        # print(line)
        if create_file(line, "EmvTagsReqbyHost.txt"):
            return 0
        else:
            return 2
    else:
        print(
            Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for EmvTagsReqbyHost")
        return 3


# ---------------------------------------------------------------------------------------------------------------

def create_CTLSConfig(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CTLSConfigINI.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CTLSConfigINI.accdb;\"")
    cur = conn.cursor()
    WHERE = "CTLSConfigINI.[Client Name] = " + str(id) + " AND lcase(CTLSConfigINI.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(CTLSConfigINI.[Device]) = \'" + device.lower() + "\' AND lcase(CTLSConfigINI.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT CTLSConfigINI.[Scheme], CTLSConfigINI.[Parameter Type], CTLSConfigINI.[Parameter], CTLSConfigINI.[Value], CTLSConfigINI.[Client Name], CTLSConfigINI.[Product Type], CTLSConfigINI.[Device], CTLSConfigINI.[Environment], CTLSConfigINI.[ID] FROM CTLSConfigINI WHERE " + WHERE
    cur.execute(query)

    section = [""]
    sectionind = 0
    sectionprev = ""
    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if (row.get("Parameter Type") == None):
                sectionprev = row.get("Scheme")
            else:
                sectionprev = row.get("Scheme") + "." + row.get("Parameter Type")
            if (section[sectionind] != sectionprev):
                if (row.get("Parameter Type") == None):
                    section.append(row.get("Scheme"))
                    # section.insert(sectionind, row.get("Scheme") + "." + row.get("Parameter Type"))
                else:
                    section.append(row.get("Scheme") + "." + row.get("Parameter Type"))
                line = line + "\n[" + section[sectionind + 1] + "]\n"
                sectionind += 1
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        uniqscheme = [""]

        for scheme_partype in section:
            if "." in scheme_partype:
                if (scheme_partype.split(".")[0] not in uniqscheme):
                    uniqscheme.append(scheme_partype.split(".")[0])

        #print(uniqscheme)
        #print(section)
        supportedschemeind = 1
        supportedschemes = "[SupportedSchemes]\n"
        for i in range(1, len(uniqscheme)):
            supportedschemes = supportedschemes + "Scheme" + str(i) + "=" + uniqscheme[i] + "\n"

        line = supportedschemes + line

        #print(line)
        # print(supportedschemes)
        if create_file(line, "CTLSConfig.INI"):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for CTLSConfig")
        return 3

#---------------------------------------------------------------------------------------------------------------

def insert_into_local_package(script, devtype, file):
    filelist = glob.glob("extract\\*.tgz")
    if not filelist:
        print(Fore.RED + "\nPackage is not present in extract folder")
        return False
    else:
        for f in filelist:
            if f:
                configusr1file = Path("extract\\" + file)
                if ( configusr1file.is_file() or (file == "All" ) ):
                    #command = "C:\\msys\\1.0\\bin\\bash.exe " + script + " " + file
                    command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " " + devtype + " " + file
                    print(command)
                    if (file == "All"):
                        print(Fore.WHITE + "\nInserting rest of the files as per selection from sharepoint to existing package. Please wait ...")
                    else:
                        print(Fore.WHITE + "\nInserting " + file + " from sharepoint to existing package in 'extract' folder. Please wait ...")
                    FNULL = open(os.devnull, 'w')
                    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                    if result.returncode != 0:
                        print(result.stderr)
                        print(result)
                        print(Fore.RED + "\nExecution of " + script + " failed")
                        return False
                        #break
                    else:
                        print(Fore.GREEN + "Updated package is available @ extract folder")
                        return True
                        #break
                else:
                    print(Fore.RED + "\n" + file + " is not present in extract folder")
                    return False
                    #break
            else:
                print(Fore.RED + "\nPackage is not present in extract folder")
                return False

    return True

#---------------------------------------------------------------------------------------------------------------

def insert_files_into_package(script, devtype, file):
    filelist = glob.glob("extract\\*.tgz")
    if not filelist:
        print(Fore.RED + "\nPackage is not present in extract folder")
        return False
    else:
        #command = "C:\\msys\\1.0\\bin\\bash.exe " + script + " " + file
        command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " " + devtype + " " + file
        #print(command)
        print(Fore.WHITE + "\nInserting " + file + " from sharepoint to existing package in 'extract' folder. Please wait ...")
        FNULL = open(os.devnull, 'w')
        result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
            print(Fore.RED + "\nExecution of " + script + " failed")
            return False
            #break
        else:
            print(Fore.GREEN + "Updated package is available @ extract folder")
            return True
            #break

    return True

def create_displayPrompts(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\displayPrompts.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\displayPrompts.accdb;\"")

    filename = "displayPrompts.ini"
    tablename = "displayPrompts"

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Language], " + tablename + ".[Parameter], " + tablename + ".[Value], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    section = ""
    line = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if (section != row.get("Language")):
                if (section == ""):
                    section = row.get("Language")
                    line = line + "[" + section + "]\n"
                else:
                    section = row.get("Language")
                    line = line + "\n[" + section + "]\n"
            parameter = str(row.get("Parameter"))
            if parameter != 'id':
                if not row.get("Value"):
                    line = line + parameter + "=\"\"\n"
                else:
                    line = line + parameter + "=\"" + str(row.get("Value")) + "\"\n"
            else:
                if not row.get("Value"):
                    line = line + parameter + "=\n"
                else:
                    line = line + parameter + "=" + str(row.get("Value")) + "\n"

            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + filename)
        return 3


def create_PCI_BIT(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\PCI_BIT.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\PCI_BIT.accdb;\"")

    filename = "PCI_BIT.DAT"
    tablename = "PCI_BIT"

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Min Bin Range], " + tablename + ".[Max Bin Range], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    #print(query)
    cur.execute(query)

    line = "# This file contains the BIN ranges that are PCI Branded cards\n"
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            minrange = str(int(row.get("Min Bin Range")))
            maxrange = str(int(row.get("Max Bin Range")))
            line = line + minrange + "-" + maxrange + ";\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + filename)
        return 3

def create_AIDListtxt(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\AIDListtxt.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\AIDListtxt.accdb;\"")

    filename = "AIDList.txt"
    tablename = "AIDListtxt"

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(
        id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Supported AID], " + tablename + ".[Payment Type], " + tablename + ".[Payment Media / Cardtype], " + tablename + ".[EMV Kernal Configurations], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    # print(query)
    cur.execute(query)

    line = "//AIDs|list|0-credit,|1-debit,|2-Loyalty,|-1|Do|nothing\n"
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            aid = str(row.get("Supported AID"))
            paymenttype = str(int(row.get("Payment Type")))
            paymentmedia = str(row.get("Payment Media / Cardtype"))
            kernel = str(row.get("EMV Kernal Configurations"))
            line = line + aid + "|" + paymenttype + "|" + paymentmedia + "|" + kernel + "\n"
            records_found = True

    # print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(
            Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + filename)
        return 3


############################### This is a dummy function for CM Form which may not be used #########################
def create_CMForm(formnumber):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Form.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")

    filename = "CMForm.txt"
    tablename = "[CM Form]"

    try:
        # Get field names in CM Form
        crsr = conn.cursor()
        res = crsr.execute("SELECT * FROM [CM Form] WHERE 1=0")
        fieldlist = [tuple[0] for tuple in res.description]
        crsr.close()
        print(fieldlist)
    except pypyodbc.Error:
        crsr.close()
        return 5

    queryfieldlist = ""
    fieldexclude = ["ID", "Title", "Who is Boarding the Devices?", 'Delivery instructions', 'Comments', 'Assignee', 'internal workflow status', 'CM Workflow', 'DeliveryDate', 'Creation time', 'Inform_CM_Form_Updates', 'Update_Graphs', 'Update_Internal_Workflow_Status', 'Update_Graphs_Requests_Status', 'Update_Graphs_Priority', 'Update_Graph_Product_Type', 'Update_Graphs_Weekly_Requests', 'Update_Graphs_Requests_Distribution', 'Create_CM_Checklist', 'Update_CM_Checklist', 'Did you input all requested parameters?', 'Did you embed correct VCL file for either production or Demo or ', 'What VCL is chosen?', 'Did you sign all packages with Verifone Demo, Point or customer ', 'What is the signature?', 'Did you embed proper media files (MX915 or MX925 or Customized m', 'Which media chosen?', 'Did you check to see if customer requested EMV and provided prop', 'Which EMV package did you chose?', 'Did you test and install your package on a device and got succes', 'Install successful?', 'Did you choose correct SCA package for the specified VX device?', 'Please answer which SCA package was chosen?', 'Do You confirm all above Checklist information is correct?', 'Update_Request_Status_one_time', 'Update_Internal_workflow_status_from_Request_status', 'CM Workflow new', 'Update_CMChecklist_Field', 'Update_Graphs_Requests_Distribution_update', 'Checklist_Validation_in_CM_Form', 'createdthismonth', 'Update_CM_report', 'Create_CM_report', 'Assignee_Validation', 'Content Type', 'App Created By', 'App Modified By', '“Deliver to” person', 'Customer Email', 'Attachments', 'Workflow Instance ID', 'File Type', 'Modified', 'Created', 'Created By', 'Modified By', 'URL Path', 'Path', 'Item Type', 'Encoded Absolute URL']
    for i in range(0, len(fieldlist)):
        if fieldlist[i] not in fieldexclude:
            queryfieldlist = queryfieldlist + ", [CM Form].[" + fieldlist[i] + "]"
    queryfieldlist = queryfieldlist[2:]

    print(queryfieldlist)

    cur = conn.cursor()
    #WHERE = tablename + ".[ID] = '" + str(int(formnumber))
    query = "SELECT " + queryfieldlist + " FROM " + tablename + " WHERE " + tablename + ".[ID] = " + str(formnumber)
    print(query)
    cur.execute(query)

    line = "# This file contains the BIN ranges that are PCI Branded cards\n"
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        #if (row.get("ID") == str(formnumber)):
        minrange = str((row.get("Product Type")))
        maxrange = str((row.get("Environment")))
        line = line + minrange + "-" + maxrange + ";\n"
        records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + filename)
        return 3

#---------------------------------------------------------------------------------------------------------------

def get_previous_CM_REquests(clintid, prodtype, Device, environment):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Form.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")

    tablename = "[CM Form]"

    cur = conn.cursor()
    query = "SELECT [CM Form].[ID] FROM [CM Form] WHERE [CM FORM].[Client Name] = " + str(clintid) + " AND [CM FORM].[Product Type] = '" + str(prodtype) + "' AND [CM FORM].[Device Model] = '" + str(Device) + "' AND [CM Form].[Environment] = '" + str(environment) + "'"
    #print(query)
    cur.execute(query)

    prev_requests_list = []

    while True:
        row = cur.fetchone()

        if row is None:
            break
        prev_requests_list.append((row.get("ID")))

    cur.close()
    conn.close()

    return prev_requests_list


#---------------------------------------------------------------------------------------------------------------


def create_PreferredAIDs(id, prodtype, device, environment, creditdebit):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1

    dbfilename = ""
    filename = ""
    tablename = ""
    line = ""

    if creditdebit == "credit":
        dbfilename = "creditPreferredAIDs.accdb"
        tablename = "creditPreferredAIDs"
        filename = "creditPreferredAIDs.INI"
        line = "#################################################################################\n"
        line = line + "#AIDs can be configured as below in PreferredAIDs section 			#\n"
        line = line +  "#Configuration  has to be in key value pair and a maximum of 32 AIDs can 	#\n"
        line = line +  "#be configured.									#\n"
        line = line +  "#The Key starts from PAID1, PAID2....PAID32					#\n"
        line = line +  "#Ex:										#\n"
        line = line +  "#PAID1=A0000000032010,9F3303E0B8C8			  			#\n"
        line = line +  "#PAID2=A000000003101001					  			#\n"
        line = line +  "#PAID3=A0000000043060,9F3303E0C0B8           		  			#\n"
        line = line +  "#this file prefers credit aid over US Debit\n"
        line = line +  "#################################################################################\n"
    elif creditdebit == "debit":
        dbfilename = "DebitPreferredAIDs.accdb"
        filename = "DebitPreferredAIDs.INI"
        tablename = "DebitPreferredAIDs"
        line = "#################################################################################\n"
        line = line + "#AIDs can be configured as below in PreferredAIDs section 			#\n"
        line = line +  "#Configuration  has to be in key value pair and a maximum of 32 AIDs can 	#\n"
        line = line +  "#be configured.									#\n"
        line = line +  "#The Key starts from PAID1, PAID2....PAID32					#\n"
        line = line +  "#Ex:										#\n"
        line = line +  "#PAID1=A0000000032010,9F3303E0B8C8			  			#\n"
        line = line +  "#PAID2=A000000003101001					  			#\n"
        line = line +  "#PAID3=A0000000043060,9F3303E0C0B8           		  			#\n"
        line = line +  "#################################################################################\n"

    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + dbfilename  +  ";\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + dbfilename + ";\"")
    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Section], " + tablename + ".[Parameter], " + tablename + ".[Value], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    section = ""
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            if (section != row.get("Section")):
                if (section == ""):
                    section = row.get("Section")
                    line = line + "\n[" + section + "]\n"
                else:
                    section = row.get("Section")
                    line = line + "\n[" + section + "]\n"
            if not row.get("Value"):
                line = line + str(row.get("Parameter")) + "=\n"
            else:
                line = line + str(row.get("Parameter")) + "=" + str(row.get("Value")) + "\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + tablename)
        return 3


# ---------------------------------------------------------------------------------------------------------------

def create_aidlistini(id, prodtype, device, environment):
    if (id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\AIDListini.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\AIDListini.accdb;\"")

    filename  = "AIDList.ini"
    tablename = "AIDListini"

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[AID], " + tablename + ".[Payment Type], " + tablename + ".[Payment Media], " + tablename + ".[Cashback Enabled], " + tablename + ".[Card Abbreviation], " + tablename + ".[Signature Limit], " + tablename + ".[Capture Signature if PIN is bypassed], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    section = [""]
    sectionind = 0
    sectionprev = ""
    line = ""
    comment = ""
    comment = comment + "##SupportedAIDS should have all the AIDs supported on terminal. Each AID\n"
    comment = comment + "##should have respective config section also. Failing any such case, that\n"
    comment = comment + "##AID will not be supported.\n"
    comment = comment + "##Each AID should be different number starting from 1, and should in continuity\n"
    comment = comment + "##without any miss. SCA will read AID1,AID2,AID3... so on.\n\n"
    comment = comment + "#Below are parameters per AID\n"
    comment = comment + "#\n"
    comment = comment + "#PaymentType			Supported Payment Type(-1 : Both Credit and Debit, 0: Only Credit, 1: Only Debit, 2 : Debit but Toggle Payment Type to Credit if PIN is bypassed)\n"
    comment = comment + "#PaymentMedia			Payment Media\n"
    comment = comment + "#CashbackEnabled		Cash back Enabled or Disabled( 0 : Disabled, 1 : Enabled)\n"
    comment = comment + "#CardAbbrv				Card Abbreviation\n"
    comment = comment + "#signaturelimit			Indicates the signature limit below which signature will not be taken for the EMV card, if signature is selected as the CVM.\n"
    comment = comment + "#capsignonpinbypass		Indicates whether to cappture signature if PIN is bypassed. The transaction amount should be more than or equal to signaturelimit.\n\n"


    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            sectionprev = row.get("AID").replace(".", "")
            if (section[sectionind] != sectionprev):
                section.append(row.get("AID").replace(".", ""))
                # section.insert(sectionind, row.get("Scheme") + "." + row.get("Parameter Type"))
                line = line + "\n[" + section[sectionind + 1] + ".config]\n"
                sectionind += 1

            if row.get("Payment Type") == 0:
                line = line + "paymenttype=0\n"
            elif not row.get("Payment Type"):
                line = line + "paymenttype=\n"
            else:
                line = line + "paymenttype=" + str(int(row.get("Payment Type"))) + "\n"

            if not row.get("Payment Media"):
                line = line + "paymentmedia=\n"
            else:
                line = line + "paymentmedia=" + str(row.get("Payment Media")) + "\n"

            if row.get("Cashback Enabled") == 0:
                line = line + "cashbackenabled=0\n"
            elif not row.get("Cashback Enabled"):
                line = line + "cashbackenabled=\n"
            else:
                line = line + "cashbackenabled=" + str(int(row.get("Cashback Enabled"))) + "\n"

            if not row.get("Card Abbreviation"):
                line = line + "cardabbrv=\n"
            else:
                line = line + "cardabbrv=" + str(row.get("Card Abbreviation")) + "\n"

            if row.get("Signature Limit") == 0:
                line = line + "signaturelimit=0\n"
            elif not row.get("Signature Limit"):
                line = line + "signaturelimit=\n"
            else:
                line = line + "signaturelimit=" + str(row.get("Signature Limit")) + "\n"

            if row.get("Capture Signature if PIN is bypassed") == 0:
                line = line + "capsignonpinbypass=0\n"
            elif not row.get("Capture Signature if PIN is bypassed"):
                line = line + "capsignonpinbypass=\n"
            else:
                line = line + "capsignonpinbypass=" + str(row.get("Capture Signature if PIN is bypassed")) + "\n"

            records_found = True

    cur.close()
    conn.close()

    if (records_found):
        uniqscheme = [""]

        for scheme_partype in section:
            if (scheme_partype.split(".")[0] not in uniqscheme):
                uniqscheme.append(scheme_partype.split(".")[0])
        #print(uniqscheme)

        supportedschemeind = 1
        supportedschemes = "[supportedaids]\n"
        for i in range(1, len(uniqscheme)):
            supportedschemes = supportedschemes + "aid" + str(i) + "= " + uniqscheme[i] + "\n"

        line = comment + supportedschemes + line

        #print(line)
        #print(supportedschemes)
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + filename)
        return 3


#---------------------------------------------------------------------------------------------------------------

def create_SAF_Err_Codes(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1

    line = ""
    dbfilename = "SAF_ERR_CODES.accdb"
    tablename = "SAF_ERR_CODES"
    filename = "SAF_ERR_CODES.DAT"

    line = "# This file contains the SAF Error codes that are considered as Host offline\/Not-Available\n"
    line = line + "# All SAF Error Codes needs to be present in single line\n"
    line = line + "# Only first line of the SAF Error Codes is considered\n"
    line = line + "# SAF Error Code line should start with PIPE\(|\) and end with PIPE\(|\)\n"
    line = line + "# Each SAF Error Code should be seperated by PIPE\(|\)\n"
    line = line + "# Format of SAF Error Codes Line : |<RESULT_CODE Value>|<RESULT_CODE Value>|<RESULT_CODE Value>|<RESULT_CODE Value>|...\n"
    line = line + "# If Result code is Prepended by 0s, please remove them and put them. For example, if 0008 is coming in the SSI response then we need to add |8| to the SAF Error Codes Line\n"

    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + dbfilename  +  ";\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + dbfilename + ";\"")
    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[SAF Error Codes], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            line = line + row.get("SAF Error Codes") + "\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + tablename)
        return 3


#---------------------------------------------------------------------------------------------------------------

def create_tpscodesforSAF(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1

    line = ""
    dbfilename = "tpscodesforSAF.accdb"
    tablename = "tpscodesforSAF"
    filename = "tpscodesforSAF.txt"

    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + dbfilename  +  ";\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + dbfilename + ";\"")
    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Value], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            line = line + row.get("Value") + "\n"
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + tablename)
        return 3


#---------------------------------------------------------------------------------------------------------------

def create_xmlReceiptFile(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1

    line = ""
    dbfilename = "xmlReceiptFile.accdb"
    tablename = "xmlReceiptFile"
    filename = "xmlReceiptFile.xml"

    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + dbfilename  +  ";\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + dbfilename + ";\"")
    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Value], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            line = line + row.get("Value")
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + tablename)
        return 3

#---------------------------------------------------------------------------------------------------------------

def create_propertiesdata(id, prodtype, device, environment):
    if ( id == -1):
        print(Fore.RED + "\nClient Name doesn't exist in 'Add Client Name' list in Sharepoint")
        return 1

    line = ""
    dbfilename = "propertiesdata.accdb"
    tablename = "propertiesdata"
    filename = "propertiesdata.xml"

    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + dbfilename  +  ";\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + dbfilename + ";\"")
    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT " + tablename + ".[Value], " + tablename + ".[Client Name], " + tablename + ".[Product Type], " + tablename + ".[Device], " + tablename + ".[Environment], " + tablename + ".[ID] FROM " + tablename + " WHERE " + WHERE
    cur.execute(query)

    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        if (row.get("Client Name") == id) and (row.get("Product Type").lower() == prodtype.lower()) and (row.get("Device").lower() == device.lower()) and (row.get("Environment").lower() == environment.lower()):
            line = line + row.get("Value")
            records_found = True

    #print(line)
    cur.close()
    conn.close()
    if (records_found):
        if create_file(line, filename):
            return 0
        else:
            return 2
    else:
        print(Fore.RED + "\nNo Records found in Sharepoint for Client / Product Type / Device / Environment combination entered for " + tablename)
        return 3


'''
cdtorder = [ ["Card Type", "Accum Index", "Host Group ID", "PAN Low", "PAN High", "Min PAN", "Max PAN", "AVS",
                "Issuer ID", "Issuer Number", "Tip Discount", "Floor Limit Amount", "CVV2", "Pment Index",
                "CDT Custom Int", "Abbreviation", "Card Label", "IPC Label", "IPC Processor", "Tracks Reqd",
                "Batch Auth Floor Limit", "Receipt Limit", "Sign Limit", "FPS Print Option", "Gift Amt Min",
                "Gift Amt Max", "Disabled", "Enable FPS", "Tax Exempt", "Visa Card", "Master Card", "AMEX Card",
                "Discover Card", "JCB Card", "Check LUHN", "Exp Date Reqd", "Manual Entry", "Allow Multi Curr",
                "Sign Line", "Card Present", "Pin Reqd", "Printer Required"],
            ["Type", "AccumIndex", "HostGroupID", "PANLo", "PANHi", "MinPANDigit", "MaxPANDigit", "AVS",
             "IssuerID", "IssuerNum", "TipDiscount", "FloorLimitAmount", "CVV_II", "PaymentIndex",
             "CDTCustomInt", "CardAbbrev", "CardLabel", "IPCLabel", "IPCProcessor", "TracksRequired",
             "BatchAuthFloorLimitAmt", "ReceiptLimitAmount", "SignLimitAmount", "FPSPrintOption", "GiftAmtMin",
             "GiftAmtMax", "DisableCardRange", "EnableFPS", "TaxExempt", "VisaCard", "MasterCard", "AmexCard",
             "DiscCard", "JCBCard", "ChkLuhn", "ExpDtReqd", "ManEntry", "AllowMultiCurr",
             "SignLine", "CardPresent", "PinpadRequired", "PrntrRequired"]
            ]

cdtvxorder = [ ["Abbreviation", "Card Label", "Card Type", "Host Card Type", "Check Size", "PAN Low", "PAN High", "Min PAN",
                "Max PAN", "Card Range Enabled", "LUHN", "SAF Limit", "QSR Enabled", "QSR Receipt Option",
                "Receipt Limit", "Signature Limit", "Manual Allowed", "Expiration Date"],
            ["CardAbbrev", "CardLabel", "CardType", "HostCardType", "ChkSize", "PANLow", "PANHigh", "PANMin",
             "PANMax", "CardRangeEnabled", "LUHN", "SAFLimit", "QSREnabled", "QSRReceiptOption",
             "ReceiptLimitAmount", "SignatureLimitAmount", "ManualAllowed", "ExpirationDate"]
            ]
'''

cdtorder = [[],[]]
cdtvxorder = [[],[]]


def read_conf():
    home = expanduser("~")
    base_path = ""
    if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
    elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
    elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
        base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
    elif os.path.isdir(home + "\\OneDrive - Verifone"):
        home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

    try:
        conffile = base_path + "CMAutomation\\conf.xlsx"
        workbook = open_workbook(conffile)
        wb = workbook.sheet_by_name("cdt order")

        for row in range(wb.nrows):
            for col in range(1, wb.ncols):
                value = wb.cell(row, col).value
                if value != "" and row == 0:
                    cdtorder[0].insert(len(cdtorder[0]), value)
                elif value != "" and row == 1:
                    cdtorder[1].insert(len(cdtorder[1]), value)
                elif value != "" and row == 2:
                    cdtvxorder[0].insert(len(cdtvxorder[0]), value)
                elif value != "" and row == 3:
                    cdtvxorder[1].insert(len(cdtvxorder[1]), value)
    except:
        return 1

    # print(values)
    # print(self.dict)

read_conf()
#print(cdtorder)
#print(cdtvxorder)

#read_cdt()
#print("Got it : " + str(getid_addclientname("Dallas Cowboys")))
#print(read_cdt(getid_addclientname("AM Retail")))

#create_cdt(getid_addclientname("99Cents"), "SCA Point Classic", "VX520", "Lab", cdtorder)
#create_cdt(getid_addclientname("Lee's tools"), "SCA Point Classic", "MX830", "Lab", cdtorder)
#insert_cdt()

#create_configusr1(getid_addclientname("Hibbett"), "SCA Point Classic", "MX830", "Lab")
#insert_configusr1()

#create_emvtables(getid_addclientname("Faber Coe & Gregg Inc"), "SCA Point Classic", "MX830", "Lab")
#mx_insert("EMVTables.INI")

#create_VHQconfig(getid_addclientname("American Signature"), "SCA Point Enterprise", "MX915-PCI 4", "QA")
#insert_into_local_package("vhq_insert.sh", "VHQconfig.ini")

#create_CAPKData(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "CAPKData.INI")

#create_CTLSConfig(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "CTLSConfig.INI")

#create_MSGXPI(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "CAPKData.INI")

#create_optflag(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "OptFlag.INI")

#create_EmvTagsReqbyHost(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "EmvTagsReqbyHost.txt")

#create_displayPrompts(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "EmvTagsReqbyHost.txt")

#create_PCI_BIT(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "EmvTagsReqbyHost.txt")

#create_CMForm("420")
#insert_into_local_package("mx_insert.sh", "EmvTagsReqbyHost.txt")

#create_PreferredAIDs(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab", "credit")
#insert_into_local_package("mx_insert.sh", "EmvTagsReqbyHost.txt")

#create_PreferredAIDs(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab", "debit")
#insert_into_local_package("mx_insert.sh", "EmvTagsReqbyHost.txt")

#create_aidlistini(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "AIDList.ini")

#create_SAF_Err_Codes(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "SAF_ERR_CODES.DAT")

#create_tpscodesforSAF(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "SAF_ERR_CODES.DAT")

#create_xmlReceiptFile(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "xmlReceiptFile.xml")

#create_propertiesdata(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "xmlReceiptFile.xml")

#create_AIDListtxt(getid_addclientname("Lee's Tools"), "SCA Point Classic", "MX830", "Lab")
#insert_into_local_package("mx_insert.sh", "xmlReceiptFile.xml")
