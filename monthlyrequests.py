import pypyodbc
import os
import calendar
import datetime
import logging
from time import strftime
from datetime import timedelta
pypyodbc.lowercase = False
from colorama import init, Fore, Back, Style
init()
from os.path import expanduser
home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"

logfile = os.getcwd() + '\\logs\\monthlyrequests.log'

logging.basicConfig(filename=logfile,level=logging.DEBUG)
createdclosed = [[]]
print(strftime("%m/%d/%Y : %H:%M"))
logging.info(strftime("%m/%d/%Y : %H:%M"))

def monthly_report(startdate, enddate, monthname, year):
    conn = pypyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
    #r"Dbq=" + msaccess_files + "\MSAccess\CM Ticket.accdb;\"")

    cur = conn.cursor()
    WHERE = "[CM Ticket].[Ticket Created on] >= #" + startdate + "# AND [CM Ticket].[Ticket Created on] <= #" + enddate + "#;"
    query = "SELECT [CM Ticket].[ID] FROM [CM Ticket] WHERE " + WHERE
    #query = "SELECT Count(*) FROM [CM Ticket] WHERE " + WHERE

    #print(query)
    cur.execute(query)
    this_month_created = 0

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            this_month_created+=1

    #print("Created_tickets : " + str(this_month_created) + " : " + startdate + " : " + enddate)

    #####################################################

    WHERE = "[CM Ticket].[Delivered_date] >= #" + startdate + "# AND [CM Ticket].[Delivered_date] <= #" + enddate + "# AND ( [CM Ticket].[Request Status] = \'Delivered\' OR [CM Ticket].[Request Status] = \'Closed\' OR [CM Ticket].[Request Status] = \'Cancelled\' ) ;"
    query = "SELECT [CM Ticket].[ID] FROM [CM Ticket] WHERE " + WHERE

    #print(query)
    cur.execute(query)
    this_month_closed = 0

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            this_month_closed += 1

    #print(this_month_closed)

    #####################################################

    createdclosed.append([monthname, this_month_created, this_month_closed, year])
    #print(str(startdate) + " : " + str(enddate) + " : " + str(createdclosed))

    cur.close()
    conn.close()
    return True

def delete_Monthly_Report_List():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Monthly_Requests.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Monthly_Requests.accdb;\"")

    tablename = "Monthly_Requests"

    try:
        query = "DELETE * FROM " + tablename
        cur = conn.cursor()
        cur.execute(query)
        cur.commit()
        #print(Fore.GREEN + "Existing Monthly_Requests records are deleted from Sharepoint")
        logging.info("Existing Monthly_Requests records are deleted from Sharepoint")
    except:
        print(Fore.RED + "\nError deleting Monthly_Requests records from Sharepoint. Exiting...")
        return False

    conn.close()
    return True


def update_Monthly_Report_List(month, created, closed, year):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Monthly_Requests.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Monthly_Requests.accdb;\"")

    tablename = "Monthly_Requests"

    monthyear = str(month) + " - " + str(year)


    WHERE = "[Monthly_Requests].[Month] = '" + monthyear + "';"
    query = "SELECT [Monthly_Requests].[ID] FROM [Monthly_Requests] WHERE " + WHERE
    #query = "SELECT Count(*) FROM [CM Form] WHERE " + WHERE

    #print(query)
    cur = conn.cursor()
    cur.execute(query)

    rowfound = 0
    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            rowfound = 1

    cur.close()

    if rowfound == 0:
        try:
            query = "INSERT INTO " + tablename + " ( [Month], [No of Requests Created], [No of Requests Closed]  ) VALUES ( '" + monthyear + "', '" + str(created) + "', '" + str(closed) +  "' );"
            cur = conn.cursor()
            cur.execute(query)
            cur.commit()
            #print(query)
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(query)
            return False
    else:
        try:
            query = "UPDATE " + tablename + " SET [No of Requests Created] = '" + str(created) + "', [No of Requests Closed] = '" + str(closed) +  "' WHERE [Month] = '" + monthyear + "';"
            cur = conn.cursor()
            cur.execute(query)
            cur.commit()
            #print(query)
        except:
            print(Fore.RED + "\nError updating records into Sharepoint. Exiting...")
            print(query)
            return False


    #print("Record inserted successfully into Monthly_Requests list")
    conn.close()
    return True

#####################################################################

def getdates(year, month):
    #print(str(year) + ":" + str(month))
    _, num_days = calendar.monthrange(year, month)
    first_day = datetime.date(year, month, 1)
    last_day = datetime.date(year, month, num_days)
    last_day_plus1 = last_day + timedelta(days=1)
    #print(last_day_plus1)
    #print("Start Date : " + first_day.strftime('%m/%d/%Y') + " End Date : " + last_day.strftime('%m/%d/%Y'))
    monthly_report(first_day.strftime('%m/%d/%Y'), last_day_plus1.strftime('%m/%d/%Y'), calendar.month_name[month], year)

def getmonthyear():
    now = datetime.datetime.now()
    #for i in range(0,6):  # Change 6 to some other number here to get count for different number of months
    i = 1  # records will be created for i + 1 number of months so change this value accordingly
    while i >= 0:
        month = 0
        year = 0
        #print(str(now.month) + ":" + str(now.year))
        if now.month-i > 0:
            month = now.month - i
            year = now.year
        else:
            month = 13 - i
            year = now.year - 1
        #print(str(month) + ":" + str(year))
        #print(calendar.month_name[month])
        getdates(year, month)
        i-=1

#monthly_report()
#monthly_report("4/1/2017", "4/30/2017")
getmonthyear()
#print(createdclosed)
#delete_Monthly_Report_List()
succ = False
for i in range (1, len(createdclosed)):
    #print(str(createdclosed[i][0]) + ":" + str(createdclosed[i][1]) + ":" + str(createdclosed[i][2]) )
    succ = update_Monthly_Report_List(createdclosed[i][0], createdclosed[i][1], createdclosed[i][2], createdclosed[i][3])

if succ == True:
    print("Records inserted / updated successfully into Monthly requests Trend")

#update_Monthly_Report_List("May", 10, 15)

