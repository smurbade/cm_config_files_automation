import os
import pypyodbc
import read_conf
read_conf.read_conf_file()
import subprocess
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"
from PIL import Image
#image = "C:\\Users\\T_SachinM2\\PycharmProjects\\First Project\\915_scaidleimage.png"
#video_file = "C:\\Users\\T_SachinM2\\PycharmProjects\\First Project\\915_scaidlevideo.avi"
media_guide_file = "logs\\media_guide_table.txt"
verify_media_log = "logs\\verify_media.log"
#im = Image.open(image)
#width, height = im.size
#print(str(width))
#print(str(height))
#print(im.format)
#print(str(os.path.getsize(image)))

def read_media_guide():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Media_Guide.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\Media_Guide.accdb;\"")

    cur = conn.cursor()
    #WHERE = "[CM Form].[Created] >= #" + startdate + "# AND [CM Form].[Created] <= #" + enddate + "#;"
    query = "SELECT [Media_Guide].[Device], [Media_Guide].[Media Area], [Media_Guide].[Width], [Media_Guide].[Height], [Media_Guide].[File Formats Supported], [Media_Guide].[Size Limit in MB], [Media_Guide].[Minimum SCA Version Required] FROM [Media_Guide]"

    if os.path.exists(media_guide_file):
        os.remove(media_guide_file)
    media_guide_file_ptr = open(media_guide_file, 'w')

    # print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(row)
            media_guide_file_ptr.write(str(row) + "\n")

    media_guide_file_ptr.close()
    cur.close()
    conn.close()

def validate_media(arguments):
    command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\verify_media.sh MX " + arguments
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
    if result.returncode != 0:
        print(result.stderr)
        print(result)
        print("Execution of verify_media.sh failed")
    else:
        data = ""
        try:
            with open(verify_media_log, 'r') as verified_medi_report:
                data=verified_medi_report.read()
        except:
            data = "error"
        #print(data)
        return data

#read_media_guide()
#validate_media("915")

