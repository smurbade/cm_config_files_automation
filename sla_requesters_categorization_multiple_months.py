import pypyodbc
from xlrd import open_workbook
import time
pypyodbc.lowercase = False
import os, datetime, calendar
from datetime import timedelta
#from datetime import datetime
from datetime import date
from time import strftime
from time import strptime
from time import mktime
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

mapping_filename = base_path + "CMAutomation\\Requesters_Mapping.xlsx"
msaccess_files = base_path + "CMAutomation"
workbook = open_workbook(mapping_filename)
wb = workbook.sheet_by_name("Sheet1")
usertitle = {}

sla_list = []
requester_category = []
weekly_createdclosed = []
cm_list = {}
cm_name_mapping = {}
global cyclecount
global lastweekstartdate
global lastweekenddate


def getmonthyear():
    now = datetime.datetime.now()
    #for i in range(0,6):  # Change 6 to some other number here to get count for different number of months
    global cyclecount
    cyclecount = 1 # records will be created for i + 1 number of months so change this value accordingly
    while cyclecount >= 0:
        month = 0
        year = 0
        if now.month-cyclecount > 0:
            month = now.month - cyclecount
            year = now.year
        else:
            month = 13 - cyclecount
            year = now.year - 1
        #print(str(month) + ":" + str(year))
        #print(calendar.month_name[month])
        getmonthdates(year, month)
        cyclecount-=1

def weekly_report(startdate, enddate):
    conn = pypyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
    #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")

    requests_per_product_type = {}

    cur = conn.cursor()
    WHERE = "[CM Ticket].[Ticket Created on] >= #" + startdate + "# AND [CM Ticket].[Ticket Created on] <= #" + enddate + "#;"
    query = "SELECT [CM Ticket].[ID], [CM Ticket].[Product Type] FROM [CM Ticket] WHERE " + WHERE
    #query = "SELECT Count(*) FROM [CM Ticket] WHERE " + WHERE

    #print(query)
    cur.execute(query)
    this_month_created = 0

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            this_month_created+=1
            if (row.get("Product Type")) in requests_per_product_type:
                requests_per_product_type[row.get("Product Type")] = requests_per_product_type[row.get("Product Type")] + 1
            else:
                requests_per_product_type[row.get("Product Type")] = 1

    #print(str(startdate) + " - " + str(enddate))
    #print(requests_per_product_type)
    update_List("Weekly_Requests_per_Product_Type", requests_per_product_type, str(startdate))

    #print("Created_tickets : " + str(this_month_created) + " : " + startdate + " : " + enddate)

    #####################################################

    WHERE = "[CM Ticket].[Delivered_date] >= #" + startdate + "# AND [CM Ticket].[Delivered_date] <= #" + enddate + "# AND ( [CM Ticket].[Request Status] = \'Delivered\' OR [CM Ticket].[Request Status] = \'Closed\' OR [CM Ticket].[Request Status] = \'Cancelled\' ) ;"
    query = "SELECT [CM Ticket].[ID] FROM [CM Ticket] WHERE " + WHERE

    #print(query)
    cur.execute(query)
    this_month_closed = 0

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            this_month_closed += 1

    #print(this_month_closed)

    #####################################################

    weekly_createdclosed.append([startdate, this_month_created, this_month_closed])

    cur.close()
    conn.close()
    return True

def getweekdates():
    global lastweekstartdate
    global lastweekenddate
    now = datetime.datetime.now()
    dt = datetime.datetime.strptime(strftime("%d/%b/%Y"), '%d/%b/%Y')
    sunday = dt - timedelta(days=dt.weekday()+1)
    monday = sunday - timedelta(days=6)
    for i in range(0, 6):
        #print(monday)
        #print(sunday)
        weekly_report(monday.strftime("%Y-%m-%d"), sunday.strftime("%Y-%m-%d"))
        if i == 0:
            lastweekstartdate = monday.strftime("%Y-%m-%d")
            lastweekenddate = sunday.strftime("%Y-%m-%d")
        sunday = sunday - timedelta(days=7)
        monday = monday - timedelta(days=7)
    #print(weekly_createdclosed)


def getmonthdates(year, month):
    _, num_days = calendar.monthrange(year, month)
    first_day = date(year, month, 1)
    last_day = date(year, month, num_days)
    last_day_plus1 = last_day + timedelta(days=1)
    #print(last_day_plus1)
    #print("Start Date : " + first_day.strftime('%m/%d/%Y') + " End Date : " + last_day.strftime('%m/%d/%Y'))
    #monthly_report(first_day.strftime('%m/%d/%Y'), last_day_plus1.strftime('%m/%d/%Y'), calendar.month_name[month])
    get_users(requesters_category_sla(first_day.strftime('%m/%d/%Y'), last_day_plus1.strftime('%m/%d/%Y'), calendar.month_name[month], year), calendar.month_name[month], year)

def delete_list(list):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + list + ".accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + list + ".accdb;\"")

    tablename = list

    try:
        query = "DELETE * FROM " + tablename
        cur = conn.cursor()
        cur.execute(query)
        cur.commit()
        #print(Fore.GREEN + "Existing Monthly_Requests records are deleted from Sharepoint")
    except:
        print("\nError deleting " + tablename + " records from Sharepoint. Exiting...")
        return False

    conn.close()
    return True

def insert_List(list):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + list + ".accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + list + ".accdb;\"")

    tablename = list

    if list == "SLA":
        for i in range(0, len(sla_list)):
            WHERE = "[SLA].[Month] = '" + str(sla_list[i][0]) + "' AND [SLA].[SLA] = '" + str(sla_list[i][1]) + "' ;"
            query = "SELECT [SLA].[ID] FROM [SLA] WHERE " + WHERE

            # print(query)
            cur = conn.cursor()
            cur.execute(query)

            rowfound = 0
            while True:
                row = cur.fetchone()

                if row is None:
                    break
                else:
                    rowfound = 1

            cur.close()

            if rowfound == 0:
                try:
                    query = "INSERT INTO " + tablename + " ( [Month], [SLA], [# of Requests], [SLA %]  ) VALUES ( '" + str(sla_list[i][0]) + "', '" + str(sla_list[i][1]) + "', '" + str(sla_list[i][2]) + "', '" + str(sla_list[i][3]) +  "' );"
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                    #print(query)
                except:
                    print("\nError inserting records into " + list + " list. Exiting...")
                    print(query)
                    return False
            else:
                try:
                    query = "UPDATE " + tablename + " SET [# of Requests] = '" + str(sla_list[i][2]) + "', [SLA %] = '" + str(sla_list[i][3]) + "' WHERE [Month] = '" + str(sla_list[i][0]) + "' and [SLA] = '" + str(sla_list[i][1]) + "';"
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                    #print(query)
                except:
                    print("\nError inserting records into " + list + " list. Exiting...")
                    print(query)
                    return False
    elif list == "CM_Requests_Categorized_on_Requester":
        for i in range(0, len(requester_category)):
            WHERE = "[CM_Requests_Categorized_on_Requester].[Month] = '" + str(requester_category[i][0]) + "' AND [CM_Requests_Categorized_on_Requester].[Requester Category] = '" + str(requester_category[i][1]) + "' ;"
            query = "SELECT [CM_Requests_Categorized_on_Requester].[ID] FROM [CM_Requests_Categorized_on_Requester] WHERE " + WHERE

            # print(query)
            cur = conn.cursor()
            cur.execute(query)

            rowfound = 0
            while True:
                row = cur.fetchone()

                if row is None:
                    break
                else:
                    rowfound = 1

            cur.close()

            if rowfound == 0:
                try:
                    query = "INSERT INTO " + tablename + " ( [Month], [Requester Category], [# of Requests Created], [% of Requests Created]  ) VALUES ( '" + str(requester_category[i][0]) + "', '" + str(requester_category[i][1]) + "', '" + str(requester_category[i][2]) + "', '" + str(requester_category[i][3]) +  "' );"
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                    #print(query)
                except:
                    print("\nError inserting records into " + list + " list. Exiting...")
                    print(query)
                    return False
            else:
                try:
                    query = "UPDATE " + tablename + " SET [# of Requests Created] = '" + str(requester_category[i][2]) + "', [% of Requests Created] = '" + str(requester_category[i][3]) + "' WHERE [Month] = '" + str(requester_category[i][0]) + "' and [Requester Category] = '" + str(requester_category[i][1]) + "';"
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                    #print(query)
                except:
                    print("\nError inserting records into " + list + " list. Exiting...")
                    print(query)
                    return False
    elif list == "Weekly_Requests":
        for i in range(len(weekly_createdclosed), 0, -1):
            WHERE = "[Weekly_Requests].[Week Starting] = '" + str(weekly_createdclosed[i-1][0]) + "' ;"
            query = "SELECT [Weekly_Requests].[ID] FROM [Weekly_Requests] WHERE " + WHERE

            # print(query)
            cur = conn.cursor()
            cur.execute(query)

            rowfound = 0
            while True:
                row = cur.fetchone()

                if row is None:
                    break
                else:
                    rowfound = 1

            cur.close()

            if rowfound == 0:
                try:
                    query = "INSERT INTO " + tablename + " ( [Week Starting], [# of Requests Created], [# of Requests Delivered] ) VALUES ( '" + str(weekly_createdclosed[i-1][0]) + "', '" + str(weekly_createdclosed[i-1][1]) + "', '" + str(weekly_createdclosed[i-1][2]) +  "' );"
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                    #print(query)
                except:
                    print("\nError inserting records into " + list + " list. Exiting...")
                    print(query)
                    return False
            else:
                try:
                    query = "UPDATE " + tablename + " SET [# of Requests Created] = '" + str(weekly_createdclosed[i-1][1]) + "', [# of Requests Delivered] = '" + str(weekly_createdclosed[i-1][2]) + "' WHERE [Week Starting] = '" + str(weekly_createdclosed[i-1][0]) + "';"
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                    #print(query)
                except:
                    print("\nError inserting records into " + list + " list. Exiting...")
                    print(query)
                    return False

    elif list == "Monthly_Request_Distribution":
        for i in (cm_list):
            try:
                query = "INSERT INTO " + tablename + " ( [Configuration Manager], [# of CM Requests In Progress], [# of CM Requests Delivered This Month], [# of Amdocs Requests In Progress], [# of Amdocs Requests Delivered This Month], [# of Misc Requests In Progress], [# of Misc Requests Delivered This Month] ) VALUES ( '" + str(cm_list[i][0]) + "', '" + str(cm_list[i][1]) + "', '" + str(cm_list[i][2]) + "', '" + str(cm_list[i][3]) + "', '" + str(cm_list[i][4]) + "', '" + str(cm_list[i][5]) + "', '" + str(cm_list[i][6]) +  "' );"
                cur = conn.cursor()
                cur.execute(query)
                cur.commit()
                #print(query)
                #print(query)
            except:
                print("\nError inserting records into " + list + " list. Exiting...")
                print(query)
                return False


    print(list + " Records inserted successfully")
    conn.close()
    return True


def read_mapping_file():
    for row in range(wb.nrows):
        requestername = wb.cell(row, 0).value.strip()
        requestercategory = wb.cell(row, 1).value.strip()
        if requestername != "" and requestercategory != "":
            usertitle[requestername] = requestercategory
    #print("field mapping : " + str(usertitle))


def get_users(requester_list, month, year):
    '''
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Form.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")

    tablename = "UserInfo"

    cur = conn.cursor()
    query = "SELECT " + tablename + ".[Name], " + tablename + ".[Title] FROM " + tablename + " WHERE " + tablename + ".[Title] <> 'None'"
    usertitle = {}

    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            usertitle[row.get("Name")] = row.get("Title")
            #print(row)
    '''

    aa = 0
    ie = 0
    se = 0
    sr = 0
    sm = 0
    sim = 0
    cse = 0
    csr = 0
    ordentry = 0
    oth = 0


    if usertitle:
        #print(usertitle)
        if requester_list:
            #print(str(month) + " : " + str(year) + " : " + str(requester_list))
            for requester in requester_list:
                #print("usertitle : " + str(usertitle))
                #print(usertitle[requester])
                if requester in usertitle:
                    if "Account Administrator" in str(usertitle[requester]):
                        aa+=1
                    elif "Implementation Engineer" in str(usertitle[requester]):
                        ie += 1
                    elif "Sales Engineer" in str(usertitle[requester]):
                        se += 1
                    elif "Sales Rep" in str(usertitle[requester]) or "Sales Executive" in str(usertitle[requester]) or "Sales Acct Manager" in str(usertitle[requester]):
                        sr += 1
                    elif "Sales Manager" in str(usertitle[requester]) or "Sales Director" in str(usertitle[requester]):
                        sm += 1
                    elif "Solution Implementation" in str(usertitle[requester]):
                        sim += 1
                    elif "Technical Support Engineer" in str(usertitle[requester]):
                        cse += 1
                    elif "Order Entry" in str(usertitle[requester]):
                        ordentry += 1
                    elif "Client Services Support" in str(usertitle[requester]) or "Customer Service Representative" in str(usertitle[requester]) or "Customer Service Mgt 2" in str(usertitle[requester]) or "Customer Service Support" in str(usertitle[requester]) or "Technical Support Rep" in str(usertitle[requester]):
                        csr += 1
                    else:
                        oth +=1
        #print(str(aa) + " : " + str(ie) + " : " + str(se) + " : " + str(sr) + " : " + str(sm) + " : " + str(sim) + " : " + str(cse) + " : " + str(ordentry) + " : " + str(csr) + " : " + str(oth) )
    else:
        print("User Title dictionary empty")

    #print("No of requests by Account Administrators :" + str(aa))
    #print("% of requests by Account Administrators :" + str((aa/len(requester_list))*100))
    #print("No of requests by Implementation Engineers :" + str(ie))
    #print("% of requests by Implementation Engineers :" + str((ie/len(requester_list))*100))
    #print("No of requests by Customer Service Representative :" + str(csr))
    #print("% of requests by Customer Service Representative :" + str((csr/len(requester_list))*100))
    #print("No of requests by Sales Engineers :" + str(se))
    #print("% of requests by Sales Engineers :" + str((se/len(requester_list))*100))
    #print("No of requests by SIMs :" + str(sim))
    #print("% of requests by SIMs :" + str((sim/len(requester_list))*100))
    #print("No of requests by Others :" + str(len(requester_list) - aa - ie - csr - se - sim))
    #print("% of requests by Others :" + str(((len(requester_list) - aa - ie - csr - se - sim)/len(requester_list))*100))

    #print("No of requests created :" + str(len(requester_list)))


    if requester_list:
        requester_category.append([str(month) + " - " + str(year), "Account Administrators", (aa), round(((aa/len(requester_list))*100), 2)])
        requester_category.append([str(month) + " - " + str(year), "Implementation Engineers", (ie), round((((ie/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "Sales Engineers", (se), round((((se/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "Sales Rep", (sr), round((((sr/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "Sales Managers", (sm), round((((sm/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "SIMs", (sim), round((((sim/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "CSE", (cse), round((((cse/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "Helpdesk", (csr), round((((csr/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "Order Entry", (ordentry), round((((ordentry/len(requester_list))*100)), 2)])
        requester_category.append([str(month) + " - " + str(year), "Others", (oth), round(((((oth)/len(requester_list))*100)), 2)])
    else:
        requester_category.append([str(month) + " - " + str(year), "Account Administrators", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Implementation Engineers", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Sales Engineers", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Sales Reps", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Sales Managers", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "SIMs", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "CSE", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Helpdesk", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Order Entry", 0, 0])
        requester_category.append([str(month) + " - " + str(year), "Others", 0, 0])

    #print(str(requester_list))
    #print(str(requester_category))

    #####################################################

    #cur.close()
    #conn.close()
    return True


def requesters_category_sla(startdate, enddate, monthname, year):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")

    #print(str(startdate) + " : " + str(enddate) + " : " + str(monthname) )

    tablename = "CM Ticket"

    #print(str(startdate) + " : " + str(enddate))

    userinfo = {}
    cur = conn.cursor()
    query = "SELECT [UserInfo].[ID], [UserInfo].[Name] FROM [UserInfo]"
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            userinfo[row.get("ID")] = row.get("Name")

    cur.close()
    #print(str(userinfo))


    cur = conn.cursor()
    #WHERE = "[CM Ticket].[Created] >= #" + startdate + "# AND [CM Ticket].[Created] <= #" + enddate + "#;"
    #query = "SELECT [" + tablename + "].[Created By], [" + tablename + "].[ID], [" + tablename + "].[Assignee], [" + tablename + "].[DeliveryDate], [" + tablename + "].[Modified], [" + tablename + "].[Request Status], [" + tablename + "].[Priority] FROM [" + tablename + "] WHERE " + WHERE
    #query = "SELECT [" + tablename + "].[Created], [" + tablename + "].[Created By], [" + tablename + "].[ID], [" + tablename + "].[Assignee], [" + tablename + "].[DeliveryDate], [" + tablename + "].[Modified], [" + tablename + "].[Request Status], [" + tablename + "].[Priority] FROM [" + tablename + "]"
    query = "SELECT [" + tablename + "].[Ticket Created on], [" + tablename + "].[Ticket Created By], [" + tablename + "].[Ticket Number], [" + tablename + "].[CM Assigned for the request], [" + tablename + "].[Delivery Date], [" + tablename + "].[Delivered_date], [" + tablename + "].[Request Status], [" + tablename + "].[Priority] FROM [" + tablename + "]"
    requester_list = []
    high_met = 0
    high_notmet = 0
    medium_met = 0
    medium_notmet = 0
    low_met = 0
    low_notmet = 0

    #print(query)
    cur.execute(query)

    strtdt = datetime.datetime.strptime(startdate, '%m/%d/%Y')
    enddt = datetime.datetime.strptime(enddate, '%m/%d/%Y')

    #print("Cyclecount : " + str(cyclecount) + " : " + startdate + " : " + enddate)
    if cyclecount == 0:
        count_amdocsrequests(startdate, enddate)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            tktcrtdate = datetime.datetime.strptime(str(row.get("Ticket Created on")), '%Y-%m-%d %H:%M:%S')
            deldate = datetime.datetime.strptime(str(row.get("Delivered_date")), '%Y-%m-%d %H:%M:%S')
            if (tktcrtdate.date() >= strtdt.date() and tktcrtdate.date() <= enddt.date() and str(row.get("Ticket Created By")) != 'None'):
                #print(str(userinfo))
                #print(str(row.get("Ticket Created By")))
                #print(str(userinfo[row.get("Ticket Created By")]))
                #print(requester_list)
                requester_list.append(userinfo[row.get("Ticket Created By")])
                #print(row.get("Ticket Created By"))
            #print(str(row.get("Assignee")))
            #print(cm_list.values())
            if cyclecount == 0 and (str(row.get("CM Assigned for the request"))) != "None" and (int(row.get("CM Assigned for the request"))) in cm_list: # Countcount 0 means caluculate only for latest month
                if (str(row.get("Request Status")) != "Delivered" and str(row.get("Request Status")) != "Closed" and str(row.get("Request Status")) != "Cancelled"):
                    cm_list[int(str(row.get("CM Assigned for the request")))][1] = cm_list[int(str(row.get("CM Assigned for the request")))][1] + 1
                #if (str(row.get("Request Status")) == "Delivered" or str(row.get("Request Status")) == "Closed" or str(row.get("Request Status")) == "Cancelled") and ( (row.get("Delivered_date").strftime("%m/%d/%Y")) >= startdate and (row.get("Delivered_date").strftime("%m/%d/%Y")) <= enddate ) :
                if (str(row.get("Request Status")) == "Delivered" or str(row.get("Request Status")) == "Closed" or str(row.get("Request Status")) == "Cancelled") and (deldate.date() >= strtdt.date() and deldate.date() <= enddt.date()):
                    cm_list[int(str(row.get("CM Assigned for the request")))][2] = cm_list[int(str(row.get("CM Assigned for the request")))][2] + 1
                    #print(str(row.get("Ticket Number")) + " : " + str(row.get("Delivered_date").strftime("%m/%d/%Y")) + " : " +  str(startdate) + " : " + str(enddate))
                #print(str(startdate) + " : " + str(enddate))
                #print(str(row.get("CM Assigned for the request")) + " : " + str(cm_list[int(str(row.get("CM Assigned for the request")))][1]) + " : " + str(cm_list[int(str(row.get("CM Assigned for the request")))][2]))
            #print(row.get("ID"))
            if (row.get("Request Status") == "Delivered" or row.get("Request Status") == "Closed" or row.get("Request Status") == "Cancelled")  and (deldate.date() >= strtdt.date() and deldate.date() <= enddt.date()):
                targetdate = datetime.datetime.strptime(str(row.get("Delivery Date")), '%Y-%m-%d %H:%M:%S')
                delivered = datetime.datetime.strptime(str(row.get("Delivered_date")), '%Y-%m-%d %H:%M:%S')
                if row.get("Priority") == "High":
                    if targetdate.date() >= delivered.date():
                        high_met+=1
                        #print(str(startdate) + " : Met : " + str(row.get("ID")) + " : Delivery date : " + str(targetdate) + " : Delivered date : " + str(delivered) )
                    else:
                        high_notmet+=1
                        #print(str(startdate) + " : Not met : " + str(row.get("ID")) + " : Delivery date : " + str(targetdate) + " : Delivered date : " + str(delivered) )
                elif row.get("Priority") == "Medium":
                    if targetdate.date() >= delivered.date():
                        medium_met+=1
                    else:
                        medium_notmet+=1
                elif row.get("Priority") == "Low":
                    if targetdate.date() >= delivered.date():
                        low_met+=1
                    else:
                        low_notmet+=1

    #print("\n####################################################################")
    #print(monthname)
    #print("High sla Met : " + str(high_met))
    #print("High sla Not Met : " + str(high_notmet))
    #print("Medium sla Met : " + str(medium_met))
    #print("Medium sla Not Met : " + str(medium_notmet))
    #print("Low sla Met : " + str(low_met))
    #print("Low sla Not Met : " + str(low_notmet))

    #print(cm_list)

    sla_list.append([str(monthname) + " - " + str(year), "High SLA Met", high_met, 0])
    sla_list.append([str(monthname) + " - " + str(year), "High SLA Not Met", high_notmet, 0])
    sla_list.append([str(monthname) + " - " + str(year), "Medium SLA Met", medium_met, 0])
    sla_list.append([str(monthname) + " - " + str(year), "Medium SLA Not Met", medium_notmet, 0])
    sla_list.append([str(monthname) + " - " + str(year), "Low SLA Met", low_met, 0])
    sla_list.append([str(monthname) + " - " + str(year), "Low SLA Not Met", low_notmet, 0])

    for i in range(0, len(sla_list)):
        if sla_list[i][0] == str(monthname) + " - " + str(year):
            if "High SLA Met" in sla_list[i][1]:
                if (sla_list[i][2] + sla_list[i+1][2]) == 0:
                    percent_hi = 0
                else:
                    percent_hi = round((sla_list[i][2] / (sla_list[i][2] + sla_list[i+1][2]) * 100), 2)
                sla_list[i][3] = percent_hi
                #print(sla_list[i][0] + " High SLA Met percentage = " + str(percent_hi))
            elif "Medium SLA Met" in sla_list[i][1]:
                if (sla_list[i][2] + sla_list[i + 1][2]) == 0:
                    percent_med = 0
                else:
                    percent_med = round((sla_list[i][2] / (sla_list[i][2] + sla_list[i+1][2]) * 100), 2)
                sla_list[i][3] = percent_med
                #print(sla_list[i][0] + " Medium SLA Met percentage = " + str(percent_med))
            elif "Low SLA Met" in sla_list[i][1]:
                if (sla_list[i][2] + sla_list[i + 1][2]) == 0:
                    percent_low = 0
                else:
                    percent_low = round((sla_list[i][2] / (sla_list[i][2] + sla_list[i+1][2]) * 100), 2)
                sla_list[i][3] = percent_low
                #print(sla_list[i][0] + " Low SLA Met percentage = " + str(percent_low))
            elif "High SLA Not Met" in sla_list[i][1]:
                if sla_list[i-1][3] == 0:
                    sla_list[i][3] = 0
                else:
                    sla_list[i][3] = round((100 - sla_list[i-1][3]), 2)
                #print(sla_list[i][0] + " High SLA Not Met percentage = " + str(percent_hi))
            elif "Medium SLA Not Met" in sla_list[i][1]:
                if sla_list[i-1][3] == 0:
                    sla_list[i][3] = 0
                else:
                    sla_list[i][3] = round((100 - sla_list[i-1][3]), 2)
                #print(sla_list[i][0] + " Medium SLA Not Met percentage = " + str(percent_med))
            elif "Low SLA Not Met" in sla_list[i][1]:
                if sla_list[i-1][3] == 0:
                    sla_list[i][3] = 0
                else:
                    sla_list[i][3] = round((100 - sla_list[i-1][3]), 2)
                #print(sla_list[i][0] + " Low SLA Not Met percentage = " + str(percent_low))

    #print(str(startdate) + " : " + str(enddate) + " : " + str(sla_list) )

    if len(requester_list) > 0:
        #print(requester_list)
        return requester_list

def read_cm_list():
    home = expanduser("~")
    base_path = ""
    if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
    elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
    elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
        base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
    elif os.path.isdir(home + "\\OneDrive - Verifone"):
        home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
        base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

    wb = open_workbook(base_path + "CMAutomation\\CMList.xlsx")
    cmitems_filename = base_path + "CMAutomation\\CMList.xlsx"
    workbook = open_workbook(cmitems_filename)
    wb = workbook.sheet_by_name("CMList")

    #for s in wb.sheets():
    for row in range(wb.nrows):
        cm_list[int(wb.cell(row, 1).value)] = [wb.cell(row, 0).value.strip(), 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0, 0, 0, 0] # CM Name, CM Requests in progress, CM Requests Delivered, Amdocs Requests in progress, Amdocs Requests Delivered, Misc Requests in progress, Misc Requests Delivered, Amdocs Total Requests in progress, Amdocs Total Requests Delivered, Total CM Requests Delivered, Total Misc Requests Delivered, Time spent in Hrs on Amdocs requests, Time spent in Hrs on CM cases, Time spent in Hrs on Misc activities,
        cm_name_mapping[wb.cell(row, 1).value] = wb.cell(row, 0).value.strip()

    #print(cm_list)
    # print(dict)

def count_amdocsrequests(startdate, enddate):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CM Report.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CM Report.accdb;\"")

    tablename = "CM Report"
    strtdt = datetime.datetime.strptime(startdate, '%m/%d/%Y')
    enddt = datetime.datetime.strptime(enddate, '%m/%d/%Y')

    '''
    cur = conn.cursor()
    WHERE = "[CM Report].[Created] >= #" + startdate + "# AND [CM Report].[Created] <= #" + enddate + "# AND [CM Report].[CM Portal ID] is Null and [CM Report].[Amdocs ID] is Not Null ;"
    query = "SELECT DISTINCT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID] FROM [" + tablename + "] WHERE " + WHERE

    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(str(row.get("Configuration Manager")))
            if (str(row.get("Completed"))) and (int(row.get("Configuration Manager"))) in cm_list: # Countcount 0 means caluculate only for latest month
                cm_list[int(str(row.get("Configuration Manager")))][4] = cm_list[int(str(row.get("Configuration Manager")))][4] + 1
            #print(row.get("ID"))
    cur.close()
    '''

    cur = conn.cursor()
    #WHERE = "[CM Report].[CM Portal ID] is Null and [CM Report].[Amdocs ID] is Not Null"
    WHERE = "[CM Report].[Configuration Manager] is Not Null and [CM Report].[CM Portal ID] is Null and [CM Report].[Amdocs ID] is Not Null"
    query = "SELECT DISTINCT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID] FROM [" + tablename + "] WHERE " + WHERE
    #query = "SELECT DISTINCT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID], [" + tablename + "].[Created], [" + tablename + "].[CM Portal ID] FROM [" + tablename + "]"

    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(str(row.get("Configuration Manager")))
            #print(str(row.get("Configuration Manager")))
            #print(str(row.get("CM Portal ID")) + " : " + str(row.get("Amdocs ID")))
            if (str(row.get("Amdocs ID")) != "" and str(row.get("Amdocs ID")) != "NA" and str(row.get("Amdocs ID")) != "na"):
                #print("Amdocs not blank : " + str(row.get("Amdocs ID")))
                if not ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list: # Countcount 0 means caluculate only for latest month
                    cm_list[int(str(row.get("Configuration Manager")))][7] = cm_list[int(str(row.get("Configuration Manager")))][7] + 1 # Amdocs Total in progress
                elif ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list:
                    cm_list[int(str(row.get("Configuration Manager")))][8] = cm_list[int(str(row.get("Configuration Manager")))][8] + 1 # Amdocs Total Delivered

    cur.close()

    cur = conn.cursor()
    #WHERE = "[CM Report].[CM Portal ID] is Null and [CM Report].[Amdocs ID] is Not Null"
    #WHERE = "[CM Report].[CM Portal ID] is Null"
    #query = "SELECT DISTINCT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID], [" + tablename + "].[Created] FROM [" + tablename + "] WHERE " + WHERE
    query = "SELECT DISTINCT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID], [" + tablename + "].[Task Start Date], [" + tablename + "].[CM Portal ID] FROM [" + tablename + "]"

    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(str(row.get("Configuration Manager")))
            #print(str(row.get("Configuration Manager")))
             #print(str(row.get("CM Portal ID")) + " : " + str(row.get("Amdocs ID")))
            if row.get("Configuration Manager") is not None and (row.get("CM Portal ID")) is None and (row.get("Amdocs ID")) is not None and row.get("Task Start Date") is not None:
                tskstrtdt = datetime.datetime.strptime(str(row.get("Task Start Date")), '%Y-%m-%d %H:%M:%S')
                if (str(row.get("Amdocs ID")) != "" and str(row.get("Amdocs ID")) != "NA" and str(row.get("Amdocs ID")) != "na"):
                    #print("Amdocs not blank : " + str(row.get("Amdocs ID")))
                    #print(str(row.get("Task Start Date")))
                    #print(startdate)
                    #print(enddate)
                    if not ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list: # Countcount 0 means caluculate only for latest month
                        cm_list[int(str(row.get("Configuration Manager")))][3] = cm_list[int(str(row.get("Configuration Manager")))][3] + 1 # Amdocs in progress
                    elif ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list and (tskstrtdt.date() >= strtdt.date() and tskstrtdt.date() <= enddt.date()):
                        cm_list[int(str(row.get("Configuration Manager")))][4] = cm_list[int(str(row.get("Configuration Manager")))][4] + 1 # Amdocs Delivered this month

    cur.close()

    cur = conn.cursor()
    #WHERE = "[CM Report].[CM Portal ID] is Null and [CM Report].[Amdocs ID] is Not Null"
    #WHERE = "[CM Report].[CM Portal ID] is Null"
    #query = "SELECT DISTINCT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID], [" + tablename + "].[Created] FROM [" + tablename + "] WHERE " + WHERE
    query = "SELECT [" + tablename + "].[Configuration Manager], [" + tablename + "].[Completed], [" + tablename + "].[Amdocs ID], [" + tablename + "].[Task Start Date], [" + tablename + "].[CM Portal ID] FROM [" + tablename + "]"

    #print(query)
    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(str(row.get("Configuration Manager")))
            #print(str(row.get("Configuration Manager")))
             #print(str(row.get("CM Portal ID")) + " : " + str(row.get("Amdocs ID")))
             if row.get("Configuration Manager") is not None and (row.get("CM Portal ID")) is None and ((row.get("Amdocs ID")) is None or str(row.get("Amdocs ID")) == "NA" or str(row.get("Amdocs ID")) == "na") and row.get("Task Start Date") is not None:
                #print("Amdocs blank : " + str(row.get("Amdocs ID")))
                tskstrtdt = datetime.datetime.strptime(str(row.get("Task Start Date")), '%Y-%m-%d %H:%M:%S')
                if not ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list:  # Countcount 0 means caluculate only for latest month
                    cm_list[int(str(row.get("Configuration Manager")))][5] = cm_list[int(str(row.get("Configuration Manager")))][5] + 1
                elif ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list and (tskstrtdt.date() >= strtdt.date() and tskstrtdt.date() <= enddt.date()):
                    cm_list[int(str(row.get("Configuration Manager")))][6] = cm_list[int(str(row.get("Configuration Manager")))][6] + 1
                elif ((row.get("Completed"))) and (int(str(row.get("Configuration Manager")))) in cm_list:
                    cm_list[int(str(row.get("Configuration Manager")))][10] = cm_list[int(str(row.get("Configuration Manager")))][10] + 1

    cur.close()


def update_List(list, requests_per_product_type, weekstartdate):
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\\" + list + ".accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\\" + list + ".accdb;\"")

    tablename = list
    request_dist_cm_list = []
    last_week_efforts_cm_list = []
    global lastweekstartdate

    if list == "Requests_Distribution":
        cur = conn.cursor()
        query = "SELECT [" + list + "].[UserID] FROM [" + tablename + "]"

        #print(query)
        cur.execute(query)

        while True:
            row = cur.fetchone()

            if row is None:
                break
            else:
                request_dist_cm_list.append(int(row.get("UserID")))

        cur.close()

        #print(request_dist_cm_list)
        for i in request_dist_cm_list:
            if i not in cm_list:
                print("CM id " + str(i) + " not in cm_list so to be deleted from request distribution")
                try:
                    query = "DELETE FROM " + tablename + " WHERE [" + list + "].[UserID] =  '" + str(i) + "' ;"
                    #print(query)
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                except:
                    print("\nError Deleting record from " + list + " list. Exiting...")
                    print(query)
                    return False

        for i in cm_list:
            if i not in request_dist_cm_list:
                print("CM id " + str(i) + " not in Request distribution list so to be added")
                try:
                    #query = "INSERT INTO " + tablename + " ( [Month], [SLA], [# of Requests], [SLA %]  ) VALUES ( '" + str(
                    #    sla_list[i][0]) + "', '" + str(sla_list[i][1]) + "', '" + str(sla_list[i][2]) + "', '" + str(
                    #    sla_list[i][3]) + "' );"
                    query = "INSERT INTO " + tablename + " ( [Amdocs_Delivered], [Amdocs_In_Progress], [Requests_Delivered], [Requests_In_Progress], [Misc_Delivered], [Misc_In_Progress], [UserID], [Title] ) VALUES ( '" + str(cm_list[i][8]) + "',  '" + str(cm_list[i][7]) + "',  '" + str(cm_list[i][9]) + "',  '" + str(cm_list[i][1]) + "',  '" + str(cm_list[i][10]) + "',  '" + str(cm_list[i][5]) + "', '" + str(i) + "',  '" + str(cm_name_mapping[i]) + "' ) ;"
                    #print(query)
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                except:
                    print("\nError Inserting record from " + list + " list. Exiting...")
                    print(query)
                    return False


        if list == "Requests_Distribution":
            for i in cm_list:
                try:
                    query = "UPDATE " + tablename + " SET [Amdocs_Delivered] = '" + str(cm_list[i][8]) + "', [Amdocs_In_Progress] = '" + str(cm_list[i][7]) + "', [Requests_Delivered] = '" + str(cm_list[i][9]) + "', [Requests_In_Progress] = '" + str(cm_list[i][1]) + "', [Misc_Delivered] = '" + str(cm_list[i][10]) + "', [Misc_In_Progress] = '" + str(cm_list[i][5]) + "' WHERE [UserID] = '" + str(i) + "' ;"
                    #print(query)
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                except:
                    print("\nError updating records into " + list + " list. Exiting...")
                    print(query)
                    return False

        print(list + " Records updated successfully")

    elif list == "Last_Week_Efforts_Spent":
        cur = conn.cursor()
        query = "SELECT [" + list + "].[Configuration Manager] FROM [" + tablename + "]"

        #print(query)
        cur.execute(query)

        while True:
            row = cur.fetchone()

            if row is None:
                break
            else:
                last_week_efforts_cm_list.append(str(row.get("Configuration Manager")))

        cur.close()

        #print(request_dist_cm_list)
        for i in last_week_efforts_cm_list:
            if i not in cm_name_mapping.values():
                print(str(i) + " not in cm_list so to be deleted from Last_Week_Efforts_Spent")
                try:
                    query = "DELETE FROM " + tablename + " WHERE [" + list + "].[Configuration Manager] =  '" + str(i) + "' ;"
                    #print(query)
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                except:
                    print("\nError Deleting record from " + list + " list. Exiting...")
                    print(query)
                    return False

        for i in cm_name_mapping.values():
            if i not in last_week_efforts_cm_list:
                print(str(i) + " not in Last_Week_Efforts_Spent list so to be added")
                try:
                    cmid = 0
                    for j, cmname in cm_name_mapping.items():
                        if cmname == i:
                            cmid = j
                    query = "INSERT INTO " + tablename + " ( [Configuration Manager], [Amdocs - Efforts spent in Hrs], [CM - Efforts spent in Hrs], [Misc - Efforts spent in Hrs] ) VALUES ( '" + str(i) + "',  " + str(cm_list[cmid][11]) + ",  " + str(cm_list[cmid][12]) + ",  " + str(cm_list[cmid][13]) + " ) ;"
                    #print(query)
                    cur = conn.cursor()
                    cur.execute(query)
                    cur.commit()
                except:
                    print("\nError Inserting record from " + list + " list. Exiting...")
                    print(query)
                    return False


        for i in (cm_list):
            try:
                #query = "INSERT INTO " + tablename + " ( [Configuration Manager], [Amdocs - Efforts spent in Hrs], [CM - Efforts spent in Hrs], [Misc - Efforts spent in Hrs] ) VALUES ( '" + str(cm_name_mapping[i]) + "', '" + str(cm_list[i][11]) + "', '" + str(cm_list[i][12]) + "', '" + str(cm_list[i][13]) + "' );"
                query = "UPDATE " + tablename + " SET [Amdocs - Efforts spent in Hrs] = " + str(cm_list[i][11]) + ", [CM - Efforts spent in Hrs] = " + str(cm_list[i][12]) + ", [Misc - Efforts spent in Hrs] = " + str(cm_list[i][13]) + " WHERE [Configuration Manager] = '" + str(cm_name_mapping[i]) + "' ;"
                #print("Update query : " + query)
                cur = conn.cursor()
                cur.execute(query)
                cur.commit()
            except:
                print("\nError updating records into " + list + " list. Exiting...")
                print(query)
                return False

        print(list + " Records updated successfully")


    elif list == "Weekly_Requests_per_Product_Type":
        cur = conn.cursor()
        query = "SELECT [" + list + "].[Week Starting], [" + list + "].[Product Type], [" + list + "].[# of Requests Created] FROM [" + tablename + "]"

        queries = []
        wk_rq_prtp_list = []
        #print(query)
        cur.execute(query)


        while True:
            row = cur.fetchone()

            if row is None:
                break
            else:
                wk_rq_prtp_list.append([str(row.get("Week Starting")), str(row.get("Product Type")), int(float(row.get("# of Requests Created")))])

        cur.close()

        update_insert_query = ""
        for i in range(0, len(wk_rq_prtp_list)):
            if wk_rq_prtp_list[i][0] == weekstartdate:
                if wk_rq_prtp_list[i][1] in requests_per_product_type:
                    update_insert_query = "UPDATE " + list + " SET [# of Requests Created] = " + str(requests_per_product_type[wk_rq_prtp_list[i][1]]) + " WHERE [Week Starting] = '" + str(weekstartdate) + "' AND [Product Type] = '" + str(wk_rq_prtp_list[i][1]) + "';"
                    queries.append(update_insert_query)
        for i in requests_per_product_type:
            found=False
            for j in range(0, len(wk_rq_prtp_list)):
                if wk_rq_prtp_list[j][0] == weekstartdate and wk_rq_prtp_list[j][1] == i:
                    found=True
            if found == False:
                update_insert_query = "INSERT INTO " + list + " ( [Week Starting], [Product Type], [# of Requests Created] ) VALUES ( '" + str(weekstartdate) + "',  '" + str(i) + "',  " + str(requests_per_product_type[i]) + " ) ;"
                queries.append(update_insert_query)

        #print(str(wk_rq_prtp_list))
        #print(queries)

        i=0
        try:
            for i in range(1, len(queries)):
                cur = conn.cursor()
                cur.execute(queries[i])
                cur.commit()
        except:
            print("\nError updating records into " + list + " list. Exiting...")
            print(queries[i])
            conn.close()
            return False

    conn.close()
    return True


def gettotal(list, assignee):
    conn = pypyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + os.getcwd() + "\MSAccess\\" + list + ".accdb;\"")
    #r"Dbq=" + msaccess_files + "\MSAccess\\" + list + ".accdb;\"")

    cur = conn.cursor()
    WHERE = "( [" + list + "].[Request Status] = \'Delivered\' OR [" + list + "].[Request Status] = \'Closed\' OR [" + list + "].[Request Status] = \'Cancelled\' ) AND [" + list + "].[CM Assigned for the request] = " + str(assignee) + " ;"
    query = "SELECT COUNT(*) FROM [" + list + "] WHERE " + WHERE

    #print(query)
    cur.execute(query)

    row = cur.fetchall()

    if row is None:
        pass
    else:
        cm_list[assignee][9]=row[0][0]

    cur.close()
    conn.close()
    return True


def time_tracking(list, startdate, enddate, cm):
    conn = pypyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + os.getcwd() + "\MSAccess\\" + list + ".accdb;\"")
    #r"Dbq=" + msaccess_files + "\MSAccess\\" + list + ".accdb;\"")

    cur = conn.cursor()
    #WHERE = "( [" + list + "].[Task Start Date] >= #" + startdate + "# AND [" + list + "].[Request Status] = \'Closed\' OR [" + list + "].[Request Status] = \'Cancelled\' ) AND [" + list + "].[Assignee] = " + str(assignee) + " ;"
    WHERE = "( [" + list + "].[Task Start Date] >= #" + startdate + "# AND [" + list + "].[Task Start Date] <= #" + enddate + "# AND [" + list + "].[Amdocs ID] is Not Null AND [" + list + "].[Amdocs ID] <> 'na' AND [" + list + "].[Amdocs ID] <> 'NA' AND [" + list + "].[CM Portal ID] is Null AND [" + list + "].[Configuration Manager] = " + str(cm) + " ) ;"
    query = "SELECT SUM([Total Time spent in Hrs]) FROM [" + list + "] WHERE " + WHERE
    #query = "SELECT SUM([Total Time spent in Hrs]) FROM [" + list + "]"

    #print(query)
    cur.execute(query)

    row = cur.fetchall()

    if row is None:
        pass
    else:
        if row[0][0]:
            cm_list[cm][11] = int(row[0][0])
        else:
            cm_list[cm][11] = 0
        #print(str(cm) + " Amdocs efforts this week : " + str(cm_list[cm][11]) + " Hrs")

    cur.close()


    cur = conn.cursor()
    #WHERE = "( [" + list + "].[Task Start Date] >= #" + startdate + "# AND [" + list + "].[Request Status] = \'Closed\' OR [" + list + "].[Request Status] = \'Cancelled\' ) AND [" + list + "].[Assignee] = " + str(assignee) + " ;"
    #print(str(startdate) + " : " + str(enddate))
    WHERE = "( [" + list + "].[Modified] >= #" + startdate + "# AND [" + list + "].[Modified] <= #" + enddate + "# AND [" + list + "].[CM Portal ID] is Not Null AND [" + list + "].[Configuration Manager] = " + str(cm) + " ) ;"
    query = "SELECT SUM([Total Time spent in Hrs]) FROM [" + list + "] WHERE " + WHERE
    #query = "SELECT SUM([Total Time spent in Hrs]) FROM [" + list + "]"

    #print(query)
    cur.execute(query)

    row = cur.fetchall()

    if row is None:
        pass
    else:
        if row[0][0]:
            #print(str(int(row[0][0])))
            cm_list[cm][12] = int(row[0][0])
        else:
            cm_list[cm][12] = 0
        #print(str(cm) + " CM Requests efforts this week : " + str(cm_list[cm][12]) + " Hrs")

    cur.close()

    cur = conn.cursor()
    #WHERE = "( [" + list + "].[Task Start Date] >= #" + startdate + "# AND [" + list + "].[Request Status] = \'Closed\' OR [" + list + "].[Request Status] = \'Cancelled\' ) AND [" + list + "].[Assignee] = " + str(assignee) + " ;"
    WHERE = "( [" + list + "].[Task Start Date] >= #" + startdate + "# AND [" + list + "].[Task Start Date] <= #" + enddate + "# AND [" + list + "].[CM Portal ID] is Null AND [" + list + "].[Amdocs ID] is Null AND [" + list + "].[Configuration Manager] = " + str(cm) + " ) ;"
    query = "SELECT SUM([Total Time spent in Hrs]) FROM [" + list + "] WHERE " + WHERE
    #query = "SELECT SUM([Total Time spent in Hrs]) FROM [" + list + "]"

    #print(query)
    cur.execute(query)

    row = cur.fetchall()

    if row is None:
        pass
    else:
        if row[0][0]:
            cm_list[cm][13] = int(row[0][0])
        else:
            cm_list[cm][13] = 0
        #print(str(cm) + " Misc Requests efforts this week : " + str(cm_list[cm][13]) + " Hrs")

    cur.close()


    conn.close()
    return True


def get_priority_count():
    conn = pypyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
    #r"Dbq=" + msaccess_files + "\MSAccess\CM Ticket.accdb;\"")

    priority = {}

    cur = conn.cursor()
    query = "SELECT COUNT(*) FROM [CM Ticket] WHERE ([CM Ticket].[Priority] = 'High')"

    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            priority["High"] = row[0]

    query = "SELECT COUNT(*) FROM [CM Ticket] WHERE ([CM Ticket].[Priority] = 'Medium')"

    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            priority["Medium"] = row[0]

    query = "SELECT COUNT(*) FROM [CM Ticket] WHERE ([CM Ticket].[Priority] = 'Low')"

    cur.execute(query)

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            priority["Low"] = row[0]

    #print(priority)

    cur.close()
    conn.close()


    conn = pypyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + os.getcwd() + "\MSAccess\Priority_Count.accdb;\"")

    cur = conn.cursor()
    query = "UPDATE Priority_Count SET [Count] = '" + str(priority["High"]) + "' WHERE [Title] = 'High';"
    cur = conn.cursor()
    cur.execute(query)
    cur.commit()

    query = "UPDATE Priority_Count SET [Count] = '" + str(priority["Medium"]) + "' WHERE [Title] = 'Medium';"
    cur = conn.cursor()
    cur.execute(query)
    cur.commit()

    query = "UPDATE Priority_Count SET [Count] = '" + str(priority["Low"]) + "' WHERE [Title] = 'Low';"
    cur = conn.cursor()
    cur.execute(query)
    cur.commit()

    cur.close()
    conn.close()

    print("Priority updated successfully")

    return True


noww = datetime.datetime.now()
saturdayw = noww - timedelta(days=noww.weekday() + 2)
tomorrow = noww + timedelta(1)
tomorrowdatew = tomorrow.strftime("%m/%d/%Y")
saturdaydatew = saturdayw.strftime("%m/%d/%Y")
#print(str(tomorrowdatew))
#print(str(saturdaydatew))

get_priority_count()

req_prod_type = {}
wkstdt = '01-01-1900'

read_mapping_file()
read_cm_list()
for i in (cm_list):
    gettotal("CM Ticket", i)
getmonthyear()
#delete_list("SLA")
insert_List("SLA")
#delete_list("CM_Requests_Categorized_on_Requester")
insert_List("CM_Requests_Categorized_on_Requester")
getweekdates()
#delete_list("Weekly_Requests")
insert_List("Weekly_Requests")
delete_list("Monthly_Request_Distribution")
insert_List("Monthly_Request_Distribution")

update_List("Requests_Distribution", req_prod_type, wkstdt)
for i in (cm_list):
    #time_tracking("CM Report", lastweekstartdate, lastweekenddate, i)
    time_tracking("CM Report", saturdaydatew, tomorrowdatew, i)
update_List("Last_Week_Efforts_Spent", req_prod_type, wkstdt)
print("Weekly_Requests_per_Product_Type Records updated successfully")

#time_tracking("CM Report", "07/01/2017", "07/06/2017", "23")


'''
for i in (cm_list):
    print("Amdocs Total Delivered   : " + str(i) + " : " + str(cm_list[i][8]))
    print("Amdocs Total in progress : " + str(i) + " : " + str(cm_list[i][7]))
    print("CM Total Delivered       : " + str(i) + " : " + str(cm_list[i][9]))
    print("Misc Total Delivered     : " + str(i) + " : " + str(cm_list[i][10]))

'''

