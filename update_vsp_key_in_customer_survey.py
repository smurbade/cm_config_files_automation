import pypyodbc, os


def update_vsp_key():
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")

    cur = conn.cursor()
    WHERE = "[Customer Survey].[VSP Key] is Not Null"
    query = "SELECT [Customer Survey].[ID], [Customer Survey].[VSP Key] FROM [Customer Survey] WHERE " + WHERE + " ORDER BY [Customer Survey].[ID]"
    #print(query)
    cur.execute(query)

    query_list = []

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(row)
            if row[1] != "" and row[1] != "None" and row[1] is not None:
                query_list.append([row[0], row[1]])


    cur.close()

    updatecur = conn.cursor()

    for i in range(0, len(query_list)):
        query = "UPDATE [Customer Survey] SET "
        value = query_list[i][1]
        if isinstance(value, str):
            changed = 0
            if (value == "CLASSIC Prod/Pilot : A-KEYVSP-PWCPRODREMOTEKEK"):
                value = "Classic Elm & Ficus : Prod : A-KEYVSP-PWCPRODREMOTEKEK"
                changed = 1
            elif (value == "CLASSIC Lab : A-KEYTST-PWCDEMOREMOTEKEK"):
                value = "Classic Elm & Ficus : Test : A-KEYTST-PWCDEMOREMOTEKEK"
                changed = 1
            elif (value == "Vantiv Enterprise Direct Prod/Pilot : A-KEYVSP-VNTVPRODUCTIONRK"):
                value = "Vantive : Prod : A-KEYVSP-VNTVPRODUCTIONRKK"
                changed = 1
            elif (value == "CLASSIC First Data Pass Through or Direct Prod/Pilot : A-KEYVSP-FDVRK-01"):
                value = "First Data - TransArmor : Prod : A-KEYVSP-FDVRK-01"
                changed = 1
            elif (value == "Chase Paymentech Direct Prod/Pilot : A-KEYVSP-CPKIF04PRDRMTKEK"):
                value = "Chase Paymentech - SafeTech : Prod : A-KEYVSP-CPKIF04PRDRMTKEK"
                changed = 1

            if changed == 1 and query_list[i][0] not in [813, 905, 1141, 1169, 1175] :
                query = query + " [VSP Key] = '" + str(value) + "' WHERE [ID] = " + str(query_list[i][0]) + " ;"
                print(query)
                updatecur.execute(query)
                updatecur.commit()

    updatecur.close()

    ############################################################################################################

    cur = conn.cursor()
    WHERE = "[Customer Survey].[Device Type Designator] is Not Null"
    query = "SELECT [Customer Survey].[ID], [Customer Survey].[Device Type Designator] FROM [Customer Survey] WHERE " + WHERE + " ORDER BY [Customer Survey].[ID]"
    #print(query)
    cur.execute(query)

    query_list = []

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            #print(row)
            if row[1] != "" and row[1] != "None" and row[1] is not None:
                query_list.append([row[0], row[1]])


    cur.close()

    updatecur = conn.cursor()

    for i in range(0, len(query_list)):
        query = "UPDATE [Customer Survey] SET "
        value = query_list[i][1]
        if isinstance(value, str):
            changed = 0
            if (value == "Gateway Corporate Console ID equals 1040"):
                value = "P - Gateway Corporate Console ID equals 1040"
                changed = 1
            elif (value == "Gateway Corporate Console ID different than 1040"):
                value = "Non P - Gateway Corporate Console ID different than 1040"
                changed = 1

            if changed == 1 and query_list[i][0] not in [813, 905, 1141, 1169, 1175] :
                query = query + " [Device Type Designator] = '" + str(value) + "' WHERE [ID] = " + str(query_list[i][0]) + " ;"
                print(query)
                #updatecur.execute(query)
                #updatecur.commit()

    updatecur.close()





    conn.close()
    return True


update_vsp_key()

