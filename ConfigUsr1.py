import Git_operations
import Artifactory_Processing
import Extract_tgz
import copy_final_tgz
import read_conf
read_conf.read_conf_file()

from xlrd import open_workbook
import subprocess
import re
import os, os.path
import pypyodbc
from pygit2 import clone_repository
import logging
logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
logging.warning("\n\n")
from colorama import init, Fore, Back, Style
init() # initialize color output priting to console through colorama
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"


class config_usr1():
    def __init__(self, formn):
        self.dict = {}
        self.cust_dict = {}
        self.urls_Dict = []
        self.devmodel_mx_pattern = r"MX"
        self.devmodel_vx_pattern = r"VX"
        self.devmodel_both_pattern = r"Both"
        self.Config_User1_Dict = {}
        '''
        self.Config_User1_Dict['cbt'] = {}
        self.Config_User1_Dict['cot'] = {}
        self.Config_User1_Dict['ctt'] = {}
        self.Config_User1_Dict['dct'] = {}
        self.Config_User1_Dict['dhi'] = {}
        self.Config_User1_Dict['hdt'] = {}
        self.Config_User1_Dict['iab'] = {}
        self.Config_User1_Dict['mct'] = {}
        self.Config_User1_Dict['mid'] = {}
        self.Config_User1_Dict['pdt'] = {}
        self.Config_User1_Dict['rchi'] = {}
        self.Config_User1_Dict['cphi'] = {}
        self.Config_User1_Dict['rct'] = {}
        self.Config_User1_Dict['saf'] = {}
        self.Config_User1_Dict['sct'] = {}
        self.Config_User1_Dict['stb'] = {}
        self.Config_User1_Dict['tdt'] = {}
        self.Config_User1_Dict['tid'] = {}
        self.Config_User1_Dict['perm'] = {}
        self.Config_User1_Dict['reg'] = {}
        '''
        self.Config_User1_Git_Dict = {}
        self.Config_User1_Git_Dict['cbt'] = {}
        self.Config_User1_Git_Dict['ctt'] = {}
        self.Config_User1_Git_Dict['dct'] = {}
        self.Config_User1_Git_Dict['dhi'] = {}
        self.Config_User1_Git_Dict['hdt'] = {}
        self.Config_User1_Git_Dict['iab'] = {}
        self.Config_User1_Git_Dict['mct'] = {}
        self.Config_User1_Git_Dict['mid'] = {}
        self.Config_User1_Git_Dict['pdt'] = {}
        self.Config_User1_Git_Dict['rchi'] = {}
        self.Config_User1_Git_Dict['cphi'] = {}
        self.Config_User1_Git_Dict['rct'] = {}
        self.Config_User1_Git_Dict['saf'] = {}
        self.Config_User1_Git_Dict['sct'] = {}
        self.Config_User1_Git_Dict['stb'] = {}
        self.Config_User1_Git_Dict['tdt'] = {}
        self.Config_User1_Git_Dict['tid'] = {}
        self.Config_User1_Git_Dict['perm'] = {}
        self.Config_User1_Git_Dict['reg'] = {}
        #self.wb = open_workbook('cmitems.xlsx')
        #self.pointparam = open_workbook('pointparameters.xlsx')
        #self.cmitems_filename = "cmitems.xlsx"
        #self.workbook = open_workbook(self.cmitems_filename)
        #self.wb = self.workbook.sheet_by_name("cmitems")
        #self.pointparam = self.workbook.sheet_by_name("pointparameters")
        #self.urls = self.workbook.sheet_by_name("verifoneurls")
        self.pointparam_dict = {} # this dictionary has key value as parameter name from point parameters file
        self.formnumber = int(formn)
        self.repo_url = 'http://stash.verifone.com:7990/scm/anvc/config_files.git'
        self.repo_path = 'From_git'

    def kickstart_config_usr1_processing(self, cust_sur_revision):
        #self.formnumber = int(input('Enter Form Number : '))
        #print("\nForm " + str(self.formnumber) + " processing started")
        #logging.info("\nForm " + str(self.formnumber) + " processing started")
        #self.read_cm_form_file()
        ret = self.create_dict()
        if self.create_cust_dict(cust_sur_revision) == False:
            return 11

        #print(self.cust_dict)
        if ret == 5:
            return 5
        if not self.dict:  # if form number is not found in sharepoint site excel then this dictionary will be empty
            print(Fore.YELLOW + "\nForm " + str(self.formnumber) + " doesnt exist in Sharepoint. Please enter correct Form number")
            print(Style.RESET_ALL)
            logging.warning("\nForm " + str(self.formnumber) + " doesnt exist in Sharepoint. Please enter correct Form number")
            return 1
        else:

            succ = self.create_config_user1()
            if succ == 2:
                return 2
            elif succ == 3:
                return 3

            succ2 = self.create_vhqconfig()
            if succ2 == 2:
                return 2
            elif succ2 == 3:
                return 3

            if succ == 0 and succ2 == 0:
                #print("Form " + str(self.formnumber) + " processing completed")
                #logging.info("Form " + str(self.formnumber) + " processing completed")
                return 0
            elif succ == 0 and succ2 != 0:
                #print("Form " + str(self.formnumber) + " processing completed")
                #logging.info("Form " + str(self.formnumber) + " processing completed")
                return 9
            if succ != 0 and succ2 == 0:
                #print("Form " + str(self.formnumber) + " processing completed")
                #logging.info("Form " + str(self.formnumber) + " processing completed")
                return 10



    def create_dict(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\CM Ticket.accdb;\"")
            #r"Dbq=" + msaccess_files + "\MSAccess\CM Form.accdb;\"")

        try:
            # Get field names in CM Form
            crsr = conn.cursor()
            res = crsr.execute("SELECT * FROM [CM Ticket] WHERE 1=0")
            fieldlist = [tuple[0] for tuple in res.description]
            crsr.close()
            #print(fieldlist)
        except pypyodbc.Error:
            crsr.close()
            return 5

        queryfieldlist = ""
        #fieldexclude = ["id", "title", "product version", "contact name", "contact phone", "contact email address", "contact fax number", "who is boarding the devices?", "files to be uploaded for sca", 'delivery instructions', 'comments', 'assignee', 'internal workflow status', 'cm workflow', 'deliverydate', 'creation time', 'inform_cm_form_updates', 'update_graphs', 'update_internal_workflow_status', 'update_graphs_requests_status', 'update_graphs_priority', 'update_graph_product_type', 'update_graphs_weekly_requests', 'update_graphs_requests_distribution', 'create_cm_checklist', 'update_cm_checklist', 'did you input all requested parameters?', 'did you embed correct vcl file for either production or demo or ', 'what vcl is chosen?', 'did you sign all packages with verifone demo, point or customer ', 'what is the signature?', 'did you embed proper media files (mx915 or mx925 or customized m', 'which media chosen?', 'did you check to see if customer requested emv and provided prop', 'which emv package did you chose?', 'did you test and install your package on a device and got succes', 'install successful?', 'did you choose correct sca package for the specified vx device?', 'please answer which sca package was chosen?', 'do you confirm all above checklist information is correct?', 'request status', 'update_request_status_one_time', 'update_internal_workflow_status_from_request_status', 'cm workflow new', 'update_cmchecklist_field', 'update_graphs_requests_distribution_update', 'checklist_validation_in_cm_form', 'createdthismonth', 'update_cm_report', 'create_cm_report', 'assignee_validation', 'content type', 'app created by', 'app modified by', '“deliver to” person', 'customer email', 'attachments', 'workflow instance id', 'file type', 'modified', 'created', 'created by', 'modified by', 'url path', 'path', 'item type', 'encoded absolute url']
        fieldexclude = ['Is this a Project ?', 'CM Ticket - Daily Mail SLA Miss', 'Salesforce Project Number', 'Item Type', 'URL Path', 'CM Ticket - Update CM Report', 'Encoded Absolute URL', 'Did you input all requested parameters?', 'App Created By', 'Which media chosen?', 'Delivery instructions', 'Please confirm that Client approval has been received for this r', '“Deliver to” person', 'Content Type', 'What VCL is chosen?', 'Did you sign all packages with Verifone Demo, Point or customer ', 'Key File', 'Did you choose correct SCA package for the specified VX device?', 'Which EMV package did you chose?', 'What is the signature?', 'Created By', 'Please confirm that Client approval has been received for this r', 'CM Ticket - New Email', 'Ticket Created By', 'Modified', 'Please answer which SCA package was chosen?', 'Delivered_date', 'Cert to use', 'What VCL is chosen?', 'Total Time Spent on CM Request in Hrs', 'Attachments', 'Days_to_exclude_detmissing_onhold', 'Workflow Instance ID', 'Modified By', 'App Created By', 'File Type', 'Comments', 'Compliance Asset Id', 'Delivery Date', 'Item Type', 'Ticket Created on', 'VHQ Config File', 'Which media chosen?', 'Did you test and install your package on a device and got succes', 'Did you sign all packages with Verifone Demo, Point or customer ', 'URL Path', 'CM Assigned for the request', 'Who is Boarding the Devices?', 'CM Ticket - Update Email', 'Did you embed proper media files (MX915 or MX925 or Customized m', 'Click to view Customer Survey selected', 'CM Ticket - Create CM Report', 'Did you check to see if customer requested EMV and provided prop', 'CM Ticket - Update Graph - Product Type', 'Delivery instructions', 'Did you input all requested parameters?', 'SLA_Stop_Date', 'Select Bid Options', 'Approver on Client Side', 'Choose Customer Survey Revision', 'VCL Configuration Package', 'Path', 'CM Ticket - Update CM Report', '“Deliver to” person', 'Install successful?', 'Did you embed correct VCL file for either production or Demo or ', 'Do You confirm all above Checklist information is correct?', 'Content Type', 'App Modified By', 'CM Ticket - Change Folder Permissions on Deliver', 'Encoded Absolute URL', "ID", "Title", "Client name", "Priority", "Product version", "Who is Boarding the Devices?", "Contact Name", "Contact Phone", "Contact Email Address", "Contact Fax Number", "Key File", "VHQ Config File", "Cert to use", "Serial number", "Debit Key Part Number / KSN", "Files to be uploaded for SCA", "Delivery instructions", "Comments", "Assignee", "CM Workflow", "DeliveryDate", "Creation time", "Inform_CM_Form_Updates", "Update_Graphs", "Update_Internal_Workflow_Status", "Update_Graphs_Requests_Status", "Update_Graphs_Priority", "Update_Graph_Product_Type", "Update_Graphs_Weekly_Requests", "Update_Graphs_Requests_Distribution", "Create_CM_Checklist", "Update_CM_Checklist", "Did you input all requested parameters?", "Did you embed correct VCL file for either production or Demo or ", "What VCL is chosen?", "Did you sign all packages with Verifone Demo, Point or customer ", "What is the signature?", "Did you embed proper media files (MX915 or MX925 or Customized m", "Which media chosen?", "Did you check to see if customer requested EMV and provided prop", "Which EMV package did you chose?", "Did you test and install your package on a device and got succes", "Install successful?", "Did you choose correct SCA package for the specified VX device?", "Please answer which SCA package was chosen?", "Do You confirm all above Checklist information is correct?", "Update_Request_Status_one_time", "Update_Internal_workflow_status_from_Request_status", "CM Workflow new", "Update_CMChecklist_Field", "Update_Graphs_Requests_Distribution_update", "Checklist_Validation_in_CM_Form", "Update_CM_report", "Create_CM_report", "Assignee_Validation", "Content Type", "App Created By", "App Modified By", "“Deliver to” person", "Customer Email", "Attachments", "Workflow Instance ID", "File Type", "Modified", "Created", "Created By", "Modified By", "URL Path", "Path", "Item Type", "Encoded Absolute URL", "internal workflow status", "createdthismonth"]
        for i in range(0, len(fieldlist)):
            if fieldlist[i] not in fieldexclude:
                queryfieldlist = queryfieldlist + ", [CM Ticket].[" + fieldlist[i] + "]"
        queryfieldlist = queryfieldlist[2:]

        try:
            cur = conn.cursor()
            WHERE = "[CM Ticket].[Ticket Number] = " + str(self.formnumber)
            query = "SELECT " + queryfieldlist + " FROM [CM Ticket] WHERE " + WHERE
            #print(query)
            cur.execute(query)

            records_found = False
            while True:
                row = cur.fetchone()

                if row is None:
                    break
                for i in range(0, len(fieldlist)):
                    if not row.get(fieldlist[i]):
                        self.dict[fieldlist[i]] = ""
                    else:
                        self.dict[fieldlist[i]] = row.get(fieldlist[i])
                records_found = True

            #print(str(self.formnumber))
            #print("dict : " + str(self.dict))
            cur.close()
            conn.close()
        except pypyodbc.Error:
            #print("execute failed")
            cur.close()
            conn.close()
            return 5

    def create_cust_dict(self, id):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Customer Survey.accdb;\"")

        try:
            # Get field names in Customer Survey
            crsr = conn.cursor()
            res = crsr.execute("SELECT * FROM [Customer Survey] WHERE 1=0")
            fieldlist = [tuple[0] for tuple in res.description]
            crsr.close()
        except pypyodbc.Error:
            crsr.close()
            print("Error")
            return 5

        #print(id)
        queryfieldlist = ""
        fieldexclude = ["How would you like to create Customer Survey Revision", "Revision Number", "Revision", "Select Revision", "LANEs (one for each device)", "ID", "Title", "Content Type", "App Created By", "App Modified By", "Attachments", "Workflow Instance ID", "File Type", "Modified", "Created", "Created By", "Modified By", "URL Path", "Path", "Item Type", "Encoded Absolute URL"]
        for i in range(0, len(fieldlist)):
            if fieldlist[i] not in fieldexclude:
                queryfieldlist = queryfieldlist + ", [Customer Survey].[" + fieldlist[i] + "]"
        queryfieldlist = queryfieldlist[2:]

        cur = conn.cursor()
        WHERE = "[Customer Survey].[ID] = " + str(id)
        query = "SELECT " + queryfieldlist + " FROM [Customer Survey] WHERE " + WHERE
        #print(query)
        cur.execute(query)
        fieldlist = [tuple[0] for tuple in cur.description]


        while True:
            row = cur.fetchone()

            if row is None:
                cur.close()
                conn.close()
                return False
                #break
            else:
                for i in range(0, len(fieldlist)):
                    if not row.get(fieldlist[i]):
                        self.cust_dict[fieldlist[i]] = ""
                    else:
                        self.cust_dict[fieldlist[i]] = row.get(fieldlist[i])
                #print("cust_dict : " + str(self.cust_dict))
                cur.close()
                conn.close()
                return True



    def read_cm_form_file(self):
        #print("Inside read_cm_form_file function")
        #workbook = open_workbook('cmitems.xlsx')
        #worksheet = workbook.sheet_by_index(0)
        #print(worksheet.cell(0, 0).value)
        #for s in self.wb.sheets():
        values = []
        for row in range(self.wb.nrows):
            col_value = []
            if self.wb.cell(row,0).value == 'ID' or self.wb.cell(row,0).value == self.formnumber:  # select only first row and row which has form id from excel
                for col in range(self.wb.ncols):
                    value  = self.wb.cell(row,col).value
                    if self.wb.cell(row, 0).value == self.formnumber:  # if row is for the form number then fill dictionary values
                        self.dict[self.wb.cell(0, col).value.strip()] = value
                    try :
                        value = str(int(value))
                    except : pass
                    col_value.append(value.strip())
                values.append(col_value)

        #print(values)
        #print(self.dict)
        #logging.info(self.dict)

    def read_point_parameter_file(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Point Parameters.accdb;\"")
            #r"Dbq=" + msaccess_files + "\MSAccess\Point Parameters.accdb;\"")

        cur = conn.cursor()
        query = "SELECT [Point Parameters].[Parameter], [Point Parameters].[Section], [Point Parameters].[Device Model Supported], [Point Parameters].[Description], [Point Parameters].[CM Form Field], [Point Parameters].[DisableValue] FROM [Point Parameters] WHERE [Point Parameters].[CM Form Field] Is Not Null"
        #print(query)
        cur.execute(query)

        records_found = False

        while True:
            row = cur.fetchone()
            if row is None:
                break
            param = row.get("Parameter")
            if row.get("CM Form Field").strip() != "":
                self.pointparam_dict.setdefault(param.strip(), [])
                self.pointparam_dict[param].append(row.get("Section").strip())
                self.pointparam_dict[param].append(row.get("Device Model Supported").strip())
                self.pointparam_dict[param].append(row.get("Description"))
                self.pointparam_dict[param].append(row.get("CM Form Field").strip())
                self.pointparam_dict[param].append(row.get("DisableValue"))

            records_found = True

        #print("Point parameters")
        #print(self.pointparam_dict)
        #logging.info("Point parameters")
        #logging.info(self.pointparam_dict)
        cur.close()
        conn.close()


    def read_point_parameter_file_old(self):
        #print("Inside read_point_parameter_file function")
        #for s in self.pointparam.sheets():
        for row in range(self.pointparam.nrows):
            if row != 0:
                for col in range(self.pointparam.ncols):
                    param = self.pointparam.cell(row, 0).value
                    if col != 0:
                        self.pointparam_dict.setdefault(param.strip(), [])
                        self.pointparam_dict[param].append(self.pointparam.cell(row, col).value.strip())

        #print("Point parameters")
        #print(self.pointparam_dict)
        #logging.info("Point parameters")
        #logging.info(self.pointparam_dict)


    def read_urls(self):
        conn = pypyodbc.connect(
            r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
            r"Dbq=" + os.getcwd() + "\MSAccess\Verifone URLs Listing.accdb;\"")
            #r"Dbq=" + msaccess_files + "\MSAccess\Verifone URLs Listing.accdb;\"")

        # Get field names in CM Form
        crsr = conn.cursor()
        res = crsr.execute("SELECT * FROM [Verifone URLs Listing] WHERE 1=0")
        fieldlist = [tuple[0] for tuple in res.description]
        crsr.close()
        #print(fieldlist)
        #fieldlist = ("Hi")

        cur = conn.cursor()
        query = "SELECT * FROM [Verifone URLs Listing]"
        #print(query)
        cur.execute(query)

        records_found = False

        while True:
            row = cur.fetchone()

            if row is None:
                break
            url_list = []
            for i in range(0, len(fieldlist)):
                url_list.append(row.get(fieldlist[i]))
            self.urls_Dict.append(url_list)
            records_found = True

        #print("URLS #####################")
        #print(self.urls_Dict)
        #logging.info(self.urls_Dict)
        cur.close()
        conn.close()

    def read_urls_file(self):
        for row in range(self.urls.nrows):
            if row != 0:
                url_list = []
                for col in range(self.urls.ncols):
                    url_list.append(self.urls.cell(row, col).value)
                self.urls_Dict.append(url_list)

        #print("URLS #####################")
        #print(self.urls_Dict)
        #logging.info(self.urls_Dict)

    def set_config_user1_Parameters(self, param_from_form):  # this function fills the Config_User1_Dict dictionary with whatever value will go in config.usr1 file
        #print("Inside set_config_user1_parameters function")
        parametername2 = ''

        if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]) == 'Yes' or (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]) == 'Y': # check if parameter is set to yes in CM form
            # add IF statement for cases where device model in form is mentioned as Non MX/VX
            if (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):   # check if device model is mentioned as MX or both for the CM form parameter in parameters file
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('MX' in values or 'Both' in values):
                            #print("MX/both parameter name is", parametername2)
                            #logging.info("MX/both parameter name is " + parametername2)
                            #print("MX Yes : " + param_from_form)
                            #print("Value length : " + str(len(values)))
                            disablevalue = ""
                            if 'MX' in values:
                                if values.index("MX") == 1:
                                    disablevalue = values[4]
                                elif values.index("MX") == 6:
                                    disablevalue = values[9]
                            elif 'Both' in values:
                                if values.index("Both") == 1:
                                    disablevalue = values[4]
                                elif values.index("Both") == 6:
                                    disablevalue = values[9]
                            #print(disablevalue)
                            if disablevalue == "N":
                                #print("Disable N : " + param_from_form)
                                #print((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)]))
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'Y'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'Y'
                                #print(param_from_form + " : Y")

                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='Y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            elif disablevalue == "0":
                                #print("Disable 0 : " + param_from_form)
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '1'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '1'
                                #print(param_from_form + " : 1")

                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='1'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            break
            if (re.match(self.devmodel_vx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('VX' in values or 'Both' in values):
                            #print("VX/Both parameter name is", parametername2)
                            #logging.info("VX/Both parameter name is" + parametername2)
                            #print("VX Yes : " + param_from_form)
                            disablevalue = ""
                            if 'VX' in values:
                                if values.index("VX") == 1:
                                    disablevalue = values[4]
                                elif values.index("VX") == 6:
                                    disablevalue = values[9]
                            elif 'Both' in values:
                                if values.index("Both") == 1:
                                    disablevalue = values[4]
                                elif values.index("Both") == 6:
                                    disablevalue = values[9]
                            if disablevalue == "N":
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'Y'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'Y'
                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='Y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            elif disablevalue == "0":
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '1'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '1'
                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='1'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            break
        elif (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]) == 'No' or (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]) == 'N': # check if parameter is set to yes in CM form
            # add IF statement for cases where device model in form is mentioned as Non MX/VX
            if (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):   # check if device model is mentioned as MX or both for the CM form parameter in parameters file
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('MX' in values or 'Both' in values):
                            #print("MX/both parameter name is", parametername2)
                            #logging.info("MX/both parameter name is " + parametername2)
                            #print("MX No : " + param_from_form)
                            disablevalue = ""
                            if 'MX' in values:
                                if values.index("MX") == 1:
                                    disablevalue = values[4]
                                elif values.index("MX") == 6:
                                    disablevalue = values[9]
                            elif 'Both' in values:
                                if values.index("Both") == 1:
                                    disablevalue = values[4]
                                elif values.index("Both") == 6:
                                    disablevalue = values[9]
                            if disablevalue == "N":
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'N'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'N'
                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='N'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            elif disablevalue == "0":
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '0'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '0'
                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='0'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            break
            if (re.match(self.devmodel_vx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('VX' in values or 'Both' in values):
                            #print("VX/Both parameter name is", parametername2)
                            #logging.info("VX/Both parameter name is" + parametername2)
                            #print("VX No : " + param_from_form)
                            disablevalue = ""
                            if 'VX' in values:
                                if values.index("VX") == 1:
                                    disablevalue = values[4]
                                elif values.index("VX") == 6:
                                    disablevalue = values[9]
                            elif 'Both' in values:
                                if values.index("Both") == 1:
                                    disablevalue = values[4]
                                elif values.index("Both") == 6:
                                    disablevalue = values[9]
                            if disablevalue == "N":
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'N'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'N'
                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='N'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            elif disablevalue == "0":
                                if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '0'
                                else:
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                    self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = '0'
                                #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='0'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            break
        #print(param_from_form)
        #print((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]))
        #print("check value : " + param_from_form)
        #print("before conversion :" + str(list(self.dict.values())[list(self.dict.keys()).index(param_from_form)]))
        #logging.info("check value : " + param_from_form)
        #logging.info("before conversion :" + str(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]))

        if isinstance((list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]), str):
            config_user1_param_value_in_dict = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
        else:
            config_user1_param_value_in_dict = str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]))
        #print("after conversion :" + config_user1_param_value_in_dict)
        #logging.info("after conversion :" + config_user1_param_value_in_dict)

        #if (config_user1_param_value_in_dict != 'Yes') and (config_user1_param_value_in_dict != 'No') and (config_user1_param_value_in_dict != 'None') and (config_user1_param_value_in_dict != '0') and (config_user1_param_value_in_dict != '0.0') and (config_user1_param_value_in_dict != '0-Disable') and (config_user1_param_value_in_dict != ''):
        if (config_user1_param_value_in_dict == 'Non P - Gateway Corporate Console ID different than 1040') or (config_user1_param_value_in_dict == 'P - Gateway Corporate Console ID equals 1040'):
            for parametername2, values in self.pointparam_dict.items():
                if (param_from_form in values):
                    #logging.info("parameter name is" + parametername2)
                    if (config_user1_param_value_in_dict == 'Non P - Gateway Corporate Console ID different than 1040'):
                        if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                            self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = ""
                        else:
                            self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                            self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = ""
                        break
                    elif (config_user1_param_value_in_dict == 'P - Gateway Corporate Console ID equals 1040'):
                        if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                            self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = "P"
                        else:
                            self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                            self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = "P"
                        break

        elif (config_user1_param_value_in_dict != 'Yes') and (config_user1_param_value_in_dict != 'No') and (config_user1_param_value_in_dict != 'Y') and (config_user1_param_value_in_dict != 'N') and (config_user1_param_value_in_dict != 'None') and (config_user1_param_value_in_dict != '0-Disable') and (config_user1_param_value_in_dict != ''):
            if (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):   # check if device model is mentioned as MX or both for the CM form parameter in parameters file
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('MX' in values or 'Both' in values):
                            #print("MX/both parameter name is", parametername2)
                            #print("MX other : " + param_from_form)
                            #logging.info("MX/both parameter name is" + parametername2)
                            if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            else:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]=(list(self.dict.values())[list(self.dict.keys()).index(param_from_form)])"))
                            break
            if (re.match(self.devmodel_vx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('VX' in values or 'Both' in values):
                            #print("VX/Both parameter name is", parametername2)
                            #print("VX Other : " + param_from_form)
                            #logging.info("VX/Both parameter name is" + parametername2)
                            if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            else:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]=(list(self.dict.values())[list(self.dict.keys()).index(param_from_form)])"))
                            break


    def set_config_user1_Parameters_update(self, param_from_form):  # this function fills the Config_User1_Dict dictionary with whatever value will go in config.usr1 file
        #print("Inside set_config_user1_parameters function")
        parametername2 = ''
        if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]) == 'Yes': # check if parameter is set to yes in CM form
    # add IF statement for cases where device model in form is mentioned as Non MX/VX
            if (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):   # check if device model is mentioned as MX or both for the CM form parameter in parameters file
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('MX' in values or 'Both' in values):
                            #print("MX/both parameter name is", parametername2)
                            #logging.info("MX/both parameter name is" + parametername2)
                            if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'y'
                            else:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'y'
                            #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            break
            if (re.match(self.devmodel_vx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('VX' in values or 'Both' in values):
                            #print("VX/Both parameter name is", parametername2)
                            #logging.info("VX/Both parameter name is" + parametername2)
                            if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'y'
                            else:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = 'y'
                            #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)
                            break
            #print(param_from_form)
            #print((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]))

        #print("check value : " + param_from_form)
        #print("before conversion :" + str(list(self.dict.values())[list(self.dict.keys()).index(param_from_form)]))
        #logging.info("check value : " + param_from_form)
        #logging.info("before conversion :" + str(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]))

        if isinstance((list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]), str):
            config_user1_param_value_in_dict = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
        else:
            config_user1_param_value_in_dict = str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)]))
        #print("after conversion :" + config_user1_param_value_in_dict)
        #logging.info("after conversion :" + config_user1_param_value_in_dict)
        if (config_user1_param_value_in_dict != 'Yes') and (config_user1_param_value_in_dict != 'No') and (config_user1_param_value_in_dict != 'None') and (config_user1_param_value_in_dict != '0') and (config_user1_param_value_in_dict != '0.0') and (config_user1_param_value_in_dict != '0-Disable') and (config_user1_param_value_in_dict != ''):
            if (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):   # check if device model is mentioned as MX or both for the CM form parameter in parameters file
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('MX' in values or 'Both' in values):
                            #print("MX/both parameter name is", parametername2)
                            #logging.info("MX/both parameter name is" + parametername2)
                            if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            else:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]=(list(self.dict.values())[list(self.dict.keys()).index(param_from_form)])"))
                            break
            if (re.match(self.devmodel_vx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])) or re.match(self.devmodel_both_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                for parametername2, values in self.pointparam_dict.items():
                    if (param_from_form in values):
                        if ('VX' in values or 'Both' in values):
                            #print("VX/Both parameter name is", parametername2)
                            #logging.info("VX/Both parameter name is"+ parametername2)
                            if (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]) in self.Config_User1_Dict:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            else:
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])] = {}
                                self.Config_User1_Dict[(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0])][parametername2] = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(param_from_form)])
                            #exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername2)][0]),"'][parametername2]=(list(self.dict.values())[list(self.dict.keys()).index(param_from_form)])"))
                            break

    def set_config_user1_Parameters2(self, param_from_form, mx_parameter_name, vx_parameter_name):
        #print("Inside set_config_user1_parameters2 function")
        if re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])):
            #print(param_from_form," is yes for MX")
            #logging.info(param_from_form + " is yes for MX")
            parametername=mx_parameter_name
        if re.match(self.devmodel_vx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])):
            #print("Counter Tip is yes for VX")
            #logging.info("Counter Tip is yes for VX")
            parametername=vx_parameter_name
        exec("%s%s%s" % ("self.Config_User1_Dict['", (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(parametername)][0]),"'][parametername]='y'"))  # this command checks what is corresponding PDT, CDT etc) value for parameter 'sigcapture' and accordingly sets respective dictionary (PDT, CDT etc)

    def read_config_usr1_from_Artifactory(self):
        #print("Current Directory : ", os.getcwd())

        #Uncomment below lines if Artifactory has folder structure as "Client name\Product\Device\Environment"
        Client_name = (list(self.dict.values())[list(self.dict.keys()).index('Client Name')])
        Product = re.sub(r'[/]', r'', str((list(self.dict.values())[list(self.dict.keys()).index('Product Type')])))
        Device = (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])
        Environment = (list(self.dict.values())[list(self.dict.keys()).index('Environment')])
        #Artifactory_config_usr1_path = Client_name + '/' + Product + '/' + Device + '/' + Environment
        #Artifactory_config_usr1_path_win_Path = Client_name + '\\' + Product + '\\' + Device + '\\' + Environment
        #artifact = Artifactory_Processing.Articatory_Processing(Artifactory_config_usr1_path)

        devtype = ""
        if "MX" in Device or "mx" in Device:
            devtype = "MX"
        elif "VX" in Device or "vx" in Device:
            devtype = "VX"

        #Uncomment below lines when we need to pull from Artifactory
        #artifact = Artifactory_Processing.Articatory_Processing("")
        #Artifactory_file_local_path = "extract"

        #Uncomment below lines while using Extract_tgz function to extract config.usr1 from tgz
        #Extract_tgz.extract(Artifactory_file_local_path)
        #update_config_usr1_path = os.getcwd() + '\\extract\\Artifactory_Extract\\CONTROL'

        #command = "C:\\msys\\1.0\\bin\\bash.exe configusr1_extract.sh"
        #command = "\"" + read_conf.git_bash_path[0] + "\" configusr1_extract.sh"
        #command = "\"" + read_conf.git_bash_path[0] + "\" untgz.sh " + devtype
        command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\untgz.sh " + devtype
        #print("Extracting config.usr1 from existing package. Please wait ...")
        # print(command)
        FNULL = open(os.devnull, 'w')
        result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
        if result.returncode != 0:
            print(result.stderr)
            print(result)
            print("Execution of untgz.sh failed")
        else:
            # print(os.getcwd())
            update_config_usr1_path = os.getcwd() + '\\extract'
            print("Update Config user1 path : " + update_config_usr1_path)
            #logging.info("Update Config user1 path : " + update_config_usr1_path)
            if not os.path.isdir(update_config_usr1_path):
                print(Fore.YELLOW + "\'" + update_config_usr1_path + "\' path doesnt's exist on Bitbucket / Artifactory")
                print(Style.RESET_ALL)
                logging.warning("Config user1 path doesnt's exist in tgz")
            else:
                #print("Update Config user1 path exitsts")
                #logging.info("Update Config user1 path exitsts")
                #print(os.listdir(update_config_usr1_path))
                configusr1_from_Artifactory = update_config_usr1_path + "\\config.usr1"
                if os.path.isfile(configusr1_from_Artifactory):
                    #print("config.usr1 exists on Bitbucket @ " + update_config_usr1_path)
                    #logging.info("config.usr1 exists in Artifactory tgz @ " + update_config_usr1_path)
                    #print("Current directory : ",os.getcwd())
                    self.process_config_usr1_from_bitbucket_Artifactory(configusr1_from_Artifactory)

                    #command = "C:\\msys\\1.0\\bin\\bash.exe configusr1_insert.sh"
                    #command = "\"" + read_conf.git_bash_path[0] + "\" configusr1_insert.sh"
                    command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\configusr1_insert.sh"
                    print("Inserting updated config.usr1 into existing package. Please wait ...")
                    # print(command)
                    FNULL = open(os.devnull, 'w')
                    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                    if result.returncode != 0:
                        print(result.stderr)
                        print(result)
                        print("Execution of configusr1_insert.sh failed")
                    else:
                        copy_final_tgz.check_directory_and_copy_tgz("extract", Artifactory_config_usr1_path)
                        print("Package with updated config.usr1 is present @ " + os.getcwd() + "\\" + Artifactory_config_usr1_path_win_Path)

                else:
                    print("config.usr1 doesnt's exist in tgz @ " + update_config_usr1_path)
                    logging.warning("config.usr1 doesnt's exist in tgz @ " + update_config_usr1_path)


    def read_config_usr1_from_bitbucket(self):
        git_obj = Git_operations.git_processing()
        git_obj.pull_from_git()
        self.read_urls()
        Client_name = (list(self.dict.values())[list(self.dict.keys()).index('Client Name')])
        Product = re.sub(r'[/]', r'', str((list(self.dict.values())[list(self.dict.keys()).index('Product Type')])))
        Device = (list(self.dict.values())[list(self.dict.keys()).index('Device Model')])
        Environment = (list(self.dict.values())[list(self.dict.keys()).index('Environment')])
        update_config_usr1_path = self.repo_path + '\\' + Client_name + '\\' + Product + '\\' + Device + '\\' + Environment
        #print("Update Config user1 path : " + update_config_usr1_path)
        #logging.info("Update Config user1 path : " + update_config_usr1_path)
        if not os.path.isdir(update_config_usr1_path):
            print(Fore.YELLOW + "\'" + update_config_usr1_path.replace('From_git\\','') + "\' path doesnt's exist on Bitbucket")
            print(Style.RESET_ALL)
            logging.warning("Config user1 path doesnt's exist on Bitbucket")
        else:
            #print("Update Config user1 path exitsts")
            #logging.info("Update Config user1 path exitsts")
            #print(os.listdir(update_config_usr1_path))
            configusr1_from_bitbucket = update_config_usr1_path + "\\config.usr1"
            if os.path.isfile(configusr1_from_bitbucket):
                #print("config.usr1 exists on Bitbucket @ " + update_config_usr1_path)
                #logging.info("config.usr1 exists on Bitbucket @ " + update_config_usr1_path)
                print("Current directory : ",os.getcwd())
                self.process_config_usr1_from_bitbucket_Artifactory(configusr1_from_bitbucket)
                for pointparam_name, pointparam_values in self.pointparam_dict.items():
                        if (pointparam_values[3]):
                            if pointparam_values[3] in self.dict:
                                self.set_config_user1_Parameters_update(pointparam_values[3])
                            else:
                                #print(pointparam_values[3] + " field is not in CM form")
                                logging.warning(pointparam_values[3] + " field is not in CM form")
                configuserfile = open(configusr1_from_bitbucket, 'r')
                lines = configuserfile.readlines()
                configuserfile.close()
                #configuserfile = open(configusr1_from_bitbucket, 'a')
                #with open(configusr1_from_bitbucket) as f:
                    #lines = f.readlines()
                #print("\nContent from bitbucket config.usr1 file\n")
                #print(lines)
                #logging.info("\nContent from bitbucket config.usr1 file\n")
                #logging.info(lines)
                for y in lines:
                    if len(y.split('[')) == 2:
                        #print("Section parameter :" + y)
                        #logging.info("Section parameter :" + y)
                        sectionname = ((y.strip('[')).strip('\n')).strip(']').lower()
                        #print("section without [] : " + sectionname )
                        #logging.info("section without [] : " + sectionname )
                    elif len(y.split('=')) == 2:
                        value = str(y.split('=')[1].strip('\n'))
                        #print("its a parameter : " + y.split('=')[0] +   value)
                        #logging.info("its a parameter : " + y.split('=')[0] +   value)
                        self.Config_User1_Git_Dict[sectionname][str(y.split('=')[0])]=value
                #print("\nDictionary prepared from config.usr1 Git file")
                #print(self.Config_User1_Git_Dict)
                #print(self.Config_User1_Dict)
                #logging.info("\nDictionary prepared from config.usr1 Git file")
                #logging.info(self.Config_User1_Git_Dict)
                #logging.info(self.Config_User1_Dict)

                '''
                if field value in form is yes
                        if that field exists in git config.usr1 file then
                            if that field is set y then do nothing
                            if that field set to n then change it to y
                        else
                            add that field and set to y to git config.usr1 file

                else if field value in form is no
                        if that field exists in git config.usr1 file then
                            if that field is set n then do nothing
                            if that field set to y then change it to n
                else if field value in form is not yes nor no nor 0 nor 0.0 nor 0-disable
                        if that field exists in git config.usr1 file then
                            if that field is set same as form field value then do nothing
                            else change it to form field value
                        else
                            add that field and set to form field value to git config.usr1 file
                '''
                #print("###########################")
                #print("original Git config usr1")
                #print(self.Config_User1_Git_Dict)
                #logging.info("###########################")
                #logging.info("original Git config usr1")
                #logging.info(self.Config_User1_Git_Dict)


                for xx in self.cust_dict:
                    #print(xx)
                    if isinstance((list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)]), str):
                        form_param = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)])
                    elif isinstance((list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)]), float):
                        form_param = int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)])
                    else:
                        form_param = str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)]))
                    #print("after conversion :" + str(form_param))
                    #logging.info("after conversion :" + str(form_param))

                    if form_param == 'Yes' or form_param == 'Y' or form_param == 'y':
                        #print(xx)
                        #print(form_param)
                        for parametername2, values in self.pointparam_dict.items():
                            if (xx in values) and ((re.match(self.pointparam_dict[parametername2][1], (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))) or (self.pointparam_dict[parametername2][1] == 'Both')):
                                #print("Yes section : form value found in point parameter")
                                #logging.info("Yes section : form value found in point parameter")
                                #print(parametername2)
                                #print(str(values[0]))
                                for sectionname_git, values_git in self.Config_User1_Git_Dict.items():
                                    if sectionname_git == values[0]:
                                        #print(self.pointparam_dict[parametername2][1])
                                        #print(sectionname_git)
                                        #print(str(values_git))
                                        if (parametername2 in values_git):
                                            #print("Got it :" + sectionname_git + " : " + parametername2)
                                            #print(str(self.Config_User1_Git_Dict[sectionname_git]) + " : " + str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                            if str(self.Config_User1_Git_Dict[sectionname_git][parametername2]).lower() == 'n':
                                                self.Config_User1_Git_Dict[sectionname_git][parametername2] = 'y'
                                                #print("Got it again" + sectionname_git + " : " + parametername2 )
                                                #print(str(self.Config_User1_Git_Dict[sectionname_git]) + " : " + str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                        else:
                                            #print(parametername2 + " not in" + str(values_git))
                                            #print("Added " + parametername2 + " parameter to config.usr1 git dictionary since it was not there")
                                            self.Config_User1_Git_Dict[sectionname_git][parametername2] = 'y'
                                            #print(str(self.Config_User1_Git_Dict[sectionname_git]) + " : " + str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                    elif form_param == 'No' or form_param == 'None' or form_param == 'N' or form_param == 'n':
                        for parametername2, values in self.pointparam_dict.items():
                            if (xx in values) and ((re.match(self.pointparam_dict[parametername2][1], (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))) or (self.pointparam_dict[parametername2][1] == 'Both')):
                                #print("No section : form value found in point parameter")
                                #logging.info("No section : form value found in point parameter")
                                for sectionname_git, values_git in self.Config_User1_Git_Dict.items():
                                    if sectionname_git == values[0]:
                                        if (parametername2 in values_git):
                                            if str(self.Config_User1_Git_Dict[sectionname_git][parametername2]).lower() == 'y':
                                                self.Config_User1_Git_Dict[sectionname_git][parametername2] = 'n'
                    elif (form_param != '0') and (form_param != '0.0') and (form_param != '0-Disable') and (form_param != '') and (form_param != 'None'):
                        for parametername2, values in self.pointparam_dict.items():
                            if (xx in values) and ((re.match(self.pointparam_dict[parametername2][1], (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))) or (self.pointparam_dict[parametername2][1] == 'Both')):
                                #print("unique section : form value found in point parameter")
                                #logging.info("unique section : form value found in point parameter")
                                for sectionname_git, values_git in self.Config_User1_Git_Dict.items():
                                    if sectionname_git == values[0]:
                                        if (parametername2 in values_git):
                                            #print(str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                            #print(self.dict[xx])
                                            #logging.info(str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                            #logging.info(self.cust_dict[xx])
                                            if str(self.Config_User1_Git_Dict[sectionname_git][parametername2]) != form_param:
                                                self.Config_User1_Git_Dict[sectionname_git][parametername2] = form_param
                                                #print("setting " + parametername2 + " to : " + str(form_param) )
                                                #logging.info("setting " + parametername2 + " to : " + str(form_param) )
                                        else:
                                            #print("Added " + parametername2 + " parameter to config.usr1 git dictionary since it was not there")
                                            #logging.info("Added " + parametername2 + " parameter to config.usr1 git dictionary since it was not there")
                                            self.Config_User1_Git_Dict[sectionname_git][parametername2] = form_param

                # HDT URLs logic below
                prod_type = (list(self.dict.values())[list(self.dict.keys()).index('Product Type')])
                env = (list(self.dict.values())[list(self.dict.keys()).index('Environment')])
                enterprise_processor = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('SCA Point Enterprise Processor')])

                #print("prod type : ", prod_type)
                #print("Env : ", env)
                #print("Enterprise processor : ", enterprise_processor)

                if (prod_type == 'SCA Point Classic') and (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                    self.set_hdt_urls_update(prod_type, env)

                if (prod_type == 'SCA Point Enterprise') and (re.match(self.devmodel_mx_pattern,(list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                    self.set_hdt_urls_update(enterprise_processor, env)

                #print("Final Git config usr1")
                #print(self.Config_User1_Git_Dict)
                #logging.info("Final Git config usr1")
                #logging.info(self.Config_User1_Git_Dict)


                self.update_config_user1_file(configusr1_from_bitbucket)
            else:
                print("config.usr1 doesnt's exist on Bitbucket @ " + update_config_usr1_path)
                logging.warning("config.usr1 doesnt's exist on Bitbucket @ " + update_config_usr1_path)


    def process_config_usr1_from_bitbucket_Artifactory(self, configusr1_path):
        for pointparam_name, pointparam_values in self.pointparam_dict.items():
                if (pointparam_values[3]):
                    if pointparam_values[3] in self.cust_dict:
                        self.set_config_user1_Parameters_update(pointparam_values[3])
                    else:
                        #print(pointparam_values[3] + " field is not in CM form")
                        logging.warning(pointparam_values[3] + " field is not in CM form")
        configuserfile = open(configusr1_path, 'r')
        lines = configuserfile.readlines()
        configuserfile.close()
        #configuserfile = open(configusr1_path, 'a')
        #with open(configusr1_path) as f:
            #lines = f.readlines()
        #print("\nContent from bitbucket config.usr1 file\n")
        #print(lines)
        #logging.info("\nContent from bitbucket / Artifactory config.usr1 file\n")
        #logging.info(lines)
        #print(self.Config_User1_Git_Dict)
        for y in lines:
            if len(y.split('[')) == 2:
                #print("Section parameter :" + y)
                #logging.info("Section parameter :" + y)
                sectionname = ((y.strip('[')).strip('\n')).strip(']').lower()
                #print("section without [] : " + sectionname )
                #logging.info("section without [] : " + sectionname )
            elif len(y.split('=')) == 2:
                value = str(y.split('=')[1].strip('\n'))
                #print("its a parameter : " + y.split('=')[0] +   value)
                #logging.info("its a parameter : " + y.split('=')[0] +   value)
                self.Config_User1_Git_Dict[sectionname][str(y.split('=')[0])]=value
        #print("\nDictionary prepared from config.usr1 Git file")
        #print(self.Config_User1_Git_Dict)
        #print(self.Config_User1_Dict)
        #logging.info("\nDictionary prepared from config.usr1 Git/Artifactory file")
        #logging.info(self.Config_User1_Git_Dict)
        #logging.info(self.Config_User1_Dict)

        '''
        if field value in form is yes
                if that field exists in git config.usr1 file then
                    if that field is set y then do nothing
                    if that field set to n then change it to y
                else
                    add that field and set to y to git config.usr1 file

        else if field value in form is no
                if that field exists in git config.usr1 file then
                    if that field is set n then do nothing
                    if that field set to y then change it to n
        else if field value in form is not yes nor no nor 0 nor 0.0 nor 0-disable
                if that field exists in git config.usr1 file then
                    if that field is set same as form field value then do nothing
                    else change it to form field value
                else
                    add that field and set to form field value to git config.usr1 file
        '''
        #print("###########################")
        #print("original Git config usr1")
        #print(self.Config_User1_Git_Dict)
        #logging.info("###########################")
        #logging.info("original Git/Artifactory config usr1")
        #logging.info(self.Config_User1_Git_Dict)

        for xx in self.cust_dict:
            #print(xx)
            if isinstance((list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)]), str):
                form_param = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)])
            elif isinstance((list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)]), float):
                form_param = int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)])
            else:
                form_param = str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index(xx)]))
            #print("after conversion :" + str(form_param))
            #logging.info("after conversion :" + str(form_param))

            if form_param == 'Yes' or form_param == 'Y' or form_param == 'y':
                #print(xx)
                #print(form_param)
                for parametername2, values in self.pointparam_dict.items():
                    if (xx in values) and ((re.match(self.pointparam_dict[parametername2][1], (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))) or (self.pointparam_dict[parametername2][1] == 'Both')):
                        #print("Yes section : form value found in point parameter")
                        #logging.info("Yes section : form value found in point parameter")
                        #print(parametername2)
                        #print(str(values[0]))
                        for sectionname_git, values_git in self.Config_User1_Git_Dict.items():
                            if sectionname_git == values[0]:
                                #print(self.pointparam_dict[parametername2][1])
                                #print(sectionname_git)
                                #print(str(values_git))
                                if (parametername2 in values_git):
                                    #print("Got it :" + sectionname_git + " : " + parametername2)
                                    #print(str(self.Config_User1_Git_Dict[sectionname_git]) + " : " + str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                    if str(self.Config_User1_Git_Dict[sectionname_git][parametername2]).lower() == 'n':
                                        self.Config_User1_Git_Dict[sectionname_git][parametername2] = 'y'
                                        #print("Got it again" + sectionname_git + " : " + parametername2 )
                                        #print(str(self.Config_User1_Git_Dict[sectionname_git]) + " : " + str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                else:
                                    #print(parametername2 + " not in" + str(values_git))
                                    #print("Added " + parametername2 + " parameter to config.usr1 git dictionary since it was not there")
                                    self.Config_User1_Git_Dict[sectionname_git][parametername2] = 'y'
                                    #print(str(self.Config_User1_Git_Dict[sectionname_git]) + " : " + str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
            elif form_param == 'No' or form_param == 'None' or form_param == 'N' or form_param == 'n':
                for parametername2, values in self.pointparam_dict.items():
                    if (xx in values) and ((re.match(self.pointparam_dict[parametername2][1], (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))) or (self.pointparam_dict[parametername2][1] == 'Both')):
                        #print("No section : form value found in point parameter")
                        #logging.info("No section : form value found in point parameter")
                        for sectionname_git, values_git in self.Config_User1_Git_Dict.items():
                            if sectionname_git == values[0]:
                                if (parametername2 in values_git):
                                    if str(self.Config_User1_Git_Dict[sectionname_git][parametername2]).lower() == 'y':
                                        self.Config_User1_Git_Dict[sectionname_git][parametername2] = 'n'
            elif (form_param != '0') and (form_param != '0.0') and (form_param != '0-Disable') and (form_param != '') and (form_param != 'None'):
                for parametername2, values in self.pointparam_dict.items():
                    if (xx in values) and ((re.match(self.pointparam_dict[parametername2][1], (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))) or (self.pointparam_dict[parametername2][1] == 'Both')):
                        #print("unique section : form value found in point parameter")
                        #logging.info("unique section : form value found in point parameter")
                        for sectionname_git, values_git in self.Config_User1_Git_Dict.items():
                            if sectionname_git == values[0]:
                                if (parametername2 in values_git):
                                    #print(str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                    #print(self.dict[xx])
                                    #logging.info(str(self.Config_User1_Git_Dict[sectionname_git][parametername2]))
                                    #logging.info(self.cust_dict[xx])
                                    if str(self.Config_User1_Git_Dict[sectionname_git][parametername2]) != form_param:
                                        self.Config_User1_Git_Dict[sectionname_git][parametername2] = form_param
                                        #print("setting " + parametername2 + " to : " + str(form_param) )
                                        #logging.info("setting " + parametername2 + " to : " + str(form_param) )
                                else:
                                    #print("Added " + parametername2 + " parameter to config.usr1 git dictionary since it was not there")
                                    #logging.info("Added " + parametername2 + " parameter to config.usr1 git dictionary since it was not there")
                                    self.Config_User1_Git_Dict[sectionname_git][parametername2] = form_param

        # HDT URLs logic below
        prod_type = (list(self.dict.values())[list(self.dict.keys()).index('Product Type')])
        env = (list(self.dict.values())[list(self.dict.keys()).index('Environment')])
        enterprise_processor = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('SCA Point Enterprise Processor')])

        #print("prod type : ", prod_type)
        #print("Env : ", env)
        #print("Enterprise processor : ", enterprise_processor)

        if (prod_type == 'SCA Point Classic') and (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
            self.set_hdt_urls_update(prod_type, env)

        if (prod_type == 'SCA Point Enterprise') and (re.match(self.devmodel_mx_pattern,(list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
            self.set_hdt_urls_update(enterprise_processor, env)

        #print("Final Git config usr1")
        #print(self.Config_User1_Git_Dict)
        #logging.info("Final Git config usr1")
        #logging.info(self.Config_User1_Git_Dict)

        self.update_config_user1_file(configusr1_path)

        #print("Formnumber dir " + formnumber_dir)
        #configuserfilename = formnumber_dir + "\\config.usr1"
        #print(configuserfilename)
        #configuserfile = open(configuserfilename, 'w')

    def set_hdt_urls(self, prod_type_processor, env):
        if 'hdt' not in self.Config_User1_Dict:
            self.Config_User1_Dict['hdt'] = {}
        for lst in self.urls_Dict:
            if env == 'Prod':
                #print("Prod environment")
                if prod_type_processor in lst and env in lst:
                    #print(prod_type_processor," : ", lst[0])
                    #print(env," : ", lst[1])
                    if lst[3] != '':
                        #print("prim url : ", lst[3])
                        self.Config_User1_Dict['hdt']['primurl'] = lst[3]
                    if lst[4] != '':
                        #print("sec url : ", lst[3])
                        self.Config_User1_Dict['hdt']['scndurl'] = lst[4]
                    break
            if env != 'Prod':
                #print("Demo environment")
                if prod_type_processor in lst and 'Demo' in lst:
                    #print(prod_type_processor," : ", lst[0])
                    #print(env," : ", lst[1])
                    if lst[3] != '':
                        #print("prim url : ", lst[3])
                        self.Config_User1_Dict['hdt']['primurl'] = lst[3]
                    if lst[4] != '':
                        #print("sec url : ", lst[3])
                        self.Config_User1_Dict['hdt']['scndurl'] = lst[4]
                    break


    def set_hdt_urls_update(self, prod_type_processor, env):
        if 'hdt' not in self.Config_User1_Git_Dict:
            self.Config_User1_Git_Dict['hdt'] = {}
        for lst in self.urls_Dict:
            if env == 'Prod':
                #print("Prod environment")
                if prod_type_processor in lst and env in lst:
                    #print(prod_type_processor," : ", lst[0])
                    #print(env," : ", lst[1])
                    if lst[2] != '':
                        #print("prim url : ", lst[2])
                        self.Config_User1_Git_Dict['hdt']['primurl'] = lst[2]
                    if lst[3] != '':
                        #print("sec url : ", lst[2])
                        self.Config_User1_Git_Dict['hdt']['scndurl'] = lst[3]
                    break
            if env != 'Prod':
                #print("Demo environment")
                if prod_type_processor in lst and 'Demo' in lst:
                    #print(prod_type_processor," : ", lst[0])
                    #print(env," : ", lst[1])
                    if lst[2] != '':
                        #print("prim url : ", lst[2])
                        self.Config_User1_Git_Dict['hdt']['primurl'] = lst[2]
                    if lst[3] != '':
                        #print("sec url : ", lst[2])
                        self.Config_User1_Git_Dict['hdt']['scndurl'] = lst[3]
                    break

    def create_vhqconfig(self):
        #if ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Initial Configuration'):
        if ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Initial Configuration') or ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Update configuration'):
            if ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic') or (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Enterprise'\
                    or ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic Passthrough'):
                # print("Product type for this form is " + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]))
                #logging.info("Product type for this form is " + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]))

                try:
                    #logging.info(os.getcwd())
                    formnumber_dir = os.getcwd() + '\\' + str(self.formnumber)
                    if not os.path.isdir(formnumber_dir):
                        os.mkdir(formnumber_dir)

                    #logging.info("Formnumber dir " + formnumber_dir)
                    configuserfilename = formnumber_dir + "\\vhqconfig.ini"
                    #logging.info(configuserfilename)
                    configuserfile = open(configuserfilename, 'w')
                    #logging.info(configuserfile)

                    vhqconfigline = ""

                    line = "[VHQ]\n"
                    if (list(self.dict.values())[list(self.dict.keys()).index('Environment')]) != "Prod":
                        if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('VHQ Company ID - Lab')]) != "":
                            line = line + "CustomerId=" + (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('VHQ Company ID - Lab')]) + "\n"
                            vhqconfigline = vhqconfigline + "CustomerId:vhqconfig.ini:VHQ:" + (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('VHQ Company ID - Lab')]) + "\n"
                    else:
                        if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('VHQ Company ID - Prod')]) != "":
                            line = line + "CustomerId=" + (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('VHQ Company ID - Prod')]) + "\n"
                            vhqconfigline = vhqconfigline + "CustomerId:vhqconfig.ini:VHQ:" + (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('VHQ Company ID - Prod')]) + "\n"
                            #print(vhqconfigline)
                    if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Heartbeat Frequency')]) == "":
                        print("")
                        #line = line + "HeartbeatFreq=\n"
                        #vhqconfigline = vhqconfigline + "HeartbeatFreq:vhqconfig.ini:VHQ:\n"
                    else:
                        line = line + "HeartbeatFreq=" + str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Heartbeat Frequency')])) + "\n"
                        vhqconfigline = vhqconfigline + "HeartbeatFreq:vhqconfig.ini:VHQ:" + str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Heartbeat Frequency')])) + "\n"
                    if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Start')]) == "":
                        print("")
                        #line = line + "MaintenanceStart=\n"
                        #vhqconfigline = vhqconfigline + "MaintenanceStart:vhqconfig.ini:VHQ:\n"
                    else:
                        line = line + "MaintenanceStart=" + str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Start')])) + "\n"
                        vhqconfigline = vhqconfigline + "MaintenanceStart:vhqconfig.ini:VHQ:" + str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Start')])) + "\n"
                    if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance End')]) == "":
                        print("")
                    else:
                        line = line + "MaintenanceEnd=" + str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance End')])) + "\n"
                        vhqconfigline = vhqconfigline + "MaintenanceEnd:vhqconfig.ini:VHQ:" + str(int(list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance End')])) + "\n"
                    if (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) == "":
                        print("")
                    elif ( " " in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')])) or ( "*" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "Su" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "M" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "Tu" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "W" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "Th" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "F" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]) and "Sa" not in (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')])):
                        print("")
                    else:
                        maintdays = ((list(self.cust_dict.values())[list(self.cust_dict.keys()).index('Maintenance Days')]))
                        maintdays = maintdays.replace(",", "").replace("/", "").replace("\\", "").replace("\"", "").replace(" ", "").replace("'", "")
                        if "default" not in maintdays.lower() and "alreadyhave" not in maintdays.lower() and "na" not in maintdays.lower() and "dontchange" not in maintdays.lower():
                            line = line + "MaintenanceDays=" + maintdays + "\n"
                            vhqconfigline = vhqconfigline + "MaintenanceDays:vhqconfig.ini:VHQ:" + maintdays + "\n"
                    line = line + "KeysExchanged=False\n\n"
                    line = line + "[Server]\n"
                    line = line + "url root=https://vhq.verifone.com/MessagingServer/MessageHandler.asmx\n"
                    line = line + "ssl validate hostname=TRUE\n"

                    if vhqconfigline != "":
                        #print(vhqconfigline);
                        vhqconfigupdatepackagefilename = "extract\\vhqconfigupdatepackage"
                        vhqconfigupdatepackagefile = open(vhqconfigupdatepackagefilename, 'w')
                        vhqconfigupdatepackagefile.write(vhqconfigline)
                        #vhqconfigupdatepackagefile.write(line)
                        vhqconfigupdatepackagefile.close()

                    configuserfile.write(line)
                    configuserfile.close()
                    #print(Fore.GREEN + "vhqconfig.ini file created at : ", configuserfilename)
                    print(Style.RESET_ALL)
                    return 0
                except:
                    return 4

            else:
                print("\nProduct type for this form is \'" + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) + "\' so vhqconfig.ini creation is skipped")
                logging.warning("\nProduct type for this form is \'" + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) + "\' so vhqconfig.ini creation is skipped")
                return 2
        # comment below two lines if need to process for update configuration
        elif ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Update configuration'):
            return 3
        else:
            #print("Add logic here if update configuration processing to be done")
            return 3


    def create_config_user1(self):
        #print("Inside set_config_usr1_dictionary function")
        self.read_urls()
        #if ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Initial Configuration'):
        if ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Initial Configuration') or ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Update configuration'):
            self.read_point_parameter_file()
            if ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic') or (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Enterprise' \
                    or ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic Passthrough'):
                #print("Product type for this form is " + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]))
                #logging.info("Product type for this form is " + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]))
                #print(self.pointparam_dict)
                for pointparam_name, pointparam_values in self.pointparam_dict.items():
                    if (pointparam_values[3]):
                        if pointparam_values[3] in self.cust_dict:
                            #print(pointparam_values[3])
                            self.set_config_user1_Parameters(pointparam_values[3])
                        else:
                            #print(pointparam_values[3] + " field is not in CM form")
                            print("")
                            #logging.warning(pointparam_values[3] + " field is not in CM form")

                #print(self.Config_User1_Dict)
                # HDT URLs logic below
                prod_type = (list(self.dict.values())[list(self.dict.keys()).index('Product Type')])
                env = (list(self.dict.values())[list(self.dict.keys()).index('Environment')])
                enterprise_processor = (list(self.cust_dict.values())[list(self.cust_dict.keys()).index('SCA Point Enterprise Processor')])

                #print("prod type : ", prod_type)
                #print("Env : ", env)
                #print("Enterprise processor : ", enterprise_processor)

                if (prod_type == 'SCA Point Classic') and (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                    self.set_hdt_urls(prod_type, env)

                if (prod_type == 'SCA Point Enterprise') and (re.match(self.devmodel_mx_pattern, (list(self.dict.values())[list(self.dict.keys()).index('Device Model')]))):
                    self.set_hdt_urls(enterprise_processor, env)

                '''
                #print("HDT values - set only for SCA Point Classic")
                #print((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index('primurl')][3]))
                #print((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index('scndurl')][3]))
                #logging.info("HDT values - set only for SCA Point Classic")
                #logging.info((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index('primurl')][3]))
                #logging.info((list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index('scndurl')][3]))
                if ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic') and ((list(self.dict.values())[list(self.dict.keys()).index('Environment')]) == 'Prod'):
                    self.Config_User1_Dict['hdt']['primurl'] = (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index('primurl')][3])
                    self.Config_User1_Dict['hdt']['scndurl'] = (list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index('scndurl')][3])
                '''

                #print(self.Config_User1_Dict)
                succ = self.create_config_user1_file()
                if succ == 0:
                    return 0
                else:
                    return 4
            else:
                print("\nProduct type for this form is \'" + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) + "\' so config.usr1 creation is skipped")
                logging.warning("\nProduct type for this form is \'" + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) + "\' so config.usr1 creation is skipped")
                return 2
        '''
        #comment below two lines if need to process for update configuration
        elif ((list(self.dict.values())[list(self.dict.keys()).index('Type of request')]) == 'Update configuration'):
            return 3
        else:
            #print("\nFor now config.usr1 logic works only for \'Initial Configuration\' requests")
            #print("Update Configuration section")
            #logging.info("Update Configuration section")
            self.read_point_parameter_file()
            if ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic') or ((list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Classic Passthrough') or (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) == 'SCA Point Enterprise':
                #print("Product type for this form is " + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]))
                #logging.info("Product type for this form is " + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]))

                #Uncomment below line when pulling from Artifactory
                #print(Fore.CYAN + "Press a for Artifactory")
                #print(Fore.CYAN + "Press b for Bitbucket")
                #ch = input('Enter your choice here : ')
                #if ch == 'a':
                #    self.read_config_usr1_from_Artifactory()
                #elif ch == 'b':
                #    self.read_config_usr1_from_bitbucket()

                #Comment below line when pulling from Artifactory
                self.read_config_usr1_from_Artifactory()

            #print(self.Config_User1_Dict)
            #self.create_config_user1_file()
            else:
                print("\nProduct type for this form is \'" + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) + "\' so config.usr1 creation is skipped")
                logging.warning("\nProduct type for this form is \'" + (list(self.dict.values())[list(self.dict.keys()).index('Product Type')]) + "\' so config.usr1 creation is skipped")
                return 2
        '''

    def create_config_user1_file(self):
        try:
            #print("Inside create_config_usr1_file function")
            #logging.info("Inside create_config_usr1_file function")
            # for x in PDT.items():
            #    print(x[0],'=',x[1])

            # for tt in self.pointparam_dict:
            #    print(list(self.pointparam_dict.values())[list(self.pointparam_dict.keys()).index(tt)][3])


            # os.chdir(os.path.dirname(__file__))
            #print(os.getcwd())
            #logging.info(os.getcwd())

            formnumber_dir = os.getcwd() + '\\' + str(self.formnumber)

            if not os.path.isdir(formnumber_dir):
                os.mkdir(formnumber_dir)

            #print("Formnumber dir " + formnumber_dir)
            #logging.info("Formnumber dir " + formnumber_dir)
            configuserfilename = formnumber_dir + "\\config.usr1"
            #print(configuserfilename)
            #logging.info(configuserfilename)
            configuserfile = open(configuserfilename, 'w')
            # with open(formnumber_dir + "\\config.usr1", 'w') as configuserfile:
            #print()
            #print(configuserfile)
            #logging.info(configuserfile)
            #print(self.Config_User1_Dict)
            configusrdictline = ""
            for x in self.Config_User1_Dict:
                for y in self.Config_User1_Dict[x]:
                    valuee = self.Config_User1_Dict[x][y]
                    if isinstance(valuee, float):
                        valuee = int(valuee)
                    if isinstance(valuee, str):
                        valuee = str(valuee)
                    if valuee != "":
                        configusrdictline =  configusrdictline + str(y) + ":CONTROL/config.usr1:" + str(x) + ":" + str(valuee) + "\n"
                    else:
                        if y == "devtypesuffix":
                            configusrdictline = configusrdictline + str(y) + ":CONTROL/config.usr1:" + str(x) + ":" + str(valuee) + "\n"
                    #print(x, ':', y, ':', self.Config_User1_Dict[x][y])
            #print(configusrdictline)
            configusr1updatepackagefilename = "extract\\configusr1updatepackage"
            configusr1updatepackagefile = open(configusr1updatepackagefilename, 'w')
            configusr1updatepackagefile.write(configusrdictline)
            #print(configusrdictline)
            configusr1updatepackagefile.close()

            for x in self.Config_User1_Dict:
                if self.Config_User1_Dict[x]:
                    header = "\n[" + x + "]\n"
                    #print("[", x, "]")
                    #logging.info("[" + x + "]")
                    configuserfile.write(header)
                    for y in self.Config_User1_Dict[x]:
                        #print("before conversion : " + str((self.Config_User1_Dict[x][y])))
                        #logging.info("before conversion : " + str((self.Config_User1_Dict[x][y])))
                        if isinstance(self.Config_User1_Dict[x][y], str):
                            config_user1_param_value = self.Config_User1_Dict[x][y]
                        else:
                            config_user1_param_value = str(int(self.Config_User1_Dict[x][y]))
                        config_user1_line = y + "=" + config_user1_param_value + "\n"
                        #print(str(config_user1_line))
                        #logging.info(str(config_user1_line))
                        configuserfile.write(config_user1_line)

            #print(str(self.Config_User1_Dict))

            configuserfile.close()
            #print(Fore.GREEN + "Config usr1 file created at : ", configuserfilename)
            print(Style.RESET_ALL)
            return 0
        except:
            return 1

    def update_config_user1_file(self, config_usr1_file):
        #print("Inside update_config_usr1_file function")
        #print(config_usr1_file)
        #logging.info("Inside update_config_usr1_file function")
        #logging.info(config_usr1_file)
        configuserfile = open(config_usr1_file, 'w')
        #configuserfile = open("config.updated.usr1", 'w')
        # with open(formnumber_dir + "\\config.usr1", 'w') as configuserfile:
        #print(configuserfile)
        #logging.info(configuserfile)
        for x in sorted(self.Config_User1_Git_Dict) :
            if self.Config_User1_Git_Dict[x]:
                header = "[" + x + "]\n"
                #print("[", x, "]")
                #logging.info("[" + x + "]")
                configuserfile.write(header)
                for y in sorted(self.Config_User1_Git_Dict[x]):
                    #print("before conversion : " + str((self.Config_User1_Git_Dict[x][y])))
                    #logging.info("before conversion : " + str((self.Config_User1_Git_Dict[x][y])))
                    if isinstance(self.Config_User1_Git_Dict[x][y], str):
                        config_user1_param_value = self.Config_User1_Git_Dict[x][y]
                    else:
                        config_user1_param_value = str(int(self.Config_User1_Git_Dict[x][y]))
                    config_user1_line = y + "=" + config_user1_param_value + "\n"
                    #print(str(config_user1_line))
                    #logging.info(str(config_user1_line))
                    configuserfile.write(config_user1_line)

        configuserfile.close()
        #print(Fore.GREEN + "Config usr1 file created at : ", os.getcwd() + '\\' + config_usr1_file)
        print(Fore.GREEN + "Config usr1 file created at : " + config_usr1_file)
        #print(Fore.GREEN + "Config usr1 file created at : ", os.getcwd() + '\\' + config_usr1_file)
        print(Style.RESET_ALL)

######################################################

#configuser1 = config_usr1("420")
#configuser1.kickstart_config_usr1_processing()
