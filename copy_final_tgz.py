import os, glob
from shutil import copy
from colorama import init, Fore, Back, Style
init()


def check_directory_and_copy_tgz(source_path, dest_path):
    if not os.path.exists(dest_path):
        print(Fore.YELLOW + "\n\"" + os.getcwd() + "/" + dest_path + "\"" + " Directory does not exists so creating")
        os.makedirs(dest_path)
    for filename in glob.glob(os.path.join(source_path, '*.tgz')):
        copy(filename, dest_path)
    for filename in glob.glob(os.path.join(source_path, '*.TGZ')):
        copy(filename, dest_path)

#check_directory_and_copy_tgz("extract","C:\\Users\\T_SachinM2\\PycharmProjects\\First Project\\test")

