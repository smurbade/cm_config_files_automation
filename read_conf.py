from xlrd import open_workbook
import os
from os.path import expanduser

home = expanduser("~")
if not os.path.isdir(home + "\\SharePoint"):
    home = home + "\\OneDrive - Verifone" # Some of the CM's see sharepoint folder with this path

base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

prdtype_device_env_gui_file = base_path + "CMAutomation\\conf.xlsx"
#prdtype_device_env_gui_file = home + "\\SharePoint\\North America Config Manageme - Doc\\CMAutomation\\conf.xlsx"
workbook = open_workbook(prdtype_device_env_gui_file)
wb = workbook.sheet_by_name("Sheet1")

prodtype = []
device = []
environment = []
git_bash_path = []

def read_conf_file():
    for row in range(wb.nrows):
        col_value = []
        if wb.cell(row, 0).value == 'Product Type':
            for col in range(wb.ncols):
                value = wb.cell(row, col).value
                if value != "":
                    if value not in prodtype:
                        prodtype.append(value)
        if wb.cell(row, 0).value == 'Device':
            for col in range(wb.ncols):
                value = wb.cell(row, col).value
                if value != "":
                    if value not in device:
                        device.append(value)
        if wb.cell(row, 0).value == 'Environment':
            for col in range(wb.ncols):
                value = wb.cell(row, col).value
                if value != "":
                    if value not in environment:
                        environment.append(value)
        if wb.cell(row, 0).value == 'gitbashpath':
            value = wb.cell(row, 1).value
            if value != "":
                if value not in git_bash_path:
                    git_bash_path.append(value)
                break


#read_conf_file()
