import pypyodbc
import os
import subprocess
from pathlib import Path
import glob
from appJar import gui
pypyodbc.lowercase = False
from time import sleep
from colorama import init, Fore, Back, Style
init()
#import gui
from os.path import expanduser

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

msaccess_files = base_path + "CMAutomation"

import read_access_db


#---------------------------------------------------------------------------------------------------------------

def insert_cdt(id, prodtype, device, environment, cdtorder):             # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\cdt.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\cdt.accdb;\"")

    cur = conn.cursor()
    WHERE = "cdt.[Client Name] = " + str(id) + " AND lcase(cdt.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(cdt.[Device]) = \'" + device.lower() + "\' AND lcase(cdt.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM cdt WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    cdt_lines = 0
    cdt_row_params = 0
    cdt_row_ind = 0

    try:
        with open('extract\\cdt.txt') as cdtfile:
            for line in cdtfile:
                if (len(line) > 1):
                    cdt_lines+=1
                    param = line.split(' ', 1)[0]

                    if ( cdt_row_params > 1 and cdt_row_params != len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')) ):
                        #print(cdt_row_params)
                        #print(len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')))
                        print(Fore.RED + "cdt.txt is not in standard format. Below line has more values than other lines above it")
                        print(line)
                        return 1
                    cdt_row_params= len((line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(','))
                    #print(line)
                    #print(cdt_row_params)
        cdtfile.close()
    except FileNotFoundError:
        print("File Not Found - extract\\cdt.txt. Please make sure file is present")
        return 2
    except IOError:
        print("Error opening extract\\cdt.txt")
        return 3


    #print(cdt_lines)
    #print(cdt_row_params)

    cdt_dict = [[0 for x in range(2)] for y in range(cdt_lines)]
    #cdt_dict = [cdt_lines][cdt_row_params]
    row_ind = 0
    col_ind = 0

    with open('extract\\cdt.txt') as cdtfile:
        for line in cdtfile:
            if (len(line) > 1):
                param = line.split(' ', 1)[0]
                if line.split(' ', 1)[0] not in cdtorder[1]:
                    print(Fore.RED + line.split(' ', 1)[0] + " parameter doesn't exist in Sharepoint. Please check cdt.txt for correctness. Exiting ...")
                    return 4
                #print(cdtorder[0][cdtorder[1].index(line.split(' ', 1)[0])])
                cdt_dict[row_ind][0] = cdtorder[0][cdtorder[1].index(line.split(' ', 1)[0])]
                cdt_dict[row_ind][1] = (line.split(' ', 1)[1]).replace(' ', '').replace('\n', '').split(',')
                #print(param)
                row_ind+=1
    cdtfile.close()

    #print(cdt_dict)

    #for j in range(0, cdt_row_params):
        #for i in range(0, cdt_lines):
            #print(cdt_dict[i][0])
            #print(cdt_dict[i][1][j])

    if records_found:
        print(Fore.WHITE + "\nSharepoint has cdt records for Client/Prod Type/Device/Environment combination entered")
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if ( replace.lower() == 'y' ):
            query = "DELETE * FROM cdt WHERE " + WHERE
            try:
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 5
        else:
            print(Fore.YELLOW + "\nExiting")
            return 6

    for k in range(0, cdt_row_params):
        query = "INSERT INTO cdt ([Title], [Client Name], [Product Type], [Device], [Environment],"
        for i in range(0, cdt_lines):
            query = query + " [" + cdt_dict[i][0] + "],"

        query = query[:-1] + ") VALUES ( 'cdt', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', "
        for j in range(k, cdt_row_params):
            for i in range(0, cdt_lines):
                if "\"" not in cdt_dict[i][1][j]:
                    query = query + "'" + (cdt_dict[i][1][j]) + "',"
                else:
                    #query = query + (cdt_dict[i][1][j]).replace('"', '\'\\"', 1) + ","
                    if cdt_dict[i][1][j][0] == '\"' and cdt_dict[i][1][j][-1] == '\"':
                        #print("First and last character quote")
                        query = query + "\'\"" + (cdt_dict[i][1][j][1:-1]) + "\"\',"
                        #print(query)
                #print(cdt_dict[i][1][j])
            query = query[:-1] + ")" # remove last comma
            break
        #print(query)
        #query = "INSERT INTO cdt ([Title], [Client Name], [Product Type], [Device], [Environment], [Card Type], [Accum Index], [Host Group ID], [PAN Low], [PAN High], [Min PAN], [Max PAN], [AVS], [Issuer ID], [Issuer Number], [Tip Discount], [Floor Limit Amount], [CVV2], [Pment Index], [CDT Custom Int], [Abbreviation], [Card Label], [IPC Label], [IPC Processor], [Tracks Reqd], [Batch Auth Floor Limit], [Receipt Limit], [Sign Limit], [FPS Print Option], [Gift Amt Min], [Gift Amt Max], [Disabled], [Enable FPS], [Tax Exempt], [Visa Card], [Master Card], [AMEX Card], [Discover Card], [JCB Card], [Check LUHN], [Exp Date Reqd], [Manual Entry], [Allow Multi Curr], [Sign Line], [Card Present], [Pin Reqd], [Printer Required]) VALUES ( 'cdt', '111', 'SCA Point Classic', 'MX830', 'Lab', '5','7','8','\"62400000\"','\"62889999\"','16','16','1','\"\"','4','0','0','3','0','0','\"JB\"','\"ENROUTE\"','\"ENRT\"','\"\"','\"2\"','0','0','0','18','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','0','0')"
        #print(query)
        try:
            cur = conn.cursor()
            cur.execute(query);
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            return 7

    print(Fore.GREEN + "Records inserted successfully from cdt.txt to Sharepoint")

    conn.close()
    return 0


#---------------------------------------------------------------------------------------------------------------

def insert_configusr1(id, prodtype, device, environment):     # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\config.usr1.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\config.usr1.accdb;\"")

    section = ""
    queries = []

    try:
        with open('extract\\config.usr1') as configfile:
            for line in configfile:
                if (len(line) > 1):
                    line.replace(' ', '')
                    line.replace('\r\n', '')
                    line.replace('\n', '')
                    #print(line)
                    if line[0] == ("[") and line[-2] == ("]") :
                        section = line[1:-2]
                    else:
                        try:
                            paramvalue = line.split("=")
                            #print(paramvalue)
                            query = "INSERT INTO config_usr1 ([Title], [Client Name], [Product Type], [Device], [Environment], [Section], [Parameter], [Value]"
                            query = query + ") VALUES ( 'config.usr" + "', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '" + section + "', '" + paramvalue[0] + "', '" + paramvalue[1][:-1] + "' )"
                            #print(query)
                            queries.append(query)
                        except:
                            print(Fore.RED + "config.usr1 file is not in correct format. Please check below line in config.usr1 file...")
                            print(paramvalue)
                            configfile.close()
                            return 1
    except FileNotFoundError:
        print(Fore.RED + "\nextract\\config.usr1 Not Found. Exiting...")
        return 2
    except IOError:
        print(Fore.RED + "\nError opening extract\\config.usr1 file. Exiting...")
        return 3

    #print(queries)
    configfile.close()

    cur = conn.cursor()
    WHERE = "config_usr1.[Client Name] = " + str(id) + " AND lcase(config_usr1.[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(config_usr1.[Device]) = \'" + device.lower() + "\' AND lcase(config_usr1.[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM config_usr1 WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        print(Fore.WHITE + "\nSharepoint has config.usr1 records for Client / Prod Type / Device / Environment combination entered")
        replace = ""
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if (replace.lower() == 'y'):
            try:
                query = "DELETE * FROM config_usr1 WHERE " + WHERE
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 4
        else:
            print(Fore.YELLOW + "\nExiting...")
            return 5

    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i]);
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            return 6

    print(Fore.GREEN + "Records inserted successfully from config.usr1 to Sharepoint")
    conn.close()
    return 0


#---------------------------------------------------------------------------------------------------------------

def insert_msgxpi(id, prodtype, device, environment):     # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\MSGXPI.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\MSGXPI.accdb;\"")

    filename = "MSGXPI.txt"
    tablename = "MSGXPI"
    section = ""
    queries = []

    try:
        with open('extract\\' + filename) as msgxpifile:
            for line in msgxpifile:
                if (len(line) > 1):
                    line.replace(' ', '')
                    line.replace('\r\n', '')
                    line.replace('\n', '')
                    #print(line)
                    try:
                        paramvalue = line.split("=")
                        #print(paramvalue)
                        paramvalue[1] = paramvalue[1].replace("'", "''")
                        query = "INSERT INTO " + tablename + " ([Title], [Client Name], [Product Type], [Device], [Environment], [Parameter], [Value]"
                        query = query + ") VALUES ( '" + tablename + "', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '" + paramvalue[0] + "', '" + paramvalue[1][:-1] + "' )"
                        #print(query)
                        queries.append(query)
                    except:
                        print(Fore.RED + filename + " file is not in correct format. Please check below line in " + filename + " file...")
                        print(paramvalue)
                        msgxpifile.close()
                        return 1
    except FileNotFoundError:
        print(Fore.RED + "\nextract\\" + filename + " Not Found. Exiting...")
        return 2
    except IOError:
        print(Fore.RED + "\nError opening extract\\" + filename + " file. Exiting...")
        return 3

    #print(queries)
    msgxpifile.close()

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM " + tablename + " WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        print(Fore.WHITE + "\nSharepoint has " + tablename + " records for Client / Prod Type / Device / Environment combination entered")
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if ( replace.lower() == 'y' ):
            try:
                query = "DELETE * FROM " + tablename + " WHERE " + WHERE
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 4
        else:
            print(Fore.YELLOW + "\nExiting...")
            return 5

    print(str(len(queries)) + " records being inserted into Sharepoint. It may take 4-5 Minutes. Please be patient...")
    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i])
            cur.commit()
            if ( i > 0 and i % 200 == 0):
                print(str(i) + " Records inserted so far in Sharepoint and couting ...")
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(queries[i])
            return 6

    print(Fore.GREEN + str(len(queries)) + " Records inserted successfully from " + filename + " to Sharepoint")
    conn.close()
    return 0

#---------------------------------------------------------------------------------------------------------------

def insert_vhqconfig(id, prodtype, device, environment):     # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\VHQconfigini.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\VHQconfigini.accdb;\"")

    filename = "VHQconfig.ini"
    tablename = "VHQconfigini"
    section = ""
    queries = []

    try:
        with open('extract\\' + filename) as file:
            for line in file:
                if (len(line) > 1):
                    #line.replace(' ', '')
                    line.replace('\r\n', '')
                    line.replace('\n', '')
                    #print(line)
                    if line[0] == ("[") and line[-2] == ("]") :
                        section = line[1:-2]
                    else:
                        try:
                            paramvalue = line.split("=")
                            #print(paramvalue)
                            query = "INSERT INTO " + tablename + " ([Title], [Client Name], [Product Type], [Device], [Environment], [Section], [Parameter], [Value]"
                            query = query + ") VALUES ( '" + tablename + "', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '" + section + "', '" + paramvalue[0] + "', '" + paramvalue[1][:-1] + "' )"
                            #print(query)
                            queries.append(query)
                        except:
                            print(Fore.RED + filename + " file is not in correct format. Please check below line in " + filename + " file...")
                            print(paramvalue)
                            file.close()
                            return 1
    except FileNotFoundError:
        print(Fore.RED + "\nextract\\" + filename + " Not Found. Exiting...")
        return 2
    except IOError:
        print(Fore.RED + "\nError opening extract\\" + filename + " file. Exiting...")
        return 3

    #print(queries)
    file.close()

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM " + tablename + " WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        print(Fore.WHITE + "\nSharepoint has " + tablename + " records for Client / Prod Type / Device / Environment combination entered")
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if ( replace.lower() == 'y' ):
            try:
                query = "DELETE * FROM " + tablename + " WHERE " + WHERE
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 4
        else:
            print(Fore.YELLOW + "\nExiting...")
            return 5

    print(str(len(queries)) + " records being inserted into Sharepoint")
    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i])
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(queries[i])
            return 6

    print(Fore.GREEN + str(len(queries)) + " Records inserted successfully from " + filename + " to Sharepoint")
    conn.close()
    return 0

#---------------------------------------------------------------------------------------------------------------

def insert_CTLSConfigini(id, prodtype, device, environment):     # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CTLSConfigINI.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CTLSConfigINI.accdb;\"")

    filename = "CTLSConfig.INI"
    tablename = "CTLSConfigINI"
    section = ""
    queries = []

    try:
        with open('extract\\' + filename) as file:
            for line in file:
                if (len(line) > 1):
                    #line.replace(' ', '')
                    line.replace('\r\n', '')
                    line.replace('\n', '')
                    #print(line)
                    if line[0] == ("[") and line[-2] == ("]") :
                        section = line[1:-2]
                    else:
                        if section.lower() != "SupportedSchemes".lower():
                            try:
                                if section.find(".") != -1:
                                    sectionpart = section.split(".")
                                else:
                                    sectionpart = [section, ""]
                                paramvalue = line.split("=")
                                #print(paramvalue)
                                query = "INSERT INTO " + tablename + " ([Title], [Client Name], [Product Type], [Device], [Environment], [Scheme], [Parameter Type], [Parameter], [Value]"
                                query = query + ") VALUES ( '" + tablename + "', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '" + sectionpart[0] + "', '" + sectionpart[1] + "', '" + paramvalue[0].strip() + "', '" + paramvalue[1][:-1].strip() + "' )"
                                #print(query)
                                queries.append(query)
                            except:
                                print(Fore.RED + filename + " file is not in correct format. Please check below line in " + filename + " file...")
                                print(paramvalue)
                                file.close()
                                return 1
    except FileNotFoundError:
        print(Fore.RED + "\nextract\\" + filename + " Not Found. Exiting...")
        return 2
    except IOError:
        print(Fore.RED + "\nError opening extract\\" + filename + " file. Exiting...")
        return 3

    #print(queries)
    file.close()

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM " + tablename + " WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        print(Fore.WHITE + "\nSharepoint has " + tablename + " records for Client / Prod Type / Device / Environment combination entered")
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if ( replace.lower() == 'y' ):
            try:
                query = "DELETE * FROM " + tablename + " WHERE " + WHERE
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 4
        else:
            print(Fore.YELLOW + "\nExiting...")
            return 5

    print(str(len(queries)) + " records being inserted into Sharepoint")
    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i])
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(queries[i])
            return 6

    print(Fore.GREEN + str(len(queries)) + " Records inserted successfully from " + filename + " to Sharepoint")
    conn.close()
    return 0


#---------------------------------------------------------------------------------------------------------------

def insert_EMVTablesINI(id, prodtype, device, environment):     # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\EMVTablesINI.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\EMVTablesINI.accdb;\"")

    filename = "EMVTables.INI"
    tablename = "EMVTablesINI"
    section = ""
    queries = []

    try:
        with open('extract\\' + filename) as file:
            for line in file:
                if (len(line) > 1):
                    #line.replace(' ', '')
                    line.replace('\r\n', '')
                    line.replace('\n', '')
                    #print(line)
                    if line[0] == ("[") and line[-2] == ("]") :
                        section = line[1:-2]
                    else:
                        if section != "SupportedSchemes":
                            try:
                                if section.find(".") != -1:
                                    sectionpart = section.split(".")
                                else:
                                    sectionpart = [section, ""]
                                paramvalue = line.split("=")
                                #print(paramvalue)
                                query = "INSERT INTO " + tablename + " ([Title], [Client Name], [Product Type], [Device], [Environment], [Scheme], [Parameter Type], [Parameter], [Value]"
                                query = query + ") VALUES ( '" + tablename + "', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '" + sectionpart[0] + "', '" + sectionpart[1] + "', '" + paramvalue[0] + "', '" + paramvalue[1][:-1] + "' )"
                                #print(query)
                                queries.append(query)
                            except:
                                print(Fore.RED + filename + " file is not in correct format. Please check below line in " + filename + " file...")
                                print(paramvalue)
                                file.close()
                                return 1
    except FileNotFoundError:
        print(Fore.RED + "\nextract\\" + filename + " Not Found. Exiting...")
        return 2
    except IOError:
        print(Fore.RED + "\nError opening extract\\" + filename + " file. Exiting...")
        return 3

    #print(queries)
    file.close()

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM " + tablename + " WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        print(Fore.WHITE + "\nSharepoint has " + tablename + " records for Client / Prod Type / Device / Environment combination entered")
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if ( replace.lower() == 'y' ):
            try:
                query = "DELETE * FROM " + tablename + " WHERE " + WHERE
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 4
        else:
            print(Fore.YELLOW + "\nExiting...")
            return 5

    print(str(len(queries)) + " records being inserted into Sharepoint")
    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i])
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(queries[i])
            return 6

    print(Fore.GREEN + str(len(queries)) + " Records inserted successfully from " + filename + " to Sharepoint")
    conn.close()
    return 0


#---------------------------------------------------------------------------------------------------------------

def insert_CAPKData(id, prodtype, device, environment):     # Returns dictionary cdt_dict with values fetched from cdt list for Clientname, product type, device, Environment combination passed
    conn = pypyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + os.getcwd() + "\MSAccess\CAPKData.accdb;\"")
        #r"Dbq=" + msaccess_files + "\MSAccess\CAPKData.accdb;\"")

    filename = "CAPKData.INI"
    tablename = "CAPKData"
    section = ""
    queries = []

    try:
        with open('extract\\' + filename) as file:
            for line in file:
                if (len(line) > 1):
                    #line.replace(' ', '')
                    line.replace('\r\n', '')
                    line.replace('\n', '')
                    #print(line)
                    if line[0] == ("[") and line[-2] == ("]") :
                        section = line[1:-2]
                    else:
                        if section != "CAPKFiles":
                            try:
                                if section.find(".") != -1:
                                    sectionpart = section.split(".")
                                    #print(sectionpart[0][:-2] + "." + sectionpart[0][-2:])
                                else:
                                    sectionpart = [section, ""]
                                paramvalue = line.split("=")
                                #print(paramvalue)
                                query = "INSERT INTO " + tablename + " ([Title], [Client Name], [Product Type], [Device], [Environment], [Filename], [Parameter Type], [Parameter], [Value]"
                                query = query + ") VALUES ( '" + tablename + "', '" + str(id) + "', '" + str(prodtype) + "', '" + str(device) + "', '" + str(environment) + "', '" + sectionpart[0][:-2] + "." + sectionpart[0][-2:] + "', '" + sectionpart[1] + "', '" + paramvalue[0] + "', '" + paramvalue[1][:-1] + "' )"
                                #print(query)
                                queries.append(query)
                            except:
                                print(Fore.RED + filename + " file is not in correct format. Please check below line in " + filename + " file...")
                                print(paramvalue)
                                file.close()
                                return 1
    except FileNotFoundError:
        print(Fore.RED + "\nextract\\" + filename + " Not Found. Exiting...")
        return 2
    except IOError:
        print(Fore.RED + "\nError opening extract\\" + filename + " file. Exiting...")
        return 3

    #print(queries)
    file.close()

    cur = conn.cursor()
    WHERE = tablename + ".[Client Name] = " + str(id) + " AND lcase(" + tablename + ".[Product Type]) = \'" + prodtype.lower() + "\' AND lcase(" + tablename + ".[Device]) = \'" + device.lower() + "\' AND lcase(" + tablename + ".[Environment]) = \'" + environment.lower() + "\'"
    query = "SELECT * FROM " + tablename + " WHERE " + WHERE
    cur.execute(query);
    records_found = False

    while True:
        row = cur.fetchone()

        if row is None:
            break
        else:
            records_found = True
            break

    cur.close()

    if records_found:
        print(Fore.WHITE + "\nSharepoint has " + tablename + " records for Client / Prod Type / Device / Environment combination entered")
        replace = input('\nDo you want to replace Sharepoint records ? y / n : ')
        if ( replace.lower() == 'y' ):
            try:
                query = "DELETE * FROM " + tablename + " WHERE " + WHERE
                cur = conn.cursor()
                cur.execute(query);
                cur.commit()
                print(Fore.GREEN + "Existing records are deleted from Sharepoint")
            except:
                print(Fore.RED + "\nError deleting records from Sharepoint. Exiting...")
                return 4
        else:
            print(Fore.YELLOW + "\nExiting...")
            return 5

    print(str(len(queries)) + " records being inserted into Sharepoint")
    for i in range(0, len(queries)):
        try:
            cur = conn.cursor()
            cur.execute(queries[i])
            cur.commit()
        except:
            print(Fore.RED + "\nError inserting records into Sharepoint. Exiting...")
            print(queries[i])
            return 6

    print(Fore.GREEN + str(len(queries)) + " Records inserted successfully from " + filename + " to Sharepoint")
    conn.close()
    return 0


#insert_cdt(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab", read_access_db.cdtorder)
#insert_configusr1(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab")
#insert_msgxpi(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab")
#insert_vhqconfig(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab")
#insert_CTLSConfigini(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab")
#insert_EMVTablesINI(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab")
#insert_CAPKData(read_access_db.getid_addclientname("99Cents"), "SCA Point Classic", "MX830", "Lab")

