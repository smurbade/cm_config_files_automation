import os
import glob
import shutil
from artifactory import ArtifactoryPath

class Articatory_Processing():
    def __init__(self, Artipath):
        i=0
        f = open('conf\BitbucketArtifactoryDetails.txt')
        for line in iter(f):
            if i == 2:
                # setting variable to 'From_git' from file's 1st line
                base_url = line.strip('\n')
            if i == 3:
                # setting variable to git repository URL from file's 2nd line
                self.user = line.strip('\n')
            if i == 4:
                # setting variable to git repository URL from file's 2nd line
                self.pwd = line.strip('\n')
            i+=1
        f.close()
        base_url = "http://artifactory.verifone.com:8081/artifactory/"
        if not Artipath:
            self.Artifactory_url = base_url + "RMS_Release_CLW/GTO-Retail/MxPoint/SCA_2.19.27B5/MxPoint-SCA_2.19.27B5-20161227.zip"
        else:
            self.Artifactory_url = base_url + Artipath
        #self.user = "T_SachinM2"
        #self.pwd = "Sona@2485"
        #print(self.Artifactory_url)
        #print(self.user)
        #print(self.pwd)
        self.path = ArtifactoryPath(
            self.Artifactory_url,
            auth=(self.user, self.pwd))
        self.download_file()

    def find_recursive_files(self,ext):
        for p in self.path.glob("**/*."+str(ext)):
            print(p)

    def download_file(self):
        self.delete_old_tgz_zip_files()
        try:
            with self.path.open() as fd:
                with open("extract\From_Artifactory.tgz", "wb") as out:
                    out.write(fd.read())
        except Exception as e: print(str(e))
            #print("Error while downloading file from Artifactory")
            #print("Artifactory path from where file is being downloaded : ", self.Artifactory_url)

    def delete_old_tgz_zip_files(self):
        filelist = glob.glob("extract\*.tgz")
        for f in filelist:
            os.remove(f)
        filelist = glob.glob("extract\*.zip")
        for f in filelist:
            os.remove(f)
        if os.path.exists("extract\Artifactory_Extract"):
            shutil.rmtree("extract\Artifactory_Extract")

### Uncomment below two lines if you want to run this program separately
#artifact = Articatory_Processing("abc/txt")

#artifact.find_recursive_files("zip")

#Artifactory_file_local_path = "."
#Extract_tgz.extract(Artifactory_file_local_path)

