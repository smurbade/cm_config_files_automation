import os
import logging
logging.basicConfig(filename='logs\\CMAutomation.log',level=logging.DEBUG)
from xlrd import open_workbook
from colorama import init, Fore, Back, Style
init() # initialize color output priting to console through colorama

class read_cmitems():
    def __init__(self):
        self.clientname = 0
        self.dict = {}
        self.cdt_dict = {}           # values read from cmitems.xlsx
        self.cdt_dict_organized = {} # cdt values organized in cdt.txt format
        #self.cdt_dict['1'] = []
        self.cmitems_filename = "cmitems.xlsx"
        self.workbook = open_workbook(self.cmitems_filename)
        self.wb = self.workbook.sheet_by_name("cdt")
        self.cdt_row_index = 0

    def kickstart_cmitems_processing(self):
        #self.clientname = input('Enter Client name : ')
        #self.prodtype = input('Enter Product Type : ')
        #self.device = input('Enter Device Model : ')
        #self.environment = input('Enter Environment : ')
        self.clientname = "9 11 MEMORIAL"
        self.prodtype = "SCA Point Classic"
        self.device = "MX860"
        self.environment = "Lab"
        print(str(self.clientname) + " processing started.")
        logging.info(str(self.clientname) + " processing started for cdt")
        self.read_cm_form_file()
        #if not self.cdt_dict:  # if cdt rows are not present in sharepoint then this dictionary will be empty
        if (1 not in self.cdt_dict): # cdt_dict stores each row in cmitems.xlsx cdt tab as a list with key as 0, 1, 2 ....  key 0 represents header in cdt tab and next keys represent valid rows ... if key 1 is not present in cdt_dict means cmitems.xlsx doesn't contain valid rows
            print(Fore.YELLOW + "cdt rows for \"" + str(self.clientname) + "/" + self.prodtype + "/" + self.device + "/" + self.environment + "\" don't exist in Sharepoint. \nEither enter correct inputs or refresh and save cmitems.xlsx file and enter Form number")
            print(Style.RESET_ALL)
            #logging.warning(Fore.YELLOW + "cdt rows for " + str(self.clientname) + " " + self.prodtype + " " + self.device + " " + self.environment + " don't exist in Sharepoint. \nEither enter correct inputs or refresh and save cmitems.xlsx file and enter Form number")
            return 1
        else:
            self.mapping_sharepoint_fields_to_cdttxt()  # Map Sharepoint field names with cdt.txt field names
            self.set_cdt_values()  # build lists for individual sharepoint field and corresponding all values for client, product, device, environment combination
            self.print_cdttxt()
            print(str(self.clientname) + " processing completed.")
            return 0

    def read_cm_form_file(self):
        values = []
        self.cdt_row_index = 0
        for row in range(self.wb.nrows):
            col_value = []
            #if self.wb.cell(row,0).value == 'ID' or self.wb.cell(row,0).value == self.clientname:  # select only first row and row which has form id from excel
            if self.wb.cell(row,1).value == "Client Name":  # select only first row and row which has form id from excel
                self.cdt_dict[self.cdt_row_index] = []
                for col in range(self.wb.ncols-5):
                    value  = self.wb.cell(row,col+5).value
                    self.cdt_dict[self.cdt_row_index].insert(0,value)
                self.cdt_row_index+=1
            if self.wb.cell(row,1).value == self.clientname and self.wb.cell(row,2).value == self.prodtype and self.wb.cell(row,3).value == self.device and self.wb.cell(row,4).value == self.environment:  # select only first row and row which has form id from excel
                self.cdt_dict[self.cdt_row_index] = []
                for col in range(self.wb.ncols-5):
                    value  = self.wb.cell(row,col+5).value
                    self.cdt_dict[self.cdt_row_index].insert(0,value)
                self.cdt_row_index+=1
                '''
                    if self.wb.cell(row, 0).value == self.clientname:  # if row is for the form number then fill dictionary values
                        self.dict[self.wb.cell(0, col).value.strip()] = value
                    try :
                        value = str(int(value))
                    except : pass
                    col_value.append(value.strip())
                values.append(col_value)
                '''
        #print(values)
        #print("cdt Dictionary from cmitems.xlsx : ")
        #print(self.cdt_dict)

    def set_cdt_values(self):
        no_of_cdt_parameters = len(self.cdt_dict[0])
        #print(no_of_cdt_parameters)
        #print(self.cdt_row_index)
        '''
        for field in self.cdt_dict[0]:
            self.cdt_dict_organized[field] = []
        for field in self.cdt_dict[0]:
            for field_no in range(self.cdt_row_index):
                for ind in range(no_of_cdt_parameters):
                    value = self.cdt_dict[field_no][ind]
                    print(value)
                    self.cdt_dict_organized[field].insert(self.cdt_row_index,value)
        print(self.cdt_dict_organized)
        '''

        for field in self.cdt_dict[0]:
            self.cdt_dict_organized[field] = []
            for field_no in range(0, self.cdt_row_index):
                for ind in range(0, no_of_cdt_parameters):
                    #print("field = " + str(field) + "       cdt_dict[0][ind] : " + str(self.cdt_dict[0][ind]))
                    if field_no > 0 and field == self.cdt_dict[0][ind]:
                        #print("field = " + str(field) + "       cdt_dict[0][ind] : " + str(self.cdt_dict[0][ind]))
                        value = self.cdt_dict[field_no][ind]
                        #print(value)
                        self.cdt_dict_organized[field].insert(field_no,value)
        #print("\ncdt Dictionary organised : ")
        #print(self.cdt_dict_organized)

    def mapping_sharepoint_fields_to_cdttxt(self):
        self.cdt_mapping = {}
        self.cdt_mapping["Card Label"] = "CardLabel"
        self.cdt_mapping["Abbreviation"] = "CardAbbrev"
        self.cdt_mapping["IPC Label"] = "IPCLabel"
        self.cdt_mapping["IPC Processor"] = "IPCProcessor"
        self.cdt_mapping["PAN Low"] = "PANLo"
        self.cdt_mapping["PAN High"] = "PANHi"
        self.cdt_mapping["Min PAN"] = "MinPANDigit"
        self.cdt_mapping["Max PAN"] = "MaxPANDigit"
        self.cdt_mapping["Issuer Number"] = "IssuerNum"
        self.cdt_mapping["Issuer ID"] = "IssuerID"
        self.cdt_mapping["Host Group ID"] = "HostGroupID"
        self.cdt_mapping["Card Type"] = "Type"
        self.cdt_mapping["Tracks Reqd"] = "TracksRequired"
        self.cdt_mapping["Accum Index"] = "AccumIndex"
        self.cdt_mapping["AVS"] = "AVS"
        self.cdt_mapping["CVV2"] = "CVV_II"
        self.cdt_mapping["FPS Print Option"] = "FPSPrintOption"
        self.cdt_mapping["Receipt Limit"] = "ReceiptLimitAmount"
        self.cdt_mapping["Sign Limit"] = "SignLimitAmount"
        self.cdt_mapping["Batch Auth Floor Limit"] = "BatchAuthFloorLimitAmt"
        self.cdt_mapping["Gift Amt Min"] = "GiftAmtMin"
        self.cdt_mapping["Gift Amt Max"] = "GiftAmtMax"
        self.cdt_mapping["Pment Index"] = "PaymentIndex"
        self.cdt_mapping["Disabled"] = "DisableCardRange"
        self.cdt_mapping["Enable FPS"] = "EnableFPS"
        self.cdt_mapping["Tax Exempt"] = "TaxExempt"
        self.cdt_mapping["Check LUHN"] = "ChkLuhn"
        self.cdt_mapping["Exp Date Reqd"] = "ExpDtReqd"
        self.cdt_mapping["Manual Entry"] = "ManEntry"
        self.cdt_mapping["Allow Multi Curr"] = "AllowMultiCurr"
        self.cdt_mapping["Sign Line"] = "SignLine"
        self.cdt_mapping["Card Present"] = "CardPresent"
        self.cdt_mapping["Pin Reqd"] = "PinpadRequired"
        self.cdt_mapping["Printer Required"] = "PrntrRequired"
        self.cdt_mapping["Visa Card"] = "VisaCard"
        self.cdt_mapping["Master Card"] = "MasterCard"
        self.cdt_mapping["AMEX Card"] = "AmexCard"
        self.cdt_mapping["Discover Card"] = "DiscCard"
        self.cdt_mapping["JCB Card"] = "JCBCard"
        #print(self.cdt_mapping)

    def print_cdttxt(self):
        cdtfile = open("extract//cdt.txt", 'w')
        for key_cdt_mapping in self.cdt_mapping:
            for key_cdt_dict_organized in self.cdt_dict_organized:
                if key_cdt_mapping == key_cdt_dict_organized:
                    cdtline = str(self.cdt_mapping[key_cdt_mapping]) + " "
                    #print(cdtline)
                    for ind in range(self.cdt_row_index-1):
                        if isinstance(self.cdt_dict_organized[key_cdt_dict_organized][ind], float):
                            cdtline = cdtline + str(int(self.cdt_dict_organized[key_cdt_dict_organized][ind])) + ", "
                        else:
                            cdtline = cdtline + str(self.cdt_dict_organized[key_cdt_dict_organized][ind]) + ", "
                        #print(cdtline)
                    cdtline = cdtline[:-2]
                    #print(cdtline)
                    cdtfile.write(cdtline + "\n")

#Uncomment below lines if you need to run this program separately
#cmitems = read_cmitems()
#cmitems.kickstart_cmitems_processing()

