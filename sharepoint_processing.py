import os
from os.path import expanduser
from sharepoint import SharePointSite, basic_auth_opener
import ssl

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

server_url = "https://verifone365.sharepoint.com/"
site_url = server_url + "sites/NAConfigMgmt"

opener = basic_auth_opener(server_url, "VERIFONE/T_SachinM2", "Sach@04inm")

site = SharePointSite(site_url, opener)

for sp_list in site.users:
    print(sp_list.id, sp_list.meta['Title'])
