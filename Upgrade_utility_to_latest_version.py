import glob
import os, zipfile
import shutil
from os.path import expanduser

import subprocess

import read_conf

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"

utility_path = ""

def upgrade():
    utility_path = base_path + "CMAutomation"
    if not os.path.isdir(base_path + "CMAutomation"):
        print(utility_path + " doesn't exist")
        return 1

    names = [os.path.basename(x) for x in glob.glob(utility_path + "\\*CMAutomation.zip")]
    names.sort()
    #print(names)
    #print(names[-1])

    shutil.copyfile(utility_path + "\\" + names[-1], os.getcwd() + "\\" + names[-1])

    zip_extract_foldername = os.path.splitext(names[-1])[0]
    #print(zip_extract_foldername)

    zip_ref = zipfile.ZipFile(os.getcwd() + "\\" + names[-1])  # create zipfile object
    zip_ref.extractall(os.getcwd())  # extract file to dir
    zip_ref.close()  # close file
    os.remove(os.getcwd() + "\\" + names[-1])  # delete zipped file
    #print(os.getcwd())

    """Copy a directory structure overwriting existing files"""
    read_conf.read_conf_file()
    command = "\"" + read_conf.git_bash_path[0] + "\" Uti_Up.sh " + zip_extract_foldername
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
    if result.returncode != 0:
        print(result.stderr)
        print(result)
        return 2

    versionno = zip_extract_foldername[1:5]

    versionfilename = "Version.txt"
    versionfilenamefile = open(versionfilename, 'w')
    versionfilenamefile.write(versionno)
    versionfilenamefile.close()

upgrade()

