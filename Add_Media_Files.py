import read_cmitems
import copy_final_tgz
import read_conf

import os
import subprocess
from colorama import init, Fore, Back, Style
import glob
import common_functions

init()
read_conf.read_conf_file()

def insert_sample_media():
    cmitems = read_cmitems.read_cmitems()
    #formexists = cmitems.kickstart_cmitems_processing()
    if (cmitems.kickstart_cmitems_processing()):
        cli = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Client name")]))
        prdt = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Product Type")]))
        devicemodel = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Device Model")]))
        envrmt = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Environment")]))
        if (devicemodel.find("MX915")  != -1) or (devicemodel.find("MX925")  != -1):
            device = 0
            if (devicemodel.find("MX915")  != -1):
                device = 915
            if (devicemodel.find("MX925")  != -1):
                device = 925

            # uncomment below 3 lines if naming convention need to be enabled
            #tgzvalidate = common_functions.validate_tgz("extract", cli, prdt, devicemodel, envrmt)
            #if tgzvalidate != 0:
            #    return tgzvalidate

            #command = "C:\\msys\\1.0\\bin\\bash.exe media.sh " + str(device)
            #command = "\"" + read_conf.git_bash_path[0] + "\" media.sh " + str(device)
            command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\media.sh " + str(device)
            print("Inserting MX" + str(device) + " Device template media file into tgz file. Please wait ...")
            #print(command)
            FNULL = open(os.devnull, 'w')
            result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
            if result.returncode != 0:
                print(result.stderr)
                print(result)
                print(Fore.RED + "\nExecution of media.sh failed")
                print(Style.RESET_ALL)
                return 1
            else:
                #final_tgz_path = cmitems.get_tgz_path()
                #copy_final_tgz.check_directory_and_copy_tgz("extract", final_tgz_path)
                #print(Fore.GREEN + "\nProcess completed. Please check updated tgz file @ " + os.getcwd() + "\\" + cmitems.client + "\\" + cmitems.prodtype + "\\" + cmitems.device + "\\" + cmitems.environment)
                print(Fore.GREEN + "\nProcess completed. Please check updated tgz file @ extract folder")
                #print("Process completed. Please check updated file @ " + os.getcwd() + "\\extract folder")
                return 0
        else:
            print(Fore.RED + "\nApplicable for MX915/MX925 only")
            print(Style.RESET_ALL)
            return 2
    else:
        return 3

def insert_sample_media_gui(formno):
    cmitems = read_cmitems.read_cmitems()
    # formexists = cmitems.kickstart_cmitems_processing()
    if (cmitems.kickstart_cmitems_processing_gui(formno)):
        cli = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Client name")]))
        prdt = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Product Type")]))
        devicemodel = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Device Model")]))
        envrmt = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Environment")]))
        if (devicemodel.find("MX915") != -1) or (devicemodel.find("MX925") != -1):
            device = 0
            if (devicemodel.find("MX915") != -1):
                device = 915
            if (devicemodel.find("MX925") != -1):
                device = 925

            # uncomment below 3 lines if naming convention need to be enabled
            #tgzvalidate = common_functions.validate_tgz("extract", cli, prdt, devicemodel, envrmt)
            #if tgzvalidate != 0:
            #    return tgzvalidate

            #command = "C:\\msys\\1.0\\bin\\bash.exe media.sh " + str(device)
            #command = "\"" + read_conf.git_bash_path[0] + "\" media.sh " + str(device)
            command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\media.sh " + str(device)
            print("Inserting MX" + str(device) + " Device template media file into tgz file. Please wait ...")
            # print(command)
            FNULL = open(os.devnull, 'w')
            result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
            if result.returncode != 0:
                print(result.stderr)
                print(result)
                print(Fore.RED + "\nExecution of media.sh failed")
                print(Style.RESET_ALL)
                return 1
            else:
                # final_tgz_path = cmitems.get_tgz_path()
                # copy_final_tgz.check_directory_and_copy_tgz("extract", final_tgz_path)
                # print(Fore.GREEN + "\nProcess completed. Please check updated tgz file @ " + os.getcwd() + "\\" + cmitems.client + "\\" + cmitems.prodtype + "\\" + cmitems.device + "\\" + cmitems.environment)
                print(Fore.GREEN + "\nProcess completed. Please check updated tgz file @ extract folder")
                print(Style.RESET_ALL)
                # print("Process completed. Please check updated file @ " + os.getcwd() + "\\extract folder")
                return 0
        else:
            print(Fore.RED + "\nApplicable for MX915/MX925 only")
            print(Style.RESET_ALL)
            return 2
    else:
        return 3

#Uncomment below line if this program need to be run separately
#insert_sample_media()

