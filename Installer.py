import os
import zipfile

path_to_create = "c:\CMAutomation"
if not os.path.exists(path_to_create):
    print("Creating Directory : " + path_to_create)
    os.makedirs(path_to_create)
    with zipfile.ZipFile('CMAutomation.zip', "r") as z:
        z.extractall(path_to_create)

