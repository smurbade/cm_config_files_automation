import read_cmitems
import copy_final_tgz
import  read_conf
from os.path import expanduser
import os
import subprocess
import re
import shutil
import glob
read_conf.read_conf_file()
import common_functions
from colorama import init, Fore, Back, Style
init()

home = expanduser("~")
base_path = ""
if os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc 1\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc 1\\"
elif os.path.isdir(home + "\\SharePoint\\North America Config Manageme - Doc\\"):
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"
elif os.path.isdir(home + "\\Verifone\\North America Config Management Team - Documents\\"):
    base_path = home + "\\Verifone\\North America Config Management Team - Documents\\"
elif os.path.isdir(home + "\\OneDrive - Verifone"):
    home = home + "\\OneDrive - Verifone"  # Some of the CM's see sharepoint folder with this path
    base_path = home + "\\SharePoint\\North America Config Manageme - Doc\\"


def insert_emv(gui_nongui, formnum, cust_sur_revision):
    cmitems = read_cmitems.read_cmitems()
    #formexists = cmitems.kickstart_cmitems_processing()
    classic_pattern = r"SCA Point Classic"
    enterprise_pattern = r"Enterprise"
    chase_pattern = r"Chase"
    fd_pattern = r"First Data"
    elavon_pattern = r"Elavon"
    global_pattern = r"Global"
    tsys_pattern = r"TSYS"
    worldpay_pattern = r"Worldpay"
    heartland_pattern = r"Heartland"
    vantive_pattern = r"Vantiv"
    cybersource_pattern = r"Cybersource"
    mx_pattern = r"MX"
    vx_pattern = r"VX"
    credit_pattern = r"Credit"
    debit_pattern = r"Debit"
    customer_pattern = r"Customer"
    prodtype = ""
    processor = ""
    device = ""
    dualaid = ""
    final_tgz_path = ""
    formexists = False

    if gui_nongui == "gui":
        formexists = cmitems.kickstart_cmitems_processing_gui(formnum)
    elif gui_nongui == "nongui":
        formexists = cmitems.kickstart_cmitems_processing()

    if cmitems.create_cust_dict(cust_sur_revision) == False:
        return 26


    if ( formexists ):
        if (re.search(classic_pattern, (list(cmitems.dict.values())[list(cmitems.dict.keys()).index('Product Type')]))):
            prodtype = "Classic"
        elif (
        re.search(enterprise_pattern, (list(cmitems.dict.values())[list(cmitems.dict.keys()).index('Product Type')]))):
            prodtype = "Enterprise"

        if (re.search(chase_pattern,
                      (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Chase"
        elif (re.search(fd_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "FD"
        elif (re.search(cybersource_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Cybersource"
        elif (re.search(elavon_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Elavon"
        elif (re.search(global_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Global"
        elif (re.search(heartland_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Heartland"
        elif (re.search(tsys_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "TSYS"
        elif (re.search(vantive_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Vantiv"
        elif (re.search(worldpay_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('Credit/Debit/EBT Processors')]))):
            processor = "Worldpay"

        if prodtype == "Enterprise":
            if (re.search(chase_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Chase"
            elif (re.search(fd_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "FD"
            elif (re.search(cybersource_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Cybersource"
            elif (re.search(elavon_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Elavon"
            elif (re.search(global_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Global"
            elif (re.search(heartland_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Heartland"
            elif (re.search(tsys_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "TSYS"
            elif (re.search(vantive_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Vantiv"
            elif (re.search(worldpay_pattern, (
            list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('SCA Point Enterprise Processor')]))):
                processor = "Worldpay"

        if (re.search(mx_pattern, (list(cmitems.dict.values())[list(cmitems.dict.keys()).index('Device Model')]))):
            device = "MX"
        elif (re.search(vx_pattern, (list(cmitems.dict.values())[list(cmitems.dict.keys()).index('Device Model')]))):
            device = "VX"

        if (
        re.search(credit_pattern, (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('EMV Payment Preferred Type')]))):
            dualaid = "Autocredit"
        elif (
        re.search(debit_pattern, (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('EMV Payment Preferred Type')]))):
            dualaid = "Autodebit"
        elif (re.search(customer_pattern,
                        (list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index('EMV Payment Preferred Type')]))):
            dualaid = "Customerchoice"

        emv_path = "\"" + processor + "/" + prodtype + "/" + device + "/" + dualaid + "\""
        #print(emv_path)
        emv_path_absolute = base_path + "\\EMV\\" + processor + "\\" + prodtype + "\\" + device + "\\" + dualaid

        if (((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Product Type")]) == 'SCA Point Classic' ) or ((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Product Type")]) == 'SCA Point Classic Passthrough' ) or ((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Product Type")]) == 'SCA Point Enterprise' )) and ((list(cmitems.cust_dict.values())[list(cmitems.cust_dict.keys()).index("EMV")]) == 'Yes' ):
            if processor != "" and prodtype != "" and device != "" and dualaid != "":
                if os.path.isdir(emv_path_absolute):
                    if (len(glob.glob(emv_path_absolute + '\\*.tgz'))) == 0 and (len(glob.glob(emv_path_absolute + '\\*.zip'))) == 0:
                        return 25
                    cli = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Client Name")]))
                    prdt = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Product Type")]))
                    devicemodel = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Device Model")]))
                    envrmt = str((list(cmitems.dict.values())[list(cmitems.dict.keys()).index("Environment")]))

                    # uncomment below 3 lines to enable naming convention
                    #tgzvalidate = common_functions.validate_tgz("extract", cli, prdt, devicemodel, envrmt)
                    #if tgzvalidate != 0:
                    #    return tgzvalidate

                    #command = "C:\\msys\\1.0\\bin\\bash.exe emv.sh " + emv_path
                    #command = "\"" + read_conf.git_bash_path[0] + "\" emv.sh " + emv_path
                    #command = "\"" + read_conf.git_bash_path[0] + "\" emv.sh " + emv_path + " " + device
                    command = "\"" + read_conf.git_bash_path[0] + "\" scripts\\emv.sh " + emv_path + " " + device
                    #print("Inserting EMV template file into tgz file. Please wait ...")
                    # print(command)
                    FNULL = open(os.devnull, 'w')
                    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
                    if result.returncode != 0:
                        print(result.stderr)
                        print(result)
                        print(Fore.RED + "Execution of emv.sh failed")
                        print(Style.RESET_ALL)
                        return 1
                    else:
                        #final_tgz_path = cmitems.get_tgz_path()
                        #copy_final_tgz.check_directory_and_copy_tgz("extract", final_tgz_path)
                        #print("Process completed. Please check updated tgz file @ " + os.getcwd() + "\\" + cmitems.client + "\\" + cmitems.prodtype + "\\" + cmitems.device + "\\" + cmitems.environment)
                        #print(Fore.GREEN + "Process completed. Please check updated file @ " + os.getcwd() + "\\extract folder")
                        print(Style.RESET_ALL)
                        return 0
                else:
                    print(Fore.RED + "EMV template package Path " + emv_path_absolute + " doesn't exist. Please create this path and place template EMV tgz")
                    print(Style.RESET_ALL)
                    return 2
            else:
                if prodtype == "":
                    print(Fore.RED + "Applicable for SCA Point Classic/SCA Point Classic Passthrough/SCA Point Enterprize")
                    print(Style.RESET_ALL)
                    return 3
                if processor == "":
                    print(Fore.RED + "Applicable for valid processor only")
                    print(Style.RESET_ALL)
                    return 4
                if device == "":
                    print(Fore.RED + "Applicable for MX or VX devices only")
                    print(Style.RESET_ALL)
                    return 5
                if dualaid == "":
                    print(Fore.RED + "Applicable either for Auto Credit / Auto Debit / Customer Choice options")
                    print(Style.RESET_ALL)
                    return 6
        else:
            print(Fore.RED + "Applicable for SCA Point Classic/SCA Point Classic Passthrough/SCA Point Enterprize and EMV as Yes")
            print(Style.RESET_ALL)
            return 7
    else:
         return 8

#Uncomment below line if this program need to be run separately
#insert_emv()
