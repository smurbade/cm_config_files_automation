from appJar import gui
import glob
import os, subprocess

import insert_access_db_gui
import read_access_db
import Add_Media_Files
import emv
import ConfigUsr1
import read_conf

#prodtype = ["SCA Point Classic", "SCA Point Classic Passthrough", "SCA Point Enterprise", "FA/XPI", "CXPI - non MX products", "VHQ", "VCL", "Other"]
#device = ["MX830", "MX850", "MX860", "MX870", "MX880", "MX915-PCI 3", "MX915-PCI 4", "MX925-PCI 3", "MX925-PCI 4", "VX520", "VX690", "VX805", "VX820", "e255", "e315", "e335", "e355", "UX100" ]
#environment = ["Lab", "QA", "UAT", "Pilot", "Prod" ]

read_conf.read_conf_file()
script = "Cybersource.sh"

sigcapt     = "Enable Signature Capture"
siglimit    = "Signature Limit Amount        :"
saf         = "Enable SAF"
tranfllimit = "Transaction Floor Limit         :"
totfllimit  = "Total Floor Limit                     :"
dayslimit   = "Days Limit                              :"
lineitdisp  = "Enable Line Item Display"
vhqinst     = "VHQ Instance Name              :"

indpackage = "Individual or local package"
indfile = "individual files"

app=gui("CMAutomation")

def cybersource_processing(device, sigenable, signaturelimit, safenable, transfloorlimit, totalfllimit, dayslimits, lineitemdisp, vhqinstance):
    print("signpatr : " + str(sigenable))
    print("Sig Limit : " + str(signaturelimit))
    print("saf : " + str(safenable))
    print("Transaction Floor Limit: " + str(transfloorlimit))
    print("Total Floor Limit: " + str(totalfllimit))
    print("Days Limit : {0}".format(dayslimits))
    print("line item disp : " + str(lineitemdisp))
    print("VHQ Instance : " + str(vhqinstance))

    sigenableparam = ""
    signaturelimitparam = ""
    safenableparam = ""
    transfloorlimitparam = ""
    totalfllimitparam = ""
    dayslimitsparam = ""
    lineitemdispparam = ""
    vhqinstanceparam = ""

    if sigenable:
        sigenableparam = "Y"
    else:
        sigenableparam = "N"

    if signaturelimit > 0:
        signaturelimitparam = str(signaturelimit)
    else:
        signaturelimitparam = "\"\""

    if safenable:
        safenableparam = "Y"
    else:
        safenableparam = "N"

    if transfloorlimit > 0:
        transfloorlimitparam = str(transfloorlimit)
    else:
        transfloorlimitparam = "\"\""

    if totalfllimit > 0:
        totalfllimitparam = str(totalfllimit)
    else:
        totalfllimitparam = "\"\""

    if dayslimits > 0:
        dayslimitsparam = str(dayslimits)
    else:
        dayslimitsparam = "\"\""

    if lineitemdisp:
        lineitemdispparam = "Y"
    else:
        lineitemdispparam = "N"

    if vhqinstance != "":
        vhqinstanceparam = str(vhqinstance)
    else:
        vhqinstanceparam = "\"\""

    if device == "MX":
        command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " MX " + str(sigenableparam) + " " + str(signaturelimitparam) + " " + str(safenableparam) + " " + str(transfloorlimitparam) + " " + str(totalfllimitparam) + " " + str(dayslimitsparam) + " " + str(lineitemdispparam) + " " + str(vhqinstanceparam)
    elif device == "VX":
        command = "\"" + read_conf.git_bash_path[0] + "\" " + script + " VX " + str(safenableparam) + " " + str(transfloorlimitparam) + " " + str(totalfllimitparam) + " " + str(dayslimitsparam) + " " + str(vhqinstanceparam)

    print(command)
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(command, shell=True, stderr=subprocess.PIPE, stdout=FNULL)
    if result.returncode != 0:
        print(result.stderr)
        print(result)
        print("\nExecution of " + script + " failed")
        return False
    else:
        print("Updated package is available @ extract folder")
        return True


def uploadindfiles(btn):
    cybersource_processing(app.getRadioButton(indpackage), app.getCheckBox(sigcapt), app.getEntry(siglimit), app.getCheckBox(saf),
                              app.getEntry(tranfllimit), app.getEntry(totfllimit), app.getEntry(dayslimit),
                              app.getCheckBox(lineitdisp), app.getEntry(vhqinst))


def proceed(btn):
    app.openTab("CMAutomationTabbedFrame", "CyberSource Automation")
    if app.getRadioButton(indpackage) == "MX":
        try:
            app.addLabel("instr1", "Select right options option below and hit 'Update Package' button")
            app.addEmptyLabel("empty3")
            app.addCheckBox(sigcapt)
            app.addLabelNumericEntry(siglimit)
            app.addEmptyLabel("empty4")
            app.addCheckBox(saf)
            app.addLabelNumericEntry(tranfllimit)
            app.addLabelNumericEntry(totfllimit)
            app.addLabelNumericEntry(dayslimit)
            app.addEmptyLabel("empty5")
            app.addCheckBox(lineitdisp)
            app.addEmptyLabel("empty6")
            app.addLabelEntry(vhqinst)
            app.addEmptyLabel("empty7")
            app.addButton("Update Package", uploadindfiles)
            app.addEmptyLabel("empty8")
        except:
            app.showWidget(kind=1, name="instr1")
            app.showWidget(kind=4, name=sigcapt) # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
            app.showWidget(kind=2, name=siglimit)
            app.showWidget(kind=4, name=saf)
            app.showWidget(kind=2, name=tranfllimit)
            app.showWidget(kind=2, name=totfllimit)
            app.showWidget(kind=2, name=dayslimit)
            app.showWidget(kind=1, name="empty5")
            app.showWidget(kind=4, name=lineitdisp)
            app.showWidget(kind=2, name=vhqinst)
            app.showWidget(kind=3, name="Update Package")
    elif app.getRadioButton(indpackage) == "VX":
        try:
            app.addLabel("instr1", "Select right options option below and hit 'Update Package' button")
            app.addCheckBox(sigcapt)
            app.addLabelNumericEntry(siglimit)
            app.addEmptyLabel("empty4")
            app.addCheckBox(saf)
            app.addLabelNumericEntry(tranfllimit)
            app.addLabelNumericEntry(totfllimit)
            app.addLabelNumericEntry(dayslimit)
            app.addEmptyLabel("empty5")
            app.addCheckBox(lineitdisp)
            app.addEmptyLabel("empty6")
            app.addLabelEntry(vhqinst)
            app.addEmptyLabel("empty7")
            app.addButton("Update Package", uploadindfiles)
            app.addEmptyLabel("empty8")
            app.hideWidget(kind=4, name=sigcapt) # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
            app.hideWidget(kind=2, name=siglimit)
            app.hideWidget(kind=1, name="empty5")
            app.hideWidget(kind=4, name=lineitdisp)
        except:
            app.hideWidget(kind=4, name=sigcapt) # Find the kind number in appjar.py against type of widget. In this example it is 4 in appjar.py since widget type is checkbox
            app.hideWidget(kind=2, name=siglimit)
            app.hideWidget(kind=1, name="empty5")
            app.hideWidget(kind=4, name=lineitdisp)
            #app.hideWidget(kind=3, name="Update Package")


##################################################################################################################

app.startTabbedFrame("CMAutomationTabbedFrame")

##################################################################################################################

app.startTab("CyberSource Automation")
app.setTabBg("CMAutomationTabbedFrame", "CyberSource Automation", "lightblue")
app.addEmptyLabel("empty1")
app.addRadioButton(indpackage, "MX")
app.addRadioButton(indpackage, "VX")
app.addButton("Proceed", proceed)
app.addEmptyLabel("empty2")
app.stopTab()

##################################################################################################################

app.stopTabbedFrame()

##################################################################################################################

app.go()

